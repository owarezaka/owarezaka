# Advanced Usage

## Home Screen

<br>

### Projects Screen

---

The Projects screen shows a list of projects we are currently a member of.

For public projects, and members of internal and private projects with permissions to display the project’s code, the project details screen shows:

    A README file.
    A list of directories in the project’s repository.

<br>

<div style="display: flex; flex-direction: row; gap: 5px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/0368b7a211cea09d11b17fcca2cfe970/projects_list.png" width="300" alt="projects list">

<img src="https://gitlab.com/owarezaka/owarezaka/uploads/21abbb626c05b404b3161e2fcedcbc9a/Project_Details.png" width="300" alt="project details screen">
</div>

<div style="display: flex; flex-direction: row; gap: 5px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/ade8ede01c7496723d3b92032bc1d392/repo.png" width="300" alt="project_repository">

<img src="https://gitlab.com/owarezaka/owarezaka/uploads/005c757f5cf71f612f5df9b0dc77d401/repo_file.png" width="300" alt="project_repository_file">
</div>

<br>

### To-Do List Screen

---

Our To-Do List is a chronological list of items.

Many to-do items are created automatically. A to-do item is added to our To-Do List when:

    1. An issue or merge request is assigned to us.
    2. We are mentioned in the description or comment of an issue, merge request, or epic.
    3. We are mentioned in a comment on a commit or design.
    4. The CI/CD pipeline for our merge request fails.
    5. An open merge request cannot be merged due to conflict, and one of the following is true:
      - We’re the author.
      - We’re the user that set the merge request to automatically merge after a pipeline succeeds.
    6. In GitLab 13.2 and later, a merge request is removed from a merge train, and we’re the user that added it.

We can manually mark a to-do item as done.

<br>

<div style="display: flex; flex-direction: row; gap: 5px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/2c82d8d8aad91fce5149a3941b9fb285/Todos.png" width="300" alt="todos">
</div>

<br>

### Groups Screen

---

All groups we are a member of are displayed. We can also see subgroups and projects related to group.

<br>

<div style="display: flex; flex-direction: row; gap: 5px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/e8f3459d38460f6d8f1f97f0e5944e1f/Groups.png" width="300" alt="group_list">

<img src="https://gitlab.com/owarezaka/owarezaka/uploads/fcba5caac0a1c694039d86c66eb1b33a/Group_Details.png" width="300" alt="group_detail_screen">
</div>

<br>

### Snippets Screen

---

Displays our personal and project snippets, supports syntax highlighting.

<br>

<div style="display: flex; flex-direction: row; gap: 5px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/7f258df59ba253cabb390ae018eaeabe/snippets__2_.png" width="300" alt="snippets_list">

<img src="https://gitlab.com/owarezaka/owarezaka/uploads/be431ea99b42016abb654653d4e4b318/Snippet_Details.png" width="300" alt="snippet_detail_screen">
</div>

<br>

### Activity Screen

---

Displays all our activities for all projects

<br>

<div style="display: flex; flex-direction: row; gap: 15px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/90287a4013fc7229878477b9f37806f9/Activity.png" width="300" alt="activity_list">
</div>

<br>

### Issues Screen

Displays all issues currently assigned to us.
We can press on any issue to view its details, leave a comment or close the issue.

<br>

<div style="display: flex; flex-direction: row; gap: 15px">
<img src="/uploads/f160ac1424560f768187caec60a0bcac/image.png" width="300" alt="issues_screen">
</div>

<br>

### MRs Screen

---

Displays all merge requests currently assigned to us.
You can view the status of the selected MR as well as browse overview and changes sections, also add comments to the MR.

<br>

<div style="display: flex; flex-direction: row; gap: 15px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/3340a8a41b222bb9d7a59dae2d8edfdf/MergeRequests.png" width="300" alt="merge_requests_screen">
</div>

<br>

### Review MRs Screen

---

Displays all merge request reviews currently assigned to us.
We can view details about the selected Merge Request, along with approving it, changing its reviewer, merging changes to main branch, or closing it.

<br>

<div style="display: flex; flex-direction: row; gap: 15px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/05777de4506fa8c6c45ffeedda7a00f5/image.png" width="300" alt="review_merge_requests_screen">
</div>

<br>

### Profile Screen

---

Displays our profile name, picture and username. In this screen we can set a status, or remove it and sign out of the application.

<br>

<div style="display: flex; flex-direction: row; gap: 15px">
<img src="https://gitlab.com/owarezaka/owarezaka/uploads/1a146258b3e88231a0ce951917d061e5/Profile.png" width="300" alt="profile_screen">
</div>

<br>
