# Installation

A quick guide for installing the app on mobile and cloning to other devices.

## Cloning

1. Clone the repo

```sh
git clone https://gitlab.com/owarezaka/owarezaka.git
```

2. Install all the dependencies inside the project folder

```sh
npm install
```

3. Start the project on Android

```sh
npm run android
```

4. Start the project on IOS

```sh
cd ios/
pod install
cd ..
npm run ios
```

## Installing to mobile

Android - Download from Google Play:

https://play.google.com/store/apps/details?id=com.owarezaka&hl=tr

iOS: Downloading from the App Store will be available soon.

## General Usage

### Logging In

1. Create a GitLab personal access token from <instanceUrl>/-/profile/personal_access_tokens where instanceUrl is either https://gitlab.com or the root url of your custom GitLab instance. The access token should preferably have all scopes enabled, otherwise some issues may occur.

2. Enter your access token along with your instance url into the app.

### Screens

1. Home Screen - Access information about the current user such as your projects or to-do list.

![Home Screen](https://gitlab.com/owarezaka/owarezaka/uploads/5397dbeb248f651622f02da479b6860e/image.png)

2. Issues - Access all issues currently assigned to you.

![Issues Screen](https://gitlab.com/owarezaka/owarezaka/uploads/13ced3fbd76ca6fde1962a131bada8bf/image.png)

3. Merge Requests - Access all merge requests currently assigned to you.

![Merge Requests](https://gitlab.com/owarezaka/owarezaka/uploads/df7f34e5ff03d3fdb5a2a6d856e4781d/image.png)

4. Review Merge Requests - Access all review requests currently assigned to you.

![Review Requests](https://gitlab.com/owarezaka/owarezaka/uploads/a29cfbb36fa86aed09417b539474ce05/image.png)

5. Profile - View your profile, change your status or sign out.

![Profile](https://gitlab.com/owarezaka/owarezaka/uploads/c5a556a0c90edb66de0ea0d269316d48/image.png)

## Usage Video

Watch it on YouTube: [Owarezaka - Mobile GitLab client](https://www.youtube.com/watch?v=s-_4Yv8SF2A)
