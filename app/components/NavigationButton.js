import React, { useContext } from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import Proptypes from 'prop-types';
import Icon from 'react-native-vector-icons/AntDesign';
import Icons from '~app/assets/Icons';
import '~config/i18n.config';
import { ThemeContext } from '~app/store/theme-context';

const NavigationButton = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);

  return (
    <View style={theme === 'light' ? styles.lightNavigation : styles.darkNavigation}>
      <View>
        <TouchableOpacity testID={props.testID} onPress={props.onPress}>
          <Icon
            name="arrowleft"
            size={24}
            color={theme === 'light' ? Colors.dark : Colors.secondary}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.titleContainer}>
        <Text
          style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
          testID="title"
          numberOfLines={1}
        >
          {t(`${props.title}`)}
        </Text>

        {props.subTitle && (
          <Text numberOfLines={1} style={styles.subTitle}>
            {' '}
            <Icons.angleRight
              color={theme === 'light' ? Colors.dark : Colors.secondary}
              testID="angleRight"
            />{' '}
            <Text
              style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
              testID="subTitle"
            >
              {t(`${props.subTitle}`)}
            </Text>
          </Text>
        )}
      </View>
    </View>
  );
};

NavigationButton.propTypes = {
  testID: Proptypes.string,
  onPress: Proptypes.func.isRequired,
  title: Proptypes.string.isRequired,
  subTitle: Proptypes.string,
};

const styles = StyleSheet.create({
  darkNavigation: {
    alignItems: 'center',
    backgroundColor: Colors.darkGray2,
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingVertical: 15,
  },
  lightNavigation: {
    alignItems: 'center',
    backgroundColor: Colors.lightGray,
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    flexDirection: 'row',
    paddingLeft: 15,
    paddingVertical: 15,
  },
  subTitle: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    paddingLeft: 25,
  },
  titleContainer: {
    flexDirection: 'row',
    flex: 1,
  },
});

export default NavigationButton;
