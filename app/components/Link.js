import React from 'react';
import { Text, StyleSheet, Linking, Pressable } from 'react-native';
import { useTranslation } from 'react-i18next';
import { Colors } from '~app/constants/GlobalStyles';
import Proptypes from 'prop-types';
import '~config/i18n.config';

const Link = (props) => {
  const { t } = useTranslation();

  return (
    <Pressable onPress={() => Linking.openURL(`${props.url}`)}>
      <Text style={styles.link}>{t(`${props.name}`)}</Text>
    </Pressable>
  );
};

Link.propTypes = {
  url: Proptypes.string.isRequired,
  name: Proptypes.string.isRequired,
};

const styles = StyleSheet.create({
  link: {
    color: Colors.primary,
    fontSize: 16,
    textDecorationLine: 'underline',
    transform: [{ translateY: 4 }],
  },
});

export default Link;
