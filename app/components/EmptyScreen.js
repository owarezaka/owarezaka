import React, { useContext } from 'react';
import { StyleSheet, Text, ScrollView, RefreshControl } from 'react-native';
import { ThemeContext } from '~app/store/theme-context';
import { useTranslation } from 'react-i18next';
import Proptypes from 'prop-types';

import s, { Colors } from '~app/constants/GlobalStyles';

const EmptyScreen = ({ refreshing, onRefresh, title, text }) => {
  const { theme } = useContext(ThemeContext);
  const { t } = useTranslation();

  return (
    <ScrollView
      contentContainerStyle={
        theme === 'light'
          ? [s.lightContainer, styles.container]
          : [s.darkContainer, styles.container]
      }
      refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
    >
      <Text style={styles.titleStyle}>{t(`${title}`)}</Text>
      <Text
        style={theme === 'light' ? [s.lightText, styles.textStyle] : [s.darkText, styles.textStyle]}
      >
        {t(`${text}`)}
      </Text>
    </ScrollView>
  );
};

EmptyScreen.propTypes = {
  refreshing: Proptypes.bool,
  onRefresh: Proptypes.func,
  title: Proptypes.string,
  text: Proptypes.string,
};

export default EmptyScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 100,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 10,
    textAlign: 'center',
  },
  titleStyle: {
    color: Colors.primary,
    fontSize: 30,
  },
});
