import React, { useContext } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { ThemeContext } from '~app/store/theme-context';
import s, { Colors } from '~app/constants/GlobalStyles';

const LoadingSpinner = () => {
  const { theme } = useContext(ThemeContext);

  return (
    <View style={theme === 'light' ? s.lightContainer : s.darkContainer}>
      <ActivityIndicator
        animating
        size="large"
        testID="spinner"
        color={theme === 'light' ? Colors.darkGray2 : Colors.secondary}
        style={styles.loaderStyle}
      />
    </View>
  );
};

export default LoadingSpinner;

const styles = StyleSheet.create({
  loaderStyle: {
    padding: 20,
  },
});
