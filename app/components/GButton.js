import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { Colors } from '~app/constants/GlobalStyles';
import Proptypes from 'prop-types';

const GButton = (props) => {
  const { t } = useTranslation();

  const setButtonWidth = () => {
    return props.size === 'small'
      ? 100
      : props.size === 'medium'
      ? '50%'
      : props.size === 'large'
      ? '100%'
      : 'auto';
  };

  const setFontSize = () => {
    return props.size === 'small'
      ? 12
      : props.size === 'medium'
      ? 15
      : props.size === 'large'
      ? 16
      : 14;
  };

  const defaultButtonStyle = {
    borderColor: Colors.border,
    borderRadius: 5,
    borderWidth: 0.7,
    flexDirection: props.flexDirection ?? 'column',
    padding: props.padding ?? (props.size === 'small' ? 6 : 10),
    width: props.width ?? (props.size && setButtonWidth()),
  };

  const defaultLabelStyle = {
    color: Colors.dark,
    fontSize: props.fontSize ?? (props.size && setFontSize()),
    textAlign: props.textAlign ?? 'center',
  };

  const primaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: props.variant === 'danger' ? Colors.red : Colors.primary,
  };

  const primaryLabelStyle = {
    ...defaultLabelStyle,
    color: Colors.secondary,
  };

  const secondaryButtonStyle = {
    ...defaultButtonStyle,
    borderColor: props.variant === 'danger' ? Colors.red : Colors.primary,
  };

  const secondaryLabelStyle = {
    ...defaultLabelStyle,
    color: props.variant === 'danger' ? Colors.red : Colors.primary,
  };

  const tertiaryButtonStyle = {
    ...defaultButtonStyle,
    borderWidth: 0,
  };

  const tertiaryLabelStyle = {
    ...defaultLabelStyle,
    color:
      props.variant === 'danger'
        ? Colors.red
        : props.variant === 'confirm'
        ? Colors.primary
        : Colors.dark,
  };

  const disabledButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.lightGray3,
  };

  const disabledLabelStyle = {
    ...defaultLabelStyle,
    color: Colors.darkGray,
  };

  //DARK MODE STYLES

  const darkPrimaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.primary500,
    borderColor: Colors.primary500,
  };

  const darkPrimaryLabelStyle = {
    ...defaultLabelStyle,
    color: Colors.dark,
    fontWeight: '500',
  };

  const darkSecondaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.dark,
    borderColor:
      props.variant === 'danger'
        ? Colors.red600
        : props.variant === 'confirm'
        ? Colors.primary500
        : Colors.lightGray,
  };

  const darkDisabledButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.darkGray3,
    borderColor: Colors.darkGray2,
  };

  const darkDisabledLabelStyle = {
    ...defaultLabelStyle,
    color: Colors.darkGray2,
  };

  const darkDefaultLabelStyle = {
    ...defaultLabelStyle,
    color:
      props.variant === 'danger'
        ? Colors.red600
        : props.variant === 'confirm'
        ? Colors.primary500
        : Colors.secondary,
  };

  let buttonStyle;
  let labelStyle;

  switch (props.category) {
    case 'primary':
      buttonStyle = primaryButtonStyle;
      labelStyle = primaryLabelStyle;
      break;
    case 'secondary':
      buttonStyle = secondaryButtonStyle;
      labelStyle = secondaryLabelStyle;
      break;
    case 'tertiary':
      buttonStyle = tertiaryButtonStyle;
      labelStyle = tertiaryLabelStyle;
      break;
    case 'disabled':
      buttonStyle = disabledButtonStyle;
      labelStyle = disabledLabelStyle;
      break;
    case 'darkPrimary':
      buttonStyle = darkPrimaryButtonStyle;
      labelStyle = darkPrimaryLabelStyle;
      break;
    case 'darkSecondary':
      buttonStyle = darkSecondaryButtonStyle;
      labelStyle = darkDefaultLabelStyle;
      break;
    case 'darkTertiary':
      buttonStyle = tertiaryButtonStyle;
      labelStyle = darkDefaultLabelStyle;
      break;
    case 'darkDisabled':
      buttonStyle = darkDisabledButtonStyle;
      labelStyle = darkDisabledLabelStyle;
      break;
    default:
      buttonStyle = defaultButtonStyle;
      labelStyle = defaultLabelStyle;
  }

  return (
    <TouchableOpacity
      testID={props.testID}
      disabled={props.disabled ?? false}
      variant={props.variant ?? 'default'}
      style={buttonStyle}
      onPress={props.click}
    >
      {props.children}
      <Text testID="label" style={labelStyle}>{`${t(`${props.label ?? ''}`)}`}</Text>
    </TouchableOpacity>
  );
};

GButton.propTypes = {
  category: Proptypes.string,
  children: Proptypes.any,
  click: Proptypes.func.isRequired,
  disabled: Proptypes.bool,
  flexDirection: Proptypes.string,
  fontSize: Proptypes.number,
  label: Proptypes.string,
  padding: Proptypes.number,
  size: Proptypes.string,
  testID: Proptypes.string,
  textAlign: Proptypes.string,
  variant: Proptypes.string,
  width: Proptypes.oneOfType([Proptypes.string, Proptypes.number]),
};

export default GButton;
