import React, { useContext, useState } from 'react';
import {
  Modal,
  Text,
  View,
  FlatList,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import emojiList from 'app/assets/emojis.json';
import { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const emojiArray = Object.keys(emojiList).map((el) => {
  return emojiList[el];
});

const EmojiItem = ({ emoji, emojiPress }) => {
  return (
    <TouchableOpacity onPress={emojiPress}>
      <View style={styles.emojiContainer}>
        <Text style={styles.emoji}>{emoji}</Text>
      </View>
    </TouchableOpacity>
  );
};

const EmojiGroups = ({ setGroup }) => {
  return (
    <View style={styles.groups}>
      <TouchableOpacity onPress={() => setGroup('activity')}>
        <Icons.dumbbell color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('people')}>
        <Icons.smiley color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('nature')} testID="nature">
        <Icons.nature color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('food')}>
        <Icons.food color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('travel')}>
        <Icons.car color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('objects')}>
        <Icons.object color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('symbols')}>
        <Icons.heart color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setGroup('flags')}>
        <Icons.flag color={Colors.secondary} size={20} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

const EmojiBox = (props) => {
  const { theme } = useContext(ThemeContext);

  const [group, setGroup] = useState('activity');

  return (
    <Modal transparent={true}>
      <View style={styles.modal}>
        <TouchableOpacity onPress={() => props.close()} style={styles.close}>
          <Icons.close color={theme === 'light' ? Colors.dark : Colors.secondary} size={24} />
        </TouchableOpacity>
        <EmojiGroups setGroup={(gr) => setGroup(gr)} />
        <FlatList
          testID="emoji-list"
          contentContainerStyle={styles.emojiList}
          data={emojiArray.filter((emoji) => emoji.c === group)}
          keyExtractor={(item) => item.d}
          renderItem={({ item }) => {
            return <EmojiItem emoji={item.e} emojiPress={() => props.onPress(item.e)} />;
          }}
          numColumns={10}
        />
      </View>
    </Modal>
  );
};

EmojiBox.propTypes = {
  close: Proptypes.func,
  onPress: Proptypes.func,
};

EmojiItem.propTypes = {
  emoji: Proptypes.string,
  emojiPress: Proptypes.func,
};

EmojiGroups.propTypes = {
  setGroup: Proptypes.func,
};

const styles = StyleSheet.create({
  close: {
    margin: 5,
    marginLeft: 'auto',
  },
  emoji: {
    fontSize: 24,
  },
  emojiContainer: {
    padding: 2,
  },
  emojiList: {
    alignItems: 'center',
    backgroundColor: Colors.dark,
  },
  groups: {
    backgroundColor: Colors.dark,
    borderBottomWidth: 1,
    borderColor: Colors.lightGray4,
    flexDirection: 'row',
    paddingVertical: 10,
    width: '100%',
  },
  icon: {
    paddingHorizontal: 16,
  },
  modal: {
    marginTop: Dimensions.get('screen').height / 2,
  },
});

export default EmojiBox;
