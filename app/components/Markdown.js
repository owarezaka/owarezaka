import React, { useContext } from 'react';
import { Linking, StyleSheet } from 'react-native';
import RNMarkdown from '~app/lib/markdown/markdown';
import Proptypes from 'prop-types';
import { Colors } from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';

const Markdown = ({ body, imagePrefix }) => {
  const { theme } = useContext(ThemeContext);
  return (
    <RNMarkdown
      styles={theme === 'light' ? styles.light : styles.dark}
      onLink={(url) => Linking.openURL(url)}
      imagePrefix={imagePrefix}
    >
      {body}
    </RNMarkdown>
  );
};

Markdown.propTypes = {
  body: Proptypes.string,
  imagePrefix: Proptypes.string,
  testID: Proptypes.string,
};

export default Markdown;

const styles = StyleSheet.create({
  dark: {
    autolink: {
      color: Colors.primary500,
    },
    blockQuoteSection: {
      flexDirection: 'row',
      color: Colors.secondary,
    },
    blockQuoteSectionBar: {
      width: 3,
      height: null,
      backgroundColor: Colors.gray,
      marginRight: 15,
    },
    blockQuoteText: {
      color: Colors.gray,
    },
    codeBlock: {
      fontFamily: 'Courier',
      fontWeight: '500',
      backgroundColor: Colors.gray,
      color: Colors.secondary,
    },
    em: {
      color: Colors.primary500,
    },
    heading1: {
      color: Colors.secondary,
    },
    heading2: {
      color: Colors.secondary,
    },
    listItemBullet: {
      color: Colors.secondary,
    },
    listItemNumber: {
      fontWeight: 'bold',
      color: Colors.secondary,
    },
    strong: {
      color: Colors.primary500,
    },
    tableHeader: {
      backgroundColor: Colors.gray,
    },
    text: {
      color: Colors.secondary,
    },
  },
  light: {
    blockQuoteSection: {
      flexDirection: 'row',
    },
    blockQuoteSectionBar: {
      width: 3,
      height: null,
      backgroundColor: Colors.lightGray2,
      marginRight: 15,
    },
    blockQuoteText: {
      color: Colors.lightGray4,
    },
    codeBlock: {
      fontFamily: 'Courier',
      fontWeight: '500',
      backgroundColor: Colors.lightGray2,
    },
    em: {
      color: Colors.primary500,
    },
    heading1: {
      color: Colors.darkGray3,
    },
    heading2: {
      color: Colors.darkGray3,
    },
    strong: {
      color: Colors.primary500,
    },
    tableHeader: {
      backgroundColor: Colors.lightGray,
    },
    text: {
      color: Colors.darkGray3,
    },
  },
});
