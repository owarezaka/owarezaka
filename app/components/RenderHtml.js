import React, { useContext } from 'react';
import { StyleSheet, useWindowDimensions } from 'react-native';
import RNRenderHtml, { HTMLElementModel, HTMLContentModel } from 'react-native-render-html';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';
import { Colors } from 'app/constants/GlobalStyles';

const RenderHtml = ({ instanceUrl, testID, body }) => {
  const { theme } = useContext(ThemeContext);
  const { width } = useWindowDimensions();

  const customHTMLElementModels = {
    input: HTMLElementModel.fromCustomModel({
      tagName: 'input',
      contentModel: HTMLContentModel.mixed,
    }),
  };

  function onElement(element) {
    if (element.tagName === 'a') {
      element.attribs.href = `${instanceUrl}${element.attribs.href}`;
    }

    if (element.tagName === 'img') {
      if (element.attribs.src.includes('uploads')) {
        return;
      }

      if (element.attribs['data-src']) {
        if (element.attribs['data-src'].includes(instanceUrl)) {
          return (element.attribs.src = element.attribs['data-src']);
        }
        element.attribs.src = `${instanceUrl}${element.attribs['data-src']}`;
      }
    }
  }

  const domVisitors = {
    onElement: onElement,
  };

  const baseStyle = {
    display: 'flex',
    flexDirection: 'column',
    flexShrink: 1,
    flexWrap: 'wrap',
    maxWidth: width - 60,
  };

  return (
    <RNRenderHtml
      testID={testID}
      contentWidth={width}
      source={{ html: body }}
      domVisitors={domVisitors}
      ignoredDomTags={['copy-code', 'gl-emoji', 'task-button']}
      baseStyle={baseStyle}
      tagsStyles={theme === 'light' ? styles.lightTagStyle : styles.darkTagStyle}
      customHTMLElementModels={customHTMLElementModels}
    />
  );
};

RenderHtml.propTypes = {
  instanceUrl: Proptypes.string,
  body: Proptypes.string,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  darkTagStyle: {
    a: {
      color: Colors.secondary,
    },
    body: {
      color: Colors.secondary,
      maxWidth: 300,
    },
    img: {
      maxWidth: 100,
    },
    table: {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
    td: {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
    th: {
      borderWidth: 1,
      borderColor: Colors.secondary,
      backgroundColor: Colors.darkGray2,
    },
  },
  lightTagStyle: {
    a: {
      color: Colors.dark,
    },
    body: {
      color: Colors.dark,
      maxWidth: 300,
    },
    img: {
      maxWidth: 100,
    },
    table: {
      borderWidth: 1,
      borderColor: Colors.dark,
    },
    td: {
      borderWidth: 1,
      borderColor: Colors.dark,
    },
    th: {
      borderWidth: 1,
      borderColor: Colors.dark,
      backgroundColor: Colors.darkGray2,
      color: Colors.secondary,
    },
  },
});

export default RenderHtml;
