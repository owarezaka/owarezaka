import React, { useContext } from 'react';
import { View } from 'react-native';
import SyntaxHighlighter from '~app/lib/highlighter/NativeSyntaxHighlighter';
import Proptypes from 'prop-types';
import { dark, defaultStyle } from 'react-syntax-highlighter/src/styles/hljs';
import { ThemeContext } from '~app/store/theme-context';

const CodeBlock = (props) => {
  const { theme } = useContext(ThemeContext);
  return (
    <View testID={props.testID} style={props.style}>
      <SyntaxHighlighter style={theme === 'light' ? defaultStyle : dark} language={props.language}>
        {props.code}
      </SyntaxHighlighter>
    </View>
  );
};

CodeBlock.propTypes = {
  code: Proptypes.string,
  testID: Proptypes.string,
  style: Proptypes.object,
  language: Proptypes.string,
};

export default CodeBlock;
