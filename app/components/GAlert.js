import React, { useContext } from 'react';
import { StyleSheet, View, Text, Modal, Dimensions } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import GButton from '~app/components/GButton';
import Icons from '~app/assets/Icons';
import Proptypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { ThemeContext } from '~app/store/theme-context';

const GAlert = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);

  let icon;
  const iconSize = 22;
  const iconStyle = { transform: [{ translateY: 2 }] };

  switch (props.variant) {
    case 'Success':
      icon = (
        <Icons.statusSuccess
          testID="statusSuccess"
          size={iconSize}
          color={theme === 'dark' ? Colors.green700 : Colors.green}
          backgroundColor={theme === 'dark' ? Colors.dark : Colors.lightGray}
          style={iconStyle}
        />
      );
      break;
    case 'Warning':
      icon = (
        <Icons.warning
          testID="warning"
          size={iconSize}
          color={theme === 'dark' ? Colors.yellow700 : Colors.yellow}
        />
      );
      break;
    case 'Danger':
      icon = (
        <Icons.statusWarning
          testID="statusWarning"
          size={20}
          backgroundColor={theme === 'dark' ? Colors.dark : Colors.lightGray}
          color={theme === 'dark' ? Colors.red600 : Colors.red}
        />
      );
      break;
    case 'Info':
      icon = (
        <Icons.information
          testID="information"
          size={iconSize}
          color={theme === 'dark' ? Colors.primary500 : Colors.primary}
        />
      );
      break;
    case 'Tip':
      icon = (
        <Icons.bulb
          testID="bulb"
          size={20}
          color={theme === 'dark' ? Colors.lightGray3 : Colors.darkGray2}
        />
      );
      break;
    default:
      throw new Error('Unknown Icon Type');
  }

  return (
    <Modal
      testID={props.testID}
      visible={props.visible}
      transparent={true}
      animationType="fade"
      hardwareAccelerated
    >
      <View style={styles.containerModal}>
        <View
          style={
            theme === 'light'
              ? [s.lightContainer, styles.modal]
              : [s.darkContainer, styles.darkBorderStyle, styles.modal]
          }
        >
          <View style={styles.icon}>
            {icon}
            <Text
              style={
                theme === 'light'
                  ? [s.lightText, styles.titleStyle]
                  : [s.darkText, styles.titleStyle]
              }
            >
              {t(props.title)}
            </Text>
          </View>
          <Text
            style={
              theme === 'light' ? [s.lightText, styles.textStyle] : [s.darkText, styles.textStyle]
            }
          >
            {t(props.message)}
          </Text>
          <View style={styles.btnContainer}>
            {props.primaryButtonText && (
              <View style={styles.primaryBtnContainer}>
                <GButton
                  category={theme === 'dark' ? 'darkPrimary' : 'primary'}
                  variant={props.variant === 'Danger' ? 'danger' : 'confirm'}
                  fontSize={12}
                  padding={6}
                  width={110}
                  click={props.primaryAction}
                  label={props.primaryButtonText}
                />
              </View>
            )}
            {props.secondaryButtonText && (
              <GButton
                category={theme === 'dark' ? 'darkSecondary' : 'secondary'}
                variant={props.variant === 'Danger' ? 'danger' : 'confirm'}
                size="small"
                click={props.secondaryAction}
                label={props.secondaryButtonText}
              />
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

GAlert.propTypes = {
  testID: Proptypes.string,
  visible: Proptypes.bool.isRequired,
  title: Proptypes.string.isRequired,
  message: Proptypes.string.isRequired,
  variant: Proptypes.string.isRequired,
  primaryAction: Proptypes.func,
  primaryButtonText: Proptypes.string,
  secondaryAction: Proptypes.func,
  secondaryButtonText: Proptypes.string,
};

const styles = StyleSheet.create({
  btnContainer: {
    flexDirection: 'row',
  },
  containerModal: {
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    flex: 1,
    justifyContent: 'center',
  },
  darkBorderStyle: {
    borderColor: Colors.lightGray3,
    borderWidth: 0.7,
  },
  icon: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    width: 200,
  },
  modal: {
    alignItems: 'center',
    borderRadius: 15,
    flex: 0,
    justifyContent: 'center',
    marginHorizontal: Dimensions.get('screen').width <= 393 ? 45 : 70,
    padding: 15,
  },
  primaryBtnContainer: {
    marginRight: 5,
  },
  textStyle: {
    fontSize: 16,
    marginHorizontal: 20,
    marginVertical: 10,
    textAlign: 'center',
  },
  titleStyle: {
    fontSize: 18,
    paddingLeft: 5,
  },
});

export default GAlert;
