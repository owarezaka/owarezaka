import React, { useState, useCallback } from 'react';
import { RefreshControl, FlatList } from 'react-native';
import Proptypes from 'prop-types';

const RefreshList = (props) => {
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async (callback) => {
    setRefreshing(true);
    await callback();
    setRefreshing(false);
  }, []);

  return (
    <FlatList
      {...props}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={() => onRefresh(props.refresh)} />
      }
    />
  );
};

RefreshList.propTypes = {
  refresh: Proptypes.func.isRequired,
};

export default RefreshList;
