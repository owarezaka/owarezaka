import React from 'react';
import { StyleSheet, View } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { Colors } from '../constants/GlobalStyles';
import Proptypes from 'prop-types';

const Logo = (props) => {
  return (
    <View testID={props.testID}>
      <View style={styles.center}>
        <Svg width={props.width} height={props.height} viewBox="0 0 36 36">
          <Path
            fill={Colors.orangeRed}
            d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"
          />
          <Path
            fill={Colors.orangeRed}
            d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"
          />
          <Path fill={Colors.orangeRed} d="M18,34.38 3,14 33,14 Z" />
          <Path fill={Colors.orange} d="M18,34.38 11.38,14 2,14 6,25Z" />
          <Path fill={Colors.orange} d="M18,34.38 24.62,14 34,14 30,25Z" />
          <Path fill={Colors.orangeYellow} d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z" />
          <Path fill={Colors.orangeYellow} d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z" />
        </Svg>
      </View>
    </View>
  );
};

Logo.propTypes = {
  width: Proptypes.string.isRequired,
  height: Proptypes.string.isRequired,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  center: {
    alignSelf: 'center',
  },
});

export default Logo;
