import React, { useState, useEffect, useContext, useCallback } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Share,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import GitlabApi from '~app/lib/auth/GitlabApi';
import s, { Colors } from '~app/constants/GlobalStyles';
import UserStorage from '~app/lib/auth/UserStorage';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import GAlert from '~app/components/GAlert';
import NavigationButton from '~app/components/NavigationButton';
import Proptypes from 'prop-types';
import CodeBlock from '~app/components/CodeBlock';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';

const SnippetDetails = (props) => {
  const { theme } = useContext(ThemeContext);
  const { t } = useTranslation();
  const [codes, setCodes] = useState('');
  const [showError, setShowError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getSnippetDetails();
    setRefreshing(false);
  }, [getSnippetDetails]);

  const getSnippetDetails = useCallback(async () => {
    if (!props.snippetId) {
      return;
    }

    const user = await UserStorage.getUserInfo();
    const id = props.snippetId.split('Snippet/')[1];
    const result = await GitlabApi.getSnippet(user.instanceUrl, id, user.userToken);

    if (result) {
      setCodes(result);
    } else {
      setShowError(true);
    }
  }, [props.snippetId]);

  useEffect(() => {
    getSnippetDetails();
  }, [getSnippetDetails]);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const handlePress = () => {
    props.onPress(null);
  };

  const onShare = async () => {
    try {
      await Share.share({
        message: codes,
      });
    } catch (error) {
      setShowError(true);
    }
  };

  return (
    <SafeAreaView
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
    >
      <NavigationButton
        title="Snippets"
        subTitle={props.snippet.id.split('Snippet/')[1]}
        onPress={handlePress}
      />

      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => onRefresh()} />}
      >
        <View style={styles.content}>
          <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
            {props.snippet.visibilityLevel === 'private' ? (
              <Icons.lock color={theme === 'light' ? Colors.dark : Colors.darkGray} />
            ) : (
              <Icons.earth
                color={theme === 'light' ? Colors.dark : Colors.darkGray}
                testID="earth"
              />
            )}{' '}
            <Text style={styles.text}>
              {`${t('Authored')} ${Utils.timeSince(props.snippet.createdAt)} ${t('by')}`}
            </Text>{' '}
            <Text style={styles.bold}>{props.snippet.author.name}</Text>
          </Text>
          <View style={theme === 'light' ? [s.line, styles.line] : [s.darkLine, styles.line]} />
          <Text
            style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
          >
            {props.snippet.title}
          </Text>

          <Text
            style={
              theme === 'light'
                ? [s.lightText, styles.description]
                : [s.darkText, styles.description]
            }
          >
            {props.snippet.description}
          </Text>

          <View style={theme === 'light' ? styles.codeContainer : styles.darkCodeContainer}>
            <GAlert
              testID="alert"
              visible={showError}
              variant="Danger"
              title="Error"
              message={`Connection failed.\nPlease retry later.`}
              secondaryButtonText="Dismiss"
              secondaryAction={displayErrorHandler}
            />
            <View style={styles.header}>
              <Text
                style={
                  theme === 'light' ? [s.lightText, styles.fileName] : [s.darkText, styles.fileName]
                }
              >
                {props.snippet.fileName}
              </Text>
              <GButton testID="shareButton" label="" flexDirection="row" click={onShare}>
                <Icons.share size={20} color={theme === 'light' ? Colors.dark : Colors.secondary} />
              </GButton>
            </View>

            <CodeBlock code={codes} testID="codes" />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

SnippetDetails.propTypes = {
  onPress: Proptypes.func.isRequired,
  snippet: Proptypes.shape({
    id: Proptypes.string.isRequired,
    title: Proptypes.string.isRequired,
    visibilityLevel: Proptypes.string.isRequired,
    createdAt: Proptypes.string.isRequired,
    description: Proptypes.string.isRequired,
    fileName: Proptypes.string.isRequired,
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
      avatarUrl: Proptypes.string.isRequired,
    }),
  }),
  snippetId: Proptypes.string.isRequired,
};

const styles = StyleSheet.create({
  codeContainer: {
    borderColor: Colors.border,
    borderWidth: 2,
    marginTop: 20,
    opacity: 0.7,
  },
  content: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  darkCodeContainer: {
    borderColor: Colors.darkGray2,
    borderWidth: 2,
    marginTop: 20,
    opacity: 0.7,
  },
  description: {
    fontSize: 16,
  },
  fileName: {
    fontSize: 16,
    fontWeight: '600',
    paddingLeft: 5,
    paddingTop: 5,
  },
  header: {
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: Colors.border,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  line: {
    borderBottomWidth: 0.3,
    borderColor: Colors.border,
    marginBottom: 15,
  },
  screen: {
    flex: 1,
  },
  scrollContainer: {
    paddingBottom: 25,
  },
  text: {
    fontSize: 16,
    marginBottom: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    marginBottom: 10,
  },
});

export default SnippetDetails;
