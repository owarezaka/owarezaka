import React, { useState, useEffect, useContext, useCallback } from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import SnippetItem from './SnippetItem';
import SnippetDetails from './SnippetDetails';
import NavigationButton from '~app/components/NavigationButton';
import RefreshList from '~app/components/RefreshList';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import s from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const SnippetsScreen = (props) => {
  const { theme } = useContext(ThemeContext);

  const [instanceUrl, setInstanceUrl] = useState(null);
  const [snippets, setSnippets] = useState([]);
  const [snippetId, setSnippetId] = useState(null);
  const [currentSnippetDetail, setCurrentSnippetDetail] = useState(null);
  const [showError, setShowError] = useState(false);
  const [animating, setAnimating] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getAllSnippets();
    setRefreshing(false);
  }, []);

  const getAllSnippets = async () => {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.getAllSnippets(user.instanceUrl, user.userToken);

    if (Array.isArray(result)) {
      setInstanceUrl(user.instanceUrl);
      setSnippets(result);
      setAnimating(false);
    }

    setShowError(result.success === false);
    setAnimating(false);
  };

  useEffect(() => {
    getAllSnippets();
  }, []);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const setCurrentSnippet = function (snippetID) {
    setSnippetId(snippetID);
    if (snippetID) {
      const [snippetDetails] = snippets.filter((item) => item.id === snippetID);
      setCurrentSnippetDetail(snippetDetails);
    }
  };

  return (
    <>
      {snippetId ? (
        <SnippetDetails
          snippetId={snippetId}
          snippet={currentSnippetDetail}
          onPress={setCurrentSnippet}
        />
      ) : (
        <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <NavigationButton title="Snippets" onPress={props.navigation.goBack} />
          <View style={styles.container}>
            <GAlert
              visible={showError}
              variant="Danger"
              title="Error"
              message={`Connection failed.\nPlease retry later.`}
              secondaryButtonText="Dismiss"
              secondaryAction={displayErrorHandler}
            />

            {animating ? (
              <LoadingSpinner />
            ) : snippets.length > 0 ? (
              <RefreshList
                testID="snippetList"
                refresh={() => getAllSnippets()}
                data={snippets}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => {
                  return (
                    <SnippetItem
                      snippet={item}
                      onPress={setCurrentSnippet}
                      instanceUrl={instanceUrl}
                    />
                  );
                }}
              />
            ) : (
              <EmptyScreen
                refreshing={refreshing}
                onRefresh={onRefresh}
                title="There is nothing here"
                text="Store, share, and embed small pieces of code and text."
              />
            )}
          </View>
        </SafeAreaView>
      )}
    </>
  );
};

SnippetsScreen.propTypes = {
  navigation: Proptypes.shape({
    goBack: Proptypes.func.isRequired,
  }).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
    marginTop: 10,
  },
});

export default SnippetsScreen;
