import React, { useContext } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import Proptypes from 'prop-types';
import s, { Colors } from '~app/constants/GlobalStyles';

import { ThemeContext } from '~app/store/theme-context';

const SnippetItem = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);

  const handlePress = () => {
    props.onPress(props.snippet.id);
  };

  return (
    <TouchableOpacity testID={`item${props.snippet.id}`} style={styles.item} onPress={handlePress}>
      <View>
        <Image
          testID={`img${props.snippet.id}`}
          source={
            props.snippet.author.avatarUrl.includes('gravatar')
              ? { uri: props.snippet.author.avatarUrl }
              : { uri: `${props.instanceUrl}${props.snippet.author.avatarUrl}` }
          }
          style={styles.avatar}
        />
      </View>

      <View style={styles.content}>
        <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
          <Text testID={`title${props.snippet.id}`} style={styles.title}>
            {props.snippet.title}
          </Text>{' '}
          {props.snippet.visibilityLevel === 'private' ? (
            <Icons.lock color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          ) : (
            <Icons.earth color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          )}
        </Text>

        <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
          <Text testID={`id${props.snippet.id}`} style={styles.text}>
            {`$${props.snippet.id.split('Snippet/')[1]}`}
          </Text>{' '}
          <Text testID={`since${props.snippet.id}`} style={styles.text}>
            {`· ${t('authored')} ${Utils.timeSince(props.snippet.createdAt)} ${t('by')}`}
          </Text>{' '}
          <Text testID={`author${props.snippet.id}`} style={styles.bold}>
            {props.snippet.author.name}
          </Text>
        </Text>
      </View>
    </TouchableOpacity>
  );
};

SnippetItem.propTypes = {
  instanceUrl: Proptypes.string.isRequired,
  onPress: Proptypes.func.isRequired,
  snippet: Proptypes.shape({
    id: Proptypes.string.isRequired,
    title: Proptypes.string.isRequired,
    visibilityLevel: Proptypes.string.isRequired,
    createdAt: Proptypes.string.isRequired,
    description: Proptypes.string.isRequired,
    fileName: Proptypes.string.isRequired,
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
      avatarUrl: Proptypes.string.isRequired,
    }),
  }),
};

const styles = StyleSheet.create({
  avatar: {
    borderRadius: 40,
    height: 40,
    marginRight: 10,
    width: 40,
  },
  bold: {
    fontWeight: '500',
  },
  content: {
    marginBottom: 10,
    width: '80%',
  },
  item: {
    borderBottomWidth: 0.25,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    padding: 10,
  },
  text: {
    fontSize: 16,
    marginTop: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default SnippetItem;
