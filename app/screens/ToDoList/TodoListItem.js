import React, { useState, useContext } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import * as Utils from '~app/utils/Utils';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';

const TodoListItem = (props) => {
  const { theme } = useContext(ThemeContext);
  const { t } = useTranslation();
  const [showError, setShowError] = useState(false);

  const markAsDone = async function (id) {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.markAsDone(user.instanceUrl, id, user.userToken);

    if (result === 201) {
      props.setTodos((prevItem) => prevItem.filter((item) => item.id !== id));
    }

    setShowError(result.success === false);
  };

  const displayErrorHandler = function () {
    setShowError(false);
  };

  return (
    <View
      style={
        theme === 'light' ? [s.lightBorderStyle, styles.item] : [s.darkBorderStyle, styles.item]
      }
    >
      <GAlert
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Something went wrong!\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />

      <View style={styles.header}>
        <Image
          testID={`todo${props.todo.id}`}
          source={{ uri: props.todo.author.avatar_url }}
          style={styles.avatar}
        />

        <View style={styles.content}>
          <Text>
            <Text>
              {props.todo.target.state !== 'opened' && (
                <Text style={styles.stateText}>&nbsp;{props.todo.target.state}&nbsp;</Text>
              )}
            </Text>
            &nbsp;
            <Text style={theme === 'light' ? s.lightText : s.darkText}>
              {props.todo.author.username}&nbsp;
            </Text>
            <Text style={theme === 'light' ? s.lightText : s.darkText}>
              {props.todo.action_name}&nbsp;
            </Text>
            &nbsp;
            <Text
              style={
                theme === 'light'
                  ? [s.lightBlueText, styles.todoLink]
                  : [s.darkBlueText, styles.todoLink]
              }
            >
              {props.todo.target_type}#{props.todo.target.iid}
            </Text>
            &nbsp;
            <Text style={theme === 'light' ? s.lightText : s.darkText}>
              &quot;{props.todo.target.title}&quot; {t('at')}&nbsp;
            </Text>
            <Text
              style={
                theme === 'light'
                  ? [s.lightBlueText, styles.todoLink]
                  : [s.darkBlueText, styles.todoLink]
              }
            >
              {props.todo.project.name_with_namespace}
            </Text>
            {props.todo.target.state !== 'merged' &&
              props.todo.target.assignees?.map((user) => (
                <Text key={user.id} style={theme === 'light' ? s.lightText : s.darkText}>
                  &nbsp;{t('to')} {user.username}&nbsp;
                </Text>
              ))}
            &nbsp;
            <Text style={theme === 'light' ? s.lightText : s.darkText}>
              {Utils.timeSince(props.todo.created_at)}
            </Text>
          </Text>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <GButton
          label="Done"
          category={theme === 'light' ? '' : 'darkSecondary'}
          size="medium"
          click={() => markAsDone(props.todo.id)}
        />
      </View>
    </View>
  );
};

TodoListItem.propTypes = {
  todo: Proptypes.shape({
    action_name: Proptypes.string,
    author: Proptypes.shape({
      username: Proptypes.string.isRequired,
      avatar_url: Proptypes.string,
    }),
    id: Proptypes.number.isRequired,
    project: Proptypes.shape({
      name_with_namespace: Proptypes.string.isRequired,
    }),
    target: Proptypes.shape({
      iid: Proptypes.number,
      state: Proptypes.string,
      title: Proptypes.string,
      assignees: Proptypes.arrayOf(
        Proptypes.shape({
          id: Proptypes.number,
          username: Proptypes.string,
        }),
      ),
    }),
    target_type: Proptypes.string,
    created_at: Proptypes.string.isRequired,
  }),
  setTodos: Proptypes.func,
};

const styles = StyleSheet.create({
  avatar: {
    borderRadius: 40,
    height: 40,
    marginRight: 10,
    width: 40,
  },
  btnContainer: {
    alignItems: 'center',
  },
  content: {
    paddingBottom: 10,
    width: '80%',
  },
  header: {
    flexDirection: 'row',
    marginTop: 5,
    padding: 10,
  },
  item: {
    borderBottomWidth: 0.25,
    paddingBottom: 10,
  },
  stateText: {
    backgroundColor: Colors.primary,
    color: Colors.secondary,
  },
  todoLink: {
    fontWeight: 'bold',
  },
});

export default TodoListItem;
