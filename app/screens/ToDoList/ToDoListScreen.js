import React, { useState, useEffect, useContext, useCallback } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import TodoListItem from './TodoListItem';
import RefreshList from '~app/components/RefreshList';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import s from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';

const ToDoListScreen = () => {
  const { theme } = useContext(ThemeContext);

  const [todos, setTodos] = useState([]);
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await fetchTodos();
    setRefreshing(false);
  }, []);

  const fetchTodos = async () => {
    const user = await UserStorage.getUserInfo();
    const response = await GitlabApi.getTodos(user.instanceUrl, user.userToken);

    if (Array.isArray(response)) {
      setTodos(response);
      setLoading(false);
    }

    setShowError(response.success === false);
    setLoading(false);
  };

  useEffect(() => {
    fetchTodos();
  }, []);

  const hideErrorAlert = function () {
    setShowError(false);
  };

  return (
    <>
      {loading ? (
        <LoadingSpinner />
      ) : showError ? (
        <View style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <GAlert
            testID="alert"
            visible={showError}
            variant="Danger"
            title="Error"
            message="Connection failed. Please retry later."
            secondaryButtonText="Dismiss"
            secondaryAction={hideErrorAlert}
          />
        </View>
      ) : todos.length > 0 ? (
        <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <RefreshList
            style={styles.container}
            refresh={() => fetchTodos()}
            testID="ToDo-List"
            data={todos}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => {
              return <TodoListItem todo={item} setTodos={setTodos} />;
            }}
          />
        </SafeAreaView>
      ) : (
        <EmptyScreen
          refreshing={refreshing}
          onRefresh={onRefresh}
          title="There is nothing here"
          text="When an issue or merge request is assigned to you, or when you receive a @mention in a comment, your To-Do List will be updated."
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginTop: 10,
  },
});

export default ToDoListScreen;
