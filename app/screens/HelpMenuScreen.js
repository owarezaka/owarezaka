import React from 'react';
import { View, Text, StyleSheet, Linking } from 'react-native';
import { useTranslation } from 'react-i18next';
import { Colors } from '~app/constants/GlobalStyles';
import Gitlab from '~app/constants/Gitlab';
import GButton from '~app/components/GButton';
import Proptypes from 'prop-types';

const HelpMenuScreen = (props) => {
  const { t } = useTranslation();

  return (
    <View style={styles.screen}>
      <Text style={styles.title}>
        {t('Owarezaka is an IOS & Android client for GitLab, an open source devops platform.')}
      </Text>
      <View style={styles.line} />
      <View style={styles.btnContainer}>
        <GButton
          category="tertiary"
          label="Help"
          size="large"
          textAlign="left"
          click={() => props.navigation.navigate('HelpScreen')}
        />
      </View>

      <View style={styles.btnContainer}>
        <GButton
          category="tertiary"
          label="Contribute to Owarezaka"
          size="large"
          textAlign="left"
          click={() => Linking.openURL(Gitlab.owarezaka)}
        />
      </View>
    </View>
  );
};

HelpMenuScreen.propTypes = {
  navigation: Proptypes.shape({
    navigate: Proptypes.func.isRequired,
  }),
};

const styles = StyleSheet.create({
  btnContainer: {
    paddingLeft: 15,
  },
  line: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    color: Colors.dark,
  },
  screen: {
    flex: 1,
  },
  title: {
    color: Colors.dark,
    fontSize: 16,
    padding: 20,
  },
});

export default HelpMenuScreen;
