import React, { useContext, useState, useCallback, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Text, SafeAreaView, ActivityIndicator } from 'react-native';
import Proptypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

import { ThemeContext } from '~app/store/theme-context';
import s, { Colors } from 'app/constants/GlobalStyles';
import NavigationButton from '~app/components/NavigationButton';
import GAlert from '~app/components/GAlert';
import OverviewHeader from './OverviewHeader';
import GroupItem from './GroupItem';

import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';

const GroupDetails = ({ group, setCurrentGroup }) => {
  const { theme } = useContext(ThemeContext);
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [showError, setShowError] = useState(false);
  const [user, setUser] = useState(null);
  const [details, setDetails] = useState(null);
  const [animating, setAnimating] = useState(false);
  const [empty, setEmpty] = useState(false);
  const [subgroup, setSubgroup] = useState(null);

  const getGroupDetails = useCallback(
    async (fullPath) => {
      setAnimating(true);
      const userInfo = await UserStorage.getUserInfo();
      setUser(userInfo);

      const result = await GitlabApi.getSubgroupsAndProjectsDetailsofGroup(
        fullPath ?? group.group.fullPath,
        userInfo.instanceUrl,
        userInfo.userToken,
      );

      if (result.data) {
        setAnimating(false);

        return checkIfEmpty(result.data) ? setEmpty(true) : setDetails(result.data);
      }

      setAnimating(false);
      setShowError(result.success === false);
    },
    [group.group.fullPath],
  );

  useEffect(() => {
    getGroupDetails();
  }, [getGroupDetails]);

  const handlePress = () => {
    if (subgroup) {
      setSubgroup(null);
      return getGroupDetails(group.group.fullPath);
    }

    setCurrentGroup(null);
  };

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const handleNavigation = (id) => {
    navigation.navigate('Projects', id);
  };

  const getSubgroupDetails = async function (fullPath, subGroup) {
    setSubgroup(subGroup);

    setAnimating(true);
    const response = await GitlabApi.getSubgroupsAndProjectsDetailsofGroup(
      fullPath,
      user.instanceUrl,
      user.userToken,
    );

    if (response.data) {
      setAnimating(false);
      return checkIfEmpty(response.data) ? setEmpty(true) : setDetails(response.data);
    }

    setAnimating(false);
    setShowError(response.success === false);
  };

  const checkIfEmpty = (data) => {
    return data.group.descendantGroups.nodes.length === 0 && data.group.projects.nodes.length === 0;
  };

  function setImageSourceHandler() {
    if (group.group.avatarUrl) {
      if (group.group.visibility === 'private') {
        if (theme === 'light') {
          return require('~app/assets/groupAvatarDefault.png');
        }

        if (theme === 'dark') {
          return require('~app/assets/groupAvatarDark.png');
        }
      }

      return { uri: group.group.avatarUrl };
    }

    if (!group.group.avatarUrl && theme === 'light') {
      return require('~app/assets/groupAvatarDefault.png');
    }

    if (!group.group.avatarUrl && theme === 'dark') {
      return require('~app/assets/groupAvatarDark.png');
    }
  }
  const source = setImageSourceHandler();

  return (
    <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
      <NavigationButton
        title="Groups"
        subTitle={subgroup?.name ?? group.group.name}
        onPress={handlePress}
      />

      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />

      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <OverviewHeader
          testID="overviewHeader"
          fullName={subgroup?.name ?? group.group.name}
          visibility={subgroup?.visibility ?? group.group.visibility}
          source={source}
        />

        <View style={theme === 'light' ? s.lightLine : s.darkLine} />

        <View style={styles.titleContainer}>
          <Text
            style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
          >
            {t('Subgroups and projects')}
          </Text>
        </View>

        <View style={styles.list}>
          {animating && <ActivityIndicator animating size="large" color={Colors.darkGray2} />}

          {details?.group?.descendantGroups?.nodes?.map((el) => (
            <GroupItem
              avatarUrl={el.avatarUrl}
              subgroup={true}
              description={el.description}
              fullPath={el.fullPath}
              key={el.id}
              name={el.name}
              setFullPath={() => getSubgroupDetails(el.fullPath, el)}
              source={
                el.avatarUrl
                  ? { uri: el.avatarUrl }
                  : theme === 'light'
                  ? require('~app/assets/groupAvatarDefault.png')
                  : require('~app/assets/groupAvatarDark.png')
              }
              testID="subgroups"
              visibility={el.visibility}
            />
          ))}

          {!empty &&
            details?.group?.projects?.nodes?.map((el) => (
              <GroupItem
                avatarUrl={el.avatarUrl}
                description={el.description}
                key={el.id}
                name={el.name}
                navigation={navigation}
                navigateTo={() => handleNavigation(el.id)}
                project={true}
                source={
                  el.avatarUrl
                    ? { uri: el.avatarUrl }
                    : theme === 'light'
                    ? require('~app/assets/groupAvatarDefault.png')
                    : require('~app/assets/groupAvatarDark.png')
                }
                testID="projects"
                visibility={el.visibility}
              />
            ))}
        </View>

        {empty && (
          <View style={styles.emptyContainer}>
            <Text style={styles.titleStyle}>
              {t('A group is a collection of several projects.')}
            </Text>
            <Text
              style={
                theme === 'light' ? [s.lightText, styles.textStyle] : [s.darkText, styles.textStyle]
              }
            >
              {t(
                'If you organize your projects under a group, it works like a folder. \n\nYou can manage your group member’s permissions and access to each project in the group.',
              )}
            </Text>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

GroupDetails.propTypes = {
  group: Proptypes.shape({
    accessLevel: Proptypes.shape({
      integerValue: Proptypes.number.isRequired,
    }),
    id: Proptypes.string.isRequired,
    group: Proptypes.shape({
      avatarUrl: Proptypes.string,
      description: Proptypes.string,
      fullPath: Proptypes.string,
      name: Proptypes.string.isRequired,
      visibility: Proptypes.string.isRequired,
    }),
  }),
  setCurrentGroup: Proptypes.func,
};

const styles = StyleSheet.create({
  emptyContainer: {
    padding: 20,
  },
  list: {
    paddingHorizontal: 20,
  },
  scrollContainer: {
    paddingBottom: 50,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    paddingLeft: 10,
    textDecorationLine: 'underline',
  },
  titleContainer: {
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  titleStyle: {
    color: Colors.primary,
    fontSize: 20,
  },
});

export default GroupDetails;
