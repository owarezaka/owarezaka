import React, { useContext } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';

import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const GroupItem = ({
  id,
  source,
  avatarUrl,
  name,
  visibility,
  accessLevel,
  description,
  fullPath,
  setGroupId,
  subgroup,
  project,
  testID,
  setFullPath,
  navigation,
  navigateTo,
}) => {
  const { theme } = useContext(ThemeContext);

  const handlePress = () => {
    setGroupId && setGroupId(id);
    setFullPath && setFullPath(fullPath);
    navigateTo && navigateTo();
  };

  return (
    <TouchableOpacity testID={testID} style={styles.item} onPress={handlePress}>
      {subgroup && (
        <Icons.subgroup
          testID="subgroup-icon"
          style={styles.iconStyle}
          size={18}
          color={theme === 'light' ? Colors.dark : Colors.darkGray}
        />
      )}
      {project && (
        <Icons.project
          testID="project-icon"
          style={styles.iconStyle}
          size={18}
          color={theme === 'light' ? Colors.dark : Colors.darkGray}
        />
      )}

      <View>
        <Image
          testID="avatar"
          source={source}
          style={theme === 'light' ? styles.avatar : styles.darkAvatar}
        />
        {!avatarUrl && (
          <Text
            style={
              theme === 'light'
                ? [s.lightText, styles.avatarLabel]
                : [s.darkText, styles.avatarLabel]
            }
          >
            {name[0].toUpperCase()}
          </Text>
        )}
      </View>

      <View style={styles.content}>
        <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
          {name}{' '}
          {visibility === 'private' ? (
            <Icons.lock color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          ) : visibility === 'internal' ? (
            <Icons.shield color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          ) : (
            <Icons.earth
              testID="earth-icon"
              color={theme === 'light' ? Colors.dark : Colors.darkGray}
            />
          )}
          {accessLevel && (
            <View style={theme === 'light' ? styles.lightRoleContainer : styles.darkRoleContainer}>
              <Text style={theme === 'light' ? [s.lightText, styles.text] : styles.darkRole}>
                {`${Utils.findRoleByAccessLevel(accessLevel)}`}
              </Text>
            </View>
          )}
        </Text>
        {description ? (
          <Text
            testID="description"
            style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
          >
            {description}
          </Text>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

GroupItem.propTypes = {
  id: Proptypes.string,
  source: Proptypes.any,
  avatarUrl: Proptypes.string,
  fullPath: Proptypes.string,
  name: Proptypes.string.isRequired,
  visibility: Proptypes.string.isRequired,
  accessLevel: Proptypes.number,
  description: Proptypes.string,
  navigateTo: Proptypes.func,
  navigation: Proptypes.object,
  setFullPath: Proptypes.func,
  setGroupId: Proptypes.func,
  project: Proptypes.bool,
  subgroup: Proptypes.bool,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  avatar: {
    borderColor: Colors.darkGray,
    borderRadius: 10,
    borderWidth: 0.4,
    height: 40,
    marginRight: 10,
    position: 'relative',
    width: 40,
  },
  avatarLabel: {
    fontSize: 24,
    position: 'absolute',
    transform: [{ translateY: 2 }, { translateX: 12 }],
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  darkAvatar: {
    borderRadius: 10,
    borderWidth: 0,
    height: 40,
    marginRight: 10,
    position: 'relative',
    width: 40,
  },
  darkRole: {
    color: Colors.lightGray4,
    fontSize: 16,
  },
  darkRoleContainer: {
    borderColor: Colors.dark,
    borderRadius: 10,
    borderWidth: 1,
    transform: [{ translateY: 5 }, { translateX: 5 }],
  },
  iconStyle: {
    alignItems: 'center',
    marginRight: 10,
    marginTop: 10,
  },
  item: {
    borderBottomWidth: 0.3,
    borderColor: Colors.border,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingVertical: 10,
  },
  lightRoleContainer: {
    borderColor: Colors.darkGray,
    borderRadius: 10,
    borderWidth: 0.4,
    transform: [{ translateY: 3 }, { translateX: 5 }],
  },
  text: {
    fontSize: 16,
    padding: 1,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    padding: 1,
  },
});

export default GroupItem;
