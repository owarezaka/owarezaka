import React, { useContext } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';
import s, { Colors } from 'app/constants/GlobalStyles';

import Icons from 'app/assets/Icons';

const OverviewHeader = ({ testID, fullName, visibility, source }) => {
  const { theme } = useContext(ThemeContext);

  const containerStyle = {
    alignItems: 'center',
    backgroundColor: theme === 'light' ? Colors.lightGray3 : Colors.none,
    flexDirection: 'row',
    paddingLeft: 10,
    paddingVertical: 10,
  };

  return (
    <View style={containerStyle} testID={testID}>
      <View>
        <Image
          source={source}
          style={theme === 'light' ? styles.avatar : styles.darkAvatar}
          testID="avatar"
        />
        <Text
          style={
            theme === 'light' ? [s.lightText, styles.avatarLabel] : [s.darkText, styles.avatarLabel]
          }
        >
          {typeof source === 'object' ? null : fullName[0].toUpperCase()}
        </Text>
      </View>
      <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
        {fullName}{' '}
        {visibility === 'private' ? (
          <Icons.lock color={theme === 'light' ? Colors.dark : Colors.darkGray} />
        ) : visibility === 'internal' ? (
          <Icons.shield color={theme === 'light' ? Colors.dark : Colors.darkGray} />
        ) : (
          <Icons.earth
            testID="public-icon"
            color={theme === 'light' ? Colors.dark : Colors.darkGray}
          />
        )}
      </Text>
    </View>
  );
};

OverviewHeader.propTypes = {
  fullName: Proptypes.string.isRequired,
  source: Proptypes.any,
  visibility: Proptypes.string,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  avatar: {
    borderColor: Colors.darkGray,
    borderRadius: 10,
    borderWidth: 0.4,
    height: 40,
    width: 40,
  },
  avatarLabel: {
    fontSize: 24,
    position: 'absolute',
    transform: [{ translateY: 2 }, { translateX: 12 }],
  },
  darkAvatar: {
    borderColor: Colors.dark,
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    width: 40,
  },
  title: {
    flexShrink: 1,
    fontSize: 20,
    fontWeight: '500',
    paddingLeft: 15,
  },
});

export default OverviewHeader;
