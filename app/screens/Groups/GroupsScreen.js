import React, { useState, useEffect, useContext, useCallback } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';
import s from 'app/constants/GlobalStyles';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import NavigationButton from '~app/components/NavigationButton';
import GAlert from '~app/components/GAlert';
import RefreshList from '~app/components/RefreshList';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import GroupItem from './GroupItem';
import GroupDetails from './GroupDetails';

const GroupsScreen = ({ navigation }) => {
  const { theme } = useContext(ThemeContext);

  const [groups, setGroups] = useState([]);
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [groupId, setGroupId] = useState(null);
  const [currentGroupDetails, setCurrentGroupDetails] = useState(null);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await fetchGroups();
    setLoading(false);
  }, []);

  const fetchGroups = async () => {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.getAllGroups(user.instanceUrl, user.userToken);

    if (Array.isArray(result)) {
      setGroups(result);
      setLoading(false);
    }

    setShowError(result.success === false);
    setLoading(false);
  };

  useEffect(() => {
    fetchGroups();
  }, []);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const setCurrentGroup = function (groupID) {
    setGroupId(groupID);

    if (groupID) {
      const [groupDetails] = groups.filter((item) => item.id === groupID);
      setCurrentGroupDetails(groupDetails);
    }
  };

  function setImageSourceHandler(group) {
    if (group.group.avatarUrl) {
      if (group.group.visibility === 'private') {
        if (theme === 'light') {
          return require('~app/assets/groupAvatarDefault.png');
        }

        if (theme === 'dark') {
          return require('~app/assets/groupAvatarDark.png');
        }
      }

      return { uri: group.group.avatarUrl };
    }

    if (!group.group.avatarUrl && theme === 'light') {
      return require('~app/assets/groupAvatarDefault.png');
    }

    if (!group.group.avatarUrl && theme === 'dark') {
      return require('~app/assets/groupAvatarDark.png');
    }
  }

  const renderItem = ({ item }) => {
    const source = setImageSourceHandler(item);

    return (
      <GroupItem
        testID={`item${item.id}`}
        accessLevel={item.accessLevel.integerValue}
        avatarUrl={item.group.avatarUrl}
        description={item.group.description}
        id={item.id}
        name={item.group.name}
        navigation={navigation}
        setGroupId={setCurrentGroup}
        source={source}
        subgroup={true}
        visibility={item.group.visibility}
      />
    );
  };

  return (
    <>
      {groupId ? (
        <GroupDetails group={currentGroupDetails} setCurrentGroup={setCurrentGroup} />
      ) : (
        <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <NavigationButton title="Groups" onPress={navigation.goBack} />

          {loading ? (
            <LoadingSpinner />
          ) : showError ? (
            <View
              style={
                theme === 'light'
                  ? [s.lightContainer, styles.container]
                  : [s.darkContainer, styles.container]
              }
            >
              <GAlert
                visible={showError}
                variant="Danger"
                title="Error"
                message={`Connection failed.\nPlease retry later.`}
                secondaryButtonText="Dismiss"
                secondaryAction={displayErrorHandler}
              />
            </View>
          ) : groups?.length > 0 ? (
            <RefreshList
              style={styles.container}
              refresh={() => fetchGroups()}
              testID="Groups-List"
              data={groups}
              keyExtractor={(item) => item.id}
              renderItem={renderItem}
            />
          ) : (
            <EmptyScreen
              refreshing={refreshing}
              onRefresh={onRefresh}
              title="There is nothing here"
              text="You are not currently part of any groups."
            />
          )}
        </SafeAreaView>
      )}
    </>
  );
};

GroupsScreen.propTypes = {
  navigation: Proptypes.object,
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 12,
  },
});

export default GroupsScreen;
