import React, { useState, useContext, useLayoutEffect } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import GButton from '~app/components/GButton';
import Proptypes from 'prop-types';
import { ThemeContext } from '~app/store/theme-context';
import EmojiBox from '~app/components/EmojiBox';

const SetStatus = ({ route, navigation }) => {
  const { theme } = useContext(ThemeContext);
  const params = route.params;

  const [message, setMessage] = useState(params.status);
  const [isFocus, setIsFocus] = useState(false);
  const [showError, setShowError] = useState(false);
  const [emojiBox, setEmojiBox] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: params.status ? 'Edit Status' : 'Set a Status',
    });
  }, [navigation, params.status]);

  async function setStatus() {
    const status = message.trim();
    if (!status) return;

    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.setUserStatus(user.instanceUrl, user.userToken, status);
    params.getUserStatus();
    setShowError(result.success === false);
  }

  async function removeStatus() {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.removeUserStatus(user.instanceUrl, user.userToken);

    clearInputHandler();
    params.getUserStatus();

    if (result) {
      setShowError(result.success === false);
    }
  }

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const clearInputHandler = function () {
    setMessage('');
  };

  return (
    <View
      style={theme === 'light' ? [s.lightContainer, styles.flex] : [s.darkContainer, styles.flex]}
    >
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />

      <View style={styles.statusBar}>
        <View
          style={[
            theme === 'light'
              ? styles.statusText
              : [styles.statusText, { backgroundColor: Colors.lightGray2 }],
            isFocus
              ? { borderColor: Colors.primary }
              : theme === 'light'
              ? { borderColor: Colors.border }
              : { borderColor: Colors.darkGray },
          ]}
        >
          <TextInput
            testID="status"
            value={message}
            onChangeText={(text) => setMessage(text)}
            autoFocus={true}
            onFocus={() => setIsFocus(true)}
            style={styles.flex}
          />
          <GButton
            category={theme === 'light' ? 'darkTertiary' : 'tertiary'}
            flexDirection="row"
            click={() => setEmojiBox(true)}
            testID="emoji"
          >
            <Icons.slightSmile size={20} />
          </GButton>
        </View>

        <GButton
          testID="clearButton"
          category={theme === 'light' ? 'secondary' : 'darkSecondary'}
          flexDirection="row"
          click={clearInputHandler}
        >
          <Icons.close size={20} color={theme === 'light' ? Colors.primary : Colors.secondary} />
        </GButton>
      </View>

      <View style={theme === 'light' ? s.lightLine : s.darkLine} />

      <View style={styles.rowDirection}>
        <View style={styles.removeBtn}>
          <GButton
            testID="removeButton"
            category={theme === 'light' ? 'secondary' : 'darkSecondary'}
            variant={theme === 'dark' ? 'confirm' : ''}
            label="Remove Status"
            click={removeStatus}
          />
        </View>

        <GButton
          testID="setStatusButton"
          disabled={message ? false : true}
          category={
            message
              ? theme === 'light'
                ? 'primary'
                : 'darkPrimary'
              : theme === 'light'
              ? 'disabled'
              : 'darkDisabled'
          }
          label="Set Status"
          click={setStatus}
        />
      </View>

      {emojiBox && <EmojiBox close={() => setEmojiBox(false)} onPress={(e) => setMessage(e)} />}
    </View>
  );
};

SetStatus.propTypes = {
  route: Proptypes.object,
  navigation: Proptypes.object,
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  removeBtn: {
    marginRight: 5,
  },
  rowDirection: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 25,
    marginTop: 25,
  },
  statusBar: {
    flexDirection: 'row',
    marginHorizontal: 30,
    marginVertical: 20,
  },
  statusText: {
    borderWidth: 1,
    color: Colors.dark,
    flexDirection: 'row',
    fontSize: 16,
    height: 42,
    paddingLeft: 10,
    width: '85%',
  },
});

export default SetStatus;
