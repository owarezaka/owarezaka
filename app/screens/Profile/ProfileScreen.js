import React, { useContext, useState, useEffect, useCallback } from 'react';
import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { AuthContext } from '../../store/auth-context';
import s, { Colors } from '~app/constants/GlobalStyles';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import AccountDetails from './AccountDetails';

import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const ProfileScreen = ({ navigation }) => {
  const { theme } = useContext(ThemeContext);
  const authCtx = useContext(AuthContext);
  const [user, setUser] = useState(null);
  const [showError, setShowError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const getUserStatus = useCallback(async function () {
    const userInfo = await UserStorage.getUserInfo();
    const response = await GitlabApi.getUserStatus(userInfo.instanceUrl, userInfo.userToken);

    if (response.username) {
      setUser(
        Object.assign(response, {
          instanceUrl: userInfo.instanceUrl,
        }),
      );
    }

    setShowError(response.success === false);
  }, []);

  useEffect(() => {
    getUserStatus();
  }, [getUserStatus]);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getUserStatus();
    setRefreshing(false);
  }, [getUserStatus]);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const navigationHandler = function () {
    navigation.navigate('SetStatusScreen', {
      getUserStatus: getUserStatus,
      status: user?.status?.message ?? '',
    });
  };

  return (
    <ScrollView
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
      refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => onRefresh()} />}
    >
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />

      <AccountDetails testID="user" user={user} />

      <View
        style={
          theme === 'light' ? [s.lightBorderStyle, styles.line] : [s.darkBorderStyle, styles.line]
        }
      />

      <View style={styles.buttonContainer}>
        <GButton
          testID="setStatusButton"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          size="large"
          textAlign="left"
          label={user?.status?.message ? 'Edit Status' : 'Set Status'}
          click={navigationHandler}
        />

        <GButton
          testID="signOutButton"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          size="large"
          textAlign="left"
          label="Sign Out"
          click={authCtx.signOutHandler}
        />
      </View>
    </ScrollView>
  );
};

ProfileScreen.propTypes = {
  navigation: Proptypes.object,
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginHorizontal: '5%',
  },
  line: {
    borderBottomWidth: 0.3,
    borderColor: Colors.dark,
  },
  screen: {
    flex: 1,
  },
});

export default ProfileScreen;
