import React, { useContext } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';
import s, { Colors } from '~app/constants/GlobalStyles';

const AccountDetails = ({ testID, user }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <View style={styles.accountContainerStyle} testID={testID}>
      <Image
        source={
          user?.avatarUrl.includes('gravatar')
            ? { uri: user?.avatarUrl }
            : { uri: `${user?.instanceUrl}${user?.avatarUrl}` }
        }
        style={styles.avatar}
        testID="avatar"
      />
      <View style={styles.accountInfoStyle}>
        <Text
          style={
            theme === 'light' ? [s.lightText, styles.titleStyle] : [s.darkText, styles.titleStyle]
          }
        >
          {user?.name}
        </Text>
        <Text
          style={
            theme === 'light'
              ? [s.lightText, styles.subTitleStyle]
              : [s.darkText, styles.subTitleStyle]
          }
        >
          {user?.username ? `@${user?.username}` : ''}
        </Text>
        {user?.status && (
          <Text
            style={
              theme === 'light'
                ? [s.lightText, styles.statusStyle]
                : [s.darkText, styles.statusStyle]
            }
          >
            {user?.status.message}
          </Text>
        )}
      </View>
    </View>
  );
};

AccountDetails.propTypes = {
  testID: Proptypes.string,
  user: Proptypes.shape({
    avatarUrl: Proptypes.string,
    instanceUrl: Proptypes.string,
    name: Proptypes.string.isRequired,
    username: Proptypes.string.isRequired,
    status: Proptypes.shape({
      emoji: Proptypes.string,
      message: Proptypes.string,
    }),
  }),
};

export default AccountDetails;

const styles = StyleSheet.create({
  accountContainerStyle: {
    flexDirection: 'row',
    marginHorizontal: '5%',
    paddingVertical: 20,
  },
  accountInfoStyle: {
    paddingLeft: 8,
  },
  avatar: {
    borderColor: Colors.dark,
    borderRadius: 60,
    borderWidth: 0.2,
    height: 60,
    marginRight: 10,
    width: 60,
  },
  statusStyle: {
    fontSize: 16,
    paddingTop: 5,
  },
  subTitleStyle: {
    fontSize: 18,
  },
  titleStyle: {
    fontSize: 20,
    fontWeight: '600',
  },
});
