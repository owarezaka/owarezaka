import React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import Proptypes from 'prop-types';

const SplashScreen = ({ testID }) => {
  return (
    <ImageBackground
      testID={testID}
      resizeMode="cover"
      source={require('../assets/splash.png')}
      style={styles.img}
    />
  );
};

SplashScreen.propTypes = {
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  img: {
    flex: 1,
  },
});

export default SplashScreen;
