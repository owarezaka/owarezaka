import React, { useState, useContext, useEffect } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Pressable,
  Linking,
  SafeAreaView,
} from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import Gitlab from '~app/constants/Gitlab';
import Icons from '~app/assets/Icons';
import GAlert from '~app/components/GAlert';
import Logo from '~app/components/Logo';
import { useTranslation } from 'react-i18next';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import PreferencesStorage from '~app/lib/auth/PreferencesStorage';
import * as Utils from '~app/utils/Utils';
import { AuthContext } from '~app/store/auth-context';
import { ThemeContext } from '~app/store/theme-context';

const LoginScreen = () => {
  const { t } = useTranslation();
  const { theme, setTheme } = useContext(ThemeContext);
  const authCtx = useContext(AuthContext);

  const [instanceUrl, setInstanceUrl] = useState(Gitlab.saasUrl);
  const [token, setToken] = useState('');
  const [showInfo, setShowInfo] = useState(false);
  const [showWarning, setShowWarning] = useState(false);
  const [showVersionWarning, setShowVersionWarning] = useState(false);
  const [showError, setShowError] = useState(false);
  const [signingLoading, setSigningLoading] = useState(false);

  useEffect(() => {
    const getPrefs = async () => {
      const prefs = await PreferencesStorage.getPreferences();

      if (prefs) {
        setTheme(prefs.darkMode);
      }
    };
    getPrefs();
  }, [setTheme]);

  const checkVersion = async function () {
    const response = await GitlabApi.checkVersion(instanceUrl, token);

    if (response.status === 401) {
      return setShowWarning(true);
    } else if (response.version) {
      return response.version;
    }

    setShowError(response.success === false);
  };

  const onSigninPress = async () => {
    if (!Utils.signinEnabled(token)) {
      return;
    }

    const version = await checkVersion();

    if (version && !Utils.isSupported(version)) {
      return setShowVersionWarning(true);
    }

    setSigningLoading(true);

    const status = await UserStorage.login(instanceUrl, token);

    if (status === 200) {
      authCtx.onSignInHandler(token);
    } else if (status === 401) {
      setShowWarning(true);
    } else {
      setShowError(true);
    }
    setSigningLoading(false);
  };

  const openLink = function () {
    Linking.openURL(`${instanceUrl}${Gitlab.tokenPath}`);
  };

  const showInfoHandler = function () {
    setShowInfo(false);
  };

  const showWarningHandler = function () {
    setShowWarning(false);
  };

  const showErrorHandler = function () {
    setShowError(false);
  };

  const showVersionWarningHandler = function () {
    setShowVersionWarning(false);
  };

  return (
    <SafeAreaView
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
    >
      <Logo width="60" height="60" />
      <View
        style={
          theme === 'light'
            ? styles.divider
            : [styles.divider, { backgroundColor: Colors.secondary }]
        }
      />
      <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
        {t('Owarezaka')}
      </Text>
      <View
        style={
          theme === 'light'
            ? [s.lightBorderStyle, styles.inputContainer]
            : [styles.inputContainer, { borderColor: Colors.lightGray4 }]
        }
      >
        <GAlert
          visible={showInfo}
          variant="Tip"
          title="Tip"
          message="You must already have an account on the GitLab Instance or on gitlab.com to generate access token"
          primaryButtonText="Open in browser"
          primaryAction={openLink}
          secondaryButtonText="Cancel"
          secondaryAction={showInfoHandler}
        />
        <Text style={theme === 'light' ? [s.lightText, styles.label] : [s.darkText, styles.label]}>
          {t('Instance Url')}
        </Text>

        <TextInput
          testID="instance"
          style={
            theme === 'light'
              ? [styles.input, s.lightBorderStyle, s.lightText]
              : [styles.input, { borderColor: Colors.primary }, s.darkText]
          }
          value={instanceUrl}
          onChangeText={(text) => setInstanceUrl(text)}
          placeholder="https://gitlab.example.com/"
          placeholderTextColor={theme === 'light' ? Colors.gray : Colors.lightGray4}
        />

        <Text style={theme === 'light' ? [s.lightText, styles.label] : [s.darkText, styles.label]}>
          {t('Personal Access Token')}
          <Pressable testID="bulbBtn" onPress={() => setShowInfo(true)}>
            <Icons.bulb
              testID="bulb"
              size={24}
              color={theme === 'light' ? Colors.darkGray2 : Colors.primary}
            />
          </Pressable>
        </Text>

        <TextInput
          testID="token"
          style={
            theme === 'light'
              ? [styles.input, s.lightBorderStyle, s.lightText]
              : [styles.input, { borderColor: Colors.primary }, s.darkText]
          }
          value={token}
          secureTextEntry={true}
          onChangeText={(val) => setToken(val)}
          placeholder="Personal Access Token"
          placeholderTextColor={theme === 'light' ? Colors.gray : Colors.lightGray4}
        />

        <TouchableOpacity
          testID="signinButton"
          style={Utils.signinEnabled(token) ? styles.btnSignin : styles.btnSigninDisabled}
          onPress={onSigninPress}
        >
          <ActivityIndicator
            testID="signinLoading"
            size="large"
            color={Colors.secondary}
            animating={signingLoading}
            style={styles.spinner}
          />
          <Text style={styles.labelButton}>{t('Sign in')}</Text>
        </TouchableOpacity>

        <GAlert
          visible={showWarning}
          variant="Danger"
          title="Error"
          message="Token is expired. You can either do re-authorization or token refresh."
          secondaryButtonText="Dismiss"
          secondaryAction={showWarningHandler}
        />

        <GAlert
          visible={showError}
          variant="Danger"
          title="Error"
          message="Connection failed. Please retry later."
          secondaryButtonText="Dismiss"
          secondaryAction={showErrorHandler}
        />

        <GAlert
          testID="warning"
          visible={showVersionWarning}
          variant="Warning"
          title="Warning"
          message="GitLab 14.9 and above is supported"
          secondaryButtonText="Dismiss"
          secondaryAction={showVersionWarningHandler}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  btnSignin: {
    backgroundColor: Colors.primary,
    borderRadius: 5,
    height: 50,
    marginTop: 20,
    position: 'relative',
    textAlign: 'center',
    width: '87%',
  },
  btnSigninDisabled: {
    display: 'none',
  },
  divider: {
    backgroundColor: Colors.dark,
    height: 1,
    marginTop: 7,
    opacity: 0.2,
    width: '100%',
  },
  input: {
    borderRadius: 5,
    borderWidth: 1,
    fontSize: 18,
    height: 45,
    marginTop: 10,
    paddingLeft: 15,
    width: '87%',
  },
  inputContainer: {
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    height: 310,
    margin: 20,
  },
  label: {
    alignSelf: 'flex-start',
    fontSize: 18,
    marginLeft: 25,
    marginTop: 15,
  },
  labelButton: {
    alignSelf: 'center',
    color: Colors.secondary,
    fontSize: 18,
    marginVertical: 10,
  },
  screen: {
    flex: 1,
  },
  spinner: {
    left: 0,
    position: 'absolute',
    top: 0,
    transform: [{ translateY: 5 }, { translateX: 70 }],
  },
  title: {
    fontSize: 24,
    margin: 6,
    textAlign: 'center',
  },
});

export default LoginScreen;
