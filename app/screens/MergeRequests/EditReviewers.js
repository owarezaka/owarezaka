import React, { useState, useContext, useEffect, useReducer } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  FlatList,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Proptypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';
import Icon from '~app/assets/Icons';

const EditReviewers = (props) => {
  const members = props.members;
  const currentReviewers = props.currentReviewers;

  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);
  const [showWarning, setShowWarning] = useState(false);
  const [users, dispatch] = useReducer(reducer, members);

  const selectedReviewers = users.filter((el) => el.isSelected);

  function reducer(state, action) {
    switch (action.type) {
      case 'toggle':
        return state.map((user) => {
          if (user?.user?.username === action.payload.user.username) {
            return { ...user, isSelected: user.isSelected ? false : true };
          } else {
            return user;
          }
        });
      case 'filter':
        return members
          .filter((member) => member.user.username.includes(action.payload.text))
          .slice(0, 10);

      default:
        return state;
    }
  }

  useEffect(() => {
    const getCurrentReviewers = () => {
      members.forEach((member) => {
        currentReviewers.forEach((reviewer) => {
          if (member.user.id === reviewer.id) {
            dispatch({ type: 'toggle', payload: { user: member.user } });
          }
        });
      });
    };
    getCurrentReviewers();
  }, [currentReviewers, members]);

  const getReviewerIds = (unassign) => {
    let userGlobalIds = [];
    let reviewerIds = [];

    selectedReviewers.forEach((reviewer, i) => {
      if (reviewer.isSelected) {
        userGlobalIds[i] = reviewer.user.id;
      }
    });

    if (unassign) {
      reviewerIds = 0;
    } else {
      userGlobalIds.forEach((id, i) => (reviewerIds[i] = id.split('User/')[1]));
    }

    return reviewerIds;
  };

  const EditReviewerHandler = async (unassign = false) => {
    const userInfo = await UserStorage.getUserInfo();

    const response = await GitlabApi.updateMergeRequestReviewer(
      props.projectId,
      props.iid,
      getReviewerIds(unassign),
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    if (response === 200) {
      props.getMergeRequestsDetails();
      props.refreshIssues && props.refreshIssues();
      return props.setShowTextInput(false);
    }

    setShowError(response.success === false);
  };

  const showInputHandler = function () {
    props.setShowTextInput(false);
  };

  const showWarningHandler = function () {
    setShowWarning(false);
  };

  const showErrorHandler = function () {
    setShowError(false);
  };

  const onChangeText = (text) => {
    dispatch({ type: 'filter', payload: { text: text } });
  };

  const unassignReviewers = () => {
    return (
      <TouchableOpacity style={styles.reviewerItem} onPress={() => EditReviewerHandler(true)}>
        <Text style={[theme === 'light' ? s.lightText : s.darkText, styles.bold]}>
          {t('Unassigned')}
        </Text>
      </TouchableOpacity>
    );
  };

  const reviewerItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.reviewerItem}
        onPress={() => {
          dispatch({ type: 'toggle', payload: { user: item.user } });
        }}
      >
        {item.isSelected && (
          <Icon.check color={theme === 'light' ? Colors.dark : Colors.secondary} />
        )}

        <Image
          key={item.user.id}
          source={
            item.user.avatarUrl.includes('gravatar')
              ? { uri: item.user.avatarUrl }
              : {
                  uri: `${props.instanceUrl}${item.user.avatarUrl}`,
                }
          }
          style={styles.avatar}
        />

        <Text style={theme === 'light' ? s.lightText : s.darkText}>{item.user.username}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View testID={props.testID} style={styles.container}>
      <GAlert
        testID="errorAlert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={showErrorHandler}
      />

      <GAlert
        testID="warningAlert"
        visible={showWarning}
        variant="Danger"
        title="Error"
        message="No matching results"
        secondaryButtonText="Dismiss"
        secondaryAction={showWarningHandler}
      />

      <View>
        <TextInput
          multiline={true}
          style={
            theme === 'light'
              ? [s.lightContainer, s.lightBorderStyle, s.lightText, styles.commentFieldStyle]
              : [s.darkContainer, s.darkBorderStyle, s.darkText, styles.commentFieldStyle]
          }
          placeholder="Request review from ..."
          placeholderTextColor={theme === 'light' ? Colors.gray : Colors.secondary}
          onChangeText={(text) => onChangeText(text)}
        />
        <ScrollView horizontal={true}>
          <FlatList
            style={styles.list}
            data={users.slice(0, 10)}
            keyExtractor={(item) => item.user.id}
            ListHeaderComponent={() => unassignReviewers()}
            renderItem={reviewerItem}
            nestedScrollEnabled={true}
          />
        </ScrollView>
      </View>

      <View style={styles.rowDirection}>
        <View style={styles.btnContainer}>
          <GButton
            testID="addButton"
            disabled={users?.length > 0 ? false : true}
            category={
              users?.length > 0
                ? theme === 'light'
                  ? 'primary'
                  : 'darkPrimary'
                : theme === 'light'
                ? 'disabled'
                : 'darkDisabled'
            }
            click={() => EditReviewerHandler()}
            label="Add as Reviewer"
          />
        </View>

        <GButton
          testID="cancelButton"
          category={theme === 'light' ? 'secondary' : 'darkSecondary'}
          click={showInputHandler}
          label="Cancel"
        />
      </View>
    </View>
  );
};

EditReviewers.propTypes = {
  getMergeRequestsDetails: Proptypes.func,
  iid: Proptypes.string,
  projectId: Proptypes.number,
  refreshIssues: Proptypes.func,
  setShowTextInput: Proptypes.func,
  testID: Proptypes.string,
  members: Proptypes.array,
  instanceUrl: Proptypes.string,
  currentReviewers: Proptypes.array,
};

const styles = StyleSheet.create({
  avatar: {
    borderColor: Colors.border,
    borderRadius: 40,
    borderWidth: 0.3,
    height: 20,
    marginRight: 10,
    width: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
  btnContainer: {
    marginRight: 8,
  },
  commentFieldStyle: {
    borderRadius: 5,
    borderWidth: 1,
    flex: 0,
    fontSize: 16,
    marginTop: 10,
    opacity: 0.7,
    paddingHorizontal: 10,
  },
  container: {
    width: '100%',
  },
  list: {
    height: 100,
    marginBottom: 10,
    width: '100%',
  },
  reviewerItem: {
    alignItems: 'center',
    borderColor: Colors.lightGray,
    borderWidth: 0.5,
    flex: 1,
    flexDirection: 'row',
    height: 50,
    paddingLeft: 15,
    width: Dimensions.get('screen').width - 40,
  },
  rowDirection: {
    flexDirection: 'row',
  },
});

export default EditReviewers;
