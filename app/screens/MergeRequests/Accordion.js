import React, { useState, useContext } from 'react';
import { StyleSheet, View } from 'react-native';
import GButton from '~app/components/GButton';
import CodeBlock from '~app/components/CodeBlock';
import Icons from '~app/assets/Icons';
import s, { Colors } from '~app/constants/GlobalStyles';
import Proptypes from 'prop-types';
import { ThemeContext } from '~app/store/theme-context';

const Accordion = (props) => {
  const [toggleExpand, setToggleExpand] = useState(false);
  const { theme } = useContext(ThemeContext);

  return (
    <View
      testID={props.testID}
      style={
        theme === 'light'
          ? [s.lightBorderStyle, styles.accordion]
          : [s.darkBorderStyle, styles.accordion]
      }
    >
      <GButton
        testID="accordion"
        label={props.fileName}
        category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
        variant="confirm"
        flexDirection="row"
        size="large"
        click={() => setToggleExpand(!toggleExpand)}
      >
        <Icons.angleRight
          style={toggleExpand ? styles.angleDown : styles.angleRight}
          color={theme === 'light' ? Colors.primary : Colors.primary500}
          degree={toggleExpand ? '90deg' : '0deg'}
        />
      </GButton>

      {toggleExpand && (
        <View style={styles.codeContainer}>
          <CodeBlock code={props.code} />
        </View>
      )}
    </View>
  );
};

Accordion.propTypes = {
  code: Proptypes.string.isRequired,
  fileName: Proptypes.string.isRequired,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  accordion: {
    borderWidth: 1,
    marginBottom: 5,
    paddingHorizontal: 8,
    paddingVertical: 3,
  },
  angleDown: {
    paddingRight: 5,
    paddingTop: 10,
  },
  angleRight: {
    alignSelf: 'center',
    paddingRight: 10,
  },
  codeContainer: {
    borderColor: Colors.border,
    borderTopWidth: 1,
  },
});

export default Accordion;
