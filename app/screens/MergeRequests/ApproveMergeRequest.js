import React, { useState, useContext } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Proptypes from 'prop-types';
import s, { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import Icons from '~app/assets/Icons';

import { ThemeContext } from '~app/store/theme-context';
import { useTranslation } from 'react-i18next';

const ApproveMergeRequest = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);

  const approveMergeRequest = async () => {
    const response = await GitlabApi.approveMergeRequest(
      props.instanceUrl,
      props.projectId,
      props.iid,
      props.userToken,
    );

    if (response && response.state) {
      return props.setIsApproved({ status: response.user_has_approved });
    }

    setShowError(response.success === false);
  };

  const revokeApproval = async () => {
    const response = await GitlabApi.revokeApproval(
      props.instanceUrl,
      props.projectId,
      props.iid,
      props.userToken,
    );

    if (response && response.state) {
      return props.setIsApproved({ status: response.user_has_approved });
    }

    setShowError(response.success === false);
  };

  return (
    <View testID={props.testID}>
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowError(false)}
      />

      <View
        style={
          theme === 'light'
            ? [s.lightBorderStyle, styles.columnDirection]
            : [s.darkBorderStyle, styles.columnDirection]
        }
      >
        <View style={styles.rowDirection}>
          <Icons.approval
            color={theme === 'dark' ? Colors.lightGray : Colors.dark}
            size={25}
            style={styles.icon}
            testID="approval"
          />
          <GButton
            testID="approveBtn"
            category={theme === 'light' ? 'secondary' : 'darkSecondary'}
            variant={theme === 'dark' ? 'confirm' : ''}
            click={props.isApproved.status ? revokeApproval : approveMergeRequest}
            label={props.isApproved.status ? 'Revoke Approval' : 'Approve'}
            textAlign="left"
          />
        </View>

        {props.isApproved.users?.length > 0 && (
          <View style={styles.approvalContainer}>
            <Text
              style={
                theme === 'light' ? [s.lightText, styles.textStyle] : [s.darkText, styles.textStyle]
              }
            >
              {props.isApproved.status && props.isApproved.users?.length > 1
                ? t('Approved by you and others ')
                : props.isApproved.status && props.isApproved.users?.length === 1
                ? t('Approved by you ')
                : t('Approved by ')}
            </Text>

            <View style={styles.avatarContainer}>
              {props.isApproved.users?.map((user) => (
                <Image
                  key={user.id}
                  testID={`img${user.id}`}
                  source={
                    user.avatarUrl.includes('gravatar')
                      ? { uri: user.avatarUrl }
                      : { uri: `${props.instanceUrl}${user.avatarUrl}` }
                  }
                  style={styles.avatar}
                />
              ))}
            </View>
          </View>
        )}

        <Text
          style={
            theme === 'light'
              ? [s.lightText, s.lightBorderStyle, styles.actionTextStyle]
              : [s.darkText, s.darkBorderStyle, styles.actionTextStyle]
          }
        >
          {props.approvalsRequired > 0
            ? t(`Requires ${props.approvalsRequired} approval from eligible users.`)
            : t('Approval is optional')}
        </Text>
      </View>
    </View>
  );
};

ApproveMergeRequest.propTypes = {
  approvalsRequired: Proptypes.number,
  projectId: Proptypes.number,
  iid: Proptypes.string,
  instanceUrl: Proptypes.string,
  isApproved: Proptypes.object,
  setIsApproved: Proptypes.func,
  testID: Proptypes.string,
  userToken: Proptypes.string,
};

const styles = StyleSheet.create({
  actionTextStyle: {
    borderTopWidth: 1,
    fontSize: 16,
    marginTop: 10,
    paddingTop: 10,
  },
  approvalContainer: {
    flexDirection: 'column',
    marginTop: 10,
  },
  avatar: {
    borderRadius: 30,
    height: 30,
    marginHorizontal: 3,
    width: 30,
  },
  avatarContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  columnDirection: {
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 15,
    padding: 10,
  },
  icon: {
    alignSelf: 'center',
    paddingRight: 10,
  },
  rowDirection: {
    flexDirection: 'row',
  },
  textStyle: {
    fontSize: 16,
    paddingTop: 6,
  },
});

export default ApproveMergeRequest;
