import React, { useState, useEffect, useContext, useCallback, useMemo } from 'react';
import {
  Text,
  StyleSheet,
  View,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import NavigationButton from '~app/components/NavigationButton';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import OverviewHeader from '../Issues/OverviewHeader';
import OverviewBody from '../Issues/OverviewBody';
import Slider from '../Issues/Slider';
import Note from '../Issues/Note';
import GButton from '~app/components/GButton';
import Accordion from './Accordion';
import SendComment from '../Issues/SendComment';
import ApproveMergeRequest from './ApproveMergeRequest';
import AcceptMergeRequest from './AcceptMergeRequest';
import * as Utils from '~app/utils/Utils';
import Proptypes from 'prop-types';
import { ThemeContext } from '~app/store/theme-context';

const ReviewMergeRequestDetails = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const [user, setUser] = useState(null);
  const [showError, setShowError] = useState(false);
  const [details, setDetails] = useState(null);
  const [toggleSlider, setToggleSlider] = useState(false);
  const [displayNote, setDisplayNote] = useState([]);
  const [showChanges, setShowChanges] = useState(false);
  const [diffs, setDiffs] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isApproved, setIsApproved] = useState({
    status: undefined,
    users: [],
  });
  const [mergeStatus, setMergeStatus] = useState({
    mergeable: undefined,
    status: null,
  });
  const [refreshing, setRefreshing] = useState(false);
  const [members, setMembers] = useState([]);

  const getMergeRequestsDetails = useCallback(async () => {
    const userInfo = await UserStorage.getUserInfo();
    setUser(userInfo);

    const result = await GitlabApi.getMergeRequestsDetails(
      props.issue.id,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    const fullPath = result.project.fullPath;

    const memberResult = await GitlabApi.getProjectMembers(
      fullPath,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    setMembers(memberResult);

    if (result.author) {
      setDetails(result);
      return setIsApproved({
        status: Utils.checkIfApprovedByMe(result.approvedBy.nodes, userInfo.username),
        users: result.approvedBy.nodes,
      });
    }

    setShowError(result.success === false || memberResult.success === false);
  }, [props.issue.id]);

  useEffect(() => {
    getMergeRequestsDetails();
  }, [getMergeRequestsDetails]);

  const displayAllIssueNotes = useCallback(async () => {
    const userInfo = await UserStorage.getUserInfo();

    const result = await GitlabApi.getAllMergeRequestNotes(
      props.issue.id,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    if (Array.isArray(result)) {
      const sorted = Utils.groupBy(result, (el) => el?.discussion?.id);
      const filtered = Utils.flattenArr(sorted);
      const reversed = Utils.reverseArr(filtered);
      return setDisplayNote(reversed);
    }

    setShowError(result.success === false);
  }, [props.issue.id]);

  useEffect(() => {
    displayAllIssueNotes();

    return () => {
      setDisplayNote([]);
    };
  }, [displayAllIssueNotes]);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getMergeRequestsDetails();
    await displayAllIssueNotes();
    diffs?.length > 0 && (await getDiff());
    setRefreshing(false);
  }, [getMergeRequestsDetails, displayAllIssueNotes, diffs?.length, getDiff]);

  const handlePress = () => {
    props.onPress(null);
  };

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const handleSlidingDrawer = () => {
    setToggleSlider(!toggleSlider);
  };

  const getDiff = useCallback(async () => {
    setLoading(true);
    const response = await GitlabApi.getMergeRequestChanges(
      user?.instanceUrl,
      details?.projectId,
      details?.targetBranch,
      details?.sourceBranch,
      user?.userToken,
    );

    if (response && response.diffs) {
      setLoading(false);
      return setDiffs(response.diffs);
    }

    setShowError(response.success === false);
    setLoading(false);
  }, [
    user?.instanceUrl,
    details?.projectId,
    details?.targetBranch,
    details?.sourceBranch,
    user?.userToken,
  ]);

  const destroyNoteHandler = useCallback(
    async (noteid) => {
      const response = await GitlabApi.destroyNote(noteid, user.instanceUrl, user.userToken);

      if (response === 200) {
        return displayAllIssueNotes();
      }

      setShowError(response.success === false);
    },
    [displayAllIssueNotes, user?.instanceUrl, user?.userToken],
  );

  const closeMergeRequestHandler = async () => {
    const response = await GitlabApi.closeMergeRequest(
      user.instanceUrl,
      props.issue.project.fullPath,
      props.issue.iid,
      user.userToken,
    );

    if (response === 200) {
      props.refreshIssues();
      return handlePress();
    }

    setShowError(response.success === false);
  };

  const checkIfMergable = useCallback(async () => {
    const userInfo = await UserStorage.getUserInfo();

    const status = await GitlabApi.checkMergeableState(
      props.issue.id,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    if (!status) {
      return setShowError(true);
    }

    setMergeStatus({ mergeable: status.mergeable, status });
  }, [props.issue.id]);

  useEffect(() => {
    checkIfMergable();
  }, [checkIfMergable, mergeStatus]);

  const markAsReadyHandler = async () => {
    const response = await GitlabApi.mergeRequestToggleAsReady(
      props.issue.iid,
      mergeStatus?.status?.draft ? false : true,
      props.issue.project.fullPath,
      user?.instanceUrl,
      user?.userToken,
    );

    if (!response === 200) {
      setShowError(true);
    }
  };

  const overviewButtonHandler = function () {
    setShowChanges(false);
  };

  const changesButtonHandler = function () {
    setShowChanges(true);
    getDiff();
  };

  const memoDiffs = useMemo(
    () =>
      diffs?.map((diff, idx) => (
        <Accordion
          testID="diffs"
          key={idx + 1}
          fileName={diff?.renamed_file || diff?.deleted_file ? diff?.new_path : diff?.old_path}
          code={diff?.diff}
        />
      )),
    [diffs],
  );

  const memoNotes = useMemo(
    () =>
      displayNote?.map((el, idx, arr) => (
        <Note
          key={el.id}
          note={el}
          instanceUrl={user?.instanceUrl}
          destroyNoteHandler={destroyNoteHandler}
          displayAllIssueNotes={displayAllIssueNotes}
        />
      )),

    [displayNote, destroyNoteHandler, displayAllIssueNotes, user?.instanceUrl],
  );

  return (
    <SafeAreaView
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
    >
      <NavigationButton
        title="Merge Requests"
        subTitle={props.issue.reference}
        onPress={handlePress}
      />

      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />

      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => onRefresh()} />}
      >
        <OverviewHeader
          testID="MergeRequestsHeader"
          stateName="Open"
          handleSlidingDrawer={handleSlidingDrawer}
          createdAt={props.issue.createdAt}
          authorUsername={details?.author?.username}
          isDraft={mergeStatus?.status?.draft}
          markAsReadyBtnAction={markAsReadyHandler}
          overviewBtnAction={overviewButtonHandler}
          changesBtnAction={changesButtonHandler}
        />

        <View style={theme === 'light' ? s.lightLine : s.darkLine} />

        <View style={styles.content}>
          {showChanges ? (
            <>
              {loading ? (
                <ActivityIndicator
                  testID="spinner"
                  animating
                  size="large"
                  color={theme === 'light' ? Colors.darkGray2 : Colors.secondary}
                />
              ) : diffs?.length > 0 ? (
                memoDiffs
              ) : (
                <Text
                  style={
                    theme === 'light'
                      ? [s.lightText, styles.textStyle]
                      : [s.darkText, styles.textStyle]
                  }
                >
                  {t(`No changes between ${details?.sourceBranch} and ${details?.targetBranch}`)}
                </Text>
              )}
            </>
          ) : (
            <>
              <OverviewBody
                testID="MRDetailsBody"
                title={props.issue.title}
                description={details?.descriptionHtml}
              />

              <ApproveMergeRequest
                testID="approveMergeRequest"
                isApproved={isApproved}
                setIsApproved={setIsApproved}
                instanceUrl={user?.instanceUrl}
                userToken={user?.userToken}
                projectId={details?.projectId}
                iid={props.issue.iid}
                approvalsRequired={details?.approvalsRequired}
              />

              {diffs.length > 0 && (
                <AcceptMergeRequest
                  testID="acceptMergeRequest"
                  iid={props.issue.iid}
                  projectFullPath={props.issue.project.fullPath}
                  status={mergeStatus?.status}
                  refreshIssues={props.refreshIssues}
                  handlePress={handlePress}
                />
              )}

              {memoNotes}

              <SendComment
                testID="sendComment"
                issueId={props.issue.id}
                displayAllIssueNotes={displayAllIssueNotes}
              />

              <View style={styles.btnContainer}>
                <GButton
                  size="large"
                  click={closeMergeRequestHandler}
                  label="Close Merge Request"
                  textAlign="center"
                  category={theme === 'light' ? '' : 'darkSecondary'}
                />
              </View>
            </>
          )}
        </View>

        <Slider
          testID="slider"
          setVisible={toggleSlider}
          issue={props.issue}
          details={details}
          getMergeRequestsDetails={getMergeRequestsDetails}
          refreshIssues={props.refreshIssues}
          handleSlidingDrawer={handleSlidingDrawer}
          instanceUrl={user?.instanceUrl}
          members={members}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

ReviewMergeRequestDetails.propTypes = {
  issue: Proptypes.shape({
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
    }),
    createdAt: Proptypes.string.isRequired,
    id: Proptypes.string.isRequired,
    iid: Proptypes.string.isRequired,
    labels: Proptypes.shape({
      nodes: Proptypes.array,
    }),
    milestone: Proptypes.shape({
      title: Proptypes.string,
    }),
    project: Proptypes.shape({
      fullPath: Proptypes.string.isRequired,
    }),
    reference: Proptypes.string.isRequired,
    title: Proptypes.string.isRequired,
  }),
  onPress: Proptypes.func.isRequired,
  refreshIssues: Proptypes.func.isRequired,
};

const styles = StyleSheet.create({
  btnContainer: {
    marginVertical: 5,
  },
  content: {
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  screen: {
    flex: 1,
  },
  scrollContainer: {
    paddingBottom: 20,
  },
  textStyle: {
    fontSize: 16,
    marginTop: 10,
    textAlign: 'center',
  },
});

export default ReviewMergeRequestDetails;
