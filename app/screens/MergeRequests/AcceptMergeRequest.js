import React, { useState, useContext } from 'react';
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native';
import Proptypes from 'prop-types';
import s, { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import Icons from '~app/assets/Icons';

import { ThemeContext } from '~app/store/theme-context';
import { useTranslation } from 'react-i18next';

const AcceptMergeRequest = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);
  const [isMerging, setIsMerging] = useState(false);

  const mergeIssue = async () => {
    const strategy = 'strategy: MERGE_WHEN_PIPELINE_SUCCEEDS';

    setIsMerging(true);
    const user = await UserStorage.getUserInfo();

    const params = {
      iid: props.iid,
      projectPath: props.projectFullPath,
      diffHeadSha: props.status.diffHeadSha,
      instanceUrl: user.instanceUrl,
      userToken: user.userToken,
      strategy: !props.status.mergeable ? strategy : '',
    };

    const response = await GitlabApi.acceptMergeRequest(params);

    if (response === 200) {
      setIsMerging(false);
      props.refreshIssues();
      return props.handlePress();
    }

    setShowError(response.success === false);
    setIsMerging(false);
  };

  const isMergable =
    !props.status?.conflicts &&
    props.status?.mergeableDiscussionsState &&
    !props.status?.draft &&
    props.status.mergeable &&
    props.status?.userPermissions.canMerge;

  const isMergableWhenPipelineSucceeds =
    !props.status?.conflicts &&
    props.status?.mergeableDiscussionsState &&
    !props.status?.draft &&
    !props.status.mergeable &&
    props.status?.userPermissions.canMerge;

  return (
    <View testID={props.testID}>
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowError(false)}
      />

      <View
        style={
          theme === 'light'
            ? [s.lightBorderStyle, styles.columnDirection]
            : [styles.columnDirection, s.darkBorderStyle]
        }
      >
        <View style={styles.actionContainer}>
          {isMerging ? (
            <ActivityIndicator
              testID="mergeSpinner"
              animating
              size="small"
              color={theme === 'light' ? Colors.darkGray2 : Colors.secondary}
            />
          ) : props.status?.mergeable ? (
            <Icons.statusSuccess
              size={30}
              style={styles.icon}
              color={Colors.green}
              backgroundColor={theme === 'light' ? Colors.lightGray : Colors.dark}
              testID="statusSuccess"
            />
          ) : (
            <Icons.statusWarning
              size={28}
              style={styles.icon}
              color={theme === 'light' ? Colors.dark : Colors.secondary}
              backgroundColor={theme === 'light' ? Colors.lightGray : Colors.dark}
              testID="statusWarning"
            />
          )}

          {!isMerging ? (
            <GButton
              testID="mergeBtn"
              disabled={isMergable || isMergableWhenPipelineSucceeds ? false : true}
              category={
                isMergable || isMergableWhenPipelineSucceeds
                  ? theme === 'light'
                    ? 'primary'
                    : 'darkPrimary'
                  : theme === 'light'
                  ? 'disabled'
                  : 'darkDisabled'
              }
              click={mergeIssue}
              label={isMergableWhenPipelineSucceeds ? 'Merge when pipeline succeeds' : 'Merge'}
              textAlign="left"
            />
          ) : (
            <Text
              style={
                theme === 'light'
                  ? [s.lightText, styles.mergeTextStyle]
                  : [s.darkText, styles.mergeTextStyle]
              }
            >
              {isMergableWhenPipelineSucceeds
                ? t('Set to be merged automatically when the pipeline succeeds')
                : t('Merging! Take a deep breath and relax...')}
            </Text>
          )}
        </View>

        {!isMerging && (
          <Text
            style={
              theme === 'light'
                ? [s.lightText, s.lightBorderStyle, styles.actionTextStyle]
                : [s.darkText, s.darkBorderStyle, styles.actionTextStyle]
            }
          >
            {props.status?.conflicts
              ? t(
                  'Merge blocked: merge conflicts must be resolved. Users who can write to the source or target branches can resolve the conflicts.',
                )
              : !props.status?.conflicts && props.status?.draft
              ? t(
                  'Merge blocked: merge request must be marked as ready. It is still marked as draft.',
                )
              : !props.status?.conflicts && !props.status?.mergeableDiscussionsState
              ? t('Merge blocked: all threads must be resolved.')
              : !props.status?.mergeable
              ? t(
                  'Merge blocked: pipeline must succeed. Push a commit that fixes the failure, or learn about other solutions.',
                )
              : t(
                  'Ready to be merged automatically. Ask someone with write access to this repository to merge this request',
                )}
          </Text>
        )}
      </View>
    </View>
  );
};

AcceptMergeRequest.propTypes = {
  projectId: Proptypes.number,
  iid: Proptypes.string,
  projectFullPath: Proptypes.string,
  status: Proptypes.object,
  refreshIssues: Proptypes.func,
  handlePress: Proptypes.func,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  actionContainer: {
    flexDirection: 'row',
    padding: 5,
  },
  actionTextStyle: {
    borderTopWidth: 1,
    fontSize: 16,
    marginTop: 10,
    paddingTop: 10,
  },
  columnDirection: {
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 15,
    padding: 10,
  },
  icon: {
    alignSelf: 'center',
    paddingRight: 10,
  },
  mergeTextStyle: {
    fontSize: 16,
    paddingHorizontal: 10,
  },
});

export default AcceptMergeRequest;
