import React, { useState, useEffect, useCallback } from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import IssueItem from '../Issues/IssueItem';
import RefreshList from '~app/components/RefreshList';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import MRDetails from './MergeRequestDetails';
import Layout from '../Issues/Layout';

const MergeRequestsScreen = () => {
  const [issues, setIssues] = useState([]);
  const [showError, setShowError] = useState(false);
  const [issueId, setIssueId] = useState(null);
  const [currentIssueDetails, setCurrentIssueDetails] = useState(null);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await fetchMergeRequests();
    setRefreshing(false);
  }, []);

  const fetchMergeRequests = async () => {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.assignedMergeRequests(user.instanceUrl, user.userToken);

    if (Array.isArray(result)) {
      setIssues(result);
      setLoading(false);
    }

    setShowError(result.success === false);
    setLoading(false);
  };

  useEffect(() => {
    fetchMergeRequests();
  }, []);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const setCurrentIssue = function (issueID) {
    setIssueId(issueID);
    if (issueID) {
      const [issueDetails] = issues.filter((item) => item.id === issueID);
      setCurrentIssueDetails(issueDetails);
    }
  };

  return (
    <>
      {issueId ? (
        <MRDetails
          issue={currentIssueDetails}
          onPress={setCurrentIssue}
          refreshIssues={fetchMergeRequests}
        />
      ) : (
        <Layout title="Merge Requests">
          <GAlert
            visible={showError}
            variant="Danger"
            title="Error"
            message={`Connection failed.\nPlease retry later.`}
            secondaryButtonText="Dismiss"
            secondaryAction={displayErrorHandler}
          />

          {loading ? (
            <LoadingSpinner />
          ) : issues.length > 0 ? (
            <RefreshList
              refresh={() => fetchMergeRequests()}
              testID="MR-List"
              data={issues}
              keyExtractor={(item) => item.id}
              renderItem={({ item }) => {
                return <IssueItem issue={item} onPress={setCurrentIssue} />;
              }}
            />
          ) : (
            <EmptyScreen
              refreshing={refreshing}
              onRefresh={onRefresh}
              title="There is nothing here"
              text="There are no merge requests currently assigned to you."
            />
          )}
        </Layout>
      )}
    </>
  );
};

export default MergeRequestsScreen;
