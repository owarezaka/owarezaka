import React, { useContext, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';

import PreferencesStorage from '~app/lib/auth/PreferencesStorage';
import Proptypes from 'prop-types';

const HomeScreen = (props) => {
  const { theme, setTheme } = useContext(ThemeContext);

  useEffect(() => {
    const getPrefs = async () => {
      const prefs = await PreferencesStorage.getPreferences();
      if (prefs) {
        setTheme(prefs.darkMode);
      }
    };
    getPrefs();
  }, [setTheme]);

  return (
    <View
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
    >
      <View style={styles.menuItem}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label="Projects"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('Projects')}
        >
          <Icons.project
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
            size={20}
          />
        </GButton>
      </View>

      <View style={styles.menuItem}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label="To-Do List"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('ToDoList')}
        >
          <Icons.todoDone
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
          />
        </GButton>
      </View>

      <View style={styles.menuItem}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label="Groups"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('Groups')}
        >
          <Icons.group
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
          />
        </GButton>
      </View>

      <View style={styles.menuItem}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label="Snippets"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('Snippets')}
        >
          <Icons.snippet
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
          />
        </GButton>
      </View>

      <View style={styles.menuItem}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label="Activity"
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('Activity')}
        >
          <Icons.history
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
          />
        </GButton>
      </View>
      <View style={styles.help}>
        <GButton
          flexDirection="row"
          textAlign="left"
          size="large"
          label=""
          category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
          variant={theme === 'light' ? 'confirm' : ''}
          click={() => props.navigation.navigate('Help')}
        >
          <Icons.question
            style={styles.icon}
            color={theme === 'light' ? Colors.primary : Colors.secondary}
            size={24}
          />
        </GButton>
      </View>
    </View>
  );
};

HomeScreen.propTypes = {
  navigation: Proptypes.object,
};

export default HomeScreen;

const styles = StyleSheet.create({
  help: {
    position: 'absolute',
    right: -10,
    top: 15,
  },
  icon: {
    paddingHorizontal: 15,
  },
  menuItem: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: 2,
  },
  screen: {
    flex: 1,
    paddingTop: 15,
  },
});
