import React, { useContext } from 'react';
import { View, Text, StyleSheet, ScrollView, Linking } from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import Gitlab from '~app/constants/Gitlab';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';

const HelpScreen = () => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);

  return (
    <ScrollView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
      <View style={styles.content}>
        <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
          {t('Owarezaka GitLab Client')}
        </Text>
        <View style={styles.line} />
        <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
          {t('Owarezaka is a mobile client for GitLab for IOS and Android mobile phones.')}
        </Text>
        <Text
          style={
            theme === 'light'
              ? [s.lightText, styles.text, styles.bold]
              : [s.darkText, styles.text, styles.bold]
          }
        >
          {t('GitLab is an open source devops platform')}
        </Text>

        <View style={styles.btnContainer}>
          <GButton
            testID="readMore"
            label="Read More About GitLab"
            click={() => Linking.openURL(Gitlab.about)}
            size="large"
            category={theme === 'light' ? 'primary' : 'darkPrimary'}
          />
        </View>

        <View style={styles.btnContainer}>
          <GButton
            testID="doc"
            label="GitLab Documentation"
            click={() => Linking.openURL(Gitlab.docs)}
            size="large"
            category={theme === 'light' ? 'primary' : 'darkPrimary'}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  bold: {
    fontWeight: '600',
  },
  btnContainer: {
    marginTop: 8,
  },
  content: {
    flex: 1,
    marginHorizontal: '5%',
    paddingBottom: 100,
    width: '90%',
  },
  line: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    color: Colors.dark,
    marginTop: 15,
  },
  text: {
    fontSize: 16,
    lineHeight: 25,
    paddingVertical: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    paddingVertical: 20,
  },
});

export default HelpScreen;
