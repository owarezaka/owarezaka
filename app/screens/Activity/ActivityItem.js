import React, { useContext } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import * as Utils from '~app/utils/Utils';
import Icons from '~app/assets/Icons';

import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const ActivityItem = (props) => {
  const { theme } = useContext(ThemeContext);

  const renderIcon = function (actionName) {
    const iconStyle = {
      transform: [{ translateY: 2.4 }],
    };

    switch (actionName) {
      case 'opened':
        return <Icons.statusOpen style={iconStyle} testID="statusOpen" />;
      case 'accepted':
        return <Icons.fork style={iconStyle} testID="fork" />;
      case 'commented on':
        return <Icons.comment style={iconStyle} testID="comment" />;
      case 'pushed to':
        return <Icons.commit style={iconStyle} testID="commit" />;
      case 'pushed new':
        return <Icons.commit style={iconStyle} testID="commit" />;
      case 'closed':
        return (
          <Icons.checkCircle
            color={theme === 'light' ? Colors.secondary : Colors.dark}
            style={iconStyle}
            testID="checkCircle"
          />
        );
      case 'deleted':
        return <Icons.remove style={iconStyle} testID="remove" />;
      default:
        return <></>;
    }
  };

  return (
    <View style={styles.item}>
      <Image
        testID="image"
        source={{ uri: props.activity.author.avatar_url }}
        style={styles.avatar}
      />

      <View style={styles.content}>
        <Text>
          <Text style={theme === 'light' ? [s.lightText, styles.bold] : [s.darkText, styles.bold]}>
            {props.activity.author.name}{' '}
          </Text>
          <Text
            style={
              theme === 'light' ? [s.lightText, styles.username] : [s.darkText, styles.username]
            }
          >
            {props.activity.author.username}
          </Text>
          {'\n'}
          {renderIcon(props.activity.action_name)}{' '}
          <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
            {' '}
            {props.activity.action_name}
          </Text>
          {props.activity.push_data?.ref_type && (
            <Text
              style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
            >
              {' '}
              {props.activity.push_data.ref_type} {props.activity.push_data?.ref}
            </Text>
          )}
          {props.activity.action_name === 'closed' ||
          props.activity.action_name === 'opened' ||
          props.activity.action_name === 'approved' ||
          props.activity.action_name === 'accepted' ? (
            <Text
              style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
            >
              {' '}
              {props.activity.target_type === 'MergeRequest'
                ? props.activity.target_type.replace('R', ' r').toLowerCase()
                : props.activity.target_type.toLowerCase()}
              <Text style={styles.link}>{` #${props.activity.target_iid}`}</Text>{' '}
              {props.activity.target_title}
            </Text>
          ) : null}
          {props.activity.action_name === 'commented on' && (
            <Text
              style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
            >
              {' '}
              {props.activity.note.noteable_type === 'MergeRequest'
                ? props.activity.note.noteable_type.replace('R', ' r').toLowerCase()
                : props.activity.note.noteable_type.toLowerCase()}{' '}
              <Text style={styles.link}>{`#${props.activity.note.noteable_iid}`}</Text>{' '}
              {props.activity.target_title}
            </Text>
          )}
          {props.activity.action_name === 'commented on' && (
            <Text
              style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
            >
              {' '}
              {props.activity.note.body}
            </Text>
          )}
          {props.activity.action_name === 'pushed to' && (
            <Text
              style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
            >
              {' '}
              {props.activity.push_data?.commit_title}
            </Text>
          )}{' '}
          <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
            {Utils.timeSince(props.activity.created_at)}
          </Text>
        </Text>
      </View>
    </View>
  );
};

ActivityItem.propTypes = {
  activity: Proptypes.shape({
    author: Proptypes.shape({
      avatar_url: Proptypes.string,
      name: Proptypes.string.isRequired,
      username: Proptypes.string.isRequired,
    }),
    action_name: Proptypes.string.isRequired,
    created_at: Proptypes.string.isRequired,
    target_title: Proptypes.string,
    target_type: Proptypes.string,
    target_iid: Proptypes.number,
    note: Proptypes.shape({
      body: Proptypes.string,
      noteable_type: Proptypes.string,
      noteable_iid: Proptypes.number,
    }),
    push_data: Proptypes.shape({
      commit_title: Proptypes.string,
      ref: Proptypes.string.isRequired,
      ref_type: Proptypes.string.isRequired,
    }),
  }),
};

const styles = StyleSheet.create({
  avatar: {
    borderRadius: 30,
    height: 30,
    marginRight: 10,
    width: 30,
  },
  bold: {
    fontSize: 16,
    fontWeight: '800',
  },
  content: {
    marginBottom: 10,
    width: '80%',
  },
  item: {
    borderBottomWidth: 0.25,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    padding: 10,
  },
  link: {
    color: Colors.primary,
    fontWeight: 'bold',
  },
  username: {
    color: Colors.darkGray,
  },
});

export default ActivityItem;
