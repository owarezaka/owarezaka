/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext, useCallback } from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  RefreshControl,
  FlatList,
  SafeAreaView,
} from 'react-native';
import { ThemeContext } from '~app/store/theme-context';

import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import ActivityItem from './ActivityItem';
import s, { Colors } from '~app/constants/GlobalStyles';

const ActivityScreen = () => {
  const { theme } = useContext(ThemeContext);

  const [activities, setActivities] = useState([]);
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [pageNum, setPageNum] = useState(1);
  const [maxItemsReached, setMaxItemsReached] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isFetching, setIsFetching] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getAllActivities(1);
    setPageNum(1);
    setMaxItemsReached(false);
    setRefreshing(false);
  }, []);

  async function getAllActivities(page) {
    setIsFetching(true);
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.getAllActivities(user.instanceUrl, user.userToken, page);

    if (Array.isArray(result)) {
      setLoading(false);

      if (result.length < 5) {
        setMaxItemsReached(true);
      }

      page === 1
        ? setActivities(result)
        : setActivities((prevResult) => [...prevResult, ...result]);

      setIsFetching(false);
    }

    setShowError(result.success === false);
    setLoading(false);
    setIsFetching(false);
  }

  useEffect(() => {
    getAllActivities(pageNum);

    return () => {
      setActivities([]);
    };
  }, []);

  const fetchMore = () => {
    if (isFetching) return;

    if (pageNum <= 3) {
      getAllActivities(pageNum + 1);
      setPageNum((prevNum) => prevNum + 1);
    }

    if (pageNum === 4) {
      setMaxItemsReached(true);
    }
  };

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const footerIndicator = () => {
    if (maxItemsReached) {
      return (
        <View style={styles.seperator}>
          <View style={styles.circle} />
        </View>
      );
    }

    return (
      <ActivityIndicator
        animating
        size="large"
        color={Colors.darkGray2}
        style={styles.loaderStyle}
      />
    );
  };

  const keyExtractor = (item) => item.id;
  const renderItem = ({ item }) => {
    return <ActivityItem activity={item} />;
  };

  return (
    <>
      {loading ? (
        <LoadingSpinner />
      ) : showError ? (
        <View style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <GAlert
            visible={showError}
            variant="Danger"
            title="Error"
            message={`Connection failed.\nPlease retry later.`}
            secondaryButtonText="Dismiss"
            secondaryAction={displayErrorHandler}
          />
        </View>
      ) : activities.length > 0 ? (
        <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <FlatList
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
            style={styles.container}
            onEndReached={fetchMore}
            onEndReachedThreshold={0.5}
            ListFooterComponent={footerIndicator}
            testID="Activity-List"
            data={activities}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />
        </SafeAreaView>
      ) : (
        <EmptyScreen
          refreshing={refreshing}
          onRefresh={onRefresh}
          title="There is nothing here"
          text="Check back later to see when anything involving you happens!"
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  circle: {
    backgroundColor: Colors.secondary,
    borderRadius: 5,
    borderWidth: 0.7,
    bottom: -5,
    height: 10,
    left: '50%',
    marginLeft: -5,
    position: 'absolute',
    width: 10,
  },
  container: {
    marginHorizontal: 20,
    marginTop: 10,
  },
  loaderStyle: {
    padding: 20,
  },
  seperator: {
    flexDirection: 'row',
    marginBottom: 20,
  },
});

export default ActivityScreen;
