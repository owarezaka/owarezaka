import React, { useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';
import { useTranslation } from 'react-i18next';
import { ThemeContext } from '~app/store/theme-context';
import s, { Colors } from '~app/constants/GlobalStyles';

import PreferencesStorage from '~app/lib/auth/PreferencesStorage';
import NavigationButton from '~app/components/NavigationButton';
import Proptypes from 'prop-types';

const SettingsScreen = (props) => {
  const [toggle, setToggle] = useState(false);
  const { theme, setTheme } = useContext(ThemeContext);

  const { t } = useTranslation();

  const handleThemeChange = () => {
    setToggle((previousState) => !previousState);
    setTheme(theme === 'light' ? 'dark' : 'light');
    PreferencesStorage.setPreferences(theme === 'light' ? 'dark' : 'light');
  };

  useEffect(() => {
    setToggle(theme === 'dark');
  }, [theme]);

  return (
    <>
      <NavigationButton title="Settings" onPress={() => props.navigation.goBack()} />
      <View
        style={
          theme === 'light'
            ? [s.lightContainer, styles.container]
            : [s.darkContainer, styles.container]
        }
      >
        <View style={styles.subContainer}>
          <Text
            style={
              theme === 'light' ? [s.lightText, styles.textStyle] : [s.darkText, styles.textStyle]
            }
            testID="dark-mode"
          >
            {t('Dark Mode')}
          </Text>
          <Switch
            trackColor={{ false: Colors.dark, true: Colors.secondary }}
            thumbColor={toggle ? Colors.lightGray3 : Colors.darkGray}
            onValueChange={handleThemeChange}
            value={toggle}
            testID="switch"
          />
        </View>
      </View>
    </>
  );
};

export default SettingsScreen;

SettingsScreen.propTypes = {
  navigation: Proptypes.object,
};

const styles = StyleSheet.create({
  container: {
    padding: '5%',
  },
  subContainer: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  textStyle: {
    fontSize: 16,
  },
});
