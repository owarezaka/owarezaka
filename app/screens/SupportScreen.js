import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { useTranslation } from 'react-i18next';
import Link from '~app/components/Link';
import { Colors } from '~app/constants/GlobalStyles';
import Gitlab from '~app/constants/Gitlab';

const SupportScreen = () => {
  const { t } = useTranslation();

  return (
    <ScrollView>
      <View style={styles.content}>
        <Text style={styles.title}>{t('Help Topics')}</Text>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Account Support')}</Text>
          <Text style={styles.text}>
            {t(
              "If you haven't received your confirmation email, you can request to resend your confirmation instructions via our",
            )}
            {'\b'}
            <Link name="confirmation page" url={Gitlab.confirmation} />
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Contributing')}</Text>
          <Text style={styles.text}>
            {t('Our development documentation describes how to submit')}
            {'\b'}
            <Link name="merge requests" url={Gitlab.mergeRequests} />
            <Text style={styles.text}>
              {'\b'}
              {t('and')}
              {'\b'}
            </Text>
            <Link name="issues." url={Gitlab.issues} />
            <Text style={styles.text}>
              {'\b'}
              {t(
                'Merge requests and issues not in line with the guidelines in these documents will be closed.',
              )}
              {'\b'}
            </Text>
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Feature Proposals')}</Text>
          <Text style={styles.text}>
            {t('Feature proposals should be submitted to the')}
            {'\b'}
            <Link name="issue tracker." url={Gitlab.issueTracker} />
            {'\n'}
            <Text style={styles.text}>
              {t('Please read the')}
              {'\b'}
            </Text>
            <Link name="contributing guidelines" url={Gitlab.issueWorkflow} />
            <Text style={styles.text}>
              {'\b'}
              {t(
                'for feature proposals before posting on the Issue tracker and make use of the "Feature Proposal" issue template.',
              )}
              {'\b'}
            </Text>
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('References')}</Text>

          <View style={styles.list}>
            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab Documentation:" url={Gitlab.docs} />
                {'\b'}
                {t('documents all GitLab applications.')}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab Forum:" url={Gitlab.forum} />
                {'\b'}
                {t('get help directly from the community.')}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab University:" url={Gitlab.university} />
                {'\b'}
                {t('contains a variety of resources for learning Git and GitLab.')}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="The GitLab Cookbook:" url={Gitlab.cookbook} />
                {'\b'}
                {t(
                  'written by core team member Jeroen van Baarsen, it is the most comprehensive book about GitLab.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab Recipes:" url={Gitlab.recipes} />
                {'\b'}
                {t(
                  'A range of useful unofficial guides to using non-packaged software in conjunction with GitLab.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="Learn Enough" url={Gitlab.learnEnough} />
                {'\b'}
                {t(
                  'Git to Be Dangerous by Michael Hartl: is a great introduction to version control and git.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                {t('Version two of the')}
                {'\b'}
                <Link name="Pro Git book" url={Gitlab.gitBook} />
                {'\b'}
                {t('has a')}
                {'\b'}
                <Link name="section about GitLab." url={Gitlab.gitlabOnGit} />
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                {t("O'Reilly Media 'Git for teams'")}
                {'\b'}
                <Link name="book" url={Gitlab.gitForTeams} />
                {'\b'}
                {t('has a chapter on GitLab. There are also')}
                {'\b'}
                <Link name="videos" url={Gitlab.collaboratingWithGit} />
                {'\b'}
                {t(
                  'about Git and GitLab. They also provide a free video about creating a GitLab account',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab YouTube Channel:" url={Gitlab.gitlabYoutubeChannel} />
                {'\b'}
                {t('the place where you can see videos of features and installation options.')}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Licensing and Subscriptions')}</Text>
          <Text style={styles.text}>
            <Link name="FAQ" url={Gitlab.licensingFaq} />
            {'\b'}
            <Text style={styles.text}>
              {t('about purchasing, renewals, payment and billable users')}
            </Text>
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Reproducible Bugs')}</Text>
          <Text style={styles.text}>
            {t('Bug reports should be submitted to the')}
            {'\b'}
            <Link name="issue tracker." url={Gitlab.issueTracker} />
            {'\n'}
            <Text style={styles.text}>
              {t('Please read the')}
              {'\b'}
            </Text>
            <Link name="contributing guidelines" url={Gitlab.issueWorkflow} />
            <Text style={styles.text}>
              {'\b'}
              {t(
                'for reporting bugs before posting on the Issue tracker and make use of the "Bug" issue template.',
              )}
              {'\b'}
            </Text>
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Other resources for discussion')}</Text>

          <View style={styles.list}>
            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="#gitlab Libera IRC channel:" url={Gitlab.liberaChat} />
                {'\b'}
                {t(
                  'a Libera channel to get in touch with other GitLab users and get help. It is managed by James Newton (newton) and Drew Blessing (dblessing).',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab Community Forum:" url={Gitlab.forum} />
                {'\b'}
                {t('this is the best place to have a discussion.')}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="Mailing list:" url={Gitlab.gitlabGroup} />
                {'\b'}
                {t(
                  "Please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it.",
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="Gitter chat room:" url={Gitlab.gitter} />
                {'\b'}
                {t('here you can ask questions when you need help.')}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Security')}</Text>

          <View style={styles.list}>
            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="The responsible disclosure page" url={Gitlab.disclosure} />
                {'\b'}
                {t(
                  'describes how to contact GitLab to report security vulnerabilities and other security information.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="The security section" url={Gitlab.docsSecurity} />
                {'\b'}
                {t(
                  'in the documentation lists what you can do to further secure your GitLab instance.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="The Trust &amp; Safety page" url={Gitlab.docsTrustAndSafety} />
                {'\b'}
                {t(
                  'describes how to contact GitLab to report abuse on the platform, including phishing pages, malware, and DMCA requests.',
                )}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="The GitLab Trust Center" url={Gitlab.security} />
                {'\b'}
                {t(
                  'provides our security and privacy FAQs, as well as how to contact our security team.',
                )}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Technical Support')}</Text>
          <Text style={styles.text}>
            {t(
              'For details on where to get paid Technical Support, and with what level of service, please see the',
            )}
            {'\b'}
            <Link name="Support page." url={Gitlab.support} />
          </Text>
        </View>

        <View style={styles.sections}>
          <Text style={styles.subTitle}>{t('Updating')}</Text>

          <View style={styles.list}>
            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="GitLab update page:" url={Gitlab.update} />
                {'\b'}
                {t('resources and information to help you update your GitLab instance.')}
              </Text>
            </View>

            <View style={styles.listStyle}>
              <Text style={styles.text}>
                <Text style={styles.mark}>
                  {'\u2022'}
                  {'\b'}
                </Text>
                <Link name="Maintenance policy:" url={Gitlab.maintenance} />
                {'\b'}
                {t('specifies what versions are supported.')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    marginHorizontal: '5%',
    width: '90%',
  },
  list: {
    marginBottom: 25,
  },
  listStyle: {
    flexDirection: 'row',
    marginBottom: -30,
  },
  mark: {
    fontSize: 22,
  },
  sections: {
    borderTopColor: Colors.border,
    borderTopWidth: 0.3,
    color: Colors.dark,
  },
  subTitle: {
    color: Colors.dark,
    fontSize: 20,
    fontWeight: '500',
    paddingTop: 20,
  },
  text: {
    color: Colors.dark,
    fontSize: 16,
    lineHeight: 25,
    paddingVertical: 20,
  },
  title: {
    color: Colors.dark,
    fontSize: 28,
    fontWeight: '600',
    paddingVertical: 20,
    textAlign: 'center',
  },
});

export default SupportScreen;
