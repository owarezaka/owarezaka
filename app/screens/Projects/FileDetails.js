import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import NavigationButton from '~app/components/NavigationButton';
import Proptypes from 'prop-types';
import s from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';
import CodeBlock from '~app/components/CodeBlock';
import GAlert from '~app/components/GAlert';
import LoadingSpinner from '~app/components/LoadingSpinner';
import Markdown from '~app/components/Markdown';
import UserStorage from '~app/lib/auth/UserStorage';

const FileDetails = (props) => {
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);
  const [instanceUrl, setInstanceUrl] = useState('');

  const code = props.content;
  const size = props.size;
  const language = props.language;

  useEffect(() => {
    const getInstanceUrl = async () => {
      setInstanceUrl(await UserStorage.getUserInfo().instanceUrl);
    };
    getInstanceUrl();
    setShowError(props.showError);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <NavigationButton
        title="Project Repository"
        subTitle={props.fileName}
        onPress={() => props.onPress(null)}
      />
      <ScrollView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
        <GAlert
          testID="alert"
          visible={showError}
          variant="Danger"
          title="Error"
          message={`Connection failed.\nPlease retry later.`}
          secondaryButtonText="Dismiss"
          secondaryAction={() => setShowError(false)}
        />
        {code === undefined || size === -1 ? (
          <LoadingSpinner />
        ) : code === null ? (
          <GAlert
            variant="Warning"
            visible={true}
            title="Invalid File"
            message="Only text documents are supported."
            secondaryButtonText="Go Back"
            secondaryAction={() => props.onPress(null)}
          />
        ) : size > 100000 ? (
          <GAlert
            variant="Danger"
            visible={true}
            title="Error"
            message="File too large!"
            secondaryButtonText="Go Back"
            secondaryAction={() => props.onPress(null)}
          />
        ) : (
          <View style={styles.code}>
            {language === 'markdown' ? (
              <Markdown body={code} imagePrefix={`${instanceUrl}/${props.fullPath}`} />
            ) : (
              <CodeBlock testID="code" code={code} language={language} />
            )}
          </View>
        )}
      </ScrollView>
    </>
  );
};

FileDetails.propTypes = {
  onPress: Proptypes.func.isRequired,
  fileName: Proptypes.string,
  content: Proptypes.string,
  size: Proptypes.number.isRequired,
  showRepo: Proptypes.bool,
  showError: Proptypes.bool,
  language: Proptypes.string,
  fullPath: Proptypes.string,
};

const styles = StyleSheet.create({
  code: {
    padding: 5,
  },
});

export default FileDetails;
