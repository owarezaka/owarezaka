import React, { useContext, useState, useEffect, useCallback } from 'react';
import { Text, StyleSheet, View, ScrollView, SafeAreaView } from 'react-native';
import Proptypes from 'prop-types';

import NavigationButton from '~app/components/NavigationButton';
import { ThemeContext } from '~app/store/theme-context';
import s, { Colors } from 'app/constants/GlobalStyles';
import GAlert from '~app/components/GAlert';
import Markdown from '~app/components/Markdown';
import Icons from 'app/assets/Icons';
import UserStorage from 'app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import ProjectRepo from 'app/screens/Projects/ProjectRepo';
import GButton from '~app/components/GButton';
import OverviewHeader from '../Groups/OverviewHeader';
import LoadingSpinner from '~app/components/LoadingSpinner';

const ProjectDetails = (props) => {
  const { theme } = useContext(ThemeContext);

  const [instanceUrl, setInstanceUrl] = useState('');
  const [showError, setShowError] = useState(false);
  const [readMe, setReadMe] = useState('');
  const [showRepo, setShowRepo] = useState(false);
  const [content, setContent] = useState({
    empty: undefined,
    button: undefined,
  });
  const [loading, setLoading] = useState(true);

  const getReadMe = useCallback(async () => {
    setLoading(true);
    const user = await UserStorage.getUserInfo();
    setInstanceUrl(user.instanceUrl);

    const projectPath = props.project.fullPath;

    const result = await GitlabApi.getReadMe(projectPath, user.instanceUrl, user.userToken);
    setLoading(false);

    if (Array.isArray(result)) {
      return setReadMe(result[0]);
    }

    setShowError(result.success === false);
  }, [props.project.fullPath]);

  useEffect(() => {
    getReadMe();
  }, [getReadMe]);

  const getProjectInfo = useCallback(async () => {
    setLoading(true);
    const user = await UserStorage.getUserInfo();
    const projectPath = props.project.fullPath;

    const fileInfo = await GitlabApi.getFileInfo(projectPath, '', user.instanceUrl, user.userToken);
    setLoading(false);

    if (!fileInfo) {
      return setContent({ empty: true });
    }

    if (fileInfo.file) {
      return setContent({ button: true });
    }

    setShowError(fileInfo.success === false);
  }, [props.project.fullPath]);

  useEffect(() => {
    getProjectInfo();
  }, [getProjectInfo]);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const handlePress = () => {
    props.onPress(null);
  };

  const displayRepoHandler = function () {
    setShowRepo(true);
  };

  function setImageSourceHandler() {
    if (props.project?.avatarUrl) {
      if (props.project.visibility === 'private') {
        if (theme === 'light') {
          return require('~app/assets/projectAvatarDefault.png');
        }

        if (theme === 'dark') {
          return require('~app/assets/projectAvatarDark.png');
        }
      }

      return { uri: props.project.avatarUrl };
    }

    if (!props.project?.avatarUrl && theme === 'light') {
      return require('~app/assets/projectAvatarDefault.png');
    }

    if (!props.project?.avatarUrl && theme === 'dark') {
      return require('~app/assets/projectAvatarDark.png');
    }
  }
  const source = setImageSourceHandler();

  return (
    <>
      {showRepo ? (
        <ProjectRepo onPress={() => setShowRepo(false)} path={props.project.fullPath} />
      ) : (
        <SafeAreaView
          style={
            theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
          }
        >
          <NavigationButton title="Projects" subTitle={props.project.name} onPress={handlePress} />
          <GAlert
            testID="alert"
            visible={showError}
            variant="Danger"
            title="Error"
            message={`Connection failed.\nPlease retry later.`}
            secondaryButtonText="Dismiss"
            secondaryAction={displayErrorHandler}
          />
          <ScrollView contentContainerStyle={styles.contentContainer}>
            <OverviewHeader
              testID="overviewHeader"
              fullName={props.project.name}
              visibility={props.project.visibility}
              source={source}
            />
            <View style={theme === 'light' ? s.lightLine : s.darkLine} />

            {loading ? (
              <LoadingSpinner />
            ) : (
              <>
                {Boolean(props.project.description) && (
                  <Text
                    style={
                      theme === 'light'
                        ? [s.lightText, styles.description]
                        : [s.darkText, styles.description]
                    }
                  >
                    {props.project.description}
                  </Text>
                )}
                {content.empty && (
                  <Text style={[theme === 'light' ? s.lightTitle : s.darkTitle, styles.emptyText]}>
                    The repository for this project is empty
                  </Text>
                )}

                {content.button && (
                  <View style={styles.repoButton}>
                    <GButton
                      style={styles.repoButton}
                      label="Go To Repository"
                      click={displayRepoHandler}
                      category={theme === 'light' ? 'secondary' : 'darkSecondary'}
                      variant="confirm"
                      size="medium"
                    />
                  </View>
                )}
                {Boolean(readMe) && (
                  <View
                    style={
                      theme === 'light'
                        ? [styles.readMeContainer, s.lightBorderStyle]
                        : [styles.readMeContainer, styles.darkReadMeBorder]
                    }
                  >
                    <View
                      style={
                        theme === 'light'
                          ? styles.readMeTitleContainer
                          : styles.darkReadMeTitleContainer
                      }
                    >
                      <Icons.docText color={theme === 'light' ? Colors.dark : Colors.secondary} />
                      <Text
                        style={
                          theme === 'light'
                            ? [s.lightText, styles.readMeTitle]
                            : [s.darkText, styles.readMeTitle]
                        }
                      >
                        {readMe.name}
                      </Text>
                    </View>
                    <View style={styles.readMe}>
                      <Markdown
                        body={readMe.rawBlob}
                        imagePrefix={`${instanceUrl}/${props.project.fullPath}`}
                      />
                    </View>
                  </View>
                )}
              </>
            )}
          </ScrollView>
        </SafeAreaView>
      )}
    </>
  );
};

ProjectDetails.propTypes = {
  project: Proptypes.shape({
    avatarUrl: Proptypes.string,
    description: Proptypes.string,
    id: Proptypes.string,
    name: Proptypes.string.isRequired,
    visibility: Proptypes.string.isRequired,
    fullPath: Proptypes.string,
  }),
  onPress: Proptypes.func,
};

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: 20,
  },
  darkReadMeBorder: {
    backgroundColor: Colors.dark,
    borderColor: Colors.darkGray2,
  },
  darkReadMeTitleContainer: {
    alignItems: 'center',
    backgroundColor: Colors.dark,
    borderBottomWidth: 1,
    borderColor: Colors.lightGray4,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: 'row',
    paddingLeft: 10,
  },
  description: {
    fontSize: 16,
    padding: 10,
  },
  emptyText: {
    borderBottomWidth: 0,
    marginTop: 20,
    textAlign: 'center',
  },
  readMe: {
    padding: 10,
  },
  readMeContainer: {
    borderRadius: 10,
    borderWidth: 1,
    margin: 5,
  },
  readMeTitle: {
    fontWeight: '800',
    padding: 10,
  },
  readMeTitleContainer: {
    alignItems: 'center',
    backgroundColor: Colors.lightGray3,
    borderBottomWidth: 1,
    borderColor: Colors.border,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexDirection: 'row',
    paddingLeft: 10,
  },
  repoButton: {
    alignItems: 'center',
    marginVertical: 30,
  },
  screen: {
    flex: 1,
  },
});

export default ProjectDetails;
