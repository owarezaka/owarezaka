import React, { useState, useEffect, useContext } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import NavigationButton from '~app/components/NavigationButton';
import Proptypes from 'prop-types';
import FileItem from './FileItem';
import FileDetails from './FileDetails';
import s, { Colors } from '~app/constants/GlobalStyles';

import LoadingSpinner from '~app/components/LoadingSpinner';
import { ThemeContext } from '~app/store/theme-context';
import GAlert from '~app/components/GAlert';

const ProjectRepo = (props) => {
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);
  const [showFileError, setShowFileError] = useState(false);
  const [currentFile, setCurrentFile] = useState();
  const [loading, setLoading] = useState(true);
  const [treePath, setTreePath] = useState('');
  const [content, setContent] = useState();
  const [repo, setRepo] = useState();

  const getFiles = async () => {
    setLoading(true);
    const user = await UserStorage.getUserInfo();
    const projectPath = props.path;
    const fileInfo = await GitlabApi.getFileInfo(
      projectPath,
      treePath,
      user.instanceUrl,
      user.userToken,
    );

    if (fileInfo) {
      setLoading(false);
      return setRepo(
        fileInfo.folder && fileInfo.file
          ? fileInfo.folder.concat(fileInfo.file)
          : !fileInfo.folder && fileInfo.file
          ? fileInfo.file
          : fileInfo.folder && !fileInfo.file
          ? fileInfo.folder
          : -1,
      );
    }

    setShowError(fileInfo?.success === false);

    setLoading(false);
  };

  const getFileContent = async () => {
    const user = await UserStorage.getUserInfo();
    const projectPath = props.path;
    let fileContent;

    if (currentFile) {
      fileContent = await GitlabApi.getFileContent(
        projectPath,
        currentFile.path,
        user.instanceUrl,
        user.userToken,
      );
    }

    if (fileContent) {
      setContent(fileContent);
    }

    setShowFileError(fileContent?.success === false);
  };

  useEffect(() => {
    getFiles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [treePath]);

  useEffect(() => {
    getFileContent();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentFile]);

  const handlePress = () => {
    props.onPress(null);
  };

  const onPress = (item) => {
    if (item.type === 'blob') {
      setCurrentFile(item);
    } else {
      setTreePath(item.path);
    }
  };

  const onPressReturn = () => {
    const returnPath = treePath.substring(0, treePath.lastIndexOf('/'));
    setTreePath(returnPath);
  };

  return (
    <>
      {currentFile ? (
        <FileDetails
          onPress={() => {
            setCurrentFile();
            setContent();
          }}
          fileName={currentFile.name}
          content={content ? content[0].rawTextBlob : undefined}
          showError={showFileError}
          size={content ? content[0].rawSize : -1}
          language={content ? content[0].language : undefined}
          fullPath={props.path}
        />
      ) : (
        <>
          <NavigationButton title="Project Repository" onPress={handlePress} />
          <GAlert
            testID="alert"
            visible={showError}
            variant="Danger"
            title="Error"
            message={`Connection failed.\nPlease retry later.`}
            secondaryButtonText="Dismiss"
            secondaryAction={() => setShowError(false)}
          />
          <View style={theme === 'light' ? s.lightContainer : s.darkContainer}>
            <View style={theme === 'light' ? styles.lightHeader : styles.darkHeader}>
              <Text
                style={
                  theme === 'light'
                    ? [s.lightText, styles.headerText]
                    : [s.darkText, styles.headerText]
                }
              >
                Name
              </Text>
            </View>
            {loading ? (
              <LoadingSpinner />
            ) : (
              <>
                {treePath !== '' && <FileItem name=".." onPress={() => onPressReturn()} />}
                <FlatList
                  contentContainerStyle={styles.listStyle}
                  testID="file-list"
                  data={repo}
                  keyExtractor={(item) => item.id}
                  renderItem={({ item }) => {
                    return (
                      <FileItem type={item.type} name={item.name} onPress={() => onPress(item)} />
                    );
                  }}
                />
              </>
            )}
          </View>
        </>
      )}
    </>
  );
};

ProjectRepo.propTypes = {
  onPress: Proptypes.func.isRequired,
  path: Proptypes.string,
};

const styles = StyleSheet.create({
  darkHeader: {
    backgroundColor: Colors.dark,
    borderColor: Colors.lightGray4,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderWidth: 0.5,
    height: 40,
    justifyContent: 'center',
    marginHorizontal: 10,
    marginTop: 20,
  },

  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 20,
  },

  lightHeader: {
    backgroundColor: Colors.lightGray3,
    borderColor: Colors.border,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderWidth: 0.5,
    height: 40,
    justifyContent: 'center',
    marginHorizontal: 10,
    marginTop: 20,
  },

  listStyle: {
    paddingBottom: 20,
  },
});

export default ProjectRepo;
