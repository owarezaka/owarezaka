import React, { useState, useEffect, useContext, useCallback } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';
import { useFocusEffect } from '@react-navigation/native';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import ProjectItem from './ProjectItem';
import RefreshList from '~app/components/RefreshList';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import s from 'app/constants/GlobalStyles';
import ProjectDetails from 'app/screens/Projects/ProjectDetails';
import NavigationButton from '~app/components/NavigationButton';

const ProjectsScreen = (props) => {
  const { theme } = useContext(ThemeContext);

  const [projects, setProjects] = useState([]);
  const [roles, setRoles] = useState([]);
  const [showError, setShowError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [projectId, setProjectId] = useState(null);
  const [currentProjectDetails, setCurrentProjectDetails] = useState();

  useFocusEffect(
    React.useCallback(() => {
      if (!loading) {
        props.route?.params && setCurrentProject(props.route.params);
      }
      return;
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.route.params, loading]),
  );

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getAllProjects();
    setRefreshing(false);
  }, []);

  const getAllProjects = async () => {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.getAllProjects(user.username, user.instanceUrl, user.userToken);

    if (result?.projects) {
      setProjects(result.projects.nodes);
      setRoles(result.user);
      setLoading(false);
    }

    setShowError(result.success === false);
    setLoading(false);
  };

  useEffect(() => {
    getAllProjects();

    return () => {
      setProjects([]);
      setRoles([]);
    };
  }, []);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const setCurrentProject = function (projectID) {
    setProjectId(projectID);
    if (projectID) {
      const [projectDetails] = projects.filter((item) => item.id === projectID);
      setCurrentProjectDetails(projectDetails);
    }
  };

  return (
    <>
      {projectId ? (
        <ProjectDetails project={currentProjectDetails} onPress={setCurrentProject} />
      ) : (
        <SafeAreaView style={theme === 'light' ? s.lightContainer : s.darkContainer}>
          <NavigationButton title="Projects" onPress={props.navigation.goBack} />
          {loading ? (
            <LoadingSpinner />
          ) : showError ? (
            <GAlert
              visible={showError}
              variant="Danger"
              title="Error"
              message={`Connection failed.\nPlease retry later.`}
              secondaryButtonText="Dismiss"
              secondaryAction={displayErrorHandler}
            />
          ) : projects?.length > 0 ? (
            <RefreshList
              style={styles.container}
              refresh={() => getAllProjects()}
              testID="projectsList"
              data={projects}
              keyExtractor={(item) => item.id}
              renderItem={({ item }) => {
                return <ProjectItem project={item} roles={roles} onPress={setCurrentProject} />;
              }}
            />
          ) : (
            <EmptyScreen
              refreshing={refreshing}
              onRefresh={onRefresh}
              title="There is nothing here"
              text="You are currently not part of any projects."
            />
          )}
        </SafeAreaView>
      )}
    </>
  );
};

ProjectsScreen.propTypes = {
  navigation: Proptypes.shape({
    goBack: Proptypes.func.isRequired,
  }).isRequired,
  route: Proptypes.shape(),
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
});

export default ProjectsScreen;
