import React, { useContext } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';

import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const ProjectItem = (props) => {
  const { theme } = useContext(ThemeContext);

  const role = Utils.findRoleById(
    props.project.id,
    props.roles?.projectMemberships?.nodes,
    props.project.group?.id,
    props.roles?.groupMemberships?.nodes,
  );

  function setImageSourceHandler() {
    if (props.project?.avatarUrl) {
      if (props.project.visibility === 'private') {
        if (theme === 'light') {
          return require('~app/assets/projectAvatarDefault.png');
        }

        if (theme === 'dark') {
          return require('~app/assets/projectAvatarDark.png');
        }
      }

      return { uri: props.project.avatarUrl };
    }

    if (!props.project?.avatarUrl && theme === 'light') {
      return require('~app/assets/projectAvatarDefault.png');
    }

    if (!props.project?.avatarUrl && theme === 'dark') {
      return require('~app/assets/projectAvatarDark.png');
    }
  }
  const source = setImageSourceHandler();

  return (
    <TouchableOpacity style={styles.item} onPress={() => props.onPress(props.project.id)}>
      <View>
        <Image source={source} style={theme === 'light' ? styles.avatar : styles.darkAvatar} />
        <Text
          style={
            theme === 'light' ? [s.lightText, styles.avatarLabel] : [s.darkText, styles.avatarLabel]
          }
        >
          {!props.project?.avatarUrl && props.project.name[0].toUpperCase()}
          {props.project?.avatarUrl &&
            props.project.visibility === 'private' &&
            props.project.name[0].toUpperCase()}
        </Text>
      </View>

      <View style={styles.content}>
        <Text>
          <Text
            testID={`namespace${props.project.id}`}
            style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
          >{`${
            props.project.group?.name ? props.project.group.name : props.project.namespace?.name
          } / `}</Text>
          <Text
            testID={`name${props.project.id}`}
            style={theme === 'light' ? [s.lightText, styles.name] : [s.darkText, styles.name]}
          >
            {props.project.name}
          </Text>{' '}
          {props.project.visibility === 'private' ? (
            <Icons.lock color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          ) : props.project.visibility === 'internal' ? (
            <Icons.shield color={theme === 'light' ? Colors.dark : Colors.darkGray} />
          ) : (
            <Icons.earth
              testID="public-icon"
              color={theme === 'light' ? Colors.dark : Colors.darkGray}
            />
          )}
          <View style={theme === 'light' ? styles.lightRoleContainer : styles.darkRoleContainer}>
            {role && (
              <Text
                style={theme === 'light' ? [s.lightText, styles.role] : styles.darkRole}
                testID={`role${props.project.id}`}
              >
                {`${role}`}
              </Text>
            )}
          </View>
        </Text>

        <Text style={styles.info}>
          {props.project.pipelines.nodes[0]?.detailedStatus.icon === 'status_success' ? (
            <>
              <Icons.statusSuccess style={styles.center} /> <Text> </Text>
            </>
          ) : props.project.pipelines.nodes[0]?.detailedStatus.icon === 'status_warning' ? (
            <>
              <Icons.statusWarning style={styles.center} /> <Text> </Text>
            </>
          ) : props.project.pipelines.nodes[0]?.detailedStatus.icon === 'status_failed' ? (
            <>
              <Icons.statusFailed style={styles.center} /> <Text> </Text>
            </>
          ) : null}
          <Icons.star
            color={theme === 'light' ? Colors.dark : Colors.darkGray}
            style={styles.center}
          />{' '}
          <Text style={theme === 'light' ? s.lightText : styles.darkText}>
            {props.project.starCount}{' '}
          </Text>
          <Text
            testID={`since${props.project.id}`}
            style={theme === 'light' ? s.lightText : styles.darkText}
          >{`Updated ${Utils.timeSince(props.project.lastActivityAt)}`}</Text>
        </Text>
      </View>
    </TouchableOpacity>
  );
};

ProjectItem.propTypes = {
  project: Proptypes.shape({
    avatarUrl: Proptypes.any,
    id: Proptypes.string.isRequired,
    name: Proptypes.string.isRequired,
    namespace: Proptypes.shape({
      name: Proptypes.string,
    }),
    group: Proptypes.shape({
      id: Proptypes.string,
      name: Proptypes.string,
    }),
    visibility: Proptypes.string.isRequired,
    starCount: Proptypes.number.isRequired,
    lastActivityAt: Proptypes.string,
    pipelines: Proptypes.shape({
      nodes: Proptypes.array,
    }),
  }),
  roles: Proptypes.shape({
    groupMemberships: Proptypes.shape({
      nodes: Proptypes.array,
    }),
    projectMemberships: Proptypes.shape({
      nodes: Proptypes.array,
    }),
  }),
  onPress: Proptypes.func,
};

const styles = StyleSheet.create({
  avatar: {
    borderColor: Colors.darkGray,
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    marginRight: 10,
    position: 'relative',
    width: 40,
  },
  avatarLabel: {
    fontSize: 24,
    position: 'absolute',
    transform: [{ translateY: 2 }, { translateX: 12 }],
  },
  center: {
    transform: [{ translateY: 2 }],
  },
  content: {
    marginBottom: 10,
    width: '80%',
  },
  darkAvatar: {
    borderColor: Colors.dark,
    borderRadius: 10,
    borderWidth: 1,
    height: 40,
    marginRight: 10,
    position: 'relative',
    width: 40,
  },
  darkRole: {
    color: Colors.lightGray4,
    fontSize: 16,
    paddingHorizontal: 5,
    paddingVertical: 2,
  },
  darkRoleContainer: {
    borderColor: Colors.dark,
    borderRadius: 10,
    borderWidth: 1,
    transform: [{ translateY: 5 }, { translateX: 5 }],
  },
  darkText: {
    color: Colors.darkGray,
  },
  info: {
    fontSize: 16,
    marginTop: 10,
  },
  item: {
    borderBottomWidth: 0.25,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    padding: 10,
  },
  lightRoleContainer: {
    borderColor: Colors.darkGray,
    borderRadius: 10,
    borderWidth: 0.4,
    transform: [{ translateY: 5 }, { translateX: 5 }],
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  role: {
    fontSize: 16,
    paddingHorizontal: 5,
    paddingVertical: 2,
  },
  title: {
    fontSize: 18,
  },
});

export default ProjectItem;
