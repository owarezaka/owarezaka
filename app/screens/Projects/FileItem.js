import React, { useContext } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import s, { Colors } from 'app/constants/GlobalStyles';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';
import Icons from '~app/assets/Icons';

const FileItem = (props) => {
  const { theme } = useContext(ThemeContext);
  return (
    <TouchableOpacity onPress={() => props.onPress(null)}>
      <View style={theme === 'light' ? styles.lightItem : styles.darkItem}>
        {props.type === 'tree' && (
          <Icons.folder style={styles.icon} color={theme === 'light' ? null : Colors.secondary} />
        )}
        <Text
          style={theme === 'light' ? [s.lightText, styles.fileName] : [s.darkText, styles.fileName]}
          s
        >
          {props.name}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

FileItem.propTypes = {
  onPress: Proptypes.func.isRequired,
  name: Proptypes.string,
  type: Proptypes.string,
};

const styles = StyleSheet.create({
  darkItem: {
    alignItems: 'center',
    borderColor: Colors.lightGray4,
    borderWidth: 0.5,
    flexDirection: 'row',
    marginHorizontal: 10,
    padding: 15,
  },

  fileName: {
    fontSize: 16,
    fontWeight: '300',
  },

  icon: {
    marginRight: 5,
  },

  lightItem: {
    alignItems: 'center',
    borderColor: Colors.border,
    borderWidth: 0.5,
    flexDirection: 'row',
    marginHorizontal: 10,
    padding: 15,
  },
});

export default FileItem;
