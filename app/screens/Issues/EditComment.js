import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import Proptypes from 'prop-types';
import s, { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';

import { ThemeContext } from '~app/store/theme-context';

const EditComment = (props) => {
  const { theme } = useContext(ThemeContext);
  const [note, setNote] = useState(undefined);
  const [showError, setShowError] = useState(false);

  const editIssueNoteHandler = async () => {
    const enteredNote = note?.replace(/"/g, '\\"');

    const user = await UserStorage.getUserInfo();
    const response = await GitlabApi.updateIssueNote(
      props.noteId,
      enteredNote,
      user.instanceUrl,
      user.userToken,
    );

    if (response === 200) {
      props.setDisplayCommentArea(false);
      props.displayAllIssueNotes();
    }

    setShowError(response.success === false);
  };

  return (
    <View testID={props.testID}>
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowError(false)}
      />

      <TextInput
        multiline={true}
        style={
          theme === 'light'
            ? [s.lightBorderStyle, s.lightText, s.lightContainer, styles.commentFieldStyle]
            : [s.darkBorderStyle, s.darkText, s.darkContainer, styles.commentFieldStyle]
        }
        value={note}
        defaultValue={props.defaultValue}
        placeholder="Write a comment here..."
        placeholderTextColor={theme === 'light' ? Colors.gray : Colors.secondary}
        onChangeText={(text) => setNote(text)}
      />

      <View style={styles.rowDirection}>
        <View style={styles.btnContainer}>
          <GButton
            category={
              note
                ? theme === 'light'
                  ? 'primary'
                  : 'darkPrimary'
                : theme === 'light'
                ? 'disabled'
                : 'darkDisabled'
            }
            disabled={note ? false : true}
            click={editIssueNoteHandler}
            label="Save Comment"
          />
        </View>

        <GButton
          category={theme === 'light' ? 'secondary' : 'darkSecondary'}
          click={() => props.setDisplayCommentArea(false)}
          label="Cancel"
        />
      </View>
    </View>
  );
};

EditComment.propTypes = {
  defaultValue: Proptypes.string,
  displayAllIssueNotes: Proptypes.func,
  noteId: Proptypes.string,
  setDisplayCommentArea: Proptypes.func,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  btnContainer: {
    marginRight: 8,
  },
  commentFieldStyle: {
    borderRadius: 5,
    borderWidth: 1,
    fontSize: 16,
    marginVertical: 5,
    opacity: 0.7,
    paddingBottom: 12.5,
    paddingHorizontal: 10,
    paddingTop: 12.5,
  },
  rowDirection: {
    flexDirection: 'row',
  },
});

export default EditComment;
