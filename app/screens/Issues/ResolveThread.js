import React, { useState, useContext } from 'react';
import { StyleSheet, View } from 'react-native';
import Proptypes from 'prop-types';
import { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import Icons from '~app/assets/Icons';
import { ThemeContext } from '~app/store/theme-context';

const ResolveThread = (props) => {
  const { theme } = useContext(ThemeContext);
  const [showError, setShowError] = useState(false);

  const resolveThreadHandler = async (noteId) => {
    const user = await UserStorage.getUserInfo();
    const response = await GitlabApi.discussionToggleResolve(
      noteId,
      props.isResolved ? false : true,
      user.instanceUrl,
      user.userToken,
    );

    if (response === 200) {
      props.displayAllIssueNotes();
    }

    setShowError(response.success === false);
  };

  return (
    <View testID={props.testID} style={styles.btnContainer}>
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowError(false)}
      />

      <GButton
        testID="resolveButton"
        label=""
        category="tertiary"
        flexDirection="row"
        padding={5}
        click={() => resolveThreadHandler(props.discussionId)}
      >
        <Icons.statusSuccess
          testID="statusSuccess"
          size={22}
          color={
            theme === 'light' && props.isResolved
              ? Colors.green
              : theme === 'light' && !props.isResolved
              ? Colors.dark
              : theme === 'dark' && !props.isResolved
              ? Colors.secondary
              : null
          }
          backgroundColor={theme === 'light' ? Colors.lightGray : Colors.dark}
        />
      </GButton>
    </View>
  );
};

ResolveThread.propTypes = {
  displayAllIssueNotes: Proptypes.func,
  discussionId: Proptypes.string,
  isResolved: Proptypes.bool,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

export default ResolveThread;
