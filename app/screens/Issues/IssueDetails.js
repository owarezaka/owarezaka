import React, { useState, useEffect, useMemo, useContext, useCallback } from 'react';
import { StyleSheet, View, ScrollView, RefreshControl, SafeAreaView } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import * as Utils from '~app/utils/Utils';
import NavigationButton from '~app/components/NavigationButton';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import Slider from './Slider';
import Note from './Note';
import OverviewHeader from './OverviewHeader';
import OverviewBody from './OverviewBody';
import GButton from '~app/components/GButton';
import SendComment from '../Issues/SendComment';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';

const IssueDetails = (props) => {
  const { theme } = useContext(ThemeContext);
  const [toggleSlider, setToggleSlider] = useState(false);
  const [showError, setShowError] = useState(false);
  const [user, setUser] = useState(null);
  const [details, setDetails] = useState(null);
  const [displayNote, setDisplayNote] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const getOpenIssueDetails = useCallback(async () => {
    const userInfo = await UserStorage.getUserInfo();
    setUser(userInfo);

    const result = await GitlabApi.getOpenIssueDetails(
      props.issue.id,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    if (result.author) {
      return setDetails(result);
    }

    setShowError(result.success === false);
  }, [props.issue.id]);

  useEffect(() => {
    getOpenIssueDetails();
  }, [getOpenIssueDetails]);

  const displayAllIssueNotes = useCallback(async () => {
    const userInfo = await UserStorage.getUserInfo();

    const result = await GitlabApi.getAllIssueNotes(
      props.issue.id,
      userInfo.instanceUrl,
      userInfo.userToken,
    );

    if (Array.isArray(result)) {
      const sorted = Utils.groupBy(result, (el) => el?.discussion?.id);
      const filtered = Utils.flattenArr(sorted);
      const reversed = Utils.reverseArr(filtered);
      return setDisplayNote(reversed);
    }

    setShowError(result.success === false);
  }, [props.issue.id]);

  useEffect(() => {
    displayAllIssueNotes();

    return () => {
      setDisplayNote([]);
    };
  }, [displayAllIssueNotes]);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getOpenIssueDetails();
    await displayAllIssueNotes();
    setRefreshing(false);
  }, [displayAllIssueNotes, getOpenIssueDetails]);

  const handlePress = () => {
    props.onPress(null);
  };

  const handleSlidingDrawer = () => {
    setToggleSlider(!toggleSlider);
  };

  const closeIssueHandler = async () => {
    const response = await GitlabApi.closeIssue(
      user.instanceUrl,
      props.issue.project.fullPath,
      props.issue.iid,
      user.userToken,
    );

    if (response === 200) {
      props.refreshOpenIssues();
      return handlePress();
    }

    setShowError(response.success === false);
  };

  const destroyNoteHandler = useCallback(
    async (noteid) => {
      const response = await GitlabApi.destroyNote(noteid, user.instanceUrl, user.userToken);

      if (response === 200) {
        return displayAllIssueNotes();
      }

      setShowError(response.success === false);
    },
    [displayAllIssueNotes, user?.instanceUrl, user?.userToken],
  );

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const memoNotes = useMemo(
    () =>
      displayNote?.map((el, idx) => (
        <Note
          key={el.id}
          note={el}
          instanceUrl={user?.instanceUrl}
          destroyNoteHandler={destroyNoteHandler}
          displayAllIssueNotes={displayAllIssueNotes}
        />
      )),

    [displayNote, destroyNoteHandler, displayAllIssueNotes, user?.instanceUrl],
  );

  return (
    <SafeAreaView
      style={
        theme === 'light' ? [s.lightContainer, styles.screen] : [s.darkContainer, styles.screen]
      }
    >
      <NavigationButton title="Issues" subTitle={props.issue.reference} onPress={handlePress} />

      <GAlert
        testID="alert-issues"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={displayErrorHandler}
      />
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
      >
        <OverviewHeader
          testID="IssueDetailsHeader"
          stateName="Open"
          handleSlidingDrawer={handleSlidingDrawer}
          createdAt={props.issue.createdAt}
          authorUsername={details?.author?.username}
        />

        <View style={styles.line} />

        <View style={styles.content}>
          <OverviewBody
            testID="IssueDetailsBody"
            title={props.issue.title}
            description={details?.descriptionHtml}
            instanceUrl={user?.instanceUrl}
            fullPath={props.issue.project.fullPath}
          />

          {memoNotes}

          <SendComment
            testID="sendComment"
            issueId={props.issue.id}
            displayAllIssueNotes={displayAllIssueNotes}
          />

          <View style={styles.btnContainer}>
            <GButton
              testID="closeIssueButton"
              size="large"
              click={closeIssueHandler}
              label="Close issue"
              textAlign="center"
              category={theme === 'light' ? '' : 'darkSecondary'}
            />
          </View>
        </View>
      </ScrollView>

      <Slider
        testID="slider"
        setVisible={toggleSlider}
        issue={props.issue}
        details={details}
        handleSlidingDrawer={handleSlidingDrawer}
        instanceUrl={user?.instanceUrl}
      />
    </SafeAreaView>
  );
};

IssueDetails.propTypes = {
  issue: Proptypes.shape({
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
    }),
    createdAt: Proptypes.string.isRequired,
    id: Proptypes.string.isRequired,
    iid: Proptypes.string.isRequired,
    labels: Proptypes.shape({
      nodes: Proptypes.array,
    }),
    milestone: Proptypes.shape({
      title: Proptypes.string,
    }),
    project: Proptypes.shape({
      fullPath: Proptypes.string,
    }),
    projectId: Proptypes.number.isRequired,
    reference: Proptypes.string.isRequired,
    title: Proptypes.string.isRequired,
  }),
  onPress: Proptypes.func.isRequired,
  refreshOpenIssues: Proptypes.func.isRequired,
};

const styles = StyleSheet.create({
  btnContainer: {
    marginVertical: 5,
  },
  content: {
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
  line: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    color: Colors.dark,
  },
  screen: {
    flex: 1,
  },
  scrollContainer: {
    paddingBottom: 20,
  },
});

export default IssueDetails;
