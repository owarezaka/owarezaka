import React, { useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import * as Utils from '~app/utils/Utils';
import Icons from '~app/assets/Icons';
import GButton from '~app/components/GButton';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';

const OverviewHeader = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);

  return (
    <View style={styles.header} testID={props.testID}>
      <View style={styles.content}>
        <View style={styles.state}>
          <Text
            style={
              theme === 'light' ? [s.darkText, styles.stateText] : [s.lightText, styles.stateText]
            }
          >
            {props.stateName}
          </Text>
        </View>

        <View style={styles.rowDirection}>
          {props.markAsReadyBtnAction && (
            <View style={styles.btnContainer}>
              <GButton
                testID="markAsReady"
                category={theme === 'light' ? 'secondary' : 'darkSecondary'}
                variant="confirm"
                click={props.markAsReadyBtnAction}
                label={props.isDraft ? 'Mark As Ready' : 'Mark As Draft'}
                textAlign="center"
              />
            </View>
          )}

          <GButton
            testID="sliderOpenButton"
            label=""
            flexDirection="row"
            click={props.handleSlidingDrawer}
            category={theme === 'light' ? '' : 'darkSecondary'}
          >
            <Icons.angleDoubleRight
              testID="angleRightIcon"
              degree="90deg"
              color={theme === 'light' ? Colors.dark : Colors.secondary}
            />
          </GButton>
        </View>
      </View>
      <View style={styles.rowDirection}>
        <Text
          style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}
          testID="createdAt"
        >
          {` ${t('Created')} ${Utils.timeSince(props.createdAt)} ${t('by')} `}
        </Text>
        {props.authorUsername && (
          <Text
            testID="authorUsername"
            style={theme === 'light' ? [s.lightText, styles.bold] : [s.darkText, styles.bold]}
          >
            {`@${props.authorUsername}`}
          </Text>
        )}
      </View>

      <View style={styles.rowDirection}>
        {props.overviewBtnAction && (
          <View style={styles.padding}>
            <GButton
              testID="overviewButton"
              label="Overview"
              click={props.overviewBtnAction}
              category={theme === 'light' ? '' : 'darkSecondary'}
            />
          </View>
        )}

        {props.changesBtnAction && (
          <View style={[styles.btnContainer, styles.padding]}>
            <GButton
              testID="changesButton"
              label="Changes"
              click={props.changesBtnAction}
              category={theme === 'light' ? '' : 'darkSecondary'}
            />
          </View>
        )}
      </View>
    </View>
  );
};

OverviewHeader.propTypes = {
  authorUsername: Proptypes.string,
  changesBtnAction: Proptypes.func,
  createdAt: Proptypes.string.isRequired,
  handleSlidingDrawer: Proptypes.func.isRequired,
  isDraft: Proptypes.bool,
  markAsReadyBtnAction: Proptypes.func,
  overviewBtnAction: Proptypes.func,
  stateName: Proptypes.string.isRequired,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  bold: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnContainer: {
    marginHorizontal: 8,
  },
  content: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  header: {
    paddingBottom: 10,
    paddingHorizontal: 15,
    paddingTop: 20,
  },
  padding: {
    paddingTop: 10,
  },
  rowDirection: {
    flexDirection: 'row',
  },
  state: {
    alignItems: 'center',
    backgroundColor: Colors.green,
    borderRadius: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  stateText: {
    fontSize: 16,
    textAlign: 'center',
  },
  text: {
    fontSize: 16,
  },
});

export default OverviewHeader;
