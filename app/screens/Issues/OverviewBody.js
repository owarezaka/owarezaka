import React, { useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Proptypes from 'prop-types';
import s from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';
import RenderHtml from '~app/components/RenderHtml';

const OverviewBody = (props) => {
  const { theme } = useContext(ThemeContext);

  return (
    <View testID={props.testID}>
      <Text style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}>
        {props.title}
      </Text>
      {Boolean(props.description) && (
        <Text
          style={
            theme === 'light' ? [s.lightText, styles.description] : [s.darkText, styles.description]
          }
        >
          <RenderHtml instanceUrl={props.instanceUrl} body={props.description} />
        </Text>
      )}
    </View>
  );
};

OverviewBody.propTypes = {
  fullPath: Proptypes.string,
  instanceUrl: Proptypes.string,
  title: Proptypes.string,
  description: Proptypes.string,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  description: {
    fontSize: 16,
    paddingVertical: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    marginBottom: 10,
  },
});

export default OverviewBody;
