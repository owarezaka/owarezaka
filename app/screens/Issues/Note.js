import React, { useState, useContext } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Icons from '~app/assets/Icons';
import GButton from '~app/components/GButton';
import EditComment from '../Issues/EditComment';
import ResolveThread from '../Issues/ResolveThread';
import s, { Colors } from '~app/constants/GlobalStyles';
import RenderHtml from '~app/components/RenderHtml';
import * as Utils from '~app/utils/Utils';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';

const Note = (props) => {
  const { theme } = useContext(ThemeContext);
  const [displayCommentArea, setDisplayCommentArea] = useState(false);
  return (
    <View
      testID={props.testID}
      style={
        theme === 'light'
          ? [s.lightBorderStyle, s.lightContainer, styles.createdNotesStyle]
          : [s.darkBorderStyle, s.darkContainer, styles.createdNotesStyle]
      }
    >
      <View style={styles.rowDirection}>
        <Image
          testID="authorAvatar"
          source={
            props.note.author.avatarUrl.includes('gravatar')
              ? { uri: props.note?.author?.avatarUrl }
              : { uri: `${props.instanceUrl}${props.note.author.avatarUrl}` }
          }
          style={styles.avatarBig}
        />

        <View style={styles.columnDirection}>
          <Text style={theme === 'light' ? s.lightText : s.darkText}>
            {props.note.author.name}{' '}
            <Icons.commentDots
              color={theme === 'light' ? Colors.dark : Colors.darkGray}
              testID="commentDots"
            />
          </Text>

          <View style={styles.rowDirection}>
            <Text
              style={theme === 'light' ? s.lightText : s.darkText}
            >{`@${props.note.author.username} `}</Text>
            <Text style={theme === 'light' ? s.lightText : s.darkText}>
              {Utils.timeSince(props.note.createdAt)}
            </Text>
          </View>
        </View>

        {props.note.discussion?.resolvable && (
          <ResolveThread
            testID="resolveThread"
            isResolved={props.note.discussion?.resolved}
            discussionId={props.note.discussion?.id}
            displayAllIssueNotes={props.displayAllIssueNotes}
          />
        )}

        {props.note.userPermissions.repositionNote && !displayCommentArea && (
          <View style={styles.btnContainer}>
            <GButton
              testID="editButton"
              label=""
              category="tertiary"
              flexDirection="row"
              padding={5}
              click={() => setDisplayCommentArea(true)}
            >
              <Icons.pencil
                testID="editIcon"
                size={18}
                color={theme === 'light' ? Colors.dark : Colors.lightGray4}
              />
            </GButton>

            <GButton
              testID="removeButton"
              label=""
              category="tertiary"
              flexDirection="row"
              padding={5}
              click={() => props.destroyNoteHandler(props.note.id)}
            >
              <Icons.close
                testID="removeIcon"
                size={22}
                color={theme === 'light' ? Colors.dark : Colors.lightGray4}
              />
            </GButton>
          </View>
        )}
      </View>

      <View style={styles.noteBodyStyle}>
        {displayCommentArea ? (
          <EditComment
            testID="editComment"
            noteId={props.note.id}
            defaultValue={props.note.body}
            setDisplayCommentArea={setDisplayCommentArea}
            displayAllIssueNotes={props.displayAllIssueNotes}
          />
        ) : (
          <Text style={theme === 'light' ? s.lightText : s.darkText}>
            <RenderHtml instanceUrl={props.instanceUrl} body={props.note.bodyHtml} />
          </Text>
        )}
      </View>
    </View>
  );
};

Note.propTypes = {
  destroyNoteHandler: Proptypes.func,
  displayAllIssueNotes: Proptypes.func,
  instanceUrl: Proptypes.string,
  note: Proptypes.shape({
    id: Proptypes.string,
    body: Proptypes.string.isRequired,
    bodyHtml: Proptypes.string.isRequired,
    createdAt: Proptypes.string.isRequired,
    userPermissions: Proptypes.object,
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
      username: Proptypes.string.isRequired,
      avatarUrl: Proptypes.string,
    }),
    discussion: Proptypes.shape({
      resolvable: Proptypes.bool,
      resolved: Proptypes.bool,
      id: Proptypes.string,
    }),
  }),
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  avatarBig: {
    borderColor: Colors.border,
    borderRadius: 40,
    borderWidth: 0.3,
    height: 40,
    marginRight: 10,
    width: 40,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  columnDirection: {
    flexDirection: 'column',
  },
  createdNotesStyle: {
    borderRadius: 5,
    borderWidth: 1,
    marginVertical: 10,
    padding: 10,
  },
  noteBodyStyle: {
    marginTop: 10,
    padding: 10,
  },
  rowDirection: {
    flexDirection: 'row',
  },
});

export default Note;
