import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import Proptypes from 'prop-types';
import { Colors } from '~app/constants/GlobalStyles';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import GAlert from '~app/components/GAlert';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';

const SendComment = (props) => {
  const { theme } = useContext(ThemeContext);
  const [note, setNote] = useState('');
  const [showError, setShowError] = useState(false);
  const [showWarning, setShowWarning] = useState({
    display: false,
    message: '',
  });

  const createIssueNoteHandler = async () => {
    const user = await UserStorage.getUserInfo();
    const response = await GitlabApi.createIssueNote(
      props.issueId,
      note,
      user.instanceUrl,
      user.userToken,
    );

    if (response === 200) {
      props.displayAllIssueNotes();
      setNote('');
    }

    if (response.message) {
      setShowWarning({ display: true, message: response.message });
    }

    setShowError(response.success === false);
  };

  return (
    <View testID={props.testID}>
      <GAlert
        testID="alert"
        visible={showError}
        variant="Danger"
        title="Error"
        message={`Connection failed.\nPlease retry later.`}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowError(false)}
      />

      <GAlert
        testID="warning"
        visible={showWarning.display}
        variant="Danger"
        title="Warning"
        message={showWarning.message}
        secondaryButtonText="Dismiss"
        secondaryAction={() => setShowWarning({ display: false })}
      />

      <TextInput
        multiline={true}
        style={theme === 'light' ? styles.lightCommentFieldStyle : styles.darkCommentFieldStyle}
        value={note}
        placeholder="Write a comment here..."
        placeholderTextColor={theme === 'light' ? Colors.gray : Colors.darkGray}
        onChangeText={(text) => setNote(text)}
      />

      <GButton
        disabled={note ? false : true}
        category={
          note
            ? theme === 'light'
              ? 'primary'
              : 'darkPrimary'
            : theme === 'light'
            ? 'disabled'
            : 'darkDisabled'
        }
        size="large"
        click={createIssueNoteHandler}
        label="Comment"
        textAlign="left"
      />
    </View>
  );
};

SendComment.propTypes = {
  displayAllIssueNotes: Proptypes.func,
  issueId: Proptypes.string,
  testID: Proptypes.string,
};

const styles = StyleSheet.create({
  darkCommentFieldStyle: {
    borderColor: Colors.secondary,
    borderRadius: 5,
    borderWidth: 1,
    color: Colors.secondary,
    fontSize: 16,
    marginVertical: 10,
    opacity: 0.7,
    paddingBottom: 12.5,
    paddingHorizontal: 10,
    paddingTop: 12.5,
  },
  lightCommentFieldStyle: {
    borderColor: Colors.border,
    borderRadius: 5,
    borderWidth: 1,
    color: Colors.dark,
    fontSize: 16,
    marginVertical: 10,
    opacity: 0.7,
    paddingBottom: 12.5,
    paddingHorizontal: 10,
    paddingTop: 12.5,
  },
});

export default SendComment;
