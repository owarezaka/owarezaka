import React, { useState, useEffect, useCallback } from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GAlert from '~app/components/GAlert';
import LoadingSpinner from '~app/components/LoadingSpinner';
import EmptyScreen from '~app/components/EmptyScreen';
import IssueItem from './IssueItem';
import IssueDetails from './IssueDetails';
import Layout from './Layout';
import RefreshList from '~app/components/RefreshList.js';
import { Issue } from '~app/model/issue';

const IssuesScreen = () => {
  const [issues, setIssues] = useState([]);
  const [showError, setShowError] = useState(false);
  const [issueId, setIssueId] = useState(null);
  const [currentIssueDetails, setCurrentIssueDetails] = useState(null);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await fetchOpenIssues();
    setRefreshing(false);
  }, []);

  const fetchOpenIssues = async () => {
    const user = await UserStorage.getUserInfo();
    const result = await GitlabApi.getAllAssignedIssues(
      user.instanceUrl,
      user.userToken,
      user.username,
    );

    if (Array.isArray(result)) {
      const filteredData = result.map((el) => new Issue(el));
      setIssues(filteredData);
      setLoading(false);
    }

    setShowError(result.success === false);
    setLoading(false);
  };

  useEffect(() => {
    fetchOpenIssues();
  }, []);

  const displayErrorHandler = function () {
    setShowError(false);
  };

  const setCurrentIssue = function (issueID) {
    setIssueId(issueID);
    if (issueID) {
      const [issueDetails] = issues.filter((item) => item.id === issueID);
      setCurrentIssueDetails(issueDetails);
    }
  };

  return (
    <>
      {issueId ? (
        <IssueDetails
          issue={currentIssueDetails}
          onPress={setCurrentIssue}
          refreshOpenIssues={fetchOpenIssues}
        />
      ) : (
        <Layout title="Issues">
          <GAlert
            visible={showError}
            variant="Danger"
            title="Error"
            message={`Connection failed.\nPlease retry later.`}
            secondaryButtonText="Dismiss"
            secondaryAction={displayErrorHandler}
          />

          {loading ? (
            <LoadingSpinner />
          ) : issues.length > 0 ? (
            <RefreshList
              testID="Issues-List"
              refresh={fetchOpenIssues}
              data={issues}
              keyExtractor={(item) => item.id}
              renderItem={({ item }) => {
                return <IssueItem issue={item} onPress={setCurrentIssue} />;
              }}
            />
          ) : (
            <EmptyScreen
              refreshing={refreshing}
              onRefresh={onRefresh}
              title="There is nothing here"
              text="There are no issues currently assigned to you. Good work!"
            />
          )}
        </Layout>
      )}
    </>
  );
};

export default IssuesScreen;
