import React, { useContext, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  Dimensions,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import GButton from '~app/components/GButton';
import EditReviewers from '../MergeRequests/EditReviewers';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';
import { ScrollView } from 'react-native-gesture-handler';

const Slider = (props) => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const [showTextInput, setShowTextInput] = useState(false);

  const showInputHandler = function () {
    setShowTextInput(true);
  };

  const closeSliderHandler = function () {
    setShowTextInput(false);
    props.handleSlidingDrawer();
  };

  return (
    <Modal
      testID={props.testID}
      visible={props.setVisible}
      animationType="slide"
      hardwareAccelerated
      transparent={true}
    >
      <View style={styles.containerModal}>
        <KeyboardAvoidingView
          behavior="height"
          style={
            theme === 'light'
              ? [s.lightContainer, styles.sliderContent]
              : [s.darkContainer, styles.sliderContent]
          }
        >
          <ScrollView>
            <View style={styles.header}>
              <View style={styles.sliderContainer}>
                <View>
                  <Text
                    testID="assigneeTitle"
                    style={
                      theme === 'light'
                        ? [s.lightText, styles.sliderTitle]
                        : [s.darkText, styles.sliderTitle]
                    }
                  >
                    {props.details?.assignees?.nodes.length > 1
                      ? t(`${props.details?.assignees?.nodes.length} Assignees`)
                      : t('Assignee')}
                  </Text>
                </View>

                <View style={styles.sliderInfo}>
                  {props.details?.assignees?.nodes.length > 1 ? (
                    props.details?.assignees?.nodes.map((assignee) => (
                      <Image
                        testID={`assignee${assignee.id}`}
                        key={assignee.id}
                        source={
                          assignee.avatarUrl.includes('gravatar')
                            ? { uri: assignee.avatarUrl }
                            : {
                                uri: `${props.instanceUrl}${assignee.avatarUrl}`,
                              }
                        }
                        style={styles.avatarBig}
                      />
                    ))
                  ) : (
                    <>
                      <Image
                        testID={`assigneeAvatar${props.issue.id}`}
                        source={
                          props.details?.assignees?.nodes[0]?.avatarUrl.includes('gravatar')
                            ? {
                                uri: props.details?.assignees?.nodes[0]?.avatarUrl,
                              }
                            : {
                                uri: `${props.instanceUrl}${props.details?.assignees?.nodes[0]?.avatarUrl}`,
                              }
                        }
                        style={styles.avatarBig}
                      />
                      <View>
                        <Text
                          testID={`assigneeName${props.issue.id}`}
                          style={theme === 'light' ? [s.lightText] : [s.darkText]}
                        >
                          {props.details?.assignees?.nodes[0]?.name}
                        </Text>
                        <Text
                          testID={`assigneeUserName${props.issue.id}`}
                          style={
                            theme === 'light'
                              ? [s.lightText, styles.textStyle]
                              : [s.darkText, styles.textStyle]
                          }
                        >{`@${props.details?.assignees?.nodes[0]?.username}`}</Text>
                      </View>
                    </>
                  )}
                </View>
              </View>

              <View style={styles.sliderCloseButton}>
                <GButton
                  category={theme === 'dark' ? 'darkSecondary' : ''}
                  testID="sliderCloseButton"
                  label=""
                  click={closeSliderHandler}
                  flexDirection="row"
                >
                  <Icons.angleDoubleRight
                    degree="-90deg"
                    testID="angleDoubleRight"
                    color={theme === 'light' ? Colors.dark : Colors.lightGray}
                  />
                </GButton>
              </View>
            </View>

            <View
              style={theme === 'light' ? [s.lightLine, styles.line] : [s.darkLine, styles.line]}
            />
            {props.details?.reviewers ? (
              <>
                <View style={styles.sliderContainer}>
                  <View>
                    <Text
                      testID="reviewerTitle"
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.sliderTitle]
                          : [s.darkText, styles.sliderTitle]
                      }
                    >
                      {!props.details?.reviewers?.nodes.length ||
                      props.details?.reviewers?.nodes.length > 1
                        ? t(`${props.details?.reviewers?.nodes.length} Reviewers`)
                        : t('Reviewer')}
                    </Text>
                    <View style={styles.editBtnContainer}>
                      <GButton
                        label="Edit"
                        fontSize={15}
                        padding={12}
                        category={theme === 'light' ? 'tertiary' : 'darkTertiary'}
                        click={showInputHandler}
                      />
                    </View>
                  </View>

                  <View style={styles.sliderInfo}>
                    {props.details?.reviewers?.nodes.length > 1 ? (
                      <>
                        {showTextInput && (
                          <EditReviewers
                            iid={props.issue.iid}
                            defaultValue={props.details?.reviewers?.nodes[0]?.username}
                            getMergeRequestsDetails={props.getMergeRequestsDetails}
                            projectId={props.details?.projectId}
                            refreshIssues={props.refreshIssues}
                            setShowTextInput={setShowTextInput}
                            testID="EditReviewers"
                            members={props.members}
                            instanceUrl={props.instanceUrl}
                            currentReviewers={props.details.reviewers.nodes}
                          />
                        )}
                        {showTextInput
                          ? null
                          : props.details?.reviewers?.nodes.map((reviewer) => (
                              <Image
                                testID={`reviwer${reviewer.id}`}
                                key={reviewer.id}
                                source={
                                  reviewer.avatarUrl.includes('gravatar')
                                    ? { uri: reviewer.avatarUrl }
                                    : {
                                        uri: `${props.instanceUrl}${reviewer.avatarUrl}`,
                                      }
                                }
                                style={styles.avatarBig}
                              />
                            ))}
                      </>
                    ) : (
                      <>
                        {!props.details?.reviewers?.nodes.length && (
                          <>
                            {showTextInput ? null : (
                              <Text
                                style={
                                  theme === 'light'
                                    ? [s.lightText, styles.textStyle]
                                    : [s.darkText, styles.textStyle]
                                }
                              >
                                None
                              </Text>
                            )}
                          </>
                        )}
                        {props.details?.reviewers?.nodes.length === 1 && (
                          <>
                            {showTextInput ? null : (
                              <>
                                <Image
                                  testID={`reviewer${props.issue.id}`}
                                  source={
                                    props.details?.reviewers?.nodes[0]?.avatarUrl.includes(
                                      'gravatar',
                                    )
                                      ? {
                                          uri: props.details?.reviewers?.nodes[0]?.avatarUrl,
                                        }
                                      : {
                                          uri: `${props.instanceUrl}${props.details?.reviewers?.nodes[0]?.avatarUrl}`,
                                        }
                                  }
                                  style={styles.avatarBig}
                                />
                                <View>
                                  <Text
                                    testID={`reviewerName${props.issue.id}`}
                                    style={theme === 'light' ? [s.lightText] : [s.darkText]}
                                  >
                                    {props.details?.reviewers?.nodes[0]?.name}
                                  </Text>
                                  <Text
                                    testID={`reviewerUserName${props.issue.id}`}
                                    style={
                                      theme === 'light'
                                        ? [s.lightText, styles.textStyle]
                                        : [s.darkText, styles.textStyle]
                                    }
                                  >{`@${props.details?.reviewers?.nodes[0]?.username}`}</Text>
                                </View>
                              </>
                            )}
                          </>
                        )}

                        {showTextInput && (
                          <EditReviewers
                            iid={props.issue.iid}
                            defaultValue={props.details?.reviewers?.nodes[0]?.username}
                            getMergeRequestsDetails={props.getMergeRequestsDetails}
                            projectId={props.details?.projectId}
                            refreshIssues={props.refreshIssues}
                            setShowTextInput={setShowTextInput}
                            testID="EditReviewers"
                            members={props.members}
                            instanceUrl={props.instanceUrl}
                            currentReviewers={props.details.reviewers.nodes}
                          />
                        )}
                      </>
                    )}
                  </View>
                </View>

                <View
                  style={theme === 'light' ? [s.lightLine, styles.line] : [s.darkLine, styles.line]}
                />

                <View style={styles.sliderContainer}>
                  <View>
                    <Text
                      testID="milestoneTitle"
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.sliderTitle]
                          : [s.darkText, styles.sliderTitle]
                      }
                    >
                      {t('Milestone')}
                    </Text>
                  </View>
                  <View style={styles.sliderInfo}>
                    <Text
                      testID={`milestone${props.issue.id}`}
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.textStyle]
                          : [s.darkText, styles.textStyle]
                      }
                    >
                      {props.issue.milestone?.title ? props.issue.milestone.title : t('None')}
                    </Text>
                  </View>
                </View>
              </>
            ) : (
              <>
                <View style={styles.sliderContainer}>
                  <View>
                    <Text
                      testID="milestoneTitle"
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.sliderTitle]
                          : [s.darkText, styles.sliderTitle]
                      }
                    >
                      {t('Milestone')}
                    </Text>
                  </View>
                  <View style={styles.sliderInfo}>
                    <Text
                      testID={`milestone${props.issue.id}`}
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.textStyle]
                          : [s.darkText, styles.textStyle]
                      }
                    >
                      {props.issue.milestone?.title ? props.issue.milestone.title : t('None')}
                    </Text>
                  </View>
                </View>

                <View
                  style={theme === 'light' ? [s.lightLine, styles.line] : [s.darkLine, styles.line]}
                />

                <View style={styles.sliderContainer}>
                  <View>
                    <Text
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.sliderTitle]
                          : [s.darkText, styles.sliderTitle]
                      }
                    >
                      {t('Due Date')}
                    </Text>
                  </View>
                  <View style={styles.sliderInfo}>
                    <Text
                      testID={`duedate${props.issue.id}`}
                      style={
                        theme === 'light'
                          ? [s.lightText, styles.textStyle]
                          : [s.darkText, styles.textStyle]
                      }
                    >
                      {props.details?.dueDate ? props.details?.dueDate : t('None')}
                    </Text>
                  </View>
                </View>
              </>
            )}

            <View
              style={theme === 'light' ? [s.lightLine, styles.line] : [s.darkLine, styles.line]}
            />

            <View style={styles.sliderContainer}>
              <View>
                <Text
                  testID="labelsTitle"
                  style={
                    theme === 'light'
                      ? [s.lightText, styles.sliderTitle]
                      : [s.darkText, styles.sliderTitle]
                  }
                >
                  {t('Labels')}
                </Text>
              </View>
              <View style={styles.sliderInfo}>
                {!props.issue.labels.nodes.length ? (
                  <Text
                    style={
                      theme === 'light'
                        ? [s.lightText, styles.textStyle]
                        : [s.darkText, styles.textStyle]
                    }
                  >
                    {t('None')}
                  </Text>
                ) : (
                  props.issue.labels.nodes?.map((label) => (
                    <View
                      key={label.id}
                      style={[styles.labelContainer, { backgroundColor: label.color }]}
                    >
                      <Text
                        testID={`label${props.issue.id}`}
                        style={
                          theme === 'light'
                            ? [s.lightText, styles.label]
                            : [s.darkText, styles.label]
                        }
                      >
                        {label.title}
                      </Text>
                    </View>
                  ))
                )}
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    </Modal>
  );
};

Slider.propTypes = {
  details: Proptypes.shape({
    assignees: Proptypes.shape({
      nodes: Proptypes.arrayOf(
        Proptypes.shape({
          id: Proptypes.string,
          name: Proptypes.string,
          username: Proptypes.string,
          avatarUrl: Proptypes.string,
        }),
      ),
    }),
    author: Proptypes.shape({
      username: Proptypes.string,
    }),
    description: Proptypes.string,
    dueDate: Proptypes.string,
    projectId: Proptypes.number,
    reviewers: Proptypes.shape({
      nodes: Proptypes.arrayOf(
        Proptypes.shape({
          id: Proptypes.string,
          name: Proptypes.string,
          username: Proptypes.string,
          avatarUrl: Proptypes.string,
        }),
      ),
    }),
  }),
  getMergeRequestsDetails: Proptypes.func,
  instanceUrl: Proptypes.string,
  issue: Proptypes.shape({
    author: Proptypes.shape({
      name: Proptypes.string,
    }),
    createdAt: Proptypes.string,
    id: Proptypes.string,
    iid: Proptypes.string,
    labels: Proptypes.shape({
      nodes: Proptypes.array,
    }),
    milestone: Proptypes.shape({
      title: Proptypes.string,
    }),
    project: Proptypes.shape({
      fullPath: Proptypes.string,
    }),
    reference: Proptypes.string,
    title: Proptypes.string,
  }),
  handleSlidingDrawer: Proptypes.func.isRequired,
  refreshIssues: Proptypes.func,
  setVisible: Proptypes.bool.isRequired,
  testID: Proptypes.string,
  members: Proptypes.array,
};

export default Slider;

const styles = StyleSheet.create({
  avatarBig: {
    borderColor: Colors.border,
    borderRadius: 40,
    borderWidth: 0.3,
    height: 40,
    marginRight: 10,
    width: 40,
  },
  containerModal: {
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    flex: 1,
    justifyContent: 'center',
  },
  editBtnContainer: {
    position: 'absolute',
    right: 0,
    top: -10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    color: Colors.secondary,
    fontSize: 14,
    paddingHorizontal: 6,
    paddingVertical: 1,
  },
  labelContainer: {
    borderColor: Colors.darkGray,
    borderRadius: 50,
    borderWidth: 0.4,
    marginRight: 10,
    transform: [{ translateY: -3 }],
  },
  line: {
    borderBottomWidth: 0.3,
    paddingBottom: 2,
  },
  sliderCloseButton: {
    alignSelf: 'center',
    marginRight: 10,
  },
  sliderContainer: {
    flexDirection: 'column',
    padding: 10,
  },
  sliderContent: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    bottom: 0,
    padding: 10,
    position: 'absolute',
    top: Dimensions.get('window').height / 3,
    width: '100%',
  },
  sliderInfo: {
    flexDirection: 'row',
  },
  sliderTitle: {
    fontSize: 16,
    fontWeight: '500',
    paddingBottom: 8,
  },
  textStyle: {
    fontSize: 14,
  },
});
