import React, { useContext } from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import s from 'app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';
import Proptypes from 'prop-types';

const Layout = ({ title, children }) => {
  const { theme } = useContext(ThemeContext);
  const { t } = useTranslation();

  return (
    <SafeAreaView
      style={
        theme === 'light'
          ? [s.lightContainer, styles.container]
          : [s.darkContainer, styles.container]
      }
    >
      <View style={styles.titleContainer}>
        <Text style={theme === 'light' ? s.lightTitle : s.darkTitle}>{t(title)}</Text>
      </View>
      {children}
    </SafeAreaView>
  );
};

Layout.propTypes = {
  children: Proptypes.any,
  title: Proptypes.string,
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  titleContainer: {
    paddingBottom: 5,
    paddingTop: 10,
  },
});

export default Layout;
