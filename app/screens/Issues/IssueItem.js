import React, { useContext } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import s, { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import Proptypes from 'prop-types';

import { ThemeContext } from '~app/store/theme-context';

const IssueItem = (props) => {
  const handlePress = () => {
    props.onPress(props.issue.id);
  };

  const { theme } = useContext(ThemeContext);

  return (
    <TouchableOpacity style={styles.item} onPress={handlePress} testID={`item${props.issue.id}`}>
      <View style={styles.content}>
        <Text
          testID={`title${props.issue.id}`}
          style={theme === 'light' ? [s.lightText, styles.title] : [s.darkText, styles.title]}
        >
          {props.issue.title}
        </Text>
        <Text style={theme === 'light' ? [s.lightText, styles.text] : [s.darkText, styles.text]}>
          <Text
            testID={`project${props.issue.id}`}
          >{`${props.issue.project?.fullPath}${props.issue.reference}`}</Text>
          {'\r'}
          <Text testID={`since${props.issue.id}`}>{`· created ${Utils.timeSince(
            props.issue.createdAt,
          )} by `}</Text>
          <Text testID={`name${props.issue.id}`} style={styles.boldText}>
            {props.issue.author.name}
          </Text>
        </Text>
        <View style={styles.container}>
          {props.issue.milestone?.title && (
            <View style={styles.milestone}>
              <Icons.clock color={theme === 'light' ? Colors.dark : Colors.secondary} />
              <Text> {'\r'}</Text>
              <Text
                testID={`milestone${props.issue.id}`}
                style={
                  theme === 'light'
                    ? [s.lightText, styles.milestoneTitle]
                    : [s.darkText, styles.milestoneTitle]
                }
              >
                {props.issue.milestone?.title}
              </Text>
            </View>
          )}
          {props.issue.labels.nodes?.map((label) => (
            <View key={label.id} style={[styles.labelContainer, { backgroundColor: label.color }]}>
              <Text testID={`label${props.issue.id}`} style={styles.label}>
                {label.title}
              </Text>
            </View>
          ))}
        </View>
      </View>
    </TouchableOpacity>
  );
};

IssueItem.propTypes = {
  issue: Proptypes.shape({
    author: Proptypes.shape({
      name: Proptypes.string.isRequired,
    }),
    createdAt: Proptypes.string.isRequired,
    id: Proptypes.string.isRequired,
    iid: Proptypes.string,
    labels: Proptypes.shape({
      nodes: Proptypes.array,
    }),
    milestone: Proptypes.shape({
      title: Proptypes.string,
    }),
    project: Proptypes.shape({
      fullPath: Proptypes.string,
    }),
    projectId: Proptypes.number,
    reference: Proptypes.string.isRequired,
    title: Proptypes.string.isRequired,
  }),
  onPress: Proptypes.func.isRequired,
};

const styles = StyleSheet.create({
  boldText: {
    fontWeight: '500',
  },
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
  },
  content: {
    marginBottom: 10,
    width: '100%',
  },
  item: {
    borderBottomWidth: 0.25,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  label: {
    color: Colors.secondary,
    fontSize: 14,
    paddingHorizontal: 6,
    paddingVertical: 1,
  },
  labelContainer: {
    alignItems: 'center',
    borderColor: Colors.darkGray,
    borderRadius: 50,
    borderWidth: 0.4,
    margin: 5,
    padding: 1.5,
    transform: [{ translateY: -7 }],
  },
  milestone: {
    flexDirection: 'row',
    marginRight: 10,
  },
  milestoneTitle: {
    transform: [{ translateY: -1 }],
  },
  text: {
    fontSize: 16,
    marginTop: 6,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default IssueItem;
