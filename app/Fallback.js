import React, { useContext } from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import { useTranslation } from 'react-i18next';
import s, { Colors } from '~app/constants/GlobalStyles';
import Logo from '~app/components/Logo';
import GButton from '~app/components/GButton';
import { ThemeContext } from '~app/store/theme-context';
import { AuthContext } from '~app/store/auth-context';

const Fallback = () => {
  const { t } = useTranslation();
  const { theme } = useContext(ThemeContext);
  const authCtx = useContext(AuthContext);

  const handlePress = function () {
    return authCtx.validateUser;
  };

  return (
    <SafeAreaView
      style={
        theme === 'light'
          ? [s.lightContainer, styles.container]
          : [s.darkContainer, styles.container]
      }
    >
      <Logo testID="logo" width="60" height="60" />
      <View style={styles.errorContainer}>
        <Text style={styles.titleStyle}>{t('Network Request Failed')}</Text>
        <Text style={styles.textStyle}>{t('Please retry again later')}</Text>

        <GButton
          label="Try Again"
          click={handlePress}
          fontSize={18}
          category="primary"
          variant="danger"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  errorContainer: {
    alignItems: 'center',
    borderColor: Colors.red500,
    borderRadius: 8,
    borderWidth: 1,
    marginTop: 100,
    paddingHorizontal: 25,
    paddingVertical: 35,
  },
  textStyle: {
    color: Colors.dark,
    fontSize: 18,
    paddingBottom: 20,
  },
  titleStyle: {
    color: Colors.red,
    fontSize: 22,
    fontWeight: '600',
    paddingBottom: 12,
  },
});

export default Fallback;
