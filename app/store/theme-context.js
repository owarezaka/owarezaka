import React, { createContext, useState } from 'react';
import Proptypes from 'prop-types';

const ThemeContext = createContext();

const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState('light');
  return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>;
};

ThemeProvider.propTypes = {
  children: Proptypes.any.isRequired,
};

export { ThemeContext, ThemeProvider };
