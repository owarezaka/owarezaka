import React, { createContext, useEffect, useReducer, useCallback } from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import Proptypes from 'prop-types';

export const AuthContext = createContext({
  isLoading: true,
  token: null,
  isSignOut: false,
  showError: false,
  onSignInHandler: (token) => {},
  validateUser: () => {},
  signOutHandler: () => {},
});

const authReducer = (state, action) => {
  if (action.type === 'NO_USER') {
    return { isLoading: false };
  }

  if (action.type === 'RESTORE_TOKEN') {
    return { token: action.token, isLoading: false, showError: false };
  }

  if (action.type === 'ADD_TOKEN') {
    return { token: action.token };
  }

  if (action.type === 'REMOVE_TOKEN') {
    return { token: null, isLoading: false, isSignOut: true, showError: false };
  }

  if (action.type === 'SERVER_ERROR') {
    return { isLoading: false, showError: true };
  }
};

function AuthContextProvider({ children }) {
  const [state, dispatch] = useReducer(authReducer, {
    isLoading: true,
    token: null,
    isSignOut: false,
    showError: false,
  });

  const onSignInHandler = function (userToken) {
    return dispatch({ type: 'ADD_TOKEN', token: userToken });
  };

  const signOutHandler = function () {
    UserStorage.logout();
    return dispatch({ type: 'REMOVE_TOKEN' });
  };

  const validateUser = useCallback(async function () {
    const user = await UserStorage.getUserInfo();
    if (!user) return dispatch({ type: 'NO_USER' });

    const status = await UserStorage.verifyToken(user.instanceUrl, user.userToken);

    if (status === 200) {
      return dispatch({ type: 'RESTORE_TOKEN', token: user.userToken });
    } else if (status === 401) {
      return signOutHandler();
    } else {
      return dispatch({ type: 'SERVER_ERROR' });
    }
  }, []);

  useEffect(() => {
    validateUser();
  }, [validateUser]);

  const value = {
    isLoading: state.isLoading,
    token: state.token,
    isSignOut: state.isSignOut,
    showError: state.showError,
    onSignInHandler: onSignInHandler,
    validateUser: validateUser,
    signOutHandler: signOutHandler,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export default AuthContextProvider;

AuthContextProvider.propTypes = {
  children: Proptypes.any,
};
