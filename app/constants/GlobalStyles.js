import { StyleSheet } from 'react-native';

export const Colors = {
  primary: '#1068bf',
  primary500: '#9dc7f1',
  primary600: '#6BB0F5',
  secondary: '#fff',
  dark: '#333',
  gray: '#71717a',
  lightGray: '#f0f0f0',
  lightGray2: '#dedede',
  lightGray3: '#e8e8e8',
  lightGray4: '#868686',
  darkGray: '#868686',
  darkGray2: '#666',
  darkGray3: '#1f1f1f',
  blue: '#292961',
  tintBlue: '#D1D1F0',
  green: '#217645',
  green700: '#91D4A8',
  red: '#C91C00',
  red500: '#eb919b',
  red600: '#f57f6c',
  yellow: '#9E5400',
  yellow700: '#e9be74',
  orange: '#fc6d26',
  orangeRed: '#e24329',
  orangeYellow: '#fca326',
  transparent: '#00000099',
  border: 'rgba(0, 3,51, 0.2)',
};

export default StyleSheet.create({
  darkBlueText: {
    color: Colors.primary600,
  },
  darkBorderStyle: {
    borderColor: Colors.lightGray3,
  },
  darkContainer: {
    backgroundColor: Colors.darkGray3,
    flex: 1,
  },
  darkHeaderBackgroundColor: {
    backgroundColor: Colors.dark,
  },
  darkLine: {
    borderBottomColor: Colors.lightGray3,
    borderBottomWidth: 0.3,
  },
  darkText: {
    color: Colors.secondary,
  },
  darkTitle: {
    borderBottomColor: Colors.secondary,
    borderBottomWidth: 0.3,
    color: Colors.secondary,
    fontSize: 22,
    fontWeight: '500',
    paddingBottom: 15,
    paddingLeft: 10,
  },
  lightBlueText: {
    color: Colors.primary,
  },
  lightBorderStyle: {
    borderColor: Colors.border,
  },
  lightContainer: {
    backgroundColor: Colors.lightGray,
    flex: 1,
  },
  lightHeaderBackgroundColor: {
    backgroundColor: Colors.blue,
  },
  lightLine: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
  },
  lightText: {
    color: Colors.dark,
  },
  lightTitle: {
    borderBottomColor: Colors.border,
    borderBottomWidth: 0.3,
    color: Colors.dark,
    fontSize: 22,
    fontWeight: '500',
    paddingBottom: 15,
    paddingLeft: 10,
  },
});
