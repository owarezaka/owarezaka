export default {
  saasUrl: 'https://gitlab.com',
  tokenPath:
    '/-/profile/personal_access_tokens?name=Owarezaka+Access+Token&scopes=api,read_user,read_api, read_repository,write_repository,read_registry,write_registry',
  about: 'https://about.gitlab.com/',
  docs: 'https://docs.gitlab.com/ee/',
  confirmation: 'https://gitlab.com/users/confirmation/new',
  mergeRequests:
    'https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/merge_request_workflow.md',
  issues:
    'https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md',
  issueTracker: 'https://gitlab.com/gitlab-org/gitlab/issues',
  issueWorkflow:
    'https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md#feature-proposals',
  forum: 'https://forum.gitlab.com/',
  university:
    'https://gitlab-org.gitlab.io/-/University/-/jobs/2243818/artifacts/public/index.html',
  cookbook: 'https://www.packtpub.com/product/gitlab-cookbook/9781783986842',
  recipes: 'https://gitlab.com/gitlab-org/gitlab-recipes',
  learnEnough: 'https://www.learnenough.com/git-tutorial',
  gitBook: 'http://git-scm.com/book/en/v2',
  gitlabOnGit: 'http://git-scm.com/book/en/v2/Git-on-the-Server-GitLab',
  gitForTeams: 'https://www.oreilly.com/library/view/git-for-teams/9781491911204/',
  collaboratingWithGit:
    'https://www.oreilly.com/library/view/collaborating-with-git/9781491916162/?code=WKGTVD',
  gitlabYoutubeChannel: 'https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg',
  licensingFaq: 'https://about.gitlab.com/pricing/licensing-faq/',
  liberaChat: 'https://web.libera.chat/#gitlab',
  gitlabGroup: 'https://groups.google.com/g/gitlabhq',
  gitter: 'https://gitter.im/gitlab/gitlab#',
  disclosure: 'https://about.gitlab.com/security/disclosure/',
  docsSecurity: 'https://docs.gitlab.com/ee/security/index.html',
  docsTrustAndSafety:
    'https://about.gitlab.com/handbook/engineering/security/security-operations/trustandsafety/#how-to-report-abuse',
  security: 'https://about.gitlab.com/security/',
  support: 'https://about.gitlab.com/support/',
  update: 'https://about.gitlab.com/update/',
  maintenance: 'https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/MAINTENANCE.md',
  submitFeedback: 'https://about.gitlab.com/submit-feedback/',
  contribute: 'https://about.gitlab.com/community/contribute/',
  owarezaka: 'https://gitlab.com/owarezaka/owarezaka',
};
