import * as Utils from '~app/utils/Utils';

exports.getTodos = async (instanceUrl, userToken) => {
  try {
    const response = await fetch(`${instanceUrl}/api/v4/todos`, {
      headers: {
        'PRIVATE-TOKEN': userToken,
        'Content-Type': 'application/json',
      },
    });

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, success: false };
  }
};

exports.markAsDone = async (instanceUrl, id, userToken) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/todos/${id}/mark_as_done`,
      {
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.status;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllProjects = async (userName, instanceUrl, userToken) => {
  const query = `query {
    projects (membership: true){
      nodes {
        fullPath
        id
        name
        namespace {
          name
        }
        group {
          id
          name
        }
        avatarUrl 
        description
        visibility 
        starCount 
        lastActivityAt
        pipelines {
          nodes {
            detailedStatus {
              icon
            }
          }
        }
      }
    }
    user(username: "${userName}") {
      groupMemberships{
          nodes {
          group {
            id
          }
          accessLevel {
            stringValue
          }
        }
      }
      projectMemberships{
        nodes {
          project {
            id
          }
          accessLevel {
            stringValue
          }
        }
      }
    }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();
    return data.data;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getOpenIssues = async (userName, instanceUrl, userToken) => {
  const query = `query {
    projects(membership: true) {
     nodes {
       id
       fullPath
       issues (state: opened, assigneeUsernames: "${userName}") {
         nodes {
           id
           iid
           title
           createdAt
           reference
           projectId
           labels {
             nodes {
               id
               title
               color
             }
           }
           milestone {
             title
           }
           author {
             name
           }
         }
       }
     }
   }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();

    return data.data.projects.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllAssignedIssues = async (instanceUrl, userToken, userName) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/issues?assignee_username=${userName}&state=opened&scope=all`,
      {
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, success: false };
  }
};

exports.getOpenIssueDetails = async (id, instanceUrl, userToken) => {
  const query = `query {
       issue (id: "${id}") {
        author {
          username
        }
        descriptionHtml
        dueDate
        assignees {
            nodes {
            id
            name
            username
            avatarUrl
         }
      }
   }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();

    return data.data.issue;
  } catch (error) {
    return { error, success: false };
  }
};

exports.createIssueNote = async (issueId, note, instanceUrl, userToken) => {
  const query = `mutation {
    createNote(input: { noteableId: "${issueId}",
      body: "${note}" }) {
      errors
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (json.errors) {
      return { message: json.errors[0].message };
    }

    return response.status;
  } catch (error) {
    return { error, success: false };
  }
};

exports.updateIssueNote = async (noteId, note, instanceUrl, userToken) => {
  const query = `mutation {
    updateNote(input: { id: "${noteId}",
      body: "${note}" }) {
      errors
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.updateNote.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.destroyNote = async (noteId, instanceUrl, userToken) => {
  const query = `mutation {
    destroyNote(input: { id: "${noteId}"}) {
      errors
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.destroyNote.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllIssueNotes = async (issueId, instanceUrl, userToken) => {
  const query = `query {
    issue(id: "${issueId}" ) {
      notes {
        nodes {
           id
           body
           bodyHtml
           createdAt
           author {
           name
           username
           avatarUrl
         }
           userPermissions {
             repositionNote
           }
        }
      }
  }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    return json.data.issue.notes.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllMergeRequestNotes = async (issueId, instanceUrl, userToken) => {
  const query = `query {
    mergeRequest(id: "${issueId}" ) {
     notes {
       nodes {
          id
          body
          bodyHtml
          createdAt
          author {
          name
          username
          avatarUrl
        }
          userPermissions {
            repositionNote
          }
          discussion {
            id
            resolvable
            resolved
          }
       }
     }
  }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    return json.data.mergeRequest.notes.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.closeIssue = async (instanceUrl, projectPath, iid, userToken) => {
  const query = ` mutation {
    updateIssue (input: {projectPath: "${projectPath}",
      iid: "${iid}", stateEvent: CLOSE}) {
      errors
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.updateIssue.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.closeMergeRequest = async (
  instanceUrl,
  projectPath,
  iid,
  userToken,
) => {
  const query = ` mutation {
    mergeRequestUpdate (input: {projectPath: "${projectPath}",
      iid: "${iid}", state: CLOSED}) {
      errors
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.mergeRequestUpdate.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.assignedMergeRequests = async (instanceUrl, userToken) => {
  const query = `query {
    currentUser {
     assignedMergeRequests (state: opened){
         nodes {
           id
           iid
           title
           createdAt
           author {
             name
           }
           milestone {
             title
           }
           project {
             fullPath
           }
           reference
           labels {
             nodes {
               id
               title
               color
             }
           }
       }
     }
    }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();

    return data.data.currentUser.assignedMergeRequests.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getMergeRequestsDetails = async (issueId, instanceUrl, userToken) => {
  const query = `query {
    mergeRequest(id: "${issueId}" ) {
      project {
        fullPath
      }
      author {
        id
        username
      }
      descriptionHtml
      assignees {
        nodes {
          id
          name
          username
          avatarUrl
        }
      }
      reviewers {
        nodes {
           id
           username
           name
           avatarUrl
         }
       }
      projectId
      targetBranch
      sourceBranch 
    	approvalsRequired
      approvedBy {
        nodes {
            id
            username
            avatarUrl
         }
      }
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();

    return data.data.mergeRequest;
  } catch (error) {
    return { error, success: false };
  }
};

exports.reviewRequestedMergeRequests = async (instanceUrl, userToken) => {
  const query = `query {
    currentUser {
      reviewRequestedMergeRequests (state: opened){
         nodes {
           id
           iid
           title
           createdAt
           author {
             name
           }
           milestone {
             title
           }
           project {
             fullPath
           }
           reference
           labels {
             nodes {
               id
               title
               color
             }
           }
         }
      }
    }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();
    return data.data.currentUser.reviewRequestedMergeRequests.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllGroups = async (instanceUrl, userToken) => {
  const query = `query {
    currentUser {
     groupMemberships {
         nodes {
           id
           group {
             name
             description
             avatarUrl
             fullPath
             visibility
           }
           accessLevel {
             integerValue
           }
       }
     }
    }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();
    return data.data.currentUser.groupMemberships.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getSubgroupsAndProjectsDetailsofGroup = async (
  fullPath,
  instanceUrl,
  userToken,
) => {
  const query = `query{
    group(fullPath: "${fullPath}"){
      descendantGroups {
        nodes {
          id
          name
          description
          fullPath
          avatarUrl
          visibility
        }
      }
      projects {
        nodes {
          id
          name
          description
          avatarUrl
          visibility
        }
      }
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllActivities = async (instanceUrl, userToken, page) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/events?scope=all&page=${page}&per_page=5`,
      {
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    const data = await response.json();
    return data;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getAllSnippets = async (instanceUrl, userToken) => {
  const query = `query {
    currentUser {
      snippets {
       nodes {
        id
        title
        description
        fileName
        visibilityLevel
        createdAt
        author {
          name
          avatarUrl
        }
       }
      }
    }
 }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const data = await response.json();

    if (!data.data.currentUser) {
      return { success: false };
    }

    return data.data.currentUser.snippets.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getSnippet = async (instanceUrl, id, userToken) => {
  try {
    const response = await fetch(`${instanceUrl}/api/v4/snippets/${id}/raw`, {
      headers: {
        'PRIVATE-TOKEN': userToken,
        'Content-Type': 'application/json',
      },
    });

    Utils.handleErrors(response);

    const data = await response.text();
    return data;
  } catch (error) {
    return null;
  }
};

exports.getUserStatus = async (instanceUrl, userToken) => {
  const query = `query{
    currentUser {
        name
        username
        avatarUrl
        status {
          emoji
          message
        }
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.currentUser) {
      return { success: false };
    }

    return json.data.currentUser;
  } catch (error) {
    return { error, success: false };
  }
};

exports.setUserStatus = async (instanceUrl, userToken, message) => {
  try {
    const response = await fetch(`${instanceUrl}/api/v4/user/status`, {
      method: 'PUT',
      headers: {
        'PRIVATE-TOKEN': userToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message }),
    });

    Utils.handleErrors(response);

    const data = await response.json();
    return data;
  } catch (error) {
    return { error, success: false };
  }
};

exports.removeUserStatus = async (instanceUrl, userToken) => {
  try {
    const response = await fetch(`${instanceUrl}/api/v4/user/status`, {
      method: 'PUT',
      headers: {
        'PRIVATE-TOKEN': userToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message: null }),
    });

    Utils.handleErrors(response);

    const data = await response.json();
    return data;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getMergeRequestChanges = async (
  instanceUrl,
  projectId,
  targetBranch,
  sourceBranch,
  userToken,
) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/projects/${projectId}/repository/compare?from=${targetBranch}&to=${sourceBranch}`,
      {
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, success: false };
  }
};

exports.approveMergeRequest = async (
  instanceUrl,
  projectId,
  iid,
  userToken,
) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/projects/${projectId}/merge_requests/${iid}/approve`,
      {
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, success: false };
  }
};

exports.revokeApproval = async (instanceUrl, projectId, iid, userToken) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/projects/${projectId}/merge_requests/${iid}/unapprove`,
      {
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, success: false };
  }
};

exports.checkVersion = async (instanceUrl, userToken) => {
  try {
    const response = await fetch(`${instanceUrl}/api/v4/version`, {
      headers: {
        'PRIVATE-TOKEN': userToken,
        'Content-Type': 'application/json',
      },
    });

    Utils.handleErrors(response);
    return response.json();
  } catch (error) {
    return { error, status: error.status, success: false };
  }
};

exports.checkMergeableState = async (issueId, instanceUrl, userToken) => {
  const query = `query {
    mergeRequest(id: "${issueId}" ) {
    	conflicts
      draft
    	mergeable
      mergeableDiscussionsState
      mergeWhenPipelineSucceeds
      autoMergeEnabled
      userPermissions {
        canMerge
      }
      diffHeadSha
    }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    return json.data.mergeRequest;
  } catch (error) {
    return null;
  }
};

exports.acceptMergeRequest = async ({
  iid,
  projectPath,
  diffHeadSha,
  instanceUrl,
  userToken,
  strategy,
}) => {
  const query = `mutation {
    mergeRequestAccept(input: {iid: "${iid}", 
      projectPath: "${projectPath}", 
      sha: "${diffHeadSha}", ${strategy}}) {
      errors
    }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.mergeRequestAccept.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.discussionToggleResolve = async (
  discussionId,
  boolean,
  instanceUrl,
  userToken,
) => {
  const query = `mutation {
    discussionToggleResolve(input: {id: "${discussionId}", resolve: ${boolean}} ) {
    	 errors
  }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.discussionToggleResolve.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.mergeRequestToggleAsReady = async (
  iid,
  boolean,
  projectPath,
  instanceUrl,
  userToken,
) => {
  const query = `mutation {
    mergeRequestSetDraft (input: {iid: "${iid}", draft: ${boolean}, 
    projectPath: "${projectPath}"}) {
    	errors
  }
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.mergeRequestSetDraft.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.getUserGlobalId = async function (userName, instanceUrl, userToken) {
  const query = `query{  
		user (username: "${userName}") { 
    	id
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    return json.data.user.id;
  } catch (error) {
    return { error, success: false };
  }
};

exports.updateMergeRequestReviewer = async (
  projectId,
  iid,
  reviewerId,
  instanceUrl,
  userToken,
) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/projects/${projectId}/merge_requests/${iid}?reviewer_ids=${reviewerId}`,
      {
        method: 'PUT',
        headers: {
          'PRIVATE-TOKEN': userToken,
          'Content-Type': 'application/json',
        },
      },
    );

    Utils.handleErrors(response);
    return response.status;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getReadMe = async (fullPath, instanceUrl, userToken) => {
  const query = `query {
    project(fullPath: "${fullPath}") {
      repository {
          blobs(paths: "README.md") {
            nodes {
              name
              rawBlob
          }
        }
      }
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();
    return json.data.project.repository.blobs.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.mergeRequestReviewerRereview = async (
  iid,
  projectPath,
  userId,
  instanceUrl,
  userToken,
) => {
  const query = `mutation {
    mergeRequestReviewerRereview(input: {iid: "${iid}", 
      projectPath: "${projectPath}", 
      userId: "${userId}", }) {
     errors
  }
} `;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });

    const json = await response.json();

    if (!json.data.mergeRequestReviewerRereview.errors.length) {
      return response.status;
    }
  } catch (error) {
    return { error, success: false };
  }
};

exports.getFileInfo = async (projectPath, treePath, instanceUrl, userToken) => {
  let file;
  let folder;

  const query = `query {
    project(fullPath: "${projectPath}") {
      repository {
        tree(path: "${treePath}") {
        blobs {
        nodes
        {
      name
      type
      path
      id
        }
      }
      trees{
        nodes{
          name
          type
          path
          id
        }
      }
    }
  }
}
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });
    const data = await response.json();

    if (!data.data.project.repository.tree) {
      return null;
    }

    file = data.data.project.repository.tree.blobs.nodes;
    folder = data.data.project.repository.tree.trees.nodes;
    return { file, folder };
  } catch (error) {
    return { error, success: false };
  }
};

exports.getFileContent = async (
  projectPath,
  blobPaths,
  instanceUrl,
  userToken,
) => {
  const query = `query {
    project(fullPath: "${projectPath}") {
      repository {
        blobs(paths: ["${blobPaths}"]){
          nodes{
            rawTextBlob
            rawSize
            language
          }
        }
  }
}
}`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });
    const data = await response.json();
    return data.data.project.repository.blobs.nodes;
  } catch (error) {
    return { error, success: false };
  }
};

exports.getProjectMembers = async (fullPath, instanceUrl, userToken) => {
  const query = `query {
    project(fullPath: "${fullPath}"){
	projectMembers {
    nodes {
     user{
       avatarUrl
      username
      id
    } 
    }
  }
    }
  }`;

  try {
    const response = await fetch(`${instanceUrl}/api/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: JSON.stringify({ query }),
    });
    const data = await response.json();
    return data.data.project.projectMembers.nodes;
  } catch (error) {
    return { error, success: false };
  }
};
