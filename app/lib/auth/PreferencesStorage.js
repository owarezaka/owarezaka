import EncryptedStorage from 'react-native-encrypted-storage';

exports.setPreferences = async darkMode => {
  const settings = {
    darkMode: darkMode,
  };

  await EncryptedStorage.setItem('userPrefs', JSON.stringify(settings));
};

exports.getPreferences = async () => {
  const sessionStr = await EncryptedStorage.getItem('userPrefs');

  let theme;

  if (sessionStr) {
    theme = JSON.parse(sessionStr);
  }

  return theme;
};
