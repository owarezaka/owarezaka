import EncryptedStorage from 'react-native-encrypted-storage';

exports.login = async (instanceUrl, userToken) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/personal_access_tokens`,
      {
        headers: {
          'PRIVATE-TOKEN': userToken,
        },
      },
    );

    if (response.ok) {
      const [{ user_id: id }] = await response.json();
      await setUserInfo(instanceUrl, id, userToken);
      return response.status;
    }
    throw response;
  } catch (err) {
    console.log(err);
    if (err.status === 401) {
      return err.status;
    }
    return -1;
  }
};

const setUserInfo = async (instanceUrl, id, userToken) => {
  const response = await fetch(`${instanceUrl}/api/v4/users?id=${id}`, {
    headers: {
      'PRIVATE-TOKEN': userToken,
    },
  });

  if (response.ok) {
    const [data] = await response.json();

    await EncryptedStorage.setItem(
      'userInfo',
      JSON.stringify({
        id: data.id,
        username: data.username,
        name: data.name,
        state: data.state,
        avatarUrl: data.avatar_url,
        instanceUrl,
        userToken,
      }),
    );
  } else {
    console.log(response);
  }
};

exports.getUserInfo = async () => {
  const sessionStr = await EncryptedStorage.getItem('userInfo');

  let user;

  if (sessionStr) {
    user = JSON.parse(sessionStr);
  }

  return user;
};

exports.verifyToken = async (instanceUrl, userToken) => {
  try {
    const response = await fetch(
      `${instanceUrl}/api/v4/personal_access_tokens`,
      {
        headers: {
          'PRIVATE-TOKEN': userToken,
        },
      },
    );

    if (response.ok) {
      return response.status;
    }
    throw response;
  } catch (error) {
    if (error.status === 401) {
      return error.status;
    }
    return { error, success: false };
  }
};

exports.logout = async () => {
  await EncryptedStorage.removeItem('userInfo');
};
