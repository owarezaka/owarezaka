import React, { useContext } from 'react';
import { TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeStack from '~app/navigation/HomeStack';
import IssuesScreen from '../screens/Issues/IssuesScreen';
import MergeRequestsScreen from '../screens/MergeRequests/MergeRequestsScreen';
import ReviewRequestsScreen from '../screens/MergeRequests/ReviewMergeRequestsScreen';
import { Colors } from '~app/constants/GlobalStyles';
import Icons from '~app/assets/Icons';
import { ThemeContext } from '~app/store/theme-context';
import ProfileStack from '~app/navigation/ProfileStack';
import SettingsScreen from '~app/screens/SettingsScreen';
import MainHeader from '~app/navigation/MainHeader';
import { useOrientation } from '~app/hooks/useOrientation';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const { theme } = useContext(ThemeContext);
  const orientation = useOrientation();
  const isLandscape = orientation === 'LANDSCAPE';

  return (
    <Tab.Navigator
      initialRouteName="HomeMenu"
      screenOptions={({ route, navigation }) => ({
        tabBarStyle: {
          backgroundColor: theme === 'light' ? Colors.blue : Colors.darkGray3,
          display: isLandscape ? 'none' : 'flex',
        },
        tabBarActiveTintColor: theme === 'light' ? Colors.blue : Colors.secondary,
        tabBarInactiveTintColor: theme === 'light' ? Colors.tintBlue : Colors.secondary,
        tabBarInactiveBackgroundColor: theme === 'light' ? Colors.blue : Colors.darkGray3,
        header: () => {
          return <MainHeader navigation={navigation} />;
        },
      })}
    >
      <Tab.Screen
        name="HomeMenu"
        component={HomeStack}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ size, focused }) => (
            <Icons.home
              color={
                focused
                  ? theme === 'light'
                    ? Colors.blue
                    : Colors.tintBlue
                  : theme === 'light'
                  ? Colors.tintBlue
                  : Colors.secondary
              }
            />
          ),
          tabBarButton: (props) => <TouchableOpacity {...props} />,
          tabBarActiveBackgroundColor: theme === 'light' ? Colors.lightGray : Colors.dark,
        }}
      />

      <Tab.Screen
        name="Issues"
        component={IssuesScreen}
        options={{
          tabBarLabel: 'Issues',
          tabBarIcon: ({ size, focused }) => (
            <Icons.issues
              color={
                focused
                  ? theme === 'light'
                    ? Colors.blue
                    : Colors.tintBlue
                  : theme === 'light'
                  ? Colors.tintBlue
                  : Colors.secondary
              }
            />
          ),
          tabBarButton: (props) => <TouchableOpacity {...props} />,
          tabBarActiveBackgroundColor: theme === 'light' ? Colors.lightGray : Colors.dark,
        }}
      />

      <Tab.Screen
        name="MRs"
        component={MergeRequestsScreen}
        options={{
          tabBarLabel: 'MRs',
          tabBarIcon: ({ size, focused }) => (
            <Icons.mergeRequest
              color={
                focused
                  ? theme === 'light'
                    ? Colors.blue
                    : Colors.tintBlue
                  : theme === 'light'
                  ? Colors.tintBlue
                  : Colors.secondary
              }
            />
          ),
          tabBarButton: (props) => <TouchableOpacity {...props} />,
          tabBarActiveBackgroundColor: theme === 'light' ? Colors.lightGray : Colors.dark,
        }}
      />

      <Tab.Screen
        name="Review MRs"
        component={ReviewRequestsScreen}
        options={{
          tabBarLabel: 'Review MRs',
          tabBarIcon: ({ size, focused }) => (
            <Icons.mergeRequest
              color={
                focused
                  ? theme === 'light'
                    ? Colors.blue
                    : Colors.tintBlue
                  : theme === 'light'
                  ? Colors.tintBlue
                  : Colors.secondary
              }
            />
          ),
          tabBarButton: (props) => <TouchableOpacity {...props} />,
          tabBarActiveBackgroundColor: theme === 'light' ? Colors.lightGray : Colors.dark,
        }}
      />

      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ size, focused }) => (
            <Icons.profile
              color={
                focused
                  ? theme === 'light'
                    ? Colors.blue
                    : Colors.tintBlue
                  : theme === 'light'
                  ? Colors.tintBlue
                  : Colors.secondary
              }
              size={20}
            />
          ),
          tabBarButton: (props) => <TouchableOpacity {...props} />,
          tabBarActiveBackgroundColor: theme === 'light' ? Colors.lightGray : Colors.dark,
        }}
      />

      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          tabBarLabel: '',
          tabBarIcon: () => null,
          tabBarButton: () => null,
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
