import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HelpMenuScreen from '~app/screens/HelpMenuScreen';
import HelpScreen from '~app/screens/HelpScreen';
import SupportScreen from '~app/screens/SupportScreen';
import { Colors } from '~app/constants/GlobalStyles';

const Stack = createStackNavigator();

const HelpStack = () => {
  const headerStyle = {
    backgroundColor: Colors.lightGray,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.border,
  };

  return (
    <Stack.Navigator
      initialRouteName="HelpMenu"
      screenOptions={{
        headerBackTitleVisible: false,
      }}
    >
      <Stack.Screen name="HelpMenu" component={HelpMenuScreen} options={{ header: () => null }} />
      <Stack.Screen
        name="HelpScreen"
        component={HelpScreen}
        options={{
          headerStyle,
          headerTitle: 'Help',
        }}
      />
      <Stack.Screen
        name="Support"
        component={SupportScreen}
        options={{
          headerStyle,
        }}
      />
    </Stack.Navigator>
  );
};

export default HelpStack;
