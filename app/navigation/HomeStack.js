import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '~app/screens/HomeScreen';
import ProjectsScreen from '~app/screens/Projects/ProjectsScreen';
import ToDoListScreen from '~app/screens/ToDoList/ToDoListScreen';
import GroupsScreen from '~app/screens/Groups/GroupsScreen';
import SnippetsScreen from '~app/screens/Snippets/SnippetsScreen';
import ActivityScreen from '~app/screens/Activity/ActivityScreen';
import HelpScreen from '~app/screens/HelpScreen';
import { Colors } from '~app/constants/GlobalStyles';
import { ThemeContext } from '~app/store/theme-context';

const Stack = createStackNavigator();

const HomeStack = () => {
  const { theme } = useContext(ThemeContext);

  const headerStyle = {
    backgroundColor: theme === 'light' ? Colors.lightGray : Colors.darkGray2,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.border,
  };

  const headerTintColor = theme === 'light' ? Colors.dark : Colors.secondary;

  return (
    <Stack.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{ headerBackTitleVisible: false }}
    >
      <Stack.Screen name="HomeScreen" component={HomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Projects" component={ProjectsScreen} options={{ headerShown: false }} />
      <Stack.Screen
        name="ToDoList"
        component={ToDoListScreen}
        options={{ headerTitle: 'To-Do List', headerStyle, headerTintColor }}
      />
      <Stack.Screen name="Groups" component={GroupsScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Snippets" component={SnippetsScreen} options={{ header: () => null }} />
      <Stack.Screen
        name="Activity"
        component={ActivityScreen}
        options={{ headerTitle: 'Activity', headerStyle, headerTintColor }}
      />
      <Stack.Screen
        name="Help"
        component={HelpScreen}
        options={{ headerTitle: 'Help', headerStyle, headerTintColor }}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
