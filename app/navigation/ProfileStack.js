import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeContext } from '~app/store/theme-context';

import ProfileScreen from '~app/screens/Profile/ProfileScreen';
import SetStatusScreen from '~app/screens/Profile/SetStatus';
import { Colors } from '~app/constants/GlobalStyles';

const Stack = createStackNavigator();

const ProfileStack = () => {
  const { theme } = useContext(ThemeContext);

  const headerStyle = {
    backgroundColor: theme === 'light' ? Colors.lightGray : Colors.darkGray2,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.border,
  };

  const headerTintColor = theme === 'light' ? Colors.dark : Colors.secondary;

  return (
    <Stack.Navigator
      initialRouteName="ProfileScreen"
      screenOptions={{ headerStyle, headerTintColor }}
    >
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="SetStatusScreen"
        component={SetStatusScreen}
        options={{ headerBackTitleVisible: false }}
      />
    </Stack.Navigator>
  );
};

export default ProfileStack;
