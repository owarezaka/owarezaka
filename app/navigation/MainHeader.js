import React, { useContext } from 'react';
import { StyleSheet, View, Pressable, SafeAreaView } from 'react-native';
import Logo from '~app/components/Logo';
import Icons from '~app/assets/Icons';
import s, { Colors } from '~app/constants/GlobalStyles';
import Proptypes from 'prop-types';
import { ThemeContext } from '~app/store/theme-context';

const MainHeader = (props) => {
  const { theme } = useContext(ThemeContext);

  return (
    <SafeAreaView>
      <View
        style={
          theme === 'light'
            ? [s.lightHeaderBackgroundColor, styles.header]
            : [s.darkHeaderBackgroundColor, styles.header]
        }
      >
        <View style={styles.btnContainer}>
          <Pressable
            onPress={() => {
              props.navigation.navigate('Settings');
            }}
            testID="button"
            style={({ pressed }) => pressed && { backgroundColor: Colors.border }}
          >
            <View style={styles.btn}>
              <Icons.settings size={20} />
            </View>
          </Pressable>
        </View>

        <View style={styles.logo}>
          <Logo width="36" height="36" testID="logo" />
        </View>
      </View>
    </SafeAreaView>
  );
};

MainHeader.propTypes = {
  navigation: Proptypes.shape({
    navigate: Proptypes.func.isRequired,
  }),
};

export default MainHeader;

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    height: '100%',
    justifyContent: 'center',
    width: '100%',
  },
  btnContainer: {
    height: '100%',
    left: 0,
    position: 'absolute',
    width: 65,
  },
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    justifyContent: 'center',
    width: '100%',
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
