import Svg, { Path, G } from 'react-native-svg';
import React from 'react';
import { View } from 'react-native';
import { Colors } from '~app/constants/GlobalStyles';

exports.checkCircle = function ({ style, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0Zm-4.22-1.72a.75.75 0 0 0-1.06-1.06L6.75 9.19 5.53 7.97a.75.75 0 0 0-1.06 1.06l1.75 1.75a.75.75 0 0 0 1.06 0l4.5-4.5Z"
          fill={Colors.red}
        />
      </Svg>
    </View>
  );
};

exports.comment = function ({ style, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M13 12H7l-2.5 2.5L3 16v-4a3 3 0 0 1-3-3V5a3 3 0 0 1 3-3h10a3 3 0 0 1 3 3v4a3 3 0 0 1-3 3Zm-8.5 0v.379l1.44-1.44.439-.439H13A1.5 1.5 0 0 0 14.5 9V5A1.5 1.5 0 0 0 13 3.5H3A1.5 1.5 0 0 0 1.5 5v4A1.5 1.5 0 0 0 3 10.5h1.5V12Z"
          fill={Colors.primary}
        />
      </Svg>
    </View>
  );
};

exports.commit = function ({ style, color, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8 10.5a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM8 12c1.953 0 3.579-1.4 3.93-3.25h3.32a.75.75 0 0 0 0-1.5h-3.32a4.001 4.001 0 0 0-7.86 0H.75a.75.75 0 0 0 0 1.5h3.32A4.001 4.001 0 0 0 8 12Z"
          fill={color ?? Colors.darkGray2}
        />
      </Svg>
    </View>
  );
};

exports.fork = function ({ style, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M5.5 3.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-.25 2.386a2.501 2.501 0 1 0-1.5 0v.364a2.5 2.5 0 0 0 2.5 2.5 1 1 0 0 1 1 1v.364a2.501 2.501 0 1 0 1.5 0V9.75a1 1 0 0 1 1-1 2.5 2.5 0 0 0 2.5-2.5v-.364a2.501 2.501 0 1 0-1.5 0v.364a1 1 0 0 1-1 1c-.681 0-1.3.273-1.75.715a2.492 2.492 0 0 0-1.75-.715 1 1 0 0 1-1-1v-.364ZM11.5 4.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm-3.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z"
          fill={Colors.primary}
        />
      </Svg>
    </View>
  );
};

exports.remove = function ({ style, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="16" height="16" fill={Colors.darkGray2} viewBox="0 0 16 16">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M5.75 3V1.5h4.5V3h-4.5zm-1.5 0V1a1 1 0 0 1 1-1h5.5a1 1 0 0 1 1 1v2h2.5a.75.75 0 0 1 0 1.5h-.365l-.743 9.653A2 2 0 0 1 11.148 16H4.852a2 2 0 0 1-1.994-1.847L2.115 4.5H1.75a.75.75 0 0 1 0-1.5h2.5zm-.63 1.5h8.76l-.734 9.538a.5.5 0 0 1-.498.462H4.852a.5.5 0 0 1-.498-.462L3.62 4.5z"
        />
      </Svg>
    </View>
  );
};

exports.statusOpen = function ({ style, color, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
        <Path d="M0 7c0-3.866 3.142-7 7-7 3.866 0 7 3.142 7 7 0 3.866-3.142 7-7 7-3.866 0-7-3.142-7-7z" />
        <Path
          d="M1 7c0 3.309 2.69 6 6 6 3.309 0 6-2.69 6-6 0-3.309-2.69-6-6-6-3.309 0-6 2.69-6 6z"
          stroke={Colors.green}
          fill={color}
        />
        <Path
          d="M7 9.219a2.218 2.218 0 1 0 0-4.436A2.218 2.218 0 0 0 7 9.22zm0 1.12a3.338 3.338 0 1 1 0-6.676 3.338 3.338 0 0 1 0 6.676z"
          fill={Colors.green}
          strokeWidth=".5"
        />
      </Svg>
    </View>
  );
};

exports.home = function ({ color }) {
  return (
    <View>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8.38 1.353 8 1.131l-.38.222-7.25 4.25a.75.75 0 0 0 .76 1.294l.87-.51V14h12V6.387l.87.51a.75.75 0 1 0 .76-1.294l-7.25-4.25Zm4.12 4.154L8 2.87 3.5 5.507V12.5H6V8h4v4.5h2.5V5.507ZM8.5 9.5v3h-1v-3h1Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.issues = function ({ color }) {
  return (
    <View>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M3 2.5h6a.5.5 0 0 1 .5.5v10a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5ZM1 3a2 2 0 0 1 2-2h6a2 2 0 0 1 1.97 1.658l2.913 1.516a1.75 1.75 0 0 1 .744 2.36l-3.878 7.45a.753.753 0 0 1-.098.145c-.36.526-.965.871-1.651.871H3a2 2 0 0 1-2-2V3Zm10 7.254 2.297-4.413a.25.25 0 0 0-.106-.337L11 4.364v5.89Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.mergeRequest = function ({ color }) {
  return (
    <View>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M10.34 1.22a.75.75 0 0 0-1.06 0L7.53 2.97 7 3.5l.53.53 1.75 1.75a.75.75 0 1 0 1.06-1.06l-.47-.47h.63c.69 0 1.25.56 1.25 1.25v4.614a2.501 2.501 0 1 0 1.5 0V5.5a2.75 2.75 0 0 0-2.75-2.75h-.63l.47-.47a.75.75 0 0 0 0-1.06ZM13.5 12.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-9 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm1.5 0a2.5 2.5 0 1 1-3.25-2.386V5.886a2.501 2.501 0 1 1 1.5 0v4.228A2.501 2.501 0 0 1 6 12.5Zm-1.5-9a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.question = function ({ color, style, size }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM4.927 4.99c-.285.429-.427.853-.427 1.27 0 .203.09.392.27.566.18.174.4.26.661.26.443 0 .744-.248.903-.746.168-.475.373-.835.616-1.08.243-.244.62-.366 1.134-.366.439 0 .797.12 1.075.363.277.242.416.54.416.892a.97.97 0 0 1-.136.502 1.91 1.91 0 0 1-.336.419 14.35 14.35 0 0 1-.648.558c-.34.282-.611.525-.812.73-.2.205-.362.443-.483.713-.322 1.245 1.35 1.345 1.736.456.047-.086.118-.18.213-.284.096-.103.223-.223.382-.36a41.14 41.14 0 0 0 1.194-1.034c.221-.204.412-.448.573-.73a1.95 1.95 0 0 0 .242-.984c0-.475-.141-.915-.424-1.32-.282-.406-.682-.726-1.2-.962-.518-.235-1.115-.353-1.792-.353-.728 0-1.365.14-1.911.423-.546.282-.961.637-1.246 1.066Zm2.14 7.08a1 1 0 1 0 2 0 1 1 0 0 0-2 0Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.project = function ({ style, color, size, testID }) {
  return (
    <View style={style} testID={testID}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="m9.5 14.5-6-2.5V4l6-2.5v13Zm-6.885-1.244A1 1 0 0 1 2 12.333V3.667a1 1 0 0 1 .615-.923L8.923.115A1.5 1.5 0 0 1 11 1.5V2h1.25c.966 0 1.75.783 1.75 1.75v8.5A1.75 1.75 0 0 1 12.25 14H11v.5a1.5 1.5 0 0 1-2.077 1.385l-6.308-2.629ZM11 12.5h1.25a.25.25 0 0 0 .25-.25v-8.5a.25.25 0 0 0-.25-.25H11v9Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.todoDone = function ({ style, color }) {
  return (
    <View style={style}>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M3 13.5a.5.5 0 0 1-.5-.5V3a.5.5 0 0 1 .5-.5h9.25a.75.75 0 0 0 0-1.5H3a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V9.75a.75.75 0 0 0-1.5 0V13a.5.5 0 0 1-.5.5H3Zm12.78-8.82a.75.75 0 0 0-1.06-1.06L9.162 9.177 7.289 7.241a.75.75 0 1 0-1.078 1.043l2.403 2.484a.75.75 0 0 0 1.07.01L15.78 4.68Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.group = function ({ style, color }) {
  return (
    <View style={style}>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Zm1.5 0a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm4 5.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm0 1.5a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-7 2.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm0 1.5a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.snippet = function ({ style, color }) {
  return (
    <View style={style}>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M3.625.1A.75.75 0 0 1 4.65.375L8 6.177 11.35.375a.75.75 0 1 1 1.3.75L8.864 7.677l1.97 3.412A2.503 2.503 0 0 1 14 13.5a2.5 2.5 0 1 1-4.425-1.595L7.999 9.176l-.26.45a.75.75 0 0 1-1.298-.751l.692-1.199L3.35 1.125A.75.75 0 0 1 3.625.1ZM5.5 13.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm1.5 0a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0Zm5.5 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.history = function ({ style, color }) {
  return (
    <View style={style}>
      <Svg width="20" height="20" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.806.665A8 8 0 1 1 .612 11.07a.75.75 0 1 1 1.385-.575A6.5 6.5 0 1 0 2.523 4.5H4.25a.75.75 0 0 1 0 1.5H0V1.75a.75.75 0 0 1 1.5 0v1.586A8 8 0 0 1 4.806.666ZM8 3a.75.75 0 0 1 .75.75v3.94l2.034 2.034a.75.75 0 1 1-1.06 1.06L7.47 8.53l-.22-.22V3.75A.75.75 0 0 1 8 3Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.star = function ({ style, color }) {
  return (
    <View style={style}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          d="M7.454 1.694a.591.591 0 0 1 1.092 0l1.585 3.81a.25.25 0 0 0 .21.154l4.114.33a.591.591 0 0 1 .338 1.038L11.658 9.71a.25.25 0 0 0-.08.247l.957 4.015a.591.591 0 0 1-.883.641l-3.522-2.15a.25.25 0 0 0-.26 0l-3.522 2.15a.591.591 0 0 1-.883-.641l.957-4.015a.25.25 0 0 0-.08-.247L1.207 7.026a.591.591 0 0 1 .338-1.038l4.113-.33a.25.25 0 0 0 .211-.153l1.585-3.81Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.earth = function ({ testID, color }) {
  return (
    <View testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8 14.5c.23 0 .843-.226 1.487-1.514.306-.612.563-1.37.742-2.236H5.771c.179.866.436 1.624.742 2.236C7.157 14.274 7.77 14.5 8 14.5ZM5.554 9.25a14.444 14.444 0 0 1 0-2.5h4.892a14.452 14.452 0 0 1 0 2.5H5.554Zm6.203 1.5c-.224 1.224-.593 2.308-1.066 3.168a6.525 6.525 0 0 0 3.2-3.168h-2.134Zm2.623-1.5h-2.43a16.019 16.019 0 0 0 0-2.5h2.429a6.533 6.533 0 0 1 0 2.5Zm-10.331 0H1.62a6.533 6.533 0 0 1 0-2.5h2.43a15.994 15.994 0 0 0 0 2.5Zm-1.94 1.5h2.134c.224 1.224.593 2.308 1.066 3.168a6.525 6.525 0 0 1-3.2-3.168Zm3.662-5.5h4.458c-.179-.866-.436-1.624-.742-2.236C8.843 1.726 8.23 1.5 8 1.5c-.23 0-.843.226-1.487 1.514-.306.612-.563 1.37-.742 2.236Zm5.986 0h2.134a6.526 6.526 0 0 0-3.2-3.168c.473.86.842 1.944 1.066 3.168ZM5.31 2.082c-.473.86-.842 1.944-1.066 3.168H2.109a6.525 6.525 0 0 1 3.2-3.168ZM8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.lock = function ({ color }) {
  return (
    <View>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4 5a4 4 0 1 1 8 0v1h1a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1V5Zm6.5 0v1h-5V5a2.5 2.5 0 0 1 5 0Zm-7 2.5v6h9v-6h-9ZM9 12V9H7v3h2Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.statusWarning = function ({ testID, style, size, color, backgroundColor }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 14}
        height={size ?? 14}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg"
      >
        <G fillRule="evenodd">
          <Path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z" fill={color ?? Colors.orange} />
          <Path
            d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
            fill={backgroundColor ?? Colors.lightGray}
          />
          <Path
            d="M6 3.5c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v4c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-4m0 6c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v1c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-1"
            fill={color ?? Colors.orange}
          />
        </G>
      </Svg>
    </View>
  );
};

exports.statusSuccess = function ({ testID, style, size, color, backgroundColor }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <G fillRule="evenodd">
          <Path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z" fill={color ?? Colors.green} />
          <Path
            d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
            fill={backgroundColor ?? Colors.lightGray}
          />
          <Path
            d="M6.278 7.697L5.045 6.464a.296.296 0 0 0-.42-.002l-.613.614a.298.298 0 0 0 .002.42l1.91 1.909a.5.5 0 0 0 .703.005l.265-.265L9.997 6.04a.291.291 0 0 0-.009-.408l-.614-.614a.29.29 0 0 0-.408-.009L6.278 7.697z"
            fill={color ?? Colors.green}
          />
        </G>
      </Svg>
    </View>
  );
};

exports.statusFailed = function ({ style, size, color, backgroundColor }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 14}
        height={size ?? 14}
        viewBox="0 0 14 14"
        xmlns="http://www.w3.org/2000/svg"
      >
        <G fillRule="evenodd">
          <Path d="M0 7a7 7 0 1 1 14 0A7 7 0 0 1 0 7z" fill={color ?? Colors.red} />
          <Path
            d="M13 7A6 6 0 1 0 1 7a6 6 0 0 0 12 0z"
            fill={backgroundColor ?? Colors.lightGray}
          />
          <Path
            d="M7 5.969L5.599 4.568a.29.29 0 0 0-.413.004l-.614.614a.294.294 0 0 0-.004.413L5.968 7l-1.4 1.401a.29.29 0 0 0 .004.413l.614.614c.113.114.3.117.413.004L7 8.032l1.401 1.4a.29.29 0 0 0 .413-.004l.614-.614a.294.294 0 0 0 .004-.413L8.032 7l1.4-1.401a.29.29 0 0 0-.004-.413l-.614-.614a.294.294 0 0 0-.413-.004L7 5.968z"
            fill={color ?? Colors.red}
          />
        </G>
      </Svg>
    </View>
  );
};

exports.clock = function ({ color }) {
  return (
    <View>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM8.75 3.75a.75.75 0 0 0-1.5 0v4.56l.22.22 2.254 2.254a.75.75 0 1 0 1.06-1.06L8.75 7.689V3.75Z"
          fill={color}
        />
      </Svg>
    </View>
  );
};

exports.angleRight = function ({ testID, style, degree, color }) {
  return (
    <View testID={testID} style={[style, { transform: [{ rotate: degree ?? '0deg' }] }]}>
      <Svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
        <Path
          fill={color ?? Colors.dark}
          fillRule="evenodd"
          d="M5.29289,3.70711 C4.90237,3.31658 4.90237,2.68342 5.29289,2.29289 C5.68342,1.90237 6.31658,1.90237 6.70711,2.29289 L11.7071,7.29289 C12.0976,7.68342 12.0976,8.31658 11.7071,8.70711 L6.70711,13.7071 C6.31658,14.0976 5.68342,14.0976 5.29289,13.7071 C4.90237,13.3166 4.90237,12.6834 5.29289,12.2929 L9.58579,8 L5.29289,3.70711 Z"
        />
      </Svg>
    </View>
  );
};

exports.angleDoubleRight = function ({ testID, style, degree, color }) {
  return (
    <View testID={testID} style={[style, { transform: [{ rotate: degree ?? '0deg' }] }]}>
      <Svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
        <Path
          fill={color ?? Colors.dark}
          d="M3.70711,2.29289 L8.70711,7.29289 C9.09763,7.68342 9.09763,8.31658 8.70711,8.70711 L3.70711,13.7071 C3.31658,14.0976 2.68342,14.0976 2.29289,13.7071 C1.90237,13.3166 1.90237,12.6834 2.29289,12.2929 L6.58579,8 L2.29289,3.70711 C1.90237,3.31658 1.90237,2.68342 2.29289,2.29289 C2.68342,1.90237 3.31658,1.90237 3.70711,2.29289 Z M8.70711,2.29289 L13.7071,7.29289 C14.0976,7.68342 14.0976,8.31658 13.7071,8.70711 L8.70711,13.7071 C8.31658,14.0976 7.68342,14.0976 7.29289,13.7071 C6.90237,13.3166 6.90237,12.6834 7.29289,12.2929 L11.5858,8 L7.29289,3.70711 C6.90237,3.31658 6.90237,2.68342 7.29289,2.29289 C7.68342,1.90237 8.31658,1.90237 8.70711,2.29289 Z"
        />
      </Svg>
    </View>
  );
};

exports.commentDots = function ({ testID, color }) {
  return (
    <View testID={testID}>
      <Svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.5 14.5 7 12h6a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v4a3 3 0 0 0 3 3v4l1.5-1.5Zm0-2.121V10.5H3A1.5 1.5 0 0 1 1.5 9V5A1.5 1.5 0 0 1 3 3.5h10A1.5 1.5 0 0 1 14.5 5v4a1.5 1.5 0 0 1-1.5 1.5H6.379l-.44.44L4.5 12.378ZM11 8a1 1 0 1 0 0-2 1 1 0 0 0 0 2ZM9 7a1 1 0 1 1-2 0 1 1 0 0 1 2 0ZM5 8a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.close = function ({ testID, size, color }) {
  return (
    <View testID={testID}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.28 3.22a.75.75 0 0 0-1.06 1.06L6.94 8l-3.72 3.72a.75.75 0 1 0 1.06 1.06L8 9.06l3.72 3.72a.75.75 0 1 0 1.06-1.06L9.06 8l3.72-3.72a.75.75 0 0 0-1.06-1.06L8 6.94 4.28 3.22Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.share = function ({ size, color }) {
  return (
    <View>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12.5 4.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm0 1.5a2.5 2.5 0 1 0-2.469-2.104L5.298 6.263a2.5 2.5 0 1 0 0 3.475l4.733 2.366a2.5 2.5 0 1 0 .671-1.341L5.97 8.396a2.519 2.519 0 0 0 0-.792l4.733-2.367c.455.47 1.092.763 1.798.763Zm1 6.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0ZM4.5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.hamburger = function ({ size }) {
  return (
    <View>
      <Svg
        width={size ?? 20}
        height={size ?? 20}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 3.75A.75.75 0 0 1 .75 3h14.5a.75.75 0 0 1 0 1.5H.75A.75.75 0 0 1 0 3.75ZM0 8a.75.75 0 0 1 .75-.75h14.5a.75.75 0 0 1 0 1.5H.75A.75.75 0 0 1 0 8Zm.75 3.5a.75.75 0 0 0 0 1.5h14.5a.75.75 0 0 0 0-1.5H.75Z"
          fill={Colors.tintBlue}
        />
      </Svg>
    </View>
  );
};

exports.settings = function ({ testID, style, size }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="m6.43 1.168-.74-.123.74.123Zm-.156.939.74.123-.74-.123Zm-.173.2.237.711-.237-.711ZM4.02 3.509l-.498-.56.498.56Zm-.26.05.263-.702-.263.702Zm-.893-.334.263-.703-.263.703Zm-.608.218-.65-.375.65.375ZM1.183 5.307l-.65-.375.65.375Zm.115.636.477-.579-.477.58Zm.736.606-.477.579.477-.58Zm.086.25.735.149-.735-.15Zm0 2.403.735-.15-.735.15Zm-.086.25.476.578-.476-.579Zm-.736.605-.476-.58.476.58Zm-.115.636-.65.375.65-.375Zm1.077 1.864.65-.375-.65.375Zm.608.218.263.703-.263-.703Zm.893-.334-.263-.703.263.703Zm.26.05-.498.56.498-.56Zm2.08 1.202.237-.711-.237.711Zm.173.2.74-.123-.74.123Zm.156.94.74-.124-.74.123Zm3.14 0-.74-.124.74.123Zm.156-.94.74.123-.74-.123Zm.173-.2.238.712-.238-.712Zm2.08-1.202-.497-.561.497.56Zm.26-.05.263-.703-.263.703Zm.893.334-.263.703.263-.703Zm.609-.218-.65-.375.65.375Zm1.076-1.864.65.375-.65-.375Zm-.115-.636-.477.579.477-.58Zm-.736-.606-.476.58.476-.58Zm-.086-.25.735.15-.735-.15Zm0-2.403.735-.15-.735.15Zm.086-.25-.476-.578.476.579Zm.736-.605-.477-.579.477.58Zm.115-.636.65-.375-.65.375ZM13.74 3.443l-.65.375.65-.375Zm-.609-.218-.263-.703.263.703Zm-.893.334-.263-.702.263.702Zm-.26-.05-.497.561.497-.56ZM9.9 2.307l-.237.711.237-.711Zm-.173-.2.74-.123-.74.123Zm-.156-.94-.74.124.74-.123ZM6.924 1.5h2.152V0H6.924v1.5Zm.246-.209a.25.25 0 0 1-.246.209V0A1.25 1.25 0 0 0 5.69 1.044l1.48.247Zm-.156.94.156-.94-1.48-.246-.156.939 1.48.246Zm-.676.787c.343-.114.613-.41.676-.788l-1.48-.246a.494.494 0 0 1 .33-.389l.474 1.423ZM4.518 4.07a5.244 5.244 0 0 1 1.82-1.052l-.474-1.423a6.744 6.744 0 0 0-2.34 1.353l.994 1.122Zm-1.02.192c.36.134.75.048 1.02-.192l-.995-1.122a.494.494 0 0 1 .501-.091l-.526 1.405Zm-.893-.335.893.335.526-1.405-.893-.335-.526 1.405Zm.304-.11a.25.25 0 0 1-.304.11l.526-1.405a1.25 1.25 0 0 0-1.521.546l1.3.75ZM1.833 5.683l1.076-1.864-1.299-.75L.534 4.932l1.299.75Zm-.058-.318a.25.25 0 0 1 .058.318l-1.3-.75a1.25 1.25 0 0 0 .289 1.59l.953-1.158Zm.735.606-.735-.606-.953 1.158.735.606.953-1.158Zm.345.978a1.006 1.006 0 0 0-.345-.978l-.953 1.158a.494.494 0 0 1-.172-.48l1.47.3ZM2.75 8c0-.361.036-.713.105-1.052l-1.47-.3c-.088.438-.135.89-.135 1.352h1.5Zm.105 1.052A5.277 5.277 0 0 1 2.75 8h-1.5c0 .462.047.914.135 1.351l1.47-.299Zm-.345.978c.296-.243.417-.624.345-.978l-1.47.3a.494.494 0 0 1 .172-.48l.953 1.158Zm-.735.606.735-.606-.953-1.158-.735.606.953 1.158Zm.058-.318a.25.25 0 0 1-.058.318L.822 9.478a1.25 1.25 0 0 0-.288 1.59l1.299-.75Zm1.076 1.864-1.076-1.864-1.3.75 1.077 1.864 1.3-.75Zm-.304-.109a.25.25 0 0 1 .304.11l-1.299.75c.306.528.949.76 1.521.545l-.526-1.405Zm.893-.335-.893.335.526 1.405.893-.335-.526-1.405Zm1.02.192a1.006 1.006 0 0 0-1.02-.191l.526 1.404a.494.494 0 0 1-.5-.091l.994-1.122Zm1.82 1.052a5.243 5.243 0 0 1-1.82-1.052l-.995 1.122a6.745 6.745 0 0 0 2.34 1.353l.475-1.423Zm.676.788a1.006 1.006 0 0 0-.676-.788l-.474 1.423a.494.494 0 0 1-.33-.388l1.48-.247Zm.156.939-.156-.94-1.48.248.157.939 1.48-.247Zm-.246-.209a.25.25 0 0 1 .246.209l-1.48.247A1.25 1.25 0 0 0 6.925 16v-1.5Zm2.152 0H6.924V16h2.152v-1.5Zm-.246.209a.25.25 0 0 1 .246-.209V16a1.25 1.25 0 0 0 1.233-1.044l-1.48-.247Zm.156-.94-.156.94 1.48.247.156-.94-1.48-.246Zm.676-.787c-.343.114-.613.41-.676.788l1.48.246a.494.494 0 0 1-.33.389l-.474-1.423Zm1.82-1.052a5.244 5.244 0 0 1-1.82 1.052l.474 1.423a6.745 6.745 0 0 0 2.34-1.353l-.994-1.122Zm1.02-.191a1.006 1.006 0 0 0-1.02.19l.995 1.123a.494.494 0 0 1-.501.091l.526-1.405Zm.893.334-.893-.335-.526 1.405.893.335.526-1.405Zm-.304.11a.25.25 0 0 1 .304-.11l-.526 1.405a1.25 1.25 0 0 0 1.521-.546l-1.299-.75Zm1.076-1.865-1.076 1.864 1.299.75 1.076-1.864-1.299-.75Zm.058.318a.25.25 0 0 1-.058-.318l1.3.75a1.25 1.25 0 0 0-.289-1.59l-.953 1.158Zm-.735-.606.735.606.953-1.158-.735-.606-.953 1.158Zm-.345-.978c-.072.354.049.735.345.978l.953-1.158c.15.123.206.311.172.48l-1.47-.3ZM13.25 8c0 .361-.036.713-.105 1.052l1.47.3c.088-.438.135-.89.135-1.352h-1.5Zm-.105-1.052c.069.339.105.69.105 1.052h1.5c0-.462-.046-.914-.135-1.351l-1.47.299Zm.345-.978a1.007 1.007 0 0 0-.345.978l1.47-.3a.494.494 0 0 1-.172.48L13.49 5.97Zm.735-.606-.735.606.953 1.158.735-.606-.953-1.158Zm-.058.318a.25.25 0 0 1 .058-.318l.953 1.158a1.25 1.25 0 0 0 .288-1.59l-1.299.75Zm-1.076-1.864 1.076 1.864 1.3-.75-1.077-1.864-1.299.75Zm.304.109a.25.25 0 0 1-.304-.11l1.299-.75a1.25 1.25 0 0 0-1.521-.545l.526 1.405Zm-.893.334.893-.334-.526-1.405-.893.335.526 1.404Zm-1.02-.19c.27.24.66.325 1.02.19l-.526-1.404a.494.494 0 0 1 .5.091l-.994 1.122Zm-1.82-1.053a5.244 5.244 0 0 1 1.82 1.052l.995-1.122a6.744 6.744 0 0 0-2.34-1.353l-.475 1.423Zm-.676-.788c.063.379.333.674.676.788l.474-1.423a.494.494 0 0 1 .33.389l-1.48.246Zm-.156-.939.156.94 1.48-.247-.157-.94-1.48.247Zm.246.209a.25.25 0 0 1-.246-.209l1.48-.246A1.25 1.25 0 0 0 9.075 0v1.5ZM10.25 8A2.25 2.25 0 0 1 8 10.25v1.5A3.75 3.75 0 0 0 11.75 8h-1.5ZM8 5.75A2.25 2.25 0 0 1 10.25 8h1.5A3.75 3.75 0 0 0 8 4.25v1.5ZM5.75 8A2.25 2.25 0 0 1 8 5.75v-1.5A3.75 3.75 0 0 0 4.25 8h1.5ZM8 10.25A2.25 2.25 0 0 1 5.75 8h-1.5A3.75 3.75 0 0 0 8 11.75v-1.5Z"
          fill={Colors.secondary}
        />
      </Svg>
    </View>
  );
};

exports.shield = function ({ size, color }) {
  return (
    <View>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M2.5 5.4V3.132l4.75-1.357v11.608l-1.782-1.528A8.5 8.5 0 0 1 2.5 5.401Zm6.25 7.982 1.782-1.528A8.5 8.5 0 0 0 13.5 5.401V3.13L8.75 1.774v11.608ZM1 2l7-2 7 2v3.4a10 10 0 0 1-3.492 7.593L8 16l-3.508-3.007A10 10 0 0 1 1 5.401V2Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.profile = function ({ size, color }) {
  return (
    <View>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8 14.5a6.47 6.47 0 0 0 3.25-.87V11.5A2.25 2.25 0 0 0 9 9.25H7a2.25 2.25 0 0 0-2.25 2.25v2.13A6.47 6.47 0 0 0 8 14.5Zm4.75-3v.937a6.5 6.5 0 1 0-9.5 0V11.5a3.752 3.752 0 0 1 2.486-3.532 3 3 0 1 1 4.528 0A3.752 3.752 0 0 1 12.75 11.5ZM8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16ZM9.5 6a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.approval = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4 6.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3ZM7 5a2.99 2.99 0 0 1-.87 2.113A3.997 3.997 0 0 1 8 10.5V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-1.5c0-1.427.747-2.679 1.87-3.387A3 3 0 1 1 7 5Zm-5.5 5.5a2.5 2.5 0 0 1 5 0V12a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-1.5Zm14.28-5.22a.75.75 0 0 0-1.06-1.06L12 6.94l-1.22-1.22a.75.75 0 1 0-1.06 1.06l1.75 1.75a.75.75 0 0 0 1.06 0l3.25-3.25Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.bulb = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.17 8.533C2.21 5.5 4.388 1.5 8 1.5s5.79 4 3.83 7.033L9.592 12H6.408L4.17 8.533ZM5 12.584 2.91 9.347C.305 5.315 3.2 0 8 0s7.694 5.315 5.09 9.347L11 12.584V14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-1.416Zm1.5.916v.5a.5.5 0 0 0 .5.5h2a.5.5 0 0 0 .5-.5v-.5h-3Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.pencil = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M2.5 13.5v-1.879l7.28-7.28 1.88 1.879-7.28 7.28H2.5Zm10.22-8.341.805-.805a.5.5 0 0 0 0-.708l-1.171-1.171a.5.5 0 0 0-.708 0l-.805.805 1.879 1.88ZM1 13.5V11l9.586-9.586a2 2 0 0 1 2.828 0l1.172 1.172a2 2 0 0 1 0 2.828L5 15H1v-1.5Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.warning = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8.429 2.746a.5.5 0 0 0-.858 0L1.58 12.743a.5.5 0 0 0 .429.757h11.984a.5.5 0 0 0 .43-.757L8.428 2.746Zm-2.144-.77C7.06.68 8.939.68 9.715 1.975l5.993 9.996c.799 1.333-.161 3.028-1.716 3.028H2.008C.453 15-.507 13.305.292 11.972l5.993-9.997ZM9 11.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-.25-5.75a.75.75 0 0 0-1.5 0v3a.75.75 0 0 0 1.5 0v-3Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.information = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0Zm-9.75 2.5a.75.75 0 0 0 0 1.5h3.5a.75.75 0 0 0 0-1.5h-1V7H7a.75.75 0 0 0 0 1.5h.25v2h-1ZM8 6a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.docText = function ({ color, size }) {
  return (
    <View>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12.5 6v8.5h-9v-13H8v2.75C8 5.216 8.784 6 9.75 6h2.75Zm-.121-1.5L9.5 1.621V4.25c0 .138.112.25.25.25h2.629ZM2 1a1 1 0 0 1 1-1h6.586a1 1 0 0 1 .707.293l3.414 3.414a1 1 0 0 1 .293.707V15a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V1Zm3.75 7a.75.75 0 0 0 0 1.5h4.5a.75.75 0 0 0 0-1.5h-4.5ZM5 11.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.folder = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M1.5 4V2.5h4.697l1 1.5H1.5ZM0 4V2a1 1 0 0 1 1-1h5.465a1 1 0 0 1 .832.445l1.667 2.5.034.055H15a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4Zm1.5 1.5v7h13v-7h-13Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.subgroup = function ({ testID, style, size, color }) {
  return (
    <View testID={testID} style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Zm1.5 0a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm7 4a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm-10 5.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm0 1.5a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.check = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12.78 4.62a.75.75 0 0 1 0 1.06l-6.097 6.097a.75.75 0 0 1-1.069-.009L3.211 9.284a.75.75 0 1 1 1.078-1.043l1.873 1.936L11.72 4.62a.75.75 0 0 1 1.06 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.slightSmile = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM6 8a1 1 0 1 0 0-2 1 1 0 0 0 0 2Zm5-1a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm-4.37 4.384a2.749 2.749 0 0 0 3.751-1.009.75.75 0 0 0-1.299-.75 1.25 1.25 0 0 1-2.163.003.75.75 0 0 0-1.297.753c.242.417.59.763 1.007 1.003Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.heart = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8.753 2.247 8 3l-.753-.753A4.243 4.243 0 0 0 1.25 8.25l5.69 5.69L8 15l1.06-1.06 5.69-5.69a4.243 4.243 0 0 0-5.997-6.003ZM8 12.879l5.69-5.69a2.743 2.743 0 0 0-3.877-3.881l-.752.753L8 5.12 6.94 4.06l-.753-.752v-.001A2.743 2.743 0 0 0 2.31 7.189L8 12.88Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.dumbbell = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4 4.5v7h1v-7H4ZM3.5 3a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V8.75h3V12a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1h-2a1 1 0 0 0-1 1v3.25h-3V4a1 1 0 0 0-1-1h-2ZM11 4.5v7h1v-7h-1Zm4.25.75A.75.75 0 0 1 16 6v4.5a.75.75 0 0 1-1.5 0V6a.75.75 0 0 1 .75-.75ZM1.5 6A.75.75 0 0 0 0 6v4.5a.75.75 0 0 0 1.5 0V6Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};
exports.car = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M11 2.5H5a.5.5 0 0 0-.5.5v2h7V3a.5.5 0 0 0-.5-.5ZM1.75 4.25H3V3a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v1.25h1.25a.75.75 0 0 1 0 1.5h-.918A3.995 3.995 0 0 1 15 9v5a1 1 0 1 1-2 0v-1H3v1a1 1 0 1 1-2 0V9c0-1.339.658-2.524 1.668-3.25H1.75a.75.75 0 0 1 0-1.5ZM12 11.5h1.5V9A2.5 2.5 0 0 0 11 6.5H5A2.5 2.5 0 0 0 2.5 9v2.5H4V11a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v.5Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.object = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M8 14.5c.8 0 1.566-.145 2.274-.409l.665-2.046h2.15a6.472 6.472 0 0 0 1.405-4.327l-1.739-1.263.665-2.045a6.512 6.512 0 0 0-3.68-2.674L8 3 6.26 1.736A6.512 6.512 0 0 0 2.58 4.41l.665 2.045-1.739 1.263a6.472 6.472 0 0 0 1.406 4.327h2.15l.664 2.046A6.486 6.486 0 0 0 8 14.5ZM8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16ZM8 5.286l2.853 2.073-1.09 3.354H6.237L5.147 7.36 8 5.286Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.smiley = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.5 8a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0ZM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0ZM7 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm1 6a3 3 0 0 0 3-3H5a3 3 0 0 0 3 3Zm3-6a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.nature = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M14.98 2.5A6.37 6.37 0 0 0 15 2V1h-1.75a6.003 6.003 0 0 0-5.761 4.32A5.99 5.99 0 0 0 2.75 3H1v1a6 6 0 0 0 6 6h.25v4.25a.75.75 0 0 0 1.5 0V8H9a6 6 0 0 0 5.98-5.5Zm-6.203 4a4.5 4.5 0 0 1 4.473-4h.223A4.5 4.5 0 0 1 9 6.5h-.223Zm-6.027-2a4.5 4.5 0 0 1 4.473 4H7a4.5 4.5 0 0 1-4.473-4h.223Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.food = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6.75 1A5.75 5.75 0 0 0 1 6.75v.518a2 2 0 0 0 0 3.464v1.518A2.75 2.75 0 0 0 3.75 15h8.5A2.75 2.75 0 0 0 15 12.25v-1.518a2 2 0 0 0 0-3.464V6.75A5.75 5.75 0 0 0 9.25 1h-2.5ZM14 8.5H2a.5.5 0 0 0 0 1h12a.5.5 0 0 0 0-1ZM13.5 7v-.25A4.25 4.25 0 0 0 9.25 2.5h-2.5A4.25 4.25 0 0 0 2.5 6.75V7h11ZM11 11h2.5v1.25c0 .69-.56 1.25-1.25 1.25h-8.5c-.69 0-1.25-.56-1.25-1.25V11H9l1 1 1-1Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};

exports.flag = function ({ color, size, style }) {
  return (
    <View style={style}>
      <Svg
        width={size ?? 16}
        height={size ?? 16}
        viewBox="0 0 16 16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M13.5 10.174v.012c-1.393.72-2.81.247-4.959-.585l-.17-.066C6.763 8.91 4.685 8.105 2.5 8.63V2.814c1.393-.72 2.81-.247 4.959.585l.17.066c1.607.624 3.685 1.43 5.871.904v5.805ZM8 11c-1.83-.708-3.659-1.416-5.5-.81v4.06a.75.75 0 0 1-1.5 0V2C3.35.2 5.675 1.1 8 2c1.83.708 3.659 1.416 5.5.81A5.068 5.068 0 0 0 15 2v9c-2.35 1.8-4.675.9-7 0Z"
          fill={color ?? Colors.dark}
        />
      </Svg>
    </View>
  );
};
