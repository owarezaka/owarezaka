import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '~app/screens/LoginScreen';
import TabNavigator from '~app/navigation/TabNavigator';
import SplashScreen from '~app/screens/SplashScreen';
import Fallback from './Fallback';
import AuthContextProvider, { AuthContext } from '~app/store/auth-context';
import { ThemeProvider } from '~app/store/theme-context';

const Stack = createStackNavigator();

export function Root() {
  const authCtx = useContext(AuthContext);

  return (
    <>
      {authCtx.showError ? (
        <Fallback />
      ) : authCtx.isLoading ? (
        <SplashScreen testID="splash" />
      ) : (
        <NavigationContainer>
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            {authCtx.token ? (
              <Stack.Screen
                name="Home"
                component={TabNavigator}
                options={{
                  animationTypeForReplace: authCtx.isSignOut ? 'pop' : 'push',
                }}
              />
            ) : (
              <Stack.Screen name="SignIn" component={LoginScreen} />
            )}
          </Stack.Navigator>
        </NavigationContainer>
      )}
    </>
  );
}

const App = () => {
  return (
    <AuthContextProvider>
      <ThemeProvider>
        <Root />
      </ThemeProvider>
    </AuthContextProvider>
  );
};

export default App;
