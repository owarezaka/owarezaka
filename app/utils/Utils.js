export function timeSince(isoDate) {
  const intervals = [
    { label: 'year', seconds: 31536000 },
    { label: 'month', seconds: 2592000 },
    { label: 'day', seconds: 86400 },
    { label: 'hour', seconds: 3600 },
    { label: 'minute', seconds: 60 },
    { label: 'second', seconds: 1 },
  ];

  try {
    const date = new Date(isoDate);
    const seconds = Math.floor((Date.now() - date.getTime()) / 1000);

    if (seconds <= 1) {
      return 'just now';
    }

    const interval = intervals.find((i) => i.seconds < seconds);
    const count = Math.floor(seconds / interval.seconds);
    return `${count} ${interval.label}${count !== 1 ? 's' : ''} ago`;
  } catch (err) {
    throw new Error(err);
  }
}

export function findRoleByAccessLevel(projectAccessLevel, groupAccessLevel) {
  const accessLevel = projectAccessLevel ? projectAccessLevel : groupAccessLevel;

  let role;

  switch (accessLevel) {
    case 50:
      role = 'Owner';
      break;
    case 40:
      role = 'Maintainer';
      break;
    case 30:
      role = 'Developer';
      break;
    case 20:
      role = 'Reporter';
      break;
    case 10:
      role = 'Guest';
      break;
    default:
      throw new Error('Unknown Access Role');
  }

  return role;
}

export function findRoleById(projectId, projectMemberships, groupId, groupMemberships) {
  let role;

  projectMemberships?.map((el) => {
    if (el.project.id === projectId) {
      role =
        el.accessLevel.stringValue[0].toUpperCase() +
        el.accessLevel.stringValue.slice(1).toLowerCase();
    }
  });

  if (!role) {
    groupMemberships?.map((el) => {
      if (el.group.id === groupId) {
        role =
          el.accessLevel.stringValue[0].toUpperCase() +
          el.accessLevel.stringValue.slice(1).toLowerCase();
      }
    });
  }

  return role;
}

export function findProjectFullPath(data, projectId) {
  let projectName;

  data.some((el) => {
    if (projectId === Number(el.id.split('Project/')[1])) {
      projectName = el.fullPath;
    }
  });

  return projectName;
}

export function isSupported(version) {
  const arr = version.split('.');

  if (Number(arr[0]) >= 15 || (Number(arr[0]) === 14 && Number(arr[1]) >= 9)) {
    return true;
  }

  return false;
}

export function checkIfApprovedByMe(arr, userName) {
  return arr.some((user) => user.username === userName);
}

export function signinEnabled(token) {
  return token.length >= 10;
}

export const groupBy = (arr, key) => arr.reduce((a, b) => ((a[key(b)] ||= []).push(b), a), {});

export const flattenArr = (arr) => Object.values(arr).flatMap((el) => el);

export function reverseArr(arr) {
  return arr.reverse();
}

export function handleErrors(response) {
  if (!response.ok) {
    throw new Error({ status: response.status, success: false });
  }
  return response;
}
