export class Issue {
  constructor(data) {
    this.id = `gid://gitlab/Issue/${data.id}`;
    this.iid = `${data.iid}`;
    this.title = data.title;
    this.createdAt = data.created_at;
    this.reference = data.references.short;
    this.projectId = data.project_id;
    this.labels = {
      nodes: data.labels
        ?.map((el) => [
          {
            id: Math.random().toString().substring(2, 8),
            title: el,
            color: '#000',
          },
        ])
        .flat(2),
    };
    this.milestone = { title: data.milestone?.title };
    this.author = { name: data.author?.name };
    this.project = { fullPath: data.references.full.split('#')[0] };
  }
}
