# Linting

A look at how we handle linting

## Our linting guidelines

We use ESLint to check for mistakes in our code. In general, we mostly use the recommended configurations for ESLint and Prettier. Here are our rules taken from `eslintrc.js` :

```
{
    'prettier/prettier': 'error',
    'react-native/no-unused-styles': 2,
    'react-native/split-platform-components': 2,
    'react-native/no-inline-styles': 2,
    'react-native/no-color-literals': 2,
    'react-native/no-raw-text': 'off',
    'react/prop-types': 'warn',
    'react/display-name': 'off',
    'react-native/sort-styles': [
      'error',
      'asc',
      {
        ignoreClassNames: false,
        ignoreStyleProperties: false,
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'import/default': 'off',
    'import/namespace': 'warn',
  }

```

We let certain rules pass with a warning, while strictly disallowing others with errors (2 also means error). If you'd like, you can check the documentations for these rules:

    react: https://github.com/jsx-eslint/eslint-plugin-react
    react-native: https://github.com/intellicode/eslint-plugin-react-native
    react-hooks: https://github.com/facebook/react/tree/main/packages/eslint-plugin-react-hooks
    prettier: https://github.com/prettier/eslint-plugin-prettier
    import: https://github.com/import-js/eslint-plugin-import

## Our exceptions

Our prettier config:

```
  "printWidth": 100,
  "singleQuote": true,
  "arrowParens": "always",
  "trailingComma": "all",
  "endOfLine": "auto"

```

- `printWidth` specifies the line length that the printer will wrap on, we use 100 characters.
- `"singleQuote": true` means we use singe quotes instead of double quotes for strings and such.
- `"arrowParens": "always"` means we use parentheses for arrow functions with single parameters. e.g. x => y corrects to (x) => y
- `"trailingComma": "all"` means we use trailing commas wherever possible.
- `"endOfLine": "auto"` means we automatically choose whether to use `\n` or `\r\n` for line endings based on what has been used previously.

We also ignore `spec/coverage-report` for linting.

## How we lint in the GitLab CI

We have a linting stage in our GitLab CI which uses eslint to check for any errors. The job runs `npm run lint`, which is a script that executes eslint with `eslint -c .eslintrc.js --ext .js .`. As described before, we allow certain impurities while disallowing others. Errors will make the job fail, while eslint rules marked with "warn" will allow the job to succeed with a warning(s).

## How we fix issues

When the GitLab CI lets us know that our linting task has failed, we use `npm run lint:fix` , which is a script that runs `eslint -c .eslintrc.js --ext .js . --fix` to automatically fix most issues. If that does not do the trick, we manually look for any errors in our code such as leftover conflict markers, missing brackets etc.

## How we lint in VS Code

In VS Code, we use mainly two plugins for linting: ESLint and Prettier. These plugins automatically check for mistakes in real time to avoid having to run eslint manually.
