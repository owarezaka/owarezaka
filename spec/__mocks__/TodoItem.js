export default {
  id: 12345,
  project: {
    name: 'AwesomeProject',
    name_with_namespace: 'Awesome Namespace / AwesomeProject',
  },
  author: {
    username: 'joe',
    avatar_url: 'https://example.org/avatar.png',
  },
  action_name: 'directly_addressed',
  target: {
    iid: 56,
    title: 'Resolve that issue',
    state: 'closed',
    assignees: [
      {
        id: 777,
        username: 'joedoe',
      },
    ],
  },
  target_type: 'MergeRequest',
  state: 'pending',
  created_at: '2020-12-22T09:07:29.336Z',
};
