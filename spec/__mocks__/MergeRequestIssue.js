export default {
  id: 'gid://gitlab/MergeRequest/134518400',
  iid: '150',
  title: 'Resolve my issues screen',
  createdAt: '2022-01-09T10:44:34.505Z',
  project: {
    fullPath: 'owarezaka/owarezaka',
  },
  reference: '#1',
  labels: {
    nodes: [
      {
        id: 'gid://gitlab/ProjectLabel/22181428',
        title: 'feature',
        color: '#428bca',
      },
    ],
  },
  milestone: {
    title: 'Jan-22',
  },
  author: {
    name: 'Joe',
  },
};
