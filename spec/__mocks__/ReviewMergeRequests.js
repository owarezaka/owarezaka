export default {
  id: 'gid://gitlab/MergeRequest/134518400',
  iid: '150',
  title: 'Resolve review merge screen',
  createdAt: '2022-01-10T10:44:34.505Z',
  project: {
    fullPath: 'owarezaka/owarezaka',
  },
  reference: '#1',
  labels: {
    nodes: [
      {
        id: 'gid://gitlab/ProjectLabel/22181428',
        title: 'feature',
        color: '#428bca',
      },
    ],
  },
  milestone: {
    title: 'Jan-22',
  },
  author: {
    name: 'Jane Doe',
  },
};
