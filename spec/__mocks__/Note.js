export default {
  id: 'gid://gitlab/Note/872926796',
  body: 'Hello from Owarezaka',
  bodyHtml: '<p>Hello from Owarezaka</p>',
  createdAt: '2022-03-13T16:53:18Z',
  author: {
    name: 'Jane',
    username: 'jane_doe',
    avatarUrl: '#',
  },
  userPermissions: {
    repositionNote: true,
  },
  discussion: {
    id: 'gid://gitlab/Discussion/123abc',
    resolvable: true,
    resolved: false,
  },
};
