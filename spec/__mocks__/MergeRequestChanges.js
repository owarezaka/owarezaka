export default {
  diffs: [
    {
      old_path: 'readme.md',
      new_path: 'readme.md',
      a_mode: '1234',
      b_mode: '1234',
      new_file: false,
      renamed_file: false,
      deleted_file: false,
      diff: '++ Owarezaka / GitLab Mobile',
    },
  ],
};
