export default {
  author: {
    username: 'jane_doe',
  },
  description: 'Summary\n(Summarize the bug encountered concisely)',
  dueDate: '2022-03-31',
  assignees: {
    nodes: [
      {
        id: 'gid://gitlab/User/1234',
        name: 'John Doe',
        username: 'john_doe',
        avatarUrl: '#',
      },
    ],
  },
  reviewers: {
    nodes: [
      {
        id: 'gid://gitlab/User/5678',
        name: 'Steve Mcdonald',
        username: 'steve_mc',
        avatarUrl: '#',
      },
    ],
  },
};
