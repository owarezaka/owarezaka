export default {
  autoMergeEnabled: false,
  conflicts: true,
  diffHeadSha: '1234abcd',
  draft: true,
  mergeable: false,
  mergeableDiscussionsState: false,
  mergeWhenPipelineSucceeds: false,
  userPermissions: {
    canMerge: false,
  },
};
