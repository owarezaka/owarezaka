export default {
  author: {
    username: 'john_doe',
  },
  descriptionHtml: '<p>Summary\n(Summarize the bug encountered concisely)</p>',
  dueDate: '2022-03-31',
  assignees: {
    nodes: [
      {
        id: 'gid://gitlab/User/12345',
        name: 'Jane Doe',
        username: 'jane_doe',
        avatarUrl: '#',
      },
    ],
  },
  updatedAt: '2022-02-24T20:37:05Z',
  updatedBy: {
    name: 'John Doe',
  },
};
