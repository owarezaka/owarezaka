export default {
  id: 'gid://gitlab/Project/30541283',
  name: 'GitLab Mobile',
  namespace: {
    name: 'Owarezaka',
  },
  fullPath: 'owarezaka/owarezaka',
  group: {
    id: 'gid://gitlab/Group/13777676',
    name: 'Owarezaka',
  },
  avatarUrl: '#',
  description: 'Yet Another GitLab Mobile',
  visibility: 'public',
  starCount: 3,
  lastActivityAt: '2022-02-04T16:29:17Z',
  pipelines: {
    nodes: [
      {
        detailedStatus: {
          icon: 'status_success',
        },
      },
    ],
  },
};
