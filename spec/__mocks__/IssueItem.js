export default {
  id: 'gid://gitlab/Issue/102081405',
  iid: '112',
  title: 'Add my issues screen',
  createdAt: '2021-12-08T12:52:25Z',
  reference: '#1',
  project: {
    fullPath: 'owarezaka/owarezaka',
  },
  projectId: 31951083,
  labels: {
    nodes: [
      {
        id: 'gid://gitlab/ProjectLabel/23273032',
        title: 'enhancement',
        color: '#ed9121',
      },
    ],
  },
  milestone: {
    title: 'v0.1',
  },
  author: {
    name: 'Jane',
  },
};
