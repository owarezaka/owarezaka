export default {
  name: 'Jane Doe',
  username: 'jane_doe',
  avatarUrl: '#',
  status: {
    emoji: 'smile',
    message: 'Busy',
  },
};
