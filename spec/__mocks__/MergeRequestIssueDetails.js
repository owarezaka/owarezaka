export default {
  author: {
    username: 'jane_doe',
  },
  descriptionHtml: 'Summary\nOverview Screen should display all details of relevant issue',
  assignees: {
    nodes: [
      {
        id: 'gid://gitlab/User/12345',
        name: 'Jane Doe',
        username: 'jane_doe',
        avatarUrl: '#',
      },
    ],
  },
  reviewers: {
    nodes: [
      {
        id: 'gid://gitlab/User/67890',
        username: 'john_doe',
        name: 'John Doe',
        avatarUrl: '#',
      },
    ],
  },
  projectId: 123456,
  targetBranch: 'main',
  sourceBranch: 'feature',
  approvalsRequired: 0,
  approvedBy: {
    nodes: {
      id: 'gid://gitlab/User/12340',
      username: 'susan_doe',
      avatarUrl: '#',
    },
  },
};
