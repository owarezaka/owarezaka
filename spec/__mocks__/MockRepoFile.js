export default {
  name: 'README.md',
  code: 'const sum = (a, b) => a + b;',
  projectPath: 'owarezaka/owarezaka',
  type: 'blob',
  blobPath: 'README.md',
};
