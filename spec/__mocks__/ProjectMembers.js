export default [
  {
    user: {
      username: 'ecede',
      avatarUrl: '/uploads/-/system/user/avatar/11770477/avatar.png',
      id: 'gid://gitlab/User/11770477',
    },
  },
  {
    user: {
      username: 'uras.somer',
      avatarUrl: '/uploads/-/system/user/avatar/10890284/avatar.png',
      id: 'gid://gitlab/User/10890284',
    },
  },
  {
    user: {
      username: 'alperakgun',
      avatarUrl: '/uploads/-/system/user/avatar/4066669/avatar.png',
      id: 'gid://gitlab/User/4066669',
    },
  },
  {
    user: {
      username: 'acelyav',
      avatarUrl: '/uploads/-/system/user/avatar/9997177/avatar.png',
      id: 'gid://gitlab/User/9997177',
    },
  },
  {
    user: {
      username: 'a_akgun',
      avatarUrl: '/uploads/-/system/user/avatar/4409816/avatar.png',
      id: 'gid://gitlab/User/4409816',
    },
  },
];
