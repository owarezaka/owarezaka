export default {
  id: 1668733354,
  action_name: 'closed',
  target_id: 100580383,
  target_iid: 80,
  target_type: 'Issue',
  target_title: 'Add readme',
  created_at: '2022-01-17T18:29:23.831Z',
  author: {
    name: 'John Doe',
    username: 'john_doe',
    avatar_url: '#',
  },
  note: {
    noteable_type: 'MergeRequest',
  },
};
