export default {
  id: 'gid://gitlab/PersonalSnippet/12345',
  title: 'areThereDuplicates',
  visibilityLevel: 'public',
  createdAt: '2022-01-19T08:29:45Z',
  description: 'Multiple Pointers Pattern',
  fileName: '.js',
  author: {
    name: 'Jane Doe',
    avatarUrl: '#',
  },
};
