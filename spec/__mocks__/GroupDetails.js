export default {
  group: {
    descendantGroups: {
      nodes: [
        {
          id: 'gid://gitlab/Group/12345',
          name: 'Learn',
          fullPath: 'Learn',
          description: 'Awesome Project',
          avatarUrl: '#',
          visibility: 'private',
        },
      ],
    },
    projects: {
      nodes: [
        {
          id: 'gid://gitlab/Project/12345',
          name: 'team',
          description: 'Awesome Project',
          avatarUrl: '#',
          visibility: 'private',
        },
      ],
    },
  },
};
