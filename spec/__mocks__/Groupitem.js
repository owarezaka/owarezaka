export default {
  accessLevel: {
    integerValue: 50,
  },
  id: 'gid://gitlab/GroupMember/44794861',
  group: {
    avatarUrl: '#',
    description: 'Projects around GitLab Mobile ',
    fullPath: 'owarezaka/owarezaka',
    name: 'Owarezaka',
    visibility: 'public',
  },
};
