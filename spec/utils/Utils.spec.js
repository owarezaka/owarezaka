import * as Utils from '~app/utils/Utils';

describe('Utils', () => {
  describe('timeSince', () => {
    it('displays time as two days ago', () => {
      var date = new Date();
      date.setDate(date.getDate() - 2);

      expect(Utils.timeSince(date.toString())).toBe('2 days ago');
    });
  });

  describe('findRoleByAccessLevel', () => {
    let projectAccessLevel = 30;
    const groupAccessLevel = 40;

    it('displays project access level as developer when given number is 30', () => {
      expect(Utils.findRoleByAccessLevel(projectAccessLevel, groupAccessLevel)).toBe('Developer');
    });

    it('displays according to group access level if project access level is not provided', () => {
      projectAccessLevel = undefined;

      expect(Utils.findRoleByAccessLevel(projectAccessLevel, groupAccessLevel)).toBe('Maintainer');
    });
  });

  describe('findRoleById', () => {
    let projectId = 'gid://gitlab/Project/30541283';
    const projectMemberships = [
      {
        project: {
          id: 'gid://gitlab/Project/33735320',
        },
        accessLevel: {
          stringValue: 'DEVELOPER',
        },
      },
      {
        project: {
          id: 'gid://gitlab/Project/30541283',
        },
        accessLevel: {
          stringValue: 'DEVELOPER',
        },
      },
    ];

    const groupId = 'gid://gitlab/Group/16139249';
    const groupMemberships = [
      {
        group: {
          id: 'gid://gitlab/Group/16139249',
        },
        accessLevel: {
          stringValue: 'MAINTANIER',
        },
      },
      {
        group: {
          id: 'gid://gitlab/Group/13777676',
        },
        accessLevel: {
          stringValue: 'DEVELOPER',
        },
      },
    ];

    it('displays project access role as developer by checking project id in projectMemberships nodes', () => {
      expect(Utils.findRoleById(projectId, projectMemberships, groupId, groupMemberships)).toBe(
        'Developer',
      );
    });

    it('checks project access role in groupMemberships nodes if projectMemberships returns undefined', () => {
      projectId = undefined;

      expect(Utils.findRoleById(projectId, projectMemberships, groupId, groupMemberships)).toBe(
        'Maintanier',
      );
    });
  });

  describe('findProjectFullPath', () => {
    it('returns project fullPath by checking data and project id provided', () => {
      const data = [
        {
          id: 'gid://gitlab/Project/30541283',
          fullPath: 'owarezaka/owarezaka',
        },
      ];
      const projectId = 30541283;

      expect(Utils.findProjectFullPath(data, projectId)).toBe('owarezaka/owarezaka');
    });
  });

  describe('isSupported', () => {
    it('return true if version is 14.9 or above', () => {
      const version = '14.10.0-pre';

      expect(Utils.isSupported(version)).toBe(true);
    });

    it('return false if version is below 14.9', () => {
      const version = '14.8.2';

      expect(Utils.isSupported(version)).toBe(false);
    });
  });

  describe('checkIfApprovedByMe', () => {
    const arr = [
      {
        id: 'gid://gitlab/User/1234',
        username: 'jane_doe',
      },
      {
        id: 'gid://gitlab/User/1234',
        username: 'mary_jones',
      },
    ];

    it('return true if user is found in the array', () => {
      const user = 'jane_doe';

      expect(Utils.checkIfApprovedByMe(arr, user)).toBe(true);
    });

    it('return false if user is not found in the array', () => {
      const user = 'john_doe';

      expect(Utils.checkIfApprovedByMe(arr, user)).toBe(false);
    });
  });

  describe('signinEnabled', () => {
    it('return true if token is equal or more than 10', () => {
      const token = '123456789012345678901234567890';

      expect(Utils.signinEnabled(token)).toBe(true);
    });

    it('return false if token is below 10', () => {
      const token = '123456789';

      expect(Utils.signinEnabled(token)).toBe(false);
    });
  });

  describe('groupBy', () => {
    it('returns object with groupping by given condition', () => {
      const arr = [
        {
          id: 'gid://gitlab/Note/1234',
          createdAt: '2022-04-04T09:55:42Z',
          discussion: {
            id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
            resolvable: false,
            resolved: false,
          },
        },
        {
          id: 'gid://gitlab/Note/4567',
          createdAt: '2022-03-30T16:22:51Z',
          discussion: {
            id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
            resolvable: false,
            resolved: false,
          },
        },
      ];

      const result = {
        'gid://gitlab/IndividualNoteDiscussion/123abc': [
          {
            createdAt: '2022-04-04T09:55:42Z',
            discussion: {
              id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
              resolvable: false,
              resolved: false,
            },
            id: 'gid://gitlab/Note/1234',
          },
          {
            createdAt: '2022-03-30T16:22:51Z',
            discussion: {
              id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
              resolvable: false,
              resolved: false,
            },
            id: 'gid://gitlab/Note/4567',
          },
        ],
      };

      expect(Utils.groupBy(arr, (el) => el.discussion.id)).toStrictEqual(result);
    });
  });

  describe('flattenArr', () => {
    it('return true if token is equal or more than 10', () => {
      const obj = {
        'gid://gitlab/IndividualNoteDiscussion/123abc': [
          {
            createdAt: '2022-04-04T09:55:42Z',
            discussion: {
              id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
              resolvable: false,
              resolved: false,
            },
            id: 'gid://gitlab/Note/1234',
          },
          {
            createdAt: '2022-03-30T16:22:51Z',
            discussion: {
              id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
              resolvable: false,
              resolved: false,
            },
            id: 'gid://gitlab/Note/4567',
          },
        ],
      };

      const result = [
        {
          createdAt: '2022-04-04T09:55:42Z',
          discussion: {
            id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
            resolvable: false,
            resolved: false,
          },
          id: 'gid://gitlab/Note/1234',
        },
        {
          createdAt: '2022-03-30T16:22:51Z',
          discussion: {
            id: 'gid://gitlab/IndividualNoteDiscussion/123abc',
            resolvable: false,
            resolved: false,
          },
          id: 'gid://gitlab/Note/4567',
        },
      ];

      expect(Utils.flattenArr(obj)).toStrictEqual(result);
    });
  });

  describe('reverseArr', () => {
    it('return reverse order of given array', () => {
      const arr = [1, 2, 3];
      const result = [3, 2, 1];

      expect(Utils.reverseArr(arr)).toStrictEqual(result);
    });
  });
});
