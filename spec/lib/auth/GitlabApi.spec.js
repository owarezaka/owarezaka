import * as GitlabApi from '~app/lib/auth/GitlabApi';
import user from '~spec/__mocks__/User.js';
import todoList from '~spec/__mocks__/TodoItem';
import projectList from '~spec/__mocks__/ProjectItem';
import assignedIssuesList from '~spec/__mocks__/AssignedIssues';
import issueList from '~spec/__mocks__/IssueItem';
import issueDetails from '~spec/__mocks__/IssueDetails';
import note from '~spec/__mocks__/Note';
import mergeRequestIssueList from '~spec/__mocks__/MergeRequestIssue';
import mergeRequestDetails from '~spec/__mocks__/MergeRequestIssueDetails';
import mergeRequestChanges from '~spec/__mocks__/MergeRequestChanges';
import reviewMergeRequestIssueList from '~spec/__mocks__/ReviewMergeRequests';
import mergeStatus from '~spec/__mocks__/MergeStatus';
import snippetList from '~spec/__mocks__/SnippetItem';
import snippetBody from '~spec/__mocks__/snippetBody';
import groupList from '~spec/__mocks__/Groupitem';
import groupDetails from '~spec/__mocks__/GroupDetails';
import activitiesList from '~spec/__mocks__/ActivityItem';
import userStatus from '~spec/__mocks__/UserStatus';
import mockReadMe from '~spec/__mocks__/MockReadMe';
import mockRepoFile from '~spec/__mocks__/MockRepoFile';

jest.mock('~app/lib/auth/GitlabApi');

describe('GitlabApi', () => {
  describe('getTodos', () => {
    it('calls function to get todos', async () => {
      GitlabApi.getTodos.mockResolvedValue([todoList]);

      const response = await GitlabApi.getTodos(user.instanceUrl, user.userToken);
      expect(response).toStrictEqual([todoList]);

      GitlabApi.getTodos.mockRestore();
    });

    it('should reject with an error if it could not get todos', async () => {
      jest.spyOn(GitlabApi, 'getTodos');
      GitlabApi.getTodos.mockImplementation(() => Promise.reject(new Error('Connection failed')));

      await expect(GitlabApi.getTodos(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.getTodos.mockRestore();
    });
  });

  describe('markAsDone', () => {
    it('calls function to mark todo as done', async () => {
      GitlabApi.markAsDone.mockResolvedValue(200);

      const response = await GitlabApi.markAsDone(user.instanceUrl, todoList.id, user.userToken);
      expect(response).toBe(200);

      GitlabApi.markAsDone.mockRestore();
    });

    it('should reject with an error if it could not mark as done', async () => {
      jest.spyOn(GitlabApi, 'markAsDone');
      GitlabApi.markAsDone.mockImplementation(() => Promise.reject(new Error('Connection failed')));

      await expect(
        GitlabApi.markAsDone(user.instanceUrl, todoList.id, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.markAsDone.mockRestore();
    });
  });

  describe('getAllProjects', () => {
    it('calls function to list all projects', async () => {
      GitlabApi.getAllProjects.mockResolvedValue([projectList]);

      const response = await GitlabApi.getAllProjects(
        user.username,
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toStrictEqual([projectList]);

      GitlabApi.getAllProjects.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getAllProjects');
      GitlabApi.getAllProjects.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getAllProjects(user.username, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getAllProjects.mockRestore();
    });
  });

  describe('getOpenIssues', () => {
    it('calls function to list all issues', async () => {
      GitlabApi.getOpenIssues.mockResolvedValue([issueList]);

      const response = await GitlabApi.getOpenIssues(
        user.instanceUrl,
        user.username,
        user.userToken,
      );
      expect(response).toStrictEqual([issueList]);

      GitlabApi.getOpenIssues.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getOpenIssues');
      GitlabApi.getOpenIssues.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getOpenIssues(user.instanceUrl, user.username, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getOpenIssues.mockRestore();
    });
  });

  describe('getAllAssignedIssues', () => {
    it('calls function to list all assigned issues', async () => {
      GitlabApi.getAllAssignedIssues.mockResolvedValue([assignedIssuesList]);

      const response = await GitlabApi.getAllAssignedIssues(
        user.instanceUrl,
        user.userToken,
        user.username,
      );
      expect(response).toStrictEqual([assignedIssuesList]);

      GitlabApi.getAllAssignedIssues.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getAllAssignedIssues');
      GitlabApi.getAllAssignedIssues.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getAllAssignedIssues(user.instanceUrl, user.userToken, user.username),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getAllAssignedIssues.mockRestore();
    });
  });

  describe('getOpenIssueDetails', () => {
    it('calls function to list issue details', async () => {
      GitlabApi.getOpenIssueDetails.mockResolvedValue(issueDetails);

      const response = await GitlabApi.getOpenIssueDetails(
        issueList.id,
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toStrictEqual(issueDetails);

      GitlabApi.getOpenIssueDetails.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getOpenIssueDetails');
      GitlabApi.getOpenIssueDetails.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getOpenIssueDetails(issueList.id, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getOpenIssueDetails.mockRestore();
    });
  });

  describe('createIssueNote', () => {
    const comment = 'Hello World!';

    it('calls createIssueNote function to create comment', async () => {
      GitlabApi.createIssueNote.mockResolvedValue(200);

      const response = await GitlabApi.createIssueNote(
        issueList.id,
        comment,
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toBe(200);

      GitlabApi.createIssueNote.mockRestore();
    });

    it('should reject with an error if it fails to create comment', async () => {
      jest.spyOn(GitlabApi, 'createIssueNote');
      GitlabApi.createIssueNote.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.createIssueNote(issueList.id, comment, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.createIssueNote.mockRestore();
    });
  });

  describe('updateIssueNote', () => {
    const comment = 'Hello World!';

    it('calls updateIssueNote function to edit comment', async () => {
      GitlabApi.updateIssueNote.mockResolvedValue(200);

      const response = await GitlabApi.updateIssueNote(
        note.id,
        comment,
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toBe(200);

      GitlabApi.updateIssueNote.mockRestore();
    });

    it('should reject with an error if it fails to edit comment', async () => {
      jest.spyOn(GitlabApi, 'updateIssueNote');
      GitlabApi.updateIssueNote.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.updateIssueNote(note.id, comment, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.updateIssueNote.mockRestore();
    });
  });

  describe('destroyNote', () => {
    it('calls function to delete comment', async () => {
      jest.spyOn(GitlabApi, 'destroyNote');
      GitlabApi.destroyNote.mockImplementation(() => Promise.resolve(200));

      const response = await GitlabApi.destroyNote(note.id, user.instanceUrl, user.userToken);

      expect(response).toBe(200);

      GitlabApi.destroyNote.mockRestore();
    });

    it('should reject with an error if it could not delete comment', async () => {
      jest.spyOn(GitlabApi, 'destroyNote');
      GitlabApi.destroyNote.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.destroyNote(note.id, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.destroyNote.mockRestore();
    });
  });

  describe('getAllIssueNotes', () => {
    it('calls function to list all comments', async () => {
      jest.spyOn(GitlabApi, 'getAllIssueNotes');
      GitlabApi.getAllIssueNotes.mockImplementation(() => Promise.resolve(note));

      const response = await GitlabApi.getAllIssueNotes(
        issueList.id,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(note);

      GitlabApi.getAllIssueNotes.mockRestore();
    });

    it('should reject with an error if it could not load comments', async () => {
      jest.spyOn(GitlabApi, 'getAllIssueNotes');
      GitlabApi.getAllIssueNotes.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getAllIssueNotes(issueList.id, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getAllIssueNotes.mockRestore();
    });
  });

  describe('getAllMergeRequestNotes', () => {
    it('calls function to list all merge request comments', async () => {
      jest.spyOn(GitlabApi, 'getAllMergeRequestNotes');
      GitlabApi.getAllMergeRequestNotes.mockImplementation(() => Promise.resolve(note));

      const response = await GitlabApi.getAllMergeRequestNotes(
        mergeRequestIssueList.id,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(note);

      GitlabApi.getAllMergeRequestNotes.mockRestore();
    });

    it('should reject with an error if it could not load comments', async () => {
      jest.spyOn(GitlabApi, 'getAllMergeRequestNotes');
      GitlabApi.getAllMergeRequestNotes.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getAllMergeRequestNotes(
          mergeRequestIssueList.id,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getAllMergeRequestNotes.mockRestore();
    });
  });

  describe('closeIssue', () => {
    const mockData = [
      {
        id: 'gid://gitlab/Project/30541283',
        fullPath: 'owarezaka/owarezaka',
      },
    ];

    it('calls function to close issue', async () => {
      GitlabApi.closeIssue.mockResolvedValue(issueDetails);

      const response = await GitlabApi.closeIssue(
        user.instanceUrl,
        mockData.fullPath,
        issueList.iid,
        user.userToken,
      );

      expect(response).toStrictEqual(issueDetails);

      GitlabApi.closeIssue.mockRestore();
    });

    it('should reject with an error if it fails to close issue', async () => {
      jest.spyOn(GitlabApi, 'closeIssue');
      GitlabApi.closeIssue.mockImplementation(() => Promise.reject(new Error('Connection failed')));

      await expect(
        GitlabApi.closeIssue(user.instanceUrl, mockData.fullPath, issueList.iid, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.closeIssue.mockRestore();
    });
  });

  describe('closeMergeRequest', () => {
    const mockData = [
      {
        id: 'gid://gitlab/Project/30541283',
        fullPath: 'owarezaka/owarezaka',
      },
    ];

    it('calls function to close merge request', async () => {
      GitlabApi.closeMergeRequest.mockResolvedValue(200);

      const response = await GitlabApi.closeMergeRequest(
        user.instanceUrl,
        mockData.fullPath,
        issueList.iid,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.closeMergeRequest.mockRestore();
    });

    it('should reject with an error if it fails to close merge request', async () => {
      jest.spyOn(GitlabApi, 'closeMergeRequest');
      GitlabApi.closeMergeRequest.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.closeMergeRequest(
          user.instanceUrl,
          mockData.fullPath,
          issueList.iid,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.closeMergeRequest.mockRestore();
    });
  });

  describe('assignedMergeRequests', () => {
    it('calls function to get assigned merge requests', async () => {
      GitlabApi.assignedMergeRequests.mockResolvedValue([mergeRequestIssueList]);

      const response = await GitlabApi.assignedMergeRequests(user.instanceUrl, user.userToken);
      expect(response).toStrictEqual([mergeRequestIssueList]);

      GitlabApi.assignedMergeRequests.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'assignedMergeRequests');
      GitlabApi.assignedMergeRequests.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.assignedMergeRequests(user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.assignedMergeRequests.mockRestore();
    });
  });

  describe('getMergeRequestsDetails', () => {
    it('calls function to get assigned merge requests', async () => {
      GitlabApi.getMergeRequestsDetails.mockResolvedValue([mergeRequestDetails]);

      const response = await GitlabApi.getMergeRequestsDetails(
        mergeRequestIssueList.id,
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toStrictEqual([mergeRequestDetails]);

      GitlabApi.getMergeRequestsDetails.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getMergeRequestsDetails');
      GitlabApi.getMergeRequestsDetails.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getMergeRequestsDetails(
          mergeRequestIssueList.id,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getMergeRequestsDetails.mockRestore();
    });
  });

  describe('reviewRequestedMergeRequests', () => {
    it('calls function to get assigned merge requests', async () => {
      GitlabApi.reviewRequestedMergeRequests.mockResolvedValue([reviewMergeRequestIssueList]);

      const response = await GitlabApi.reviewRequestedMergeRequests(
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toStrictEqual([reviewMergeRequestIssueList]);

      GitlabApi.reviewRequestedMergeRequests.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'reviewRequestedMergeRequests');
      GitlabApi.reviewRequestedMergeRequests.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.reviewRequestedMergeRequests(user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.reviewRequestedMergeRequests.mockRestore();
    });
  });

  describe('getAllGroups', () => {
    it('calls function to get groups', async () => {
      GitlabApi.getAllGroups.mockResolvedValue([groupList]);

      const response = await GitlabApi.getAllGroups(user.instanceUrl, user.userToken);
      expect(response).toStrictEqual([groupList]);

      GitlabApi.getAllGroups.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getAllGroups');
      GitlabApi.getAllGroups.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.getAllGroups(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.getAllGroups.mockRestore();
    });
  });

  describe('getSubgroupsAndProjectsDetailsofGroup', () => {
    it('calls function to get groups', async () => {
      GitlabApi.getSubgroupsAndProjectsDetailsofGroup.mockResolvedValue(groupDetails);

      const response = await GitlabApi.getSubgroupsAndProjectsDetailsofGroup(
        user.instanceUrl,
        user.userToken,
      );
      expect(response).toStrictEqual(groupDetails);

      GitlabApi.getSubgroupsAndProjectsDetailsofGroup.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getSubgroupsAndProjectsDetailsofGroup');
      GitlabApi.getSubgroupsAndProjectsDetailsofGroup.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getSubgroupsAndProjectsDetailsofGroup(
          groupList.group.fullPath,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getSubgroupsAndProjectsDetailsofGroup.mockRestore();
    });
  });

  describe('getAllActivities', () => {
    const page = 1;

    it('calls function to get all activities', async () => {
      jest.spyOn(GitlabApi, 'getAllActivities');
      GitlabApi.getAllActivities.mockImplementation(() => Promise.resolve([activitiesList]));

      const response = await GitlabApi.getAllActivities(user.instanceUrl, user.userToken, page);

      expect(response).toStrictEqual([activitiesList]);

      GitlabApi.getAllActivities.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getAllActivities');
      GitlabApi.getAllActivities.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getAllActivities(user.instanceUrl, user.userToken, page),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getAllActivities.mockRestore();
    });
  });

  describe('getAllSnippets', () => {
    it('calls function to get all snippets', async () => {
      jest.spyOn(GitlabApi, 'getAllSnippets');
      GitlabApi.getAllSnippets.mockImplementation(() => Promise.resolve([snippetList]));
      const response = await GitlabApi.getAllSnippets(user.instanceUrl, user.userToken);

      expect(response).toStrictEqual([snippetList]);

      GitlabApi.getAllSnippets.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getAllSnippets');
      GitlabApi.getAllSnippets.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.getAllSnippets(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.getAllSnippets.mockRestore();
    });
  });

  describe('getSnippet', () => {
    const id = snippetList.id.split('Snippet/')[1];

    it('calls function to fetch snippet body', async () => {
      jest.spyOn(GitlabApi, 'getSnippet');
      GitlabApi.getSnippet.mockImplementation(() => Promise.resolve(snippetBody.code));

      const response = await GitlabApi.getSnippet(user.instanceUrl, id, user.userToken);

      expect(response).toBe(snippetBody.code);

      GitlabApi.getSnippet.mockRestore();
    });

    it('should reject with an error if it could not get codes', async () => {
      jest.spyOn(GitlabApi, 'getSnippet');
      GitlabApi.getSnippet.mockImplementation(() => Promise.reject(new Error('Connection failed')));

      await expect(GitlabApi.getSnippet(user.instanceUrl, id, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.getSnippet.mockRestore();
    });
  });

  describe('getUserStatus', () => {
    it('calls function to get user status', async () => {
      jest.spyOn(GitlabApi, 'getUserStatus');
      GitlabApi.getUserStatus.mockImplementation(() => Promise.resolve(userStatus));

      const response = await GitlabApi.getUserStatus(user.instanceUrl, user.userToken);
      expect(response).toBe(userStatus);

      GitlabApi.getUserStatus.mockRestore();
    });

    it('should reject with an error if it could not get data', async () => {
      jest.spyOn(GitlabApi, 'getUserStatus');
      GitlabApi.getUserStatus.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.getUserStatus(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.getUserStatus.mockRestore();
    });
  });

  describe('setUserStatus', () => {
    const data = { message: 'Busy' };

    it('calls function to set user status', async () => {
      jest.spyOn(GitlabApi, 'setUserStatus');
      GitlabApi.setUserStatus.mockImplementation(() => Promise.resolve(data));

      const response = await GitlabApi.setUserStatus(user.instanceUrl, user.userToken, 'Busy');

      expect(response).toBe(data);

      GitlabApi.setUserStatus.mockRestore();
    });

    it('should reject with an error if it could not set data', async () => {
      jest.spyOn(GitlabApi, 'setUserStatus');
      GitlabApi.setUserStatus.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.setUserStatus(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.setUserStatus.mockRestore();
    });
  });

  describe('removeUserStatus', () => {
    it('calls function to remove status', async () => {
      jest.spyOn(GitlabApi, 'removeUserStatus');
      GitlabApi.removeUserStatus.mockImplementation(() => Promise.resolve(200));

      const response = await GitlabApi.removeUserStatus(user.userToken);
      expect(response).toBe(200);

      GitlabApi.removeUserStatus.mockRestore();
    });

    it('should reject with an error if it could not remove status', async () => {
      jest.spyOn(GitlabApi, 'removeUserStatus');
      GitlabApi.removeUserStatus.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.removeUserStatus(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.removeUserStatus.mockRestore();
    });
  });

  describe('getMergeRequestChanges', () => {
    it('calls function to get diffs', async () => {
      GitlabApi.getMergeRequestChanges.mockResolvedValue(mergeRequestChanges);

      const response = await GitlabApi.getMergeRequestChanges(
        user.instanceUrl,
        mergeRequestDetails.projectId,
        mergeRequestDetails.targetBranch,
        mergeRequestDetails.sourceBranch,
        user.userToken,
      );

      expect(response).toBe(mergeRequestChanges);

      GitlabApi.getMergeRequestChanges.mockRestore();
    });

    it('should reject with an error if it fails to get diffs', async () => {
      jest.spyOn(GitlabApi, 'getMergeRequestChanges');
      GitlabApi.getMergeRequestChanges.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getMergeRequestChanges(
          user.instanceUrl,
          mergeRequestDetails.projectId,
          mergeRequestDetails.targetBranch,
          mergeRequestDetails.sourceBranch,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getMergeRequestChanges.mockRestore();
    });
  });

  describe('approveMergeRequest', () => {
    it('calls function to approve merge request', async () => {
      GitlabApi.approveMergeRequest.mockResolvedValue(true);

      const response = await GitlabApi.approveMergeRequest(
        user.instanceUrl,
        mergeRequestDetails.projectId,
        mergeRequestIssueList.iid,
        user.userToken,
      );

      expect(response).toBe(true);

      GitlabApi.approveMergeRequest.mockRestore();
    });

    it('should reject with an error if it fails to approve merge request', async () => {
      jest.spyOn(GitlabApi, 'approveMergeRequest');
      GitlabApi.approveMergeRequest.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.approveMergeRequest(
          user.instanceUrl,
          mergeRequestDetails.projectId,
          mergeRequestIssueList.iid,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.approveMergeRequest.mockRestore();
    });
  });

  describe('revokeApproval', () => {
    it('calls function to revoke approval', async () => {
      GitlabApi.revokeApproval.mockResolvedValue(true);

      const response = await GitlabApi.revokeApproval(
        user.instanceUrl,
        mergeRequestDetails.projectId,
        mergeRequestIssueList.iid,
        user.userToken,
      );

      expect(response).toBe(true);

      GitlabApi.revokeApproval.mockRestore();
    });

    it('should reject with an error if it fails to revoke approval', async () => {
      jest.spyOn(GitlabApi, 'revokeApproval');
      GitlabApi.revokeApproval.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.revokeApproval(
          user.instanceUrl,
          mergeRequestDetails.projectId,
          mergeRequestIssueList.iid,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.revokeApproval.mockRestore();
    });
  });

  describe('checkVersion', () => {
    const version = '14.10.0-pre';

    it('calls function to get version number', async () => {
      GitlabApi.checkVersion.mockResolvedValue(version);

      const response = await GitlabApi.checkVersion(user.instanceUrl, user.userToken);

      expect(response).toBe(version);

      GitlabApi.checkVersion.mockRestore();
    });

    it('should reject with an error if it fails to get data', async () => {
      jest.spyOn(GitlabApi, 'checkVersion');
      GitlabApi.checkVersion.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(GitlabApi.checkVersion(user.instanceUrl, user.userToken)).rejects.toThrow(
        'Connection failed',
      );

      GitlabApi.checkVersion.mockRestore();
    });
  });

  describe('checkMergeableState', () => {
    it('calls function to get merge status', async () => {
      GitlabApi.checkMergeableState.mockResolvedValue(mergeStatus);

      const response = await GitlabApi.checkMergeableState(
        mergeRequestIssueList.id,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(mergeStatus);

      GitlabApi.checkMergeableState.mockRestore();
    });

    it('should reject with an error if it fails to get merge status', async () => {
      jest.spyOn(GitlabApi, 'checkMergeableState');
      GitlabApi.checkMergeableState.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.checkMergeableState(mergeRequestIssueList.id, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.checkMergeableState.mockRestore();
    });
  });

  describe('acceptMergeRequest', () => {
    it('calls function to accept merge request', async () => {
      GitlabApi.acceptMergeRequest.mockResolvedValue(200);

      const response = await GitlabApi.acceptMergeRequest(
        mergeRequestIssueList.iid,
        mergeRequestIssueList.project.fullPath,
        mergeStatus.diffHeadSha,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.acceptMergeRequest.mockRestore();
    });

    it('should reject with an error if it fails to merge', async () => {
      jest.spyOn(GitlabApi, 'acceptMergeRequest');
      GitlabApi.acceptMergeRequest.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.acceptMergeRequest(
          mergeRequestIssueList.iid,
          mergeRequestIssueList.project.fullPath,
          mergeStatus.diffHeadSha,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.acceptMergeRequest.mockRestore();
    });
  });

  describe('discussionToggleResolve', () => {
    it('calls function to resolve thread', async () => {
      GitlabApi.discussionToggleResolve.mockResolvedValue(200);

      const response = await GitlabApi.discussionToggleResolve(
        note.id,
        note.discussion.resolved,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.discussionToggleResolve.mockRestore();
    });

    it('should reject with an error if it fails to resolve thread', async () => {
      jest.spyOn(GitlabApi, 'discussionToggleResolve');
      GitlabApi.discussionToggleResolve.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.discussionToggleResolve(
          note.id,
          note.discussion.resolved,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.discussionToggleResolve.mockRestore();
    });
  });

  describe('mergeRequestToggleAsReady', () => {
    it('calls function to mark as ready', async () => {
      GitlabApi.mergeRequestToggleAsReady.mockResolvedValue(200);

      const response = await GitlabApi.mergeRequestToggleAsReady(
        mergeRequestIssueList.iid,
        mergeStatus.draft,
        mergeRequestIssueList.project.fullPath,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.mergeRequestToggleAsReady.mockRestore();
    });

    it('should reject with an error if it fails to mark as ready', async () => {
      jest.spyOn(GitlabApi, 'mergeRequestToggleAsReady');
      GitlabApi.mergeRequestToggleAsReady.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.mergeRequestToggleAsReady(
          mergeRequestIssueList.iid,
          mergeStatus.draft,
          mergeRequestIssueList.project.fullPath,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.mergeRequestToggleAsReady.mockRestore();
    });
  });

  describe('getUserGlobalId', () => {
    const userName = 'john_doe';
    const userId = 'gid://gitlab/User/123456';

    it('calls function to get user global id', async () => {
      GitlabApi.getUserGlobalId.mockResolvedValue(userId);

      const response = await GitlabApi.getUserGlobalId(userName, user.instanceUrl, user.userToken);

      expect(response).toBe(userId);

      GitlabApi.getUserGlobalId.mockRestore();
    });

    it('should reject with an error if it fails to get data', async () => {
      jest.spyOn(GitlabApi, 'getUserGlobalId');
      GitlabApi.getUserGlobalId.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.getUserGlobalId(userName, user.instanceUrl, user.userToken),
      ).rejects.toThrow('Connection failed');

      GitlabApi.getUserGlobalId.mockRestore();
    });
  });

  describe('updateMergeRequestReviewer', () => {
    const reviewerId = 123456;

    it('calls function to update merge request reviewer', async () => {
      GitlabApi.updateMergeRequestReviewer.mockResolvedValue(200);

      const response = await GitlabApi.updateMergeRequestReviewer(
        mergeRequestIssueList.iid,
        mergeRequestDetails.projectId,
        reviewerId,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.updateMergeRequestReviewer.mockRestore();
    });

    it('should reject with an error if it fails to update user', async () => {
      jest.spyOn(GitlabApi, 'updateMergeRequestReviewer');
      GitlabApi.updateMergeRequestReviewer.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.updateMergeRequestReviewer(
          mergeRequestIssueList.iid,
          mergeRequestDetails.projectId,
          reviewerId,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.updateMergeRequestReviewer.mockRestore();
    });
  });

  describe('mergeRequestReviewerRereview', () => {
    const userId = 'gid://gitlab/User/123456';

    it('calls function to request rereview from reviewer', async () => {
      GitlabApi.mergeRequestReviewerRereview.mockResolvedValue(200);

      const response = await GitlabApi.mergeRequestReviewerRereview(
        mergeRequestIssueList.iid,
        mergeRequestIssueList.project.fullPath,
        userId,
        user.instanceUrl,
        user.userToken,
      );

      expect(response).toBe(200);

      GitlabApi.mergeRequestReviewerRereview.mockRestore();
    });

    it('should reject with an error if it fails', async () => {
      jest.spyOn(GitlabApi, 'mergeRequestReviewerRereview');
      GitlabApi.mergeRequestReviewerRereview.mockImplementation(() =>
        Promise.reject(new Error('Connection failed')),
      );

      await expect(
        GitlabApi.mergeRequestReviewerRereview(
          mergeRequestIssueList.iid,
          mergeRequestIssueList.project.fullPath,
          userId,
          user.instanceUrl,
          user.userToken,
        ),
      ).rejects.toThrow('Connection failed');

      GitlabApi.mergeRequestReviewerRereview.mockRestore();
    });

    describe('getReadMe', () => {
      it('calls function to get readme', async () => {
        jest.spyOn(GitlabApi, 'getReadMe');
        GitlabApi.getReadMe.mockImplementation(() => Promise.resolve(mockReadMe));

        const response = await GitlabApi.getReadMe(
          projectList.fullPath,
          user.instanceUrl,
          user.userToken,
        );

        expect(response).toBe(mockReadMe);

        GitlabApi.getReadMe.mockRestore();
      });

      it('should reject with an error if it fails to get readme', async () => {
        jest.spyOn(GitlabApi, 'getReadMe');
        GitlabApi.getReadMe.mockImplementation(() =>
          Promise.reject(new Error('Connection failed')),
        );

        await expect(
          GitlabApi.getReadMe(projectList.fullPath, user.instanceUrl, user.userToken),
        ).rejects.toThrow('Connection failed');

        GitlabApi.getReadMe.mockRestore();
      });
    });

    describe('getFileInfo', () => {
      const fullPath = mockRepoFile.projectPath;
      const treePath = '';

      it('calls function to get file name', async () => {
        jest.spyOn(GitlabApi, 'getFileInfo');
        GitlabApi.getFileInfo.mockImplementation(() => Promise.resolve(mockRepoFile));

        const response = await GitlabApi.getFileInfo(
          fullPath,
          treePath,
          user.instanceUrl,
          user.userToken,
        );

        expect(response.name).toBe(mockRepoFile.name);

        GitlabApi.getFileInfo.mockRestore();
      });

      it('should reject with an error if it fails to get file name', async () => {
        jest.spyOn(GitlabApi, 'getFileInfo');
        GitlabApi.getFileInfo.mockImplementation(() =>
          Promise.reject(new Error('Connection failed')),
        );

        await expect(
          GitlabApi.getFileInfo(fullPath, treePath, user.instanceUrl, user.userToken),
        ).rejects.toThrow('Connection failed');

        GitlabApi.getFileInfo.mockRestore();
      });
    });

    describe('getFileContent', () => {
      const fullPath = mockRepoFile.projectPath;
      const blobPath = mockRepoFile.blobPath;

      it('calls function to get file content', async () => {
        jest.spyOn(GitlabApi, 'getFileContent');
        GitlabApi.getFileContent.mockImplementation(() => Promise.resolve(mockRepoFile));

        const response = await GitlabApi.getFileContent(
          fullPath,
          blobPath,
          user.instanceUrl,
          user.userToken,
        );

        expect(response.code).toBe(mockRepoFile.code);

        GitlabApi.getFileContent.mockRestore();
      });

      it('should reject with an error if it fails to get file content', async () => {
        jest.spyOn(GitlabApi, 'getFileContent');
        GitlabApi.getFileContent.mockImplementation(() =>
          Promise.reject(new Error('Connection failed')),
        );

        await expect(
          GitlabApi.getFileContent(fullPath, blobPath, user.instanceUrl, user.userToken),
        ).rejects.toThrow('Connection failed');

        GitlabApi.getFileContent.mockRestore();
      });
    });
  });
});
