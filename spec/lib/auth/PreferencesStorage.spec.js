import * as RNEncryptedStorage from 'react-native-encrypted-storage';
import PreferencesStorage from '~app/lib/auth/PreferencesStorage';

const darkMode = 'light';

jest.mock('~spec/__mocks__/react-native-encrypted-storage', () => {
  return {
    setItem: jest.fn(() => Promise.resolve(true)),
    getItem: jest.fn(() => Promise.resolve({ darkMode })),
    removeItem: jest.fn(() => Promise.resolve()),
    clear: jest.fn(() => Promise.resolve()),
  };
});

describe('PreferencesStorage', () => {
  describe('getPreferences', () => {
    it('calls getPrefItem to return values', () => {
      PreferencesStorage.getPreferences();
      return expect(RNEncryptedStorage.getItem('userPrefs')).resolves.toEqual({
        darkMode,
      });
    });
  });
});
