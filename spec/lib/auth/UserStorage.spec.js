import * as RNEncryptedStorage from 'react-native-encrypted-storage';
import UserStorage from '~app/lib/auth/UserStorage';
const instanceUrl = 'https://gitlab.com';
const userToken = '123456789012345678901234567890';

jest.mock('~spec/__mocks__/react-native-encrypted-storage', () => {
  return {
    setItem: jest.fn(() => Promise.resolve(true)),
    getItem: jest.fn(() => Promise.resolve({ instanceUrl, userToken })),
    removeItem: jest.fn(() => Promise.resolve()),
    clear: jest.fn(() => Promise.resolve()),
  };
});

describe('UserStorage', () => {
  describe('login', () => {
    const data = {
      avatar_url: 'https://gitlab.com/uploads/-/system/user/avatar/4066669/avatar.png',
      id: 123456,
      name: 'AA',
      state: 'active',
      username: 'AA',
      web_url: 'https://gitlab.com/alperakgun',
    };

    it('calls function to login', async () => {
      jest.spyOn(UserStorage, 'login');
      UserStorage.login.mockImplementation(() => Promise.resolve(200));
      const response = await UserStorage.login(instanceUrl, userToken);
      expect(response).toBe(200);

      global.fetch = jest.fn(() =>
        Promise.resolve({
          json: () =>
            Promise.resolve({
              data,
            }),
        }),
      );

      const result = await RNEncryptedStorage.setItem(
        'userInfo',
        JSON.stringify({
          id: data.id,
          username: data.username,
          name: data.name,
          state: data.state,
          avatarUrl: data.avatar_url,
          instanceUrl,
          userToken,
        }),
      );

      await expect(result).toBeTruthy();

      UserStorage.login.mockRestore();
      global.fetch.mockClear();
    });
  });

  describe('getUserInfo', () => {
    it('calls getItem to return values', () => {
      UserStorage.getUserInfo();
      return expect(RNEncryptedStorage.getItem('userInfo')).resolves.toEqual({
        instanceUrl,
        userToken,
      });
    });
  });

  describe('verifyToken', () => {
    it('calls function to verify token', async () => {
      jest.spyOn(UserStorage, 'verifyToken');
      UserStorage.verifyToken.mockImplementation(() => Promise.resolve(200));
      const response = await UserStorage.verifyToken(userToken);
      expect(response).toBe(200);

      UserStorage.verifyToken.mockRestore();
    });
  });

  describe('logout', () => {
    it('calls removeItem to clear storage', async () => {
      UserStorage.logout();
      return expect(RNEncryptedStorage.removeItem('userInfo')).resolves.toBeUndefined();
    });
  });
});
