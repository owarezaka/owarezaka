import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import MainHeader from '~app/navigation/MainHeader';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('MainHeader', () => {
  const title = 'GitLab';
  const onPress = jest.fn();

  describe('render elements correctly', () => {
    let tree;

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <MainHeader onPress={onPress} title={title} />
        </ThemeProvider>,
      );
    });

    it('renders settings button', () => {
      expect(tree.getByTestId('button')).toBeDefined();
    });

    it('renders icon', () => {
      expect(tree.getByTestId('logo')).toBeDefined();
    });
  });

  it('calls function to open settings', () => {
    const navigation = { navigate: jest.fn() };
    const { getByTestId } = render(
      <ThemeProvider>
        <MainHeader onPress={onPress} title={title} navigation={navigation} />
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId('button'));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
