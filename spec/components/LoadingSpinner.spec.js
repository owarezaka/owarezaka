import React from 'react';
import { render } from '@testing-library/react-native';
import LoadingSpinner from '~app/components/LoadingSpinner';
import { ThemeProvider } from '~app/store/theme-context';
import { Colors } from '~app/constants/GlobalStyles';

it('renders correctly', () => {
  const screen = render(
    <ThemeProvider>
      <LoadingSpinner />
    </ThemeProvider>,
  );

  expect(screen.getByTestId('spinner').props).toStrictEqual({
    animating: true,
    children: undefined,
    color: Colors.darkGray2,
    style: {
      padding: 20,
    },
    size: 'large',
    testID: 'spinner',
  });
});
