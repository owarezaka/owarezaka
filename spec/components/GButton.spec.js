import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import GButton from '~app/components/GButton';
import { Colors } from '~app/constants/GlobalStyles';
import '~config/i18n.config';

describe('GButton', () => {
  const mockAction = jest.fn();

  const defaultButtonStyle = {
    borderColor: Colors.border,
    borderRadius: 5,
    borderWidth: 0.7,
    flexDirection: 'column',
    opacity: 1,
    padding: 6,
    width: 100,
  };

  const primaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.primary,
  };

  const secondaryButtonStyle = {
    ...defaultButtonStyle,
    borderColor: Colors.primary,
  };

  const tertiaryButtonStyle = {
    ...defaultButtonStyle,
    borderWidth: 0,
  };

  const disabledButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.lightGray3,
  };

  const darkPrimaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.primary500,
    borderColor: Colors.primary500,
  };

  const darkSecondaryButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.dark,
    borderColor: Colors.lightGray,
  };

  const darkTertiaryButtonStyle = {
    ...defaultButtonStyle,
    borderWidth: 0,
  };

  const darkDisabledButtonStyle = {
    ...defaultButtonStyle,
    backgroundColor: Colors.darkGray3,
    borderColor: Colors.darkGray2,
  };

  it('renders button correctly', () => {
    const { getByText } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} size="small" />,
    );

    expect(getByText('Click me')).toBeDefined();
  });

  it('calls function when press button', () => {
    const { getByText } = render(
      <GButton label="Click me" click={() => mockAction()} size="small" />,
    );

    fireEvent.press(getByText(/Click me/));
    expect(mockAction).toHaveBeenCalledTimes(1);
  });

  it('renders button with default settings when category is not defined', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} size="small" />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(defaultButtonStyle);
  });

  it('renders button with relevant settings when category is primary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="primary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(primaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.secondary);
  });

  it('renders button with relevant settings when category is secondary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="secondary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(secondaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.primary);
  });

  it('renders button with relevant settings when category is tertiary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="tertiary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(tertiaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.dark);
  });

  it('renders button with relevant settings when category is dark primary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="darkPrimary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(darkPrimaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.dark);
  });

  it('renders button with relevant settings when category is dark secondary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="darkSecondary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(darkSecondaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.secondary);
  });

  it('renders button with relevant settings when category is dark tertiary', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="darkTertiary"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(darkTertiaryButtonStyle);
    expect(getByTestId('label').props.style.color).toBe(Colors.secondary);
  });

  it('renders button with relevant settings when category is disabled in light mode', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="disabled"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(disabledButtonStyle);
  });

  it('renders button with relevant settings when category is disabled in dark mode', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        category="darkDisabled"
        label="Click me"
        click={() => mockAction()}
        size="small"
      />,
    );

    expect(getByTestId('button').props.style).toStrictEqual(darkDisabledButtonStyle);
  });

  it('renders button width as 100, font size as 12 when size is defined as small', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} size="small" />,
    );

    expect(getByTestId('button').props.style.width).toBe(100);
    expect(getByTestId('label').props.style.fontSize).toBe(12);
  });

  it('renders button width as 50%, font size as 14 when size is defined as medium', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} size="medium" />,
    );

    expect(getByTestId('button').props.style.width).toBe('50%');
    expect(getByTestId('label').props.style.fontSize).toBe(15);
  });

  it('renders button width as 100%, font size as 16 when size is defined as large', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} size="large" />,
    );

    expect(getByTestId('button').props.style.width).toBe('100%');
    expect(getByTestId('label').props.style.fontSize).toBe(16);
  });

  it('renders primary button background as red when variant is defined as danger', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        label="Click me"
        click={() => mockAction()}
        size="large"
        category="primary"
        variant="danger"
      />,
    );

    expect(getByTestId('button').props.style.backgroundColor).toBe(Colors.red);
  });

  it('renders secondary button border color as red when variant is defined as danger', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        label="Click me"
        click={() => mockAction()}
        size="large"
        category="secondary"
        variant="danger"
      />,
    );

    expect(getByTestId('button').props.style.borderColor).toBe(Colors.red);
    expect(getByTestId('label').props.style.color).toBe(Colors.red);
  });

  it('renders given font size', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} fontSize={18} />,
    );

    expect(getByTestId('label').props.style.fontSize).toBe(18);
  });

  it('renders given width size', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} width={100} />,
    );

    expect(getByTestId('button').props.style.width).toBe(100);
  });

  it('renders given padding size', () => {
    const { getByTestId } = render(
      <GButton testID="button" label="Click me" click={() => mockAction()} padding={12} />,
    );

    expect(getByTestId('button').props.style.padding).toBe(12);
  });

  it('renders dark secondary button border and text color as red when variant is defined as danger', () => {
    const { getByTestId } = render(
      <GButton
        testID="button"
        label="Click me"
        click={() => mockAction()}
        size="large"
        category="darkSecondary"
        variant="danger"
      />,
    );

    expect(getByTestId('button').props.style.borderColor).toBe(Colors.red600);
    expect(getByTestId('label').props.style.color).toBe(Colors.red600);
  });
});
