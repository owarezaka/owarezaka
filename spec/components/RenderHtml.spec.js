import React from 'react';
import { render } from '@testing-library/react-native';
import RenderHtml from '~app/components/RenderHtml';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';
import user from '~spec/__mocks__/User.js';

describe('RenderHtml', () => {
  let screen;

  beforeEach(() => {
    screen = render(
      <ThemeProvider>
        <RenderHtml instanceUrl={user.instanceUrl} body={'<p>Hello from Owarezaka</p>'} />
      </ThemeProvider>,
    );
  });

  it('renders elements properly', () => {
    expect(screen.getByText(/Hello from Owarezaka/)).toBeDefined();
  });
});
