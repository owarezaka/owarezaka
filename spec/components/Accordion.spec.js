import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import Accordion from '~app/screens/MergeRequests/Accordion';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('Accordion', () => {
  const mockSnippet = 'const sum = (a, b) => a + b;';

  let screen;

  beforeEach(() => {
    screen = render(
      <ThemeProvider>
        <Accordion fileName="index.js" code={mockSnippet} />
      </ThemeProvider>,
    );
  });

  it('displays file name', () => {
    expect(screen.getByText(/index\.js/)).toBeDefined();
  });

  it('displays button', () => {
    expect(screen.getByTestId('accordion')).toBeDefined();
  });

  it('displays code when pressing button', () => {
    fireEvent.press(screen.getByTestId('accordion'));
    expect(screen.getByText(/const sum =/)).toBeDefined();
  });
});
