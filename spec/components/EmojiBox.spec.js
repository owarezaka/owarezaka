import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import { ThemeProvider } from '~app/store/theme-context';
import EmojiBox from 'app/components/EmojiBox';
import emojis from 'app/assets/emojis.json';

describe('EmojiBox', () => {
  it('renders initial emojis', () => {
    const close = jest.fn();
    const screen = render(
      <ThemeProvider>
        <EmojiBox close={close} />
      </ThemeProvider>,
    );

    const activityEmojis = Object.keys(emojis)
      .map((el) => emojis[el])
      .filter((e) => e.c === 'activity');

    expect(screen.getByTestId('emoji-list').props.data).toEqual(activityEmojis);
  });

  it('renders correct emojis after switching emoji tabs', () => {
    const close = jest.fn();
    const screen = render(
      <ThemeProvider>
        <EmojiBox close={close} />
      </ThemeProvider>,
    );

    const natureEmojis = Object.keys(emojis)
      .map((el) => emojis[el])
      .filter((e) => e.c === 'nature');

    fireEvent.press(screen.getByTestId('nature'));

    expect(screen.getByTestId('emoji-list').props.data).toEqual(natureEmojis);
    expect(screen.getByText('🐒')).toBeDefined();
  });

  it('outputs emoji when pressed', () => {
    const onPress = jest.fn();
    const screen = render(
      <ThemeProvider>
        <EmojiBox onPress={onPress} />
      </ThemeProvider>,
    );

    fireEvent.press(screen.getByText('🎱'));
    expect(onPress).toHaveBeenCalledTimes(1);
  });
});
