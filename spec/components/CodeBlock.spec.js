import React from 'react';
import { render } from '@testing-library/react-native';
import CodeBlock from '~app/components/CodeBlock';
import { ThemeProvider } from '~app/store/theme-context';

it('renders correctly', () => {
  const mockSnippet = 'const sum = (a, b) => a + b;';
  const screen = render(
    <ThemeProvider>
      <CodeBlock code={mockSnippet} />
    </ThemeProvider>,
  );

  expect(screen.getByText(/const sum =/)).toBeDefined();
});
