import React from 'react';
import { TouchableOpacity } from 'react-native';
import { render, fireEvent } from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import NavigationButton from '~app/components/NavigationButton';
import Icon from 'react-native-vector-icons/AntDesign';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('NavigationButton', () => {
  const mockOnPress = jest.fn();

  describe('renders elements properly', () => {
    let testInstance;

    beforeEach(() => {
      const testRenderer = renderer.create(
        <ThemeProvider>
          <NavigationButton title="Issues" subTitle="#49" onPress={mockOnPress} />
        </ThemeProvider>,
      );
      testInstance = testRenderer.root;
    });

    it('renders navigation button', () => {
      expect(testInstance.findByType(TouchableOpacity).props.onPress).toBeDefined();
    });

    it('renders navigation left arrow icon', () => {
      expect(testInstance.findByType(Icon).props.name).toBe('arrowleft');
    });

    it('renders navigation title', () => {
      expect(testInstance.findByProps({ testID: 'title' }).props.children).toBe('Issues');
    });

    it('renders navigation right arrow icon', () => {
      expect(testInstance.findByProps({ testID: 'angleRight' })).toBeDefined();
    });

    it('renders navigation subtitle', () => {
      expect(testInstance.findByProps({ testID: 'subTitle' }).props.children).toBe('#49');
    });
  });

  it('calls onPress function when pressing button to navigate back', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <NavigationButton title="Issues" testID="btn" onPress={mockOnPress} />
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId('btn'));
    expect(mockOnPress).toHaveBeenCalledTimes(1);
  });
});
