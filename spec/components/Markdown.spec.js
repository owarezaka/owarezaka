import React from 'react';
import { render } from '@testing-library/react-native';
import Markdown from '~app/components/Markdown';
import { ThemeProvider } from '~app/store/theme-context';

it('renders correctly', () => {
  const mockBody = '### Heading One';

  const screen = render(
    <ThemeProvider>
      <Markdown body={mockBody} />
    </ThemeProvider>,
  );

  expect(screen.getByText(/Heading One/)).toBeDefined();
});
