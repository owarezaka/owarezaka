import React from 'react';
import { render } from '@testing-library/react-native';
import EmptyScreen from '~app/components/EmptyScreen';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

it('renders correctly', () => {
  const refreshing = false;
  const setRefreshing = jest.fn();

  const screen = render(
    <ThemeProvider>
      <EmptyScreen
        refreshing={refreshing}
        setRefreshing={setRefreshing}
        title="There is nothing here"
        text="Check back later to see when anything involving you happens!"
      />
    </ThemeProvider>,
  );

  expect(screen.getByText('There is nothing here')).toBeDefined();
  expect(
    screen.getByText('Check back later to see when anything involving you happens!'),
  ).toBeDefined();
});
