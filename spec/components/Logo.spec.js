import React from 'react';
import Svg, { Path } from 'react-native-svg';
import Logo from '~app/components/Logo';
import { Colors } from '~app/constants/GlobalStyles';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const testRenderer = renderer.create(<Logo width="24" height="24" />);
  const testInstance = testRenderer.root;

  expect(testInstance.findByType(Svg).props.width).toBe('24');
  expect(testInstance.findByType(Svg).props.height).toBe('24');
  expect(testInstance.findAllByType(Path)[0].props.d).toBe(
    'M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z',
  );
  expect(testInstance.findAllByType(Path)[0].props.fill).toBe(Colors.orangeRed);
});
