import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import GAlert from '~app/components/GAlert';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('GAlert', () => {
  it('renders elements properly', () => {
    const mockPrimaryAction = jest.fn();
    const mockSecondaryAction = jest.fn();

    const { getByText, getByTestId } = render(
      <ThemeProvider>
        <GAlert
          testID="alert"
          visible={true}
          variant="Tip"
          title="Tip"
          message="You must already have an account on the GitLab Instance or on gitlab.com to generate access token"
          primaryButtonText="Open in browser"
          secondaryButtonText="Cancel"
          primaryAction={() => mockPrimaryAction()}
          secondaryAction={() => mockSecondaryAction()}
        />
      </ThemeProvider>,
    );

    expect(getByTestId('alert')).toBeDefined();
    expect(getByText(/Tip/)).toBeDefined();
    expect(
      getByText(
        /You must already have an account on the GitLab Instance or on gitlab.com to generate access/,
      ),
    ).toBeDefined();
    expect(getByText(/Open in browser/)).toBeDefined();
    expect(getByText(/Cancel/)).toBeDefined();

    fireEvent.press(getByText(/Open in browser/));
    expect(mockPrimaryAction).toHaveBeenCalledTimes(1);

    fireEvent.press(getByText(/Cancel/));
    expect(mockSecondaryAction).toHaveBeenCalledTimes(1);
  });

  it('render correct icon if variant is success', () => {
    const screen = render(
      <ThemeProvider>
        <GAlert variant="Success" visible={true} title="Title" message="Message" />
      </ThemeProvider>,
    );

    expect(screen.getByTestId('statusSuccess')).toBeDefined();
  });

  it('render correct icon if variant is warning', () => {
    const screen = render(
      <ThemeProvider>
        <GAlert variant="Warning" visible={true} title="Title" message="Message" />
      </ThemeProvider>,
    );

    expect(screen.getByTestId('warning')).toBeDefined();
  });

  it('render correct icon if variant is danger', () => {
    const screen = render(
      <ThemeProvider>
        <GAlert variant="Danger" visible={true} title="Title" message="Message" />
      </ThemeProvider>,
    );

    expect(screen.getByTestId('statusWarning')).toBeDefined();
  });

  it('render correct icon if variant is info', () => {
    const screen = render(
      <ThemeProvider>
        <GAlert variant="Info" visible={true} title="Title" message="Message" />
      </ThemeProvider>,
    );

    expect(screen.getByTestId('information')).toBeDefined();
  });

  it('render correct icon if variant is tip', () => {
    const screen = render(
      <ThemeProvider>
        <GAlert variant="Tip" visible={true} title="Title" message="Message" />
      </ThemeProvider>,
    );

    expect(screen.getByTestId('bulb')).toBeDefined();
  });

  it('throw error if variant is not defined or mistyped', () => {
    const spyError = jest.spyOn(console, 'error');
    spyError.mockImplementation(() => jest.fn());

    expect(() =>
      render(
        <ThemeProvider>
          <GAlert />
        </ThemeProvider>,
      ),
    ).toThrow('Unknown Icon Type');
    expect(() =>
      render(
        <ThemeProvider>
          <GAlert variant="error" />
        </ThemeProvider>,
      ),
    ).toThrow('Unknown Icon Type');

    spyError.mockRestore();
  });
});
