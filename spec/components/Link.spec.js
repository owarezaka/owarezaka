import React from 'react';
import { Pressable, Text } from 'react-native';
import Link from '~app/components/Link';
import Gitlab from '~app/constants/Gitlab';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const testRenderer = renderer.create(<Link name="about.gitlab.com" url={Gitlab.about} />);
  const testInstance = testRenderer.root;

  expect(testInstance.findByType(Text).props.children).toEqual('about.gitlab.com');
  expect(testInstance.findByType(Pressable).props.url).toEqual(Gitlab.url);
});
