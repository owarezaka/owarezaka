import React from 'react';
import { render } from '@testing-library/react-native';
import { Root } from '~app/App';
import { AuthContext } from '~app/store/auth-context';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';
import user from '~spec/__mocks__/User.js';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('./__mocks__/react-native-reanimated.js');

const customRender = (ui, { providerProps, ...renderOptions }) => {
  return render(
    <AuthContext.Provider {...providerProps}>{ui}</AuthContext.Provider>,
    renderOptions,
  );
};

describe('App screen', () => {
  it('renders splash as initial', () => {
    const screen = render(
      <AuthContext.Provider value={{ isLoading: true }}>
        <ThemeProvider>
          <Root />
        </ThemeProvider>
      </AuthContext.Provider>,
    );

    expect(screen.getByTestId('splash')).toBeDefined();
  });

  it('renders Fallback Screen on error', () => {
    const providerProps = {
      value: { isLoading: false, showError: true },
    };

    const screen = customRender(
      <ThemeProvider>
        <Root />
      </ThemeProvider>,
      { providerProps },
    );

    expect(screen.getByText(/Network Request Failed/)).toBeDefined();
  });

  it('renders Login Screen if user is not logged in', () => {
    const providerProps = {
      value: { isLoading: false, token: null },
    };

    const screen = customRender(
      <ThemeProvider>
        <Root />
      </ThemeProvider>,
      { providerProps },
    );

    expect(screen.getByText(/Owarezaka/)).toBeDefined();
  });

  it('renders Home Screen if user is logged in successfully', () => {
    const providerProps = {
      value: { isLoading: false, token: user.userToken, isSignOut: false },
    };

    const screen = customRender(
      <ThemeProvider>
        <Root />
      </ThemeProvider>,
      { providerProps },
    );

    expect(screen.getByText(/Home/)).toBeDefined();
  });
});
