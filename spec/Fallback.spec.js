import React from 'react';
import { render } from '@testing-library/react-native';
import Fallback from '~app/Fallback';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('./__mocks__/react-native-reanimated.js');

describe('Fallback', () => {
  let screen;

  beforeEach(() => {
    screen = render(
      <ThemeProvider>
        <Fallback />
      </ThemeProvider>,
    );
  });

  it('renders Logo', () => {
    expect(screen.getByTestId('logo')).toBeDefined();
  });

  it('renders error message', () => {
    expect(screen.getByText('Network Request Failed')).toBeDefined();
    expect(screen.getByText('Please retry again later')).toBeDefined();
  });

  it('renders try again button', () => {
    expect(screen.getByText('Try Again')).toBeDefined();
  });
});
