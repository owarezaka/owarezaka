import React from 'react';
import { FlatList } from 'react-native';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import ActivityScreen from '~app/screens/Activity/ActivityScreen';
import ActivityItem from '~app/screens/Activity/ActivityItem';
import '~config/i18n.config';
import data from '~spec/__mocks__/ActivityItem';
import joe from '~spec/__mocks__/User.js';
import { Colors } from '~app/constants/GlobalStyles';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Activity screen', () => {
  const mockDataActivities = [data];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getAllActivities.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <ActivityScreen />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.getAllActivities.mockRestore();
    });
  });

  describe('when Activity list is loaded', () => {
    describe('when Activity list has items', () => {
      beforeEach(() => {
        GitlabApi.getAllActivities.mockResolvedValue(mockDataActivities);
      });

      it('displays fetched Activities', async () => {
        const screen = render(
          <ThemeProvider>
            <ActivityScreen />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.queryByTestId('Activity-List')).not.toBeNull());
      });

      afterEach(() => {
        GitlabApi.getAllActivities.mockRestore();
      });
    });

    describe('when Activity list is empty', () => {
      beforeEach(() => {
        GitlabApi.getAllActivities.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <ActivityScreen />
          </ThemeProvider>,
        );

        expect(
          await screen.findByText('Check back later to see when anything involving you happens!'),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllActivities.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getAllActivities.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <ActivityScreen />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.queryByText(/Connection failed./)).not.toBeNull());
      });

      afterEach(() => {
        GitlabApi.getAllActivities.mockRestore();
      });
    });
  });

  it('calls function to fetch more items when scroll down', async () => {
    const mockList = Array(50).fill(data);

    GitlabApi.getAllActivities.mockResolvedValue(mockList);

    const fetchMore = jest.fn();

    const screen = render(
      <ThemeProvider>
        <FlatList
          testID="Activity-List"
          data={mockDataActivities}
          renderItem={({ item }) => {
            return <ActivityItem activity={item} />;
          }}
          onEndReached={fetchMore}
          onEndReachedThreshold={0.5}
        />
      </ThemeProvider>,
    );

    const list = await waitFor(() => screen.queryByTestId('Activity-List'));

    const eventData = {
      nativeEvent: {
        contentOffset: {
          y: 322,
        },
        contentSize: {
          // Dimensions of the scrollable content
          height: 720,
          width: 320,
        },
        layoutMeasurement: {
          // Dimensions of the device
          height: 400,
          width: 320,
        },
      },
    };

    fireEvent.scroll(list, eventData);
    expect(fetchMore).toHaveBeenCalledTimes(1);

    GitlabApi.getAllActivities.mockRestore();
  });
});
