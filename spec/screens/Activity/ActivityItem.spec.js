import React from 'react';
import { render } from '@testing-library/react-native';
import ActivityItem from '~app/screens/Activity/ActivityItem';
import activities from '~spec/__mocks__/ActivityItem';
import * as Utils from '~app/utils/Utils';
import { ThemeProvider } from '~app/store/theme-context';

describe('ActivityItem', () => {
  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <ActivityItem activity={activities} />
        </ThemeProvider>,
      );
    });

    it('renders Image element', () => {
      expect(screen.getByTestId('image').props.source).toStrictEqual({
        uri: activities.author.avatar_url,
      });
    });

    it('renders author name', () => {
      expect(screen.getByText(activities.author.name)).toBeDefined();
    });

    it('renders author username', () => {
      expect(screen.getByText(activities.author.username)).toBeDefined();
    });

    it('renders Icon element', () => {
      expect(screen.getByTestId('checkCircle')).toBeDefined();
    });

    it('renders created time', () => {
      expect(screen.getByText(Utils.timeSince(activities.created_at))).toBeDefined();
    });
  });

  describe('renders Icons', () => {
    it('should render statusOpen icon when action name is opened', () => {
      const activity = {
        id: 1668733354,
        action_name: 'opened',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('statusOpen')).toBeDefined();
    });

    it('should render fork icon when action name is accepted', () => {
      const activity = {
        id: 1668733354,
        action_name: 'accepted',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('fork')).toBeDefined();
    });

    it('should render comment icon when action name is commented on', () => {
      const activity = {
        id: 1668733354,
        action_name: 'commented on',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
        note: {
          noteable_type: 'MergeRequest',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('comment')).toBeDefined();
    });

    it('should render commit icon when action name is pushed to', () => {
      const activity = {
        id: 1668733354,
        action_name: 'pushed to',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('commit')).toBeDefined();
    });

    it('should render commit icon when action name is pushed new', () => {
      const activity = {
        id: 1668733354,
        action_name: 'pushed new',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('commit')).toBeDefined();
    });

    it('should render checkCircle icon when action name is closed', () => {
      const activity = {
        id: 1668733354,
        action_name: 'closed',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('checkCircle')).toBeDefined();
    });

    it('should render remove icon when action name is deleted', () => {
      const activity = {
        id: 1668733354,
        action_name: 'deleted',
        target_id: 100580383,
        target_iid: 80,
        target_type: 'Issue',
        target_title: 'Add readme',
        created_at: '2022-01-17T18:29:23.831Z',
        author: {
          name: 'John Doe',
          username: 'john_doe',
          avatar_url: '#',
        },
      };

      const screen = render(
        <ThemeProvider>
          <ActivityItem activity={activity} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('remove')).toBeDefined();
    });
  });
});
