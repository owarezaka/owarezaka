import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import { ThemeProvider } from '~app/store/theme-context';
import SetStatus from '~app/screens/Profile/SetStatus';
import GitlabApi from '~app/lib/auth/GitlabApi';
import user from '~spec/__mocks__/User.js';
import '~config/i18n.config';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

describe('Set Status screen', () => {
  const navigation = { setOptions: jest.fn() };
  const getUserStatus = jest.fn();
  const status = 'Busy';

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <SetStatus navigation={navigation} route={{ params: { getUserStatus, status } }} />
        </ThemeProvider>,
      );
    });

    it('renders Alert element', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders input element', () => {
      expect(screen.getByTestId('status')).toBeDefined();
    });

    it('renders clear button', () => {
      expect(screen.getByTestId('clearButton')).toBeDefined();
    });

    it('renders remove button', () => {
      expect(screen.getByTestId('removeButton')).toBeDefined();
    });

    it('renders set status button', () => {
      expect(screen.getByTestId('setStatusButton')).toBeDefined();
    });

    it('renders emoji button', () => {
      expect(screen.getByTestId('emoji')).toBeDefined();
    });
  });

  it('should allow change input element', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SetStatus navigation={navigation} route={{ params: { getUserStatus, status } }} />
      </ThemeProvider>,
    );

    const input = getByTestId('status');
    fireEvent.changeText(input, 'Busy');
    expect(input.props.value).toBe('Busy');
  });

  it('should clear input element by clicking', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SetStatus navigation={navigation} route={{ params: { getUserStatus, status } }} />
      </ThemeProvider>,
    );

    const input = getByTestId('status');
    const clearBtn = getByTestId('clearButton');
    fireEvent.changeText(input, 'Busy');
    fireEvent.press(clearBtn);
    expect(input.props.value).toBe('');
  });

  it('should reject with an error if it could not be successfully set a status', async () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SetStatus navigation={navigation} route={{ params: { getUserStatus, status } }} />
      </ThemeProvider>,
    );

    const input = getByTestId('status');
    const setStatusBtn = getByTestId('setStatusButton');

    fireEvent.changeText(input, 'Busy');
    fireEvent.press(setStatusBtn);

    jest.spyOn(GitlabApi, 'setUserStatus');
    GitlabApi.setUserStatus.mockImplementation(() =>
      Promise.reject(new Error('Connection failed')),
    );

    await expect(GitlabApi.setUserStatus(user.userToken, 'Busy')).rejects.toThrow(
      'Connection failed',
    );

    GitlabApi.setUserStatus.mockRestore();
  });

  it('should reject with an error if it could not be successfully removed', async () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SetStatus navigation={navigation} route={{ params: { getUserStatus, status } }} />
      </ThemeProvider>,
    );

    const removeBtn = getByTestId('removeButton');
    fireEvent.press(removeBtn);

    jest.spyOn(GitlabApi, 'removeUserStatus');
    GitlabApi.removeUserStatus.mockImplementation(() =>
      Promise.reject(new Error('Connection failed')),
    );

    await expect(GitlabApi.removeUserStatus(user.instanceUrl, user.userToken)).rejects.toThrow(
      'Connection failed',
    );

    GitlabApi.removeUserStatus.mockRestore();
  });
});
