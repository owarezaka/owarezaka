import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import ProfileScreen from '~app/screens/Profile/ProfileScreen';
import { AuthContext } from '~app/store/auth-context';
import { ThemeProvider } from '~app/store/theme-context';
import GitlabApi from '~app/lib/auth/GitlabApi';
import userStatus from '~spec/__mocks__/UserStatus';
import '~config/i18n.config';

jest.mock('~app/lib/auth/GitlabApi');

describe('Profile screen', () => {
  const signOutHandler = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      GitlabApi.getUserStatus.mockResolvedValue(userStatus);

      screen = render(
        <AuthContext.Provider value={{ signOutHandler }}>
          <ThemeProvider>
            <ProfileScreen />
          </ThemeProvider>
        </AuthContext.Provider>,
      );
    });

    it('renders AccountDetails component', () => {
      expect(screen.getByTestId('user')).toBeDefined();
    });

    it('renders set status button', () => {
      expect(screen.getByTestId('setStatusButton')).toBeDefined();
    });

    it('renders sign out button', () => {
      expect(screen.getByTestId('signOutButton')).toBeDefined();
    });
  });

  describe('when an error occurs', () => {
    beforeEach(() => {
      GitlabApi.getUserStatus.mockResolvedValue({ success: false });
    });

    it('displays an alert', async () => {
      const screen = render(
        <AuthContext.Provider value={{ signOutHandler }}>
          <ThemeProvider>
            <ProfileScreen />
          </ThemeProvider>
        </AuthContext.Provider>,
      );

      await waitFor(() => expect(screen.queryByTestId('alert')).not.toBeNull());
    });

    afterEach(() => {
      GitlabApi.getUserStatus.mockRestore();
    });
  });

  it('calls exit function by pressing button', () => {
    const { getByText } = render(
      <AuthContext.Provider value={{ signOutHandler }}>
        <ThemeProvider>
          <ProfileScreen />
        </ThemeProvider>
      </AuthContext.Provider>,
    );

    fireEvent.press(getByText(/Sign Out/));
    expect(signOutHandler).toHaveBeenCalledTimes(1);
  });
});
