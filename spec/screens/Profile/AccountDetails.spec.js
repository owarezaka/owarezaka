import React from 'react';
import { render } from '@testing-library/react-native';
import AccountDetails from '~app/screens/Profile/AccountDetails';
import userStatus from '~spec/__mocks__/UserStatus';
import user from '~spec/__mocks__/User.js';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('AccountDetails', () => {
  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <AccountDetails
            testID="user"
            user={Object.assign(userStatus, { instanceUrl: user.instanceUrl })}
          />
        </ThemeProvider>,
      );
    });

    it('renders image component', () => {
      expect(screen.getByTestId('avatar').props.source).toStrictEqual({
        uri: `${user.instanceUrl}${userStatus.avatarUrl}`,
      });
    });

    it('renders user name', () => {
      expect(screen.getByText(userStatus.name)).toBeDefined();
    });

    it('renders username', () => {
      expect(screen.getByText(`@${userStatus.username}`)).toBeDefined();
    });

    it('renders user status', () => {
      expect(screen.getByText(userStatus.status.message)).toBeDefined();
    });
  });
});
