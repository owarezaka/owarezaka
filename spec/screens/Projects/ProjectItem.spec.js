import React from 'react';
import { Image } from 'react-native';
import renderer from 'react-test-renderer';
import ProjectItem from '~app/screens/Projects/ProjectItem';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import { ThemeProvider } from '~app/store/theme-context';

describe('render elements correctly in Project Item', () => {
  const mockDataProject = {
    id: 'gid://gitlab/Project/30541283',
    name: 'GitLab Mobile',
    namespace: {
      name: 'Owarezaka',
    },
    group: {
      id: 'gid://gitlab/Group/13777676',
      name: 'Owarezaka',
    },
    avatarUrl: null,
    description: 'Yet Another GitLab Mobile',
    visibility: 'public',
    starCount: 3,
    lastActivityAt: '2022-02-04T16:29:17Z',
    pipelines: {
      nodes: [
        {
          detailedStatus: {
            icon: 'status_success',
          },
        },
      ],
    },
  };

  const mockDataRole = {
    groupMemberships: {
      nodes: [
        {
          group: {
            id: 'gid://gitlab/Group/13777676',
          },
          accessLevel: {
            stringValue: 'DEVELOPER',
          },
        },
      ],
    },
    projectMemberships: {
      nodes: [
        {
          project: {
            id: 'gid://gitlab/Project/30541283',
          },
          accessLevel: {
            stringValue: 'DEVELOPER',
          },
        },
      ],
    },
  };

  let testInstance;

  beforeEach(() => {
    const testRenderer = renderer.create(
      <ThemeProvider>
        <ProjectItem project={mockDataProject} roles={mockDataRole} />
      </ThemeProvider>,
    );
    testInstance = testRenderer.root;
  });

  it('renders Image element', () => {
    expect(testInstance.findByType(Image)).toBeDefined();
  });

  it('renders Project namespace', () => {
    expect(
      testInstance.findByProps({ testID: `namespace${mockDataProject.id}` }).props.children,
    ).toEqual(`${mockDataProject.group.name} / `);
  });

  it('renders Project name', () => {
    expect(
      testInstance.findByProps({ testID: `name${mockDataProject.id}` }).props.children,
    ).toEqual(mockDataProject.name);
  });

  it('renders visibility Icon element if project is public', () => {
    expect(testInstance.findAllByType(Icons.earth)).toBeDefined();
  });

  it('renders role element correctly', () => {
    expect(
      testInstance.findByProps({ testID: `role${mockDataProject.id}` }).props.children,
    ).toEqual(
      `${Utils.findRoleById(
        mockDataProject.id,
        mockDataRole.projectMemberships?.nodes,
        mockDataProject.group?.id,
        mockDataRole.groupMemberships?.nodes,
      )}`,
    );
  });

  it('renders correct Icon element if pipeline status is success', () => {
    expect(testInstance.findAllByType(Icons.statusSuccess)).toBeDefined();
  });

  it('renders star Icon element', () => {
    expect(testInstance.findAllByType(Icons.star)).toBeDefined();
  });

  it('renders authored time', () => {
    expect(
      testInstance.findByProps({ testID: `since${mockDataProject.id}` }).props.children,
    ).toEqual(`Updated ${Utils.timeSince(mockDataProject.lastActivityAt)}`);
  });
});
