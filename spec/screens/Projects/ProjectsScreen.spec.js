import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, waitFor } from '@testing-library/react-native';
import ProjectsScreen from '~app/screens/Projects/ProjectsScreen';
import '~config/i18n.config';
import data from '~spec/__mocks__/ProjectItem';
import joe from '~spec/__mocks__/User.js';
import { Colors } from '~app/constants/GlobalStyles';
import { ThemeProvider } from '~app/store/theme-context';
import { NavigationContainer } from '@react-navigation/native';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Projects screen', () => {
  const mockDataProjects = [data];
  const navigation = { goBack: jest.fn() };
  const route = { params: undefined };

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getAllProjects.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <NavigationContainer>
          <ThemeProvider>
            <ProjectsScreen navigation={navigation} route={route} />
          </ThemeProvider>
        </NavigationContainer>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.getAllProjects.mockRestore();
    });
  });

  describe('when Projects list is loaded', () => {
    describe('when Projects list has items', () => {
      beforeEach(() => {
        GitlabApi.getAllProjects.mockResolvedValue(mockDataProjects);
      });

      it('displays fetched projects', async () => {
        const screen = render(
          <NavigationContainer>
            <ThemeProvider>
              <ProjectsScreen navigation={navigation} route={route} />
            </ThemeProvider>
          </NavigationContainer>,
        );

        await waitFor(() => expect(screen.queryByTestId(`projectsList`)).toBeDefined());
      });

      afterEach(() => {
        GitlabApi.getAllProjects.mockRestore();
      });
    });

    describe('when Projects list is empty', () => {
      beforeEach(() => {
        GitlabApi.getAllProjects.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <NavigationContainer>
            <ThemeProvider>
              <ProjectsScreen navigation={navigation} route={route} />
            </ThemeProvider>
          </NavigationContainer>,
        );

        expect(
          await screen.findByText('You are currently not part of any projects.'),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllProjects.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getAllProjects.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <NavigationContainer>
            <ThemeProvider>
              <ProjectsScreen navigation={navigation} route={route} />
            </ThemeProvider>
          </NavigationContainer>,
        );

        expect(await screen.findByText(/Connection failed./)).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllProjects.mockRestore();
      });
    });
  });
});
