import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';
import ProjectRepo from '~app/screens/Projects/ProjectRepo';
import mockRepoFile from '/spec/__mocks__/MockRepoFile';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { Colors } from '~app/constants/GlobalStyles';

jest.mock('~app/lib/auth/GitlabApi');

describe('Project Repo Screen', () => {
  let screen;
  const onPress = () => jest.fn();

  beforeEach(() => {
    screen = render(
      <ThemeProvider>
        <ProjectRepo onPress={onPress} path={mockRepoFile.projectPath} />,
      </ThemeProvider>,
    );
  });
  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getFileInfo.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const spinner = screen.getByTestId('spinner');

      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.getFileInfo.mockRestore();
    });
  });

  describe('when file names are loaded', () => {
    beforeEach(() => {
      GitlabApi.getFileInfo.mockResolvedValue(mockRepoFile);
    });

    it('renders file item', async () => {
      await waitFor(() => expect(screen.queryByTestId(`file-list`)).toBeDefined());
    });

    afterEach(() => {
      GitlabApi.getFileInfo.mockRestore();
    });
  });
});
