import React from 'react';
import { render } from '@testing-library/react-native';
import GitlabApi from '~app/lib/auth/GitlabApi';
import mockReadMe from '~spec/__mocks__/MockReadMe';
import mockProjectData from '~spec/__mocks__/ProjectItem';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';
import ProjectDetails from '~app/screens/Projects/ProjectDetails';

describe('Project Details screen', () => {
  const onPress = () => jest.fn();
  describe('renders elements correctly', () => {
    let screen;

    beforeEach(() => {
      jest.spyOn(GitlabApi, 'getReadMe');
      GitlabApi.getReadMe.mockImplementation(() => Promise.resolve(mockReadMe));

      screen = render(
        <ThemeProvider>
          <ProjectDetails project={mockProjectData} onPress={onPress} />
        </ThemeProvider>,
      );
    });

    afterEach(() => {
      GitlabApi.getReadMe.mockRestore();
    });

    it('render alert element', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders overview header component', () => {
      expect(screen.getByTestId('overviewHeader')).toBeDefined();
    });

    it('renders project description', () => {
      expect(screen.queryByText('Yet Another GitLab Mobile')).toBeDefined();
    });

    it('renders repository button', () => {
      expect(screen.queryByText('Go To Repository')).toBeDefined();
    });

    it('renders README.md', () => {
      expect(screen.queryByText(mockReadMe.rawBlob)).toBeDefined();
    });
  });
});
