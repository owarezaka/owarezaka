import React from 'react';
import { render } from '@testing-library/react-native';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';
import FileItem from '~app/screens/Projects/FileItem';
import mockFile from '/spec/__mocks__/MockRepoFile';

describe('File Item', () => {
  let screen;
  const onPress = () => jest.fn();

  beforeEach(() => {
    screen = render(
      <ThemeProvider>
        <FileItem name={mockFile.name} onPress={onPress} />,
      </ThemeProvider>,
    );
  });
  it('renders correct file name', () => {
    expect(screen.getByText(mockFile.name)).toBeDefined();
  });
});
