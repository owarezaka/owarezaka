import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';
import FileDetails from '~app/screens/Projects/FileDetails';
import mockFile from 'spec/__mocks__/MockRepoFile.js';

describe('File Details', () => {
  const onPress = () => jest.fn();

  describe('loading state', () => {
    let screen;
    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <FileDetails
            fileName={mockFile.name}
            onPress={onPress}
            code={undefined}
            showError={false}
            size={10}
          />
          ,
        </ThemeProvider>,
      );
    });

    it('renders a spinner', () => {
      expect(screen.getByTestId('spinner')).toBeDefined();
    });
  });

  describe('when code exists', () => {
    let screen;
    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <FileDetails
            fileName={mockFile.name}
            onPress={onPress}
            code={mockFile.code}
            showError={false}
            size={10}
          />
          ,
        </ThemeProvider>,
      );
    });

    it('renders code block', async () => {
      await waitFor(() => expect(screen.queryByTestId(`code`)).toBeDefined());
    });

    it('renders code', async () => {
      await waitFor(() => expect(screen.queryByText(/const sum/)).toBeDefined());
    });
  });

  describe('when an error occurs', () => {
    it('should throw an error when the file is too big', async () => {
      const screen = render(
        <ThemeProvider>
          <FileDetails
            fileName={mockFile.name}
            onPress={onPress}
            code={mockFile.code}
            showError={false}
            size={100000000}
          />
          ,
        </ThemeProvider>,
      );

      await waitFor(() => expect(screen.queryByText(/File too large!/)).toBeDefined());
    });

    it('should throw an error when file is not a text document', async () => {
      const screen = render(
        <ThemeProvider>
          <FileDetails
            fileName={mockFile.name}
            onPress={onPress}
            code={null}
            showError={false}
            size={10}
          />
          ,
        </ThemeProvider>,
      );
      await waitFor(() =>
        expect(screen.queryByText(/Only text documents are supported./)).toBeDefined(),
      );
    });

    it('should throw an error when it fails to retrieve data', async () => {
      const screen = render(
        <ThemeProvider>
          <FileDetails
            fileName={mockFile.name}
            onPress={onPress}
            code={mockFile.code}
            showError={true}
            size={10}
          />
          ,
        </ThemeProvider>,
      );
      await waitFor(() => expect(screen.queryByText(/Connection failed/)).toBeDefined());
    });
  });
});
