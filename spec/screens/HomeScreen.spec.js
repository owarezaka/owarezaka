import React from 'react';
import { Text } from 'react-native';
import { fireEvent, render } from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import HomeScreen from '~app/screens/HomeScreen';
import Icons from '~app/assets/Icons';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('Home screen', () => {
  describe('render elements correctly', () => {
    let testInstance;

    beforeEach(() => {
      const testRenderer = renderer.create(
        <ThemeProvider>
          <HomeScreen />
        </ThemeProvider>,
      );
      testInstance = testRenderer.root;
    });

    it('renders project icon', () => {
      expect(testInstance.findByType(Icons.project)).toBeDefined();
    });

    it('renders project button', () => {
      expect(testInstance.findAllByType(Text)[0].props.children).toBe('Projects');
    });

    it('renders to-do list icon', () => {
      expect(testInstance.findByType(Icons.todoDone)).toBeDefined();
    });

    it('renders to-do list button', () => {
      expect(testInstance.findAllByType(Text)[1].props.children).toBe('To-Do List');
    });

    it('renders groups icon', () => {
      expect(testInstance.findByType(Icons.group)).toBeDefined();
    });

    it('renders groups button', () => {
      expect(testInstance.findAllByType(Text)[2].props.children).toBe('Groups');
    });

    it('renders snippets icon', () => {
      expect(testInstance.findByType(Icons.snippet)).toBeDefined();
    });

    it('renders snippets button', () => {
      expect(testInstance.findAllByType(Text)[3].props.children).toBe('Snippets');
    });

    it('renders activity icon', () => {
      expect(testInstance.findByType(Icons.history)).toBeDefined();
    });

    it('renders activity button', () => {
      expect(testInstance.findAllByType(Text)[4].props.children).toBe('Activity');
    });

    it('renders help icon', () => {
      expect(testInstance.findByType(Icons.question)).toBeDefined();
    });
  });

  it('should navigate to Projects Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    );
    fireEvent.press(getByText(/Projects/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('Projects');
  });

  it('should navigate to To-Do List Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    );
    fireEvent.press(getByText(/To-Do List/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('ToDoList');
  });

  it('should navigate to Groups Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    );
    fireEvent.press(getByText(/Groups/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('Groups');
  });

  it('should navigate to Snippets Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    );
    fireEvent.press(getByText(/Snippets/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('Snippets');
  });

  it('should navigate to Activity Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    );
    fireEvent.press(getByText(/Activity/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('Activity');
  });

  it('should navigate to Help Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const testInstance = renderer.create(
      <ThemeProvider>
        <HomeScreen navigation={navigation} />
      </ThemeProvider>,
    ).root;
    fireEvent.press(testInstance.findByType(Icons.question));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('Help');
  });
});
