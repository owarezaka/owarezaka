import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import SnippetDetails from '~app/screens/Snippets/SnippetDetails';
import GitlabApi from '~app/lib/auth/GitlabApi';
import * as Utils from '~app/utils/Utils';
import mockDataSnippet from '~spec/__mocks__/SnippetItem';
import snippetBody from '~spec/__mocks__/snippetBody';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('Snippet Details screen', () => {
  const handlePress = jest.fn();

  describe('renders elements correctly', () => {
    let screen;

    beforeEach(() => {
      jest.spyOn(GitlabApi, 'getSnippet');
      GitlabApi.getSnippet.mockImplementation(() => Promise.resolve(snippetBody.code));

      screen = render(
        <ThemeProvider>
          <SnippetDetails
            snippet={mockDataSnippet}
            snippetId={mockDataSnippet.id}
            onPress={handlePress}
          />
          ,
        </ThemeProvider>,
      );
    });

    afterEach(() => {
      GitlabApi.getSnippet.mockRestore();
    });

    it('renders navigation button', () => {
      expect(screen.getByText('Snippets')).toBeDefined();
      expect(screen.getByText(mockDataSnippet.id.split('Snippet/')[1])).toBeDefined();
    });

    it('render correct Icon element according to visibility level', () => {
      expect(screen.getByTestId('earth')).toBeDefined();
    });

    it('render authored time', () => {
      expect(
        screen.getByText(`Authored ${Utils.timeSince(mockDataSnippet.createdAt)} by`),
      ).toBeDefined();
    });

    it('render author name', () => {
      expect(screen.getByText(mockDataSnippet.author.name)).toBeDefined();
    });

    it('render snippet title', () => {
      expect(screen.getByText(mockDataSnippet.title)).toBeDefined();
    });

    it('render snippet description', () => {
      expect(screen.getByText(mockDataSnippet.description)).toBeDefined();
    });

    it('render Alert element', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('render filename', () => {
      expect(screen.getByText(mockDataSnippet.fileName)).toBeDefined();
    });

    it('renders share button', () => {
      expect(screen.getByTestId('shareButton')).toBeDefined();
    });

    it('renders code container', () => {
      expect(screen.getByTestId('codes')).toBeDefined();
    });
  });

  it('should navigate to Snippet lists by pressing back button', () => {
    const { getByText } = render(
      <ThemeProvider>
        <SnippetDetails
          snippetId={mockDataSnippet.id}
          snippet={mockDataSnippet}
          onPress={handlePress}
        />
        ,
      </ThemeProvider>,
    );

    fireEvent.press(getByText('Snippets'));
    expect(handlePress).toHaveBeenCalledTimes(1);
  });
});
