import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import SnippetsScreen from '~app/screens/Snippets/SnippetsScreen';
import GitlabApi from '~app/lib/auth/GitlabApi';
import UserStorage from '~app/lib/auth/UserStorage';
import user from '~spec/__mocks__/User.js';
import mockSnippet from '~spec/__mocks__/SnippetItem';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Snippets screen', () => {
  const navigation = { goBack: jest.fn() };
  const mockDataSnippet = [mockSnippet];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(user);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getAllSnippets.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <SnippetsScreen navigation={navigation} />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props.animating).toBe(true);
    });

    afterEach(() => {
      GitlabApi.getAllSnippets.mockRestore();
    });
  });

  describe('when Snippet list is loaded', () => {
    describe('when Snippet list has items', () => {
      beforeEach(() => {
        GitlabApi.getAllSnippets.mockResolvedValue(mockDataSnippet);
      });

      it('displays list', async () => {
        const screen = render(
          <ThemeProvider>
            <SnippetsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Snippets')).toBeDefined();
        expect(screen.queryByTestId('snippetList')).not.toBeNull();
      });

      it('displays Snippet Details Screen when press item', async () => {
        const screen = render(
          <ThemeProvider>
            <SnippetsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Snippets')).toBeDefined();
        expect(screen.queryByTestId('snippetList')).not.toBeNull();

        fireEvent.press(screen.getByTestId(`item${mockDataSnippet[0].id}`));

        expect(screen.getByTestId('shareButton')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllSnippets.mockRestore();
      });
    });

    describe('when MR list is empty', () => {
      beforeEach(() => {
        GitlabApi.getAllSnippets.mockResolvedValue([]);
      });

      it('displays text element', async () => {
        const screen = render(
          <ThemeProvider>
            <SnippetsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(
          await screen.findByText('Store, share, and embed small pieces of code and text.'),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllSnippets.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getAllSnippets.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <SnippetsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(await screen.findByText(/Connection failed./)).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllSnippets.mockRestore();
      });
    });
  });
});
