import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react-native';
import { ThemeProvider } from '~app/store/theme-context';
import SnippetItem from '~app/screens/Snippets/SnippetItem';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import user from '~spec/__mocks__/User.js';
import mockSnippet from '~spec/__mocks__/SnippetItem';
import '~config/i18n.config';

describe('Snippet Item', () => {
  const handlePress = jest.fn();

  describe('render elements correctly in Snippet Item', () => {
    let testInstance;

    beforeEach(() => {
      const testRenderer = renderer.create(
        <ThemeProvider>
          <SnippetItem snippet={mockSnippet} instanceUrl={user.instanceUrl} onPress={handlePress} />
        </ThemeProvider>,
      );
      testInstance = testRenderer.root;
    });

    it('render Image element', () => {
      expect(
        testInstance.findByProps({ testID: `img${mockSnippet.id}` }).props.source,
      ).toStrictEqual({
        uri: `${user.instanceUrl}${mockSnippet.author.avatarUrl}`,
      });
    });

    it('render snippet title', () => {
      expect(testInstance.findByProps({ testID: `title${mockSnippet.id}` }).props.children).toEqual(
        mockSnippet.title,
      );
    });

    it('render correct Icon element due to visibility level', () => {
      expect(testInstance.findByType(Icons.earth)).toBeDefined();
    });

    it('render snippet id number', () => {
      expect(testInstance.findByProps({ testID: `id${mockSnippet.id}` }).props.children).toEqual(
        `$${mockSnippet.id.split('Snippet/')[1]}`,
      );
    });

    it('render authored time', () => {
      expect(testInstance.findByProps({ testID: `since${mockSnippet.id}` }).props.children).toEqual(
        `· authored ${Utils.timeSince(mockSnippet.createdAt)} by`,
      );
    });

    it('render author name', () => {
      expect(
        testInstance.findByProps({ testID: `author${mockSnippet.id}` }).props.children,
      ).toEqual(mockSnippet.author.name);
    });
  });

  it('calls handlePress function by pressing Snippet Item', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SnippetItem snippet={mockSnippet} instanceUrl={user.instanceUrl} onPress={handlePress} />
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId(`item${mockSnippet.id}`));
    expect(handlePress).toHaveBeenCalledTimes(1);
  });
});
