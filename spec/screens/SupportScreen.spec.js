import React from 'react';
import { Linking } from 'react-native';
import { render, fireEvent } from '@testing-library/react-native';
import SupportScreen from '~app/screens/SupportScreen';
import Gitlab from '~app/constants/Gitlab';
import '~config/i18n.config';

describe('Home screen', () => {
  it('renders correctly', () => {
    const { getByText } = render(<SupportScreen />);
    expect(getByText(/Help Topics/)).toBeDefined();
    expect(getByText(/Account Support/)).toBeDefined();
    expect(getByText(/If you haven't received your confirmation email/)).toBeDefined();
    expect(getByText(/Contributing/)).toBeDefined();
    expect(getByText(/Our development documentation describes how to submit/)).toBeDefined();
    expect(getByText(/Feature Proposals/)).toBeDefined();
    expect(getByText(/Feature proposals should be submitted to the/)).toBeDefined();
    expect(getByText(/References/)).toBeDefined();
    expect(getByText(/GitLab Documentation/)).toBeDefined();
    expect(getByText(/GitLab Forum/)).toBeDefined();
    expect(getByText(/GitLab University/)).toBeDefined();
    expect(getByText(/The GitLab Cookbook/)).toBeDefined();
    expect(getByText(/GitLab Recipes/)).toBeDefined();
    expect(getByText(/Learn Enough/)).toBeDefined();
    expect(getByText(/Pro Git book/)).toBeDefined();
    expect(getByText(/O'Reilly Media 'Git for teams'/)).toBeDefined();
    expect(getByText(/GitLab YouTube Channel/)).toBeDefined();
    expect(getByText(/Licensing and Subscriptions/)).toBeDefined();
    expect(getByText(/Reproducible Bugs/)).toBeDefined();
    expect(getByText(/Bug reports should be submitted to the/)).toBeDefined();
    expect(getByText(/Other resources for discussion/)).toBeDefined();
    expect(getByText(/#gitlab Libera IRC channel/)).toBeDefined();
    expect(getByText(/GitLab Community Forum/)).toBeDefined();
    expect(getByText(/Mailing list/)).toBeDefined();
    expect(getByText(/Gitter chat room/)).toBeDefined();
    expect(getByText(/Security/)).toBeDefined();
    expect(getByText(/The responsible disclosure page/)).toBeDefined();
    expect(getByText(/The security section/)).toBeDefined();
    expect(getByText(/The Trust & Safety page/)).toBeDefined();
    expect(getByText(/The GitLab Trust Center/)).toBeDefined();
    expect(getByText('Technical Support')).toBeDefined();
    expect(getByText(/Updating/)).toBeDefined();
    expect(getByText(/GitLab update page/)).toBeDefined();
    expect(getByText(/Maintenance policy/)).toBeDefined();
  });

  it('should navigate to https://gitlab.com/users/confirmation/new by pressing link', () => {
    const { getByText } = render(<SupportScreen />);

    fireEvent.press(getByText('confirmation page'));

    const spy = jest.spyOn(Linking, 'openURL');
    expect(spy).toHaveBeenCalledWith(Gitlab.confirmation);
  });
});
