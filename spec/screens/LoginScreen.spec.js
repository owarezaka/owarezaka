import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import '~config/i18n.config';
import LoginScreen from '~app/screens/LoginScreen';
import { Colors } from '~app/constants/GlobalStyles';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { ThemeProvider } from '~app/store/theme-context';

describe('Login screen', () => {
  const instanceUrl = 'https://gitlab.com';
  const token = '123456789012345678901234567890';

  describe('renders elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <LoginScreen />
        </ThemeProvider>,
      );
    });

    it('displays app title', () => {
      expect(screen.getByText('Owarezaka')).toBeDefined();
    });

    it('displays instance label', () => {
      expect(screen.getByText('Instance Url')).toBeDefined();
    });

    it('displays instance input', () => {
      expect(screen.getByTestId('instance')).toBeDefined();
    });

    it('displays token label', () => {
      expect(screen.getByText('Personal Access Token')).toBeDefined();
    });

    it('displays token input', () => {
      expect(screen.getByTestId('token')).toBeDefined();
    });

    it('displays bulb button', () => {
      expect(screen.getByTestId('bulbBtn')).toBeDefined();
      expect(screen.getByTestId('bulb')).toBeDefined();
    });

    it('displays sign in button', () => {
      expect(screen.getByText('Sign in')).toBeDefined();
      expect(screen.getByTestId('signinLoading')).toBeDefined();
    });
  });

  it('disables signin for short token length', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <LoginScreen />
      </ThemeProvider>,
    );

    fireEvent.changeText(getByTestId('token'), '1234');
    expect(getByTestId('signinButton').props.style.display).toBe('none');
  });

  it('enables the signin for long token length', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <LoginScreen />
      </ThemeProvider>,
    );

    fireEvent.changeText(getByTestId('token'), token);
    expect(getByTestId('signinButton').props.style.backgroundColor).toBe(Colors.primary);
  });

  it('fails when invalid token is given', async () => {
    jest.spyOn(GitlabApi, 'checkVersion');
    GitlabApi.checkVersion.mockImplementation(() => Promise.resolve('14.10.2'));

    jest.spyOn(UserStorage, 'login');
    UserStorage.login.mockImplementation(() => Promise.reject(new Error('Invalid Token')));

    const { getByTestId } = render(
      <ThemeProvider>
        <LoginScreen />
      </ThemeProvider>,
    );
    const invalidToken = '010101010101010';

    fireEvent.changeText(getByTestId('token'), invalidToken);
    fireEvent.press(getByTestId('signinButton'));

    expect(getByTestId('signinLoading').props.animating).toBe(false);
    await waitFor(() => expect(UserStorage.login(invalidToken)).rejects.toThrow('Invalid Token'));

    GitlabApi.checkVersion.mockRestore();
    UserStorage.login.mockRestore();
  });

  it('fails when Gitlab version is below 14.9', async () => {
    jest.spyOn(GitlabApi, 'checkVersion');
    GitlabApi.checkVersion.mockImplementation(() => Promise.reject(new Error({ success: false })));

    jest.spyOn(UserStorage, 'login');
    UserStorage.login.mockImplementation(() => Promise.resolve(null));

    const { getByTestId, queryByTestId } = render(
      <ThemeProvider>
        <LoginScreen />
      </ThemeProvider>,
    );

    fireEvent.changeText(getByTestId('token'), token);
    fireEvent.press(getByTestId('signinButton'));

    await waitFor(() => expect(queryByTestId('warning')).not.toBeNull());

    GitlabApi.checkVersion.mockRestore();
    UserStorage.login.mockRestore();
  });

  it('confirms when signin succeeds', async () => {
    jest.spyOn(GitlabApi, 'checkVersion');
    GitlabApi.checkVersion.mockImplementation(() => Promise.resolve('14.10.2'));

    jest.spyOn(UserStorage, 'login');
    UserStorage.login.mockImplementation(() => Promise.resolve(200));

    const screen = render(
      <ThemeProvider>
        <LoginScreen />
      </ThemeProvider>,
    );

    fireEvent.changeText(screen.getByTestId('token'), token);
    fireEvent.press(screen.getByTestId('signinButton'));

    expect(screen.getByTestId('signinLoading').props.animating).toBe(false);
    await waitFor(() => expect(UserStorage.login).toHaveBeenCalledWith(instanceUrl, token));

    UserStorage.login.mockRestore();
    GitlabApi.checkVersion.mockRestore();
  });
});
