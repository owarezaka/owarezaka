import React from 'react';
import { render } from '@testing-library/react-native';
import ToDoListItem from '~app/screens/ToDoList/TodoListItem';
import '~config/i18n.config';
import todo from '~spec/__mocks__/TodoItem.js';
import * as Utils from '~app/utils/Utils';
import { ThemeProvider } from '~app/store/theme-context';

describe('To-Do List Item', () => {
  const setTodos = jest.fn();

  describe('displays todo list items', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <ToDoListItem todo={todo} setTodos={setTodos} />
        </ThemeProvider>,
      );
    });

    it('shows avatar', () => {
      expect(screen.getByTestId(`todo${todo.id}`).props.source).toStrictEqual({
        uri: todo.author.avatar_url,
      });
    });

    it('shows todo target state', () => {
      expect(screen.getByText(todo.target.state)).toBeDefined();
    });

    it('shows author username', () => {
      expect(screen.getByText(todo.author.username)).toBeDefined();
    });

    it('shows action name', () => {
      expect(screen.getByText(todo.action_name)).toBeDefined();
    });

    it('shows todo target type', () => {
      expect(screen.getByText(`${todo.target_type}#${todo.target.iid}`)).toBeDefined();
    });

    it('shows todo target title', () => {
      expect(screen.getByText(`"${todo.target.title}" at`)).toBeDefined();
    });

    it('shows todo project namespace', () => {
      expect(screen.getByText(todo.project.name_with_namespace)).toBeDefined();
    });

    it('shows todo assignee username', () => {
      expect(screen.getByText(`to ${todo.target.assignees[0].username}`)).toBeDefined();
    });

    it('shows created time', () => {
      expect(screen.getByText(Utils.timeSince(todo.created_at))).toBeDefined();
    });

    it('shows done button', () => {
      expect(screen.getByText('Done')).toBeDefined();
    });
  });
});
