import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, waitFor } from '@testing-library/react-native';
import ToDoListScreen from '~app/screens/ToDoList/ToDoListScreen';
import { Colors } from '~app/constants/GlobalStyles';
import '~config/i18n.config';
import todo from '~spec/__mocks__/TodoItem.js';
import joe from '~spec/__mocks__/User.js';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('To-Do List screen', () => {
  const mockDataTodoList = [todo];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('initial state', () => {
    beforeEach(() => {
      GitlabApi.getTodos.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <ToDoListScreen />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.getTodos.mockRestore();
    });
  });

  describe('when todo list is loaded', () => {
    describe('when a todo list is available', () => {
      beforeEach(() => {
        GitlabApi.getTodos.mockResolvedValue(mockDataTodoList);
      });

      it('displays the fetched todos', async () => {
        const screen = render(
          <ThemeProvider>
            <ToDoListScreen />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.queryByTestId('ToDo-List')).not.toBeNull());
      });

      afterEach(() => {
        GitlabApi.getTodos.mockRestore();
      });
    });

    describe('when issue list is empty', () => {
      beforeEach(() => {
        GitlabApi.getTodos.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <ToDoListScreen />
          </ThemeProvider>,
        );

        expect(
          await screen.findByText(
            'When an issue or merge request is assigned to you, or when you receive a @mention in a comment, your To-Do List will be updated.',
          ),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getTodos.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getTodos.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <ToDoListScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Connection failed. Please retry later.')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getTodos.mockRestore();
      });
    });
  });
});
