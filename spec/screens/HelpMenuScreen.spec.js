import React from 'react';
import { Linking } from 'react-native';
import { render, fireEvent } from '@testing-library/react-native';
import HelpMenuScreen from '~app/screens/HelpMenuScreen';
import Gitlab from '~app/constants/Gitlab';
import '~config/i18n.config';

describe('Help Menu screen', () => {
  it('renders correctly', () => {
    const { getByText } = render(<HelpMenuScreen />);
    expect(
      getByText(/Owarezaka is an IOS & Android client for GitLab, an open source devops platform./),
    ).toBeDefined();
    expect(getByText(/Help/)).toBeDefined();
    expect(getByText(/Contribute to Owarezaka/)).toBeDefined();
  });

  it('should navigate to Help Screen by pressing button', () => {
    const navigation = { navigate: jest.fn() };
    const { getByText } = render(<HelpMenuScreen navigation={navigation} />);
    fireEvent.press(getByText(/Help/));
    const spy = jest.spyOn(navigation, 'navigate');
    expect(spy).toHaveBeenCalledWith('HelpScreen');
  });

  it('should navigate to https://gitlab.com/owarezaka/owarezaka by pressing button', () => {
    const { getByText } = render(<HelpMenuScreen />);

    fireEvent.press(getByText('Contribute to Owarezaka'));

    const spy = jest.spyOn(Linking, 'openURL');
    expect(spy).toHaveBeenCalledWith(Gitlab.owarezaka);
  });
});
