import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import Note from '~app/screens/Issues/Note';
import * as Utils from '~app/utils/Utils';
import mockNote from '~spec/__mocks__/Note';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('Note', () => {
  const destroyNoteHandler = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <Note note={mockNote} />
        </ThemeProvider>,
      );
    });

    it('renders author avatar', () => {
      expect(screen.getByTestId('authorAvatar')).toBeDefined();
    });

    it('renders author name', () => {
      expect(screen.getByText(mockNote.author.name)).toBeDefined();
    });

    it('renders author username', () => {
      expect(screen.getByText(`@${mockNote.author.username}`)).toBeDefined();
    });

    it('renders comment icon', () => {
      expect(screen.getByTestId('commentDots')).toBeDefined();
    });

    it('renders created time', () => {
      expect(screen.getByText(Utils.timeSince(mockNote.createdAt))).toBeDefined();
    });

    it('renders resolve thread button', () => {
      expect(screen.getByTestId('resolveThread')).toBeDefined();
    });

    it('renders edit button', () => {
      expect(screen.getByTestId('editButton')).toBeDefined();
      expect(screen.getByTestId('editIcon')).toBeDefined();
    });

    it('renders remove button', () => {
      expect(screen.getByTestId('removeButton')).toBeDefined();
      expect(screen.getByTestId('removeIcon')).toBeDefined();
    });

    it('renders note body', () => {
      expect(screen.getByText(mockNote.body)).toBeDefined();
    });
  });

  it('should display comment area when pressing edit button', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <Note note={mockNote} />
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId('editButton'));
    expect(getByTestId('editComment')).toBeDefined();
  });

  it('calls function when pressing remove button', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <Note note={mockNote} destroyNoteHandler={destroyNoteHandler} />
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId('removeButton'));
    expect(destroyNoteHandler).toHaveBeenCalledTimes(1);
  });
});
