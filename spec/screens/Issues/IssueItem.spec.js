import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react-native';
import IssueItem from '~app/screens/Issues/IssueItem';
import Icons from '~app/assets/Icons';
import * as Utils from '~app/utils/Utils';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('Issue Item', () => {
  const handlePress = jest.fn();
  const mockData = [
    {
      id: 'gid://gitlab/Project/30541283',
      fullPath: 'owarezaka/owarezaka',
    },
  ];

  const mockDataIssues = {
    id: 'gid://gitlab/Issue/102081405',
    iid: '112',
    title: 'Add my issues screen',
    createdAt: '2021-12-08T12:52:25Z',
    reference: '#1',
    projectId: 31951083,
    labels: {
      nodes: [
        {
          id: 'gid://gitlab/ProjectLabel/23273032',
          title: 'enhancement',
          color: '#ed9121',
        },
      ],
    },
    milestone: {
      title: 'v0.1',
    },
    author: {
      name: 'AA',
    },
  };

  describe('render elements correctly', () => {
    let testInstance;

    beforeEach(() => {
      const testRenderer = renderer.create(
        <ThemeProvider>
          <IssueItem issue={mockDataIssues} data={mockData} onPress={handlePress} />
        </ThemeProvider>,
      );
      testInstance = testRenderer.root;
    });

    it('renders Issue title', () => {
      expect(
        testInstance.findByProps({ testID: `title${mockDataIssues.id}` }).props.children,
      ).toEqual(mockDataIssues.title);
    });

    it('renders project name', () => {
      expect(
        testInstance.findByProps({ testID: `project${mockDataIssues.id}` }).props.children,
      ).toEqual(`${mockData.fullPath}${mockDataIssues.reference}`);
    });

    it('renders created time', () => {
      expect(
        testInstance.findByProps({ testID: `since${mockDataIssues.id}` }).props.children,
      ).toEqual(`· created ${Utils.timeSince(mockDataIssues.createdAt)} by `);
    });

    it('renders author name', () => {
      expect(
        testInstance.findByProps({ testID: `name${mockDataIssues.id}` }).props.children,
      ).toEqual(mockDataIssues.author.name);
    });

    it('renders milestone icon if there is one', () => {
      expect(testInstance.findByType(Icons.clock)).toBeDefined();
    });

    it('renders milestone title if there is one', () => {
      expect(
        testInstance.findByProps({ testID: `milestone${mockDataIssues.id}` }).props.children,
      ).toEqual(mockDataIssues.milestone.title);
    });

    it('renders label title if there is one', () => {
      expect(
        testInstance.findByProps({ testID: `label${mockDataIssues.id}` }).props.children,
      ).toEqual(mockDataIssues.labels.nodes[0].title);
    });
  });

  it('calls handlePress function by pressing Issue Item', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <IssueItem issue={mockDataIssues} data={mockData} onPress={handlePress} />{' '}
      </ThemeProvider>,
    );

    fireEvent.press(getByTestId(`item${mockDataIssues.id}`));
    expect(handlePress).toHaveBeenCalledTimes(1);
  });
});
