import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, waitFor } from '@testing-library/react-native';
import IssuesScreen from '~app/screens/Issues/IssuesScreen';
import mockIssue from '~spec/__mocks__/IssueItem';
import user from '~spec/__mocks__/User.js';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Issues screen', () => {
  const issueList = [mockIssue];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(user);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getAllAssignedIssues.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <IssuesScreen />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props.animating).toBe(true);
    });

    afterEach(() => {
      GitlabApi.getAllAssignedIssues.mockRestore();
    });
  });

  describe('when issue list is loaded', () => {
    describe('when issue list has items', () => {
      beforeEach(() => {
        GitlabApi.getAllAssignedIssues.mockResolvedValue(issueList);
      });

      let screen;

      it('displays fetched issues', async () => {
        screen = render(
          <ThemeProvider>
            <IssuesScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Issues')).toBeDefined();
        expect(screen.queryByTestId('Issues-List')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllAssignedIssues.mockRestore();
      });
    });

    describe('when list is empty', () => {
      beforeEach(() => {
        GitlabApi.getAllAssignedIssues.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <IssuesScreen />
          </ThemeProvider>,
        );

        await waitFor(() =>
          expect(
            screen.getByText('There are no issues currently assigned to you. Good work!'),
          ).toBeDefined(),
        );
      });

      afterEach(() => {
        GitlabApi.getAllAssignedIssues.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getAllAssignedIssues.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <IssuesScreen />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.getByText(/Connection failed./)).toBeDefined());
      });

      afterEach(() => {
        GitlabApi.getAllAssignedIssues.mockRestore();
      });
    });
  });
});
