import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import SendComment from '~app/screens/Issues/SendComment';
import GitlabApi from '~app/lib/auth/GitlabApi';
import '~config/i18n.config';
import mockIssue from '~spec/__mocks__/IssueItem';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

describe('SendComment', () => {
  const displayAllIssueNotes = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <SendComment issueId={mockIssue.id} displayAllIssueNotes={displayAllIssueNotes} />
        </ThemeProvider>,
      );
    });

    it('renders alert component', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders author name', () => {
      expect(screen.getByPlaceholderText('Write a comment here...')).toBeDefined();
    });

    it('renders comment button', () => {
      expect(screen.getByText('Comment')).toBeDefined();
    });
  });

  it('shoud allow to change textarea', () => {
    const screen = render(
      <ThemeProvider>
        <SendComment issueId={mockIssue.id} displayAllIssueNotes={displayAllIssueNotes} />
      </ThemeProvider>,
    );

    fireEvent.changeText(
      screen.getByPlaceholderText('Write a comment here...'),
      'This is a test comment',
    );
    expect(screen.getByPlaceholderText('Write a comment here...').props.value).toBe(
      'This is a test comment',
    );
  });

  it('alerts if it fails to create issue note', async () => {
    jest.spyOn(GitlabApi, 'createIssueNote');
    GitlabApi.createIssueNote.mockImplementation(() =>
      Promise.reject(new Error({ success: false })),
    );

    const { queryByTestId } = render(
      <ThemeProvider>
        <SendComment issueId={mockIssue.id} displayAllIssueNotes={displayAllIssueNotes} />
      </ThemeProvider>,
    );

    await waitFor(() => expect(queryByTestId('alert')).not.toBeNull());

    GitlabApi.createIssueNote.mockRestore();
  });
});
