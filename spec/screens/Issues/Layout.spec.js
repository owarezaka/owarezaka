import React from 'react';
import { render } from '@testing-library/react-native';
import Layout from '~app/screens/Issues/Layout';

import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('Layout', () => {
  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <Layout title="Title" />
        </ThemeProvider>,
      );
    });

    it('renders given title', () => {
      expect(screen.getByText('Title')).toBeDefined();
    });
  });
});
