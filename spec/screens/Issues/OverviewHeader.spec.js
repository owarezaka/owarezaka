import React from 'react';
import { render } from '@testing-library/react-native';
import OverviewHeader from '~app/screens/Issues/OverviewHeader';
import * as Utils from '~app/utils/Utils';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('OverviewHeader', () => {
  const authorUsername = 'AA';
  const createdAt = '2022-03-13T16:53:18Z';
  const handleSlidingDrawer = jest.fn();
  const overviewBtnAction = jest.fn();
  const changesBtnAction = jest.fn();
  const markAsReadyBtnAction = jest.fn();
  const stateName = 'Open';

  describe('render elements correctly', () => {
    let tree;

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <OverviewHeader
            authorUsername={authorUsername}
            createdAt={createdAt}
            handleSlidingDrawer={handleSlidingDrawer}
            overviewBtnAction={overviewBtnAction}
            changesBtnAction={changesBtnAction}
            stateName={stateName}
            markAsReadyBtnAction={markAsReadyBtnAction}
          />
        </ThemeProvider>,
      );
    });

    it('renders given state name', () => {
      expect(tree.getByText(stateName)).toBeDefined();
    });

    it('renders mark as ready button', () => {
      expect(tree.getByTestId('markAsReady')).toBeDefined();
    });

    it('renders ovreview button', () => {
      expect(tree.getByTestId('overviewButton')).toBeDefined();
    });

    it('renders changes button', () => {
      expect(tree.getByTestId('changesButton')).toBeDefined();
    });

    it('renders slider open button', () => {
      expect(tree.getByTestId('sliderOpenButton')).toBeDefined();
    });

    it('renders angle double right icon', () => {
      expect(tree.getByTestId('angleRightIcon')).toBeDefined();
    });

    it('renders author username', () => {
      expect(tree.getByTestId('authorUsername').props.children).toBe(`@${authorUsername}`);
    });

    it('renders created time', () => {
      expect(tree.getByTestId('createdAt').props.children).toStrictEqual(
        ` Created ${Utils.timeSince(createdAt)} by `,
      );
    });
  });

  it('renders mark as draft button when isDraft status is false', () => {
    const screen = render(
      <ThemeProvider>
        <OverviewHeader
          authorUsername={authorUsername}
          createdAt={createdAt}
          handleSlidingDrawer={handleSlidingDrawer}
          overviewBtnAction={overviewBtnAction}
          changesBtnAction={changesBtnAction}
          stateName={stateName}
          markAsReadyBtnAction={markAsReadyBtnAction}
          isDraft={false}
        />
      </ThemeProvider>,
    );

    expect(screen.getByText('Mark As Draft')).toBeDefined();
  });
});
