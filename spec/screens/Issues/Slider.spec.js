import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import Slider from '~app/screens/Issues/Slider';
import user from '~spec/__mocks__/User.js';
import mockIssue from '~spec/__mocks__/IssueItem';
import mockIssueDetails from '~spec/__mocks__/Slider';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';
import mockMembers from '~spec/__mocks__/ProjectMembers';

describe('Slider', () => {
  const setVisible = true;
  const handleSlidingDrawer = jest.fn();
  const getMergeRequestsDetails = jest.fn();

  describe('render elements correctly in Issues Screen', () => {
    let tree;

    const issuesScreenMockDetails = {
      author: {
        username: 'jane_doe',
      },
      description: 'Summary\n(Summarize the bug encountered concisely)',
      dueDate: '2022-03-31',
      assignees: {
        nodes: [
          {
            id: 'gid://gitlab/User/1234',
            name: 'John Doe',
            username: 'john_doe',
            avatarUrl: '#',
          },
        ],
      },
    };

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <Slider
            issue={mockIssue}
            details={issuesScreenMockDetails}
            getMergeRequestsDetails={getMergeRequestsDetails}
            handleSlidingDrawer={handleSlidingDrawer}
            setVisible={setVisible}
            instanceUrl={user.instanceUrl}
            members={mockMembers}
          />
          ,
        </ThemeProvider>,
      );
    });

    it('renders Assignee title', () => {
      expect(tree.getByTestId('assigneeTitle').props.children).toBe('Assignee');
    });

    it('renders assignee avatar element', () => {
      expect(tree.queryByTestId(`assigneeAvatar${mockIssue.id}`)).not.toBeNull();
    });

    it('renders assignee name', () => {
      expect(tree.queryByTestId(`assigneeName${mockIssue.id}`).props.children).toEqual(
        mockIssueDetails.assignees.nodes[0].name,
      );
    });

    it('renders assignee username', () => {
      expect(tree.getByTestId(`assigneeUserName${mockIssue.id}`).props.children).toEqual(
        `@${mockIssueDetails.assignees.nodes[0].username}`,
      );
    });

    it('renders slider close button', () => {
      expect(tree.getByTestId('sliderCloseButton')).toBeDefined();
      expect(tree.getByTestId('angleDoubleRight')).toBeDefined();
    });

    it('renders Milestone title', () => {
      expect(tree.queryByTestId('milestoneTitle').props.children).toBe('Milestone');
    });

    it('renders milestone', () => {
      expect(tree.getByTestId(`milestone${mockIssue.id}`).props.children).toBe(
        mockIssue.milestone.title,
      );
    });

    it('renders Due date title', () => {
      expect(tree.getByText('Due Date')).toBeDefined();
    });

    it('renders due date', () => {
      expect(tree.queryByTestId(`duedate${mockIssue.id}`)).not.toBeNull();
    });

    it('renders Labels title', () => {
      expect(tree.getByTestId('labelsTitle').props.children).toBe('Labels');
    });

    it('renders labels', () => {
      expect(tree.getByTestId(`label${mockIssue.id}`).props.children).toBe(
        mockIssue.labels.nodes[0].title,
      );
    });
  });

  describe('render elements correctly in MR & Review MRs Screen', () => {
    let tree;

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <Slider
            issue={mockIssue}
            details={mockIssueDetails}
            getMergeRequestsDetails={getMergeRequestsDetails}
            handleSlidingDrawer={handleSlidingDrawer}
            setVisible={setVisible}
            instanceUrl={user.instanceUrl}
            members={mockMembers}
          />
          ,
        </ThemeProvider>,
      );
    });

    it('renders Assignee title', () => {
      expect(tree.getByTestId('assigneeTitle').props.children).toBe('Assignee');
    });

    it('renders assignee avatar element', () => {
      expect(tree.queryByTestId(`assigneeAvatar${mockIssue.id}`)).not.toBeNull();
    });

    it('renders assignee name', () => {
      expect(tree.queryByTestId(`assigneeName${mockIssue.id}`).props.children).toEqual(
        mockIssueDetails.assignees.nodes[0].name,
      );
    });

    it('renders assignee username', () => {
      expect(tree.getByTestId(`assigneeUserName${mockIssue.id}`).props.children).toEqual(
        `@${mockIssueDetails.assignees.nodes[0].username}`,
      );
    });

    it('renders slider close button', () => {
      expect(tree.getByTestId('sliderCloseButton')).toBeDefined();
      expect(tree.getByTestId('angleDoubleRight')).toBeDefined();
    });

    it('renders Reviewers title', () => {
      expect(tree.getByTestId('reviewerTitle').props.children).toBe('Reviewer');
    });

    it('renders reviewer avatar element', () => {
      expect(tree.queryByTestId(`reviewer${mockIssue.id}`)).not.toBeNull();
    });

    it('renders reviewer name', () => {
      expect(tree.queryByTestId(`reviewerName${mockIssue.id}`).props.children).toEqual(
        mockIssueDetails.reviewers.nodes[0].name,
      );
    });

    it('renders reviewer username', () => {
      expect(tree.queryByTestId(`reviewerUserName${mockIssue.id}`).props.children).toEqual(
        `@${mockIssueDetails.reviewers.nodes[0].username}`,
      );
    });

    it('renders edit button', () => {
      expect(tree.getByText(`Edit`)).toBeDefined();
    });

    it('renders Milestone title', () => {
      expect(tree.getByTestId('milestoneTitle').props.children).toBe('Milestone');
    });

    it('renders milestone', () => {
      expect(tree.getByTestId(`milestone${mockIssue.id}`).props.children).toBe(
        mockIssue.milestone.title,
      );
    });

    it('renders Labels title', () => {
      expect(tree.getByTestId('labelsTitle').props.children).toBe('Labels');
    });

    it('renders labels', () => {
      expect(tree.getByTestId(`label${mockIssue.id}`).props.children).toBe(
        mockIssue.labels.nodes[0].title,
      );
    });
  });

  it('renders editReviewer component when press edit button', () => {
    const screen = render(
      <ThemeProvider>
        <Slider
          issue={mockIssue}
          details={mockIssueDetails}
          getMergeRequestsDetails={getMergeRequestsDetails}
          handleSlidingDrawer={handleSlidingDrawer}
          setVisible={setVisible}
          instanceUrl={user.instanceUrl}
          members={mockMembers}
        />
        ,
      </ThemeProvider>,
    );

    fireEvent.press(screen.getByText('Edit'));
    expect(screen.queryByTestId('EditReviewers')).not.toBeNull();
  });
});
