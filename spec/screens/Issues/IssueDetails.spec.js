import React from 'react';
import { render, fireEvent, waitFor, act } from '@testing-library/react-native';
import IssueDetails from '~app/screens/Issues/IssueDetails';
import GitlabApi from '~app/lib/auth/GitlabApi';
import '~config/i18n.config';
import mockNote from '~spec/__mocks__/Note';
import mockIssue from '~spec/__mocks__/IssueItem';
import issueDetails from '~spec/__mocks__/MergeRequestIssueDetails';
import { ThemeProvider } from '~app/store/theme-context';

import { enableFetchMocks } from 'jest-fetch-mock';
enableFetchMocks();

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('~app/lib/auth/GitlabApi');

describe('Issue Details Screen', () => {
  const handlePress = jest.fn();
  const refreshOpenIssues = jest.fn();

  const mockIssueDetails = [issueDetails];

  describe('render elements correctly', () => {
    let tree;

    beforeEach(async () => {
      fetch.resetMocks();
      fetch.mockResponseOnce(JSON.stringify(mockNote));

      jest.spyOn(GitlabApi, 'getOpenIssueDetails');
      GitlabApi.getOpenIssueDetails.mockImplementation(() => Promise.resolve(mockIssueDetails));

      tree = render(
        <ThemeProvider>
          <IssueDetails
            issue={mockIssue}
            onPress={handlePress}
            refreshOpenIssues={refreshOpenIssues}
          />
        </ThemeProvider>,
      );

      await act(async () => {
        tree.update(
          <ThemeProvider>
            <IssueDetails
              issue={mockIssue}
              onPress={handlePress}
              refreshOpenIssues={refreshOpenIssues}
            />
          </ThemeProvider>,
        );
      });
    });

    afterEach(() => {
      GitlabApi.getOpenIssueDetails.mockRestore();
    });

    it('renders navigation button', () => {
      expect(tree.getByText('Issues')).toBeDefined();
      expect(tree.getByText(`${mockIssue.reference}`)).toBeDefined();
    });

    it('renders header component', () => {
      expect(tree.getByTestId('IssueDetailsHeader')).toBeDefined();
    });

    it('renders issue body component', () => {
      expect(tree.getByTestId('IssueDetailsBody')).toBeDefined();
    });

    it('renders sendComment component', () => {
      expect(tree.getByTestId('sendComment')).toBeDefined();
    });

    it('renders close issue button', () => {
      expect(tree.getByTestId('closeIssueButton')).toBeDefined();
    });

    it('renders slider', () => {
      expect(tree.getByTestId('slider')).toBeDefined();
    });
  });

  it('calls handlePress function by pressing navigation back button', async () => {
    fetch.resetMocks();
    fetch.mockResponseOnce(JSON.stringify(mockNote));

    jest.spyOn(GitlabApi, 'getOpenIssueDetails');
    GitlabApi.getOpenIssueDetails.mockImplementation(() => Promise.resolve(mockIssueDetails));

    const tree = render(
      <ThemeProvider>
        <IssueDetails
          issue={mockIssue}
          onPress={handlePress}
          refreshOpenIssues={refreshOpenIssues}
        />
      </ThemeProvider>,
    );

    await act(async () => {
      tree.update(
        <ThemeProvider>
          <IssueDetails
            issue={mockIssue}
            onPress={handlePress}
            refreshOpenIssues={refreshOpenIssues}
          />
        </ThemeProvider>,
      );
    });

    fireEvent.press(tree.getByText('Issues'));
    expect(handlePress).toHaveBeenCalledTimes(1);

    GitlabApi.getOpenIssueDetails.mockRestore();
  });

  it('open slider by pressing angle down button', async () => {
    fetch.resetMocks();
    fetch.mockResponseOnce(JSON.stringify(mockNote));

    jest.spyOn(GitlabApi, 'getOpenIssueDetails');
    GitlabApi.getOpenIssueDetails.mockImplementation(() => Promise.resolve(mockIssueDetails));

    const tree = render(
      <ThemeProvider>
        <IssueDetails
          issue={mockIssue}
          onPress={handlePress}
          refreshOpenIssues={refreshOpenIssues}
        />
      </ThemeProvider>,
    );

    await act(async () => {
      tree.update(
        <ThemeProvider>
          <IssueDetails
            issue={mockIssue}
            onPress={handlePress}
            refreshOpenIssues={refreshOpenIssues}
          />
        </ThemeProvider>,
      );
    });

    fireEvent.press(tree.getByTestId('sliderOpenButton'));
    expect(tree.getByTestId('slider').props.visible).toBe(true);

    GitlabApi.getOpenIssueDetails.mockRestore();
  });

  it('close slider by pressing angle up button', async () => {
    fetch.resetMocks();
    fetch.mockResponseOnce(JSON.stringify(mockNote));

    jest.spyOn(GitlabApi, 'getOpenIssueDetails');
    GitlabApi.getOpenIssueDetails.mockImplementation(() => Promise.resolve(mockIssueDetails));

    const tree = render(
      <ThemeProvider>
        <IssueDetails
          issue={mockIssue}
          onPress={handlePress}
          refreshOpenIssues={refreshOpenIssues}
        />
      </ThemeProvider>,
    );

    await act(async () => {
      tree.update(
        <ThemeProvider>
          <IssueDetails
            issue={mockIssue}
            onPress={handlePress}
            refreshOpenIssues={refreshOpenIssues}
          />
        </ThemeProvider>,
      );
    });

    fireEvent.press(tree.getByTestId('sliderOpenButton'));
    fireEvent.press(tree.getByTestId('sliderCloseButton'));
    expect(tree.getByTestId('slider').props.visible).toBe(false);

    GitlabApi.getOpenIssueDetails.mockRestore();
  });

  it('alerts if it fails to fetch issue details', async () => {
    fetch.resetMocks();
    fetch.mockResponseOnce(JSON.stringify(mockNote));

    jest.spyOn(GitlabApi, 'getOpenIssueDetails');
    GitlabApi.getOpenIssueDetails.mockImplementation(() =>
      Promise.reject(new Error({ success: false })),
    );

    const { queryByTestId } = render(
      <ThemeProvider>
        <IssueDetails
          issue={mockIssue}
          onPress={handlePress}
          refreshOpenIssues={refreshOpenIssues}
        />
      </ThemeProvider>,
    );

    await waitFor(() => expect(queryByTestId('alert-issues')).not.toBeNull());

    GitlabApi.getOpenIssueDetails.mockRestore();
  });
});
