import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import EditComment from '~app/screens/Issues/EditComment';
import GitlabApi from '~app/lib/auth/GitlabApi';
import mockIssue from '~spec/__mocks__/IssueItem';
import mockNote from '~spec/__mocks__/Note';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('EditComment', () => {
  const displayAllIssueNotes = jest.fn();
  const setDisplayCommentArea = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <EditComment
            noteId={mockNote.id}
            defaultValue={mockNote.body}
            setDisplayCommentArea={setDisplayCommentArea}
            displayAllIssueNotes={displayAllIssueNotes}
          />
        </ThemeProvider>,
      );
    });

    it('renders alert component', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders author name', () => {
      expect(screen.getByPlaceholderText('Write a comment here...')).toBeDefined();
    });

    it('renders save comment button', () => {
      expect(screen.getByText('Save Comment')).toBeDefined();
    });

    it('renders cancel button', () => {
      expect(screen.getByText('Cancel')).toBeDefined();
    });
  });

  it('shoud display old comment before edit', () => {
    const screen = render(
      <ThemeProvider>
        <EditComment
          noteId={mockNote.id}
          defaultValue={mockNote.body}
          setDisplayCommentArea={setDisplayCommentArea}
          displayAllIssueNotes={displayAllIssueNotes}
        />
      </ThemeProvider>,
    );

    expect(screen.getByPlaceholderText('Write a comment here...').props.defaultValue).toBe(
      mockNote.body,
    );
  });

  it('shoud allow to change textarea', () => {
    const screen = render(
      <ThemeProvider>
        <EditComment
          noteId={mockNote.id}
          defaultValue={mockNote.body}
          setDisplayCommentArea={setDisplayCommentArea}
          displayAllIssueNotes={displayAllIssueNotes}
        />
      </ThemeProvider>,
    );

    fireEvent.changeText(
      screen.getByPlaceholderText('Write a comment here...'),
      'This is a test comment',
    );
    expect(screen.getByPlaceholderText('Write a comment here...').props.value).toBe(
      'This is a test comment',
    );
  });

  it('alerts if it fails to create issue note', async () => {
    jest.spyOn(GitlabApi, 'updateIssueNote');
    GitlabApi.updateIssueNote.mockImplementation(() =>
      Promise.reject(new Error({ success: false })),
    );

    const screen = render(
      <ThemeProvider>
        <EditComment issueId={mockIssue.id} displayAllIssueNotes={displayAllIssueNotes} />
      </ThemeProvider>,
    );

    await waitFor(() => expect(screen.queryByTestId('alert')).not.toBeNull());

    GitlabApi.updateIssueNote.mockRestore();
  });
});
