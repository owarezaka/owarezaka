import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import ResolveThread from '~app/screens/Issues/ResolveThread';
import GitlabApi from '~app/lib/auth/GitlabApi';
import mockNote from '~spec/__mocks__/Note';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('ResolveThread', () => {
  const displayAllIssueNotes = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <ResolveThread
            isResolved={mockNote.discussion.resolved}
            discussionId={mockNote.discussion.id}
            displayAllIssueNotes={displayAllIssueNotes}
          />
        </ThemeProvider>,
      );
    });

    it('renders alert component', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders resolve thread button', () => {
      expect(screen.getByTestId('resolveButton')).toBeDefined();
    });

    it('renders status success icon', () => {
      expect(screen.getByTestId('statusSuccess')).toBeDefined();
    });
  });

  it('alerts if it fails to resolve thread', async () => {
    jest.spyOn(GitlabApi, 'discussionToggleResolve');
    GitlabApi.discussionToggleResolve.mockImplementation(() =>
      Promise.reject(new Error({ success: false })),
    );

    const { queryByTestId } = render(
      <ThemeProvider>
        <ResolveThread
          isResolved={mockNote.discussion.resolved}
          discussionId={mockNote.discussion.id}
          displayAllIssueNotes={displayAllIssueNotes}
        />
      </ThemeProvider>,
    );

    await waitFor(() => expect(queryByTestId('alert')).not.toBeNull());

    GitlabApi.discussionToggleResolve.mockRestore();
  });
});
