import React from 'react';
import { render } from '@testing-library/react-native';
import OverviewBody from '~app/screens/Issues/OverviewBody';
import { ThemeProvider } from '~app/store/theme-context';

describe('OverviewBody', () => {
  describe('render elements correctly', () => {
    let tree;

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <OverviewBody
            testID="issueBody"
            title="Update contributing guide"
            description="The guide should describe how to contribute project"
          />
        </ThemeProvider>,
      );
    });

    it('renders test ID', () => {
      expect(tree.getByTestId('issueBody')).toBeDefined();
    });

    it('renders issue title', () => {
      expect(tree.getByText('Update contributing guide')).toBeDefined();
    });

    it('renders issue descripton', () => {
      expect(tree.getByText('The guide should describe how to contribute project')).toBeDefined();
    });
  });
});
