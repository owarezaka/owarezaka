import React from 'react';
import { ImageBackground } from 'react-native';
import SplashScreen from '~app/screens/SplashScreen';
import image from '~app/assets/splash.png';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const testRenderer = renderer.create(<SplashScreen />);
  const testInstance = testRenderer.root;

  expect(testInstance.findByType(ImageBackground).props.source).toEqual(image);
});
