import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import AcceptMergeRequest from '~app/screens/MergeRequests/AcceptMergeRequest';
import mockIssue from '~spec/__mocks__/MergeRequestIssue';
import MergeStatus from '~spec/__mocks__/MergeStatus';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('AcceptMergeRequest', () => {
  const refreshIssues = jest.fn();
  const handlePress = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <AcceptMergeRequest
            testID="acceptMergeRequest"
            iid={mockIssue.iid}
            projectFullPath={mockIssue.project.fullPath}
            status={MergeStatus}
            refreshIssues={refreshIssues}
            handlePress={handlePress}
          />
        </ThemeProvider>,
      );
    });

    it('renders alert component', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders merge button', () => {
      expect(screen.getByTestId('mergeBtn')).toBeDefined();
    });
  });

  describe('Icon', () => {
    it('renders warning icon when mr is not mergeable', () => {
      const status = {
        conflicts: true,
        draft: true,
        mergeable: false,
        mergeableDiscussionsState: false,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('statusWarning')).toBeDefined();
    });

    it('renders success icon when mr is mergeable', () => {
      const status = {
        conflicts: true,
        draft: true,
        mergeable: true,
        mergeableDiscussionsState: false,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('statusSuccess')).toBeDefined();
    });
  });

  describe('Mr statements when it is not mergeable', () => {
    it('displays related statement when there is a conflict', () => {
      const status = {
        conflicts: true,
        draft: true,
        mergeable: false,
        mergeableDiscussionsState: false,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(
        screen.getByText(
          'Merge blocked: merge conflicts must be resolved. Users who can write to the source or target branches can resolve the conflicts.',
        ),
      ).toBeDefined();
    });

    it('displays related statement when there mr is not marked as ready', () => {
      const status = {
        conflicts: false,
        draft: true,
        mergeable: false,
        mergeableDiscussionsState: false,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(
        screen.getByText(
          'Merge blocked: merge request must be marked as ready. It is still marked as draft.',
        ),
      ).toBeDefined();
    });

    it('displays related statement when all discussions are not resolved', () => {
      const status = {
        conflicts: false,
        draft: false,
        mergeable: false,
        mergeableDiscussionsState: false,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(screen.getByText('Merge blocked: all threads must be resolved.')).toBeDefined();
    });

    it('displays related statement when pipeline is failed', () => {
      const status = {
        conflicts: false,
        draft: false,
        mergeable: false,
        mergeableDiscussionsState: true,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(
        screen.getByText(
          'Merge blocked: pipeline must succeed. Push a commit that fixes the failure, or learn about other solutions.',
        ),
      ).toBeDefined();
    });

    it('displays related statement when is mergeable', () => {
      const status = {
        conflicts: false,
        draft: false,
        mergeable: true,
        mergeableDiscussionsState: true,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest status={status} />
        </ThemeProvider>,
      );

      expect(
        screen.getByText(
          'Ready to be merged automatically. Ask someone with write access to this repository to merge this request',
        ),
      ).toBeDefined();
    });
  });

  describe('when mr is mergeable', () => {
    it('renders merging text instead of merge button', () => {
      const status = {
        conflicts: false,
        draft: false,
        mergeable: true,
        mergeableDiscussionsState: true,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest
            testID="acceptMergeRequest"
            iid={mockIssue.iid}
            projectFullPath={mockIssue.project.fullPath}
            status={status}
            refreshIssues={refreshIssues}
            handlePress={handlePress}
          />
        </ThemeProvider>,
      );

      const button = screen.getByTestId('mergeBtn');
      fireEvent.press(button);

      expect(screen.queryByText('Merging! Take a deep breath and relax...')).toBeDefined();
    });

    it('renders set to merged text instead of merge button when autoMergeEnabled is true', () => {
      const status = {
        autoMergeEnabled: true,
        conflicts: false,
        draft: false,
        mergeable: true,
        mergeableDiscussionsState: true,
        mergeWhenPipelineSucceeds: true,
        userPermissions: {
          canMerge: false,
        },
        diffHeadSha: '1234abcd',
      };

      const screen = render(
        <ThemeProvider>
          <AcceptMergeRequest
            testID="acceptMergeRequest"
            iid={mockIssue.iid}
            projectFullPath={mockIssue.project.fullPath}
            status={status}
            refreshIssues={refreshIssues}
            handlePress={handlePress}
          />
        </ThemeProvider>,
      );

      const button = screen.getByTestId('mergeBtn');
      fireEvent.press(button);

      expect(
        screen.queryByText('Set to be merged automatically when the pipeline succeeds'),
      ).toBeDefined();
    });
  });
});
