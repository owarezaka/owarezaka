import React from 'react';
import { render } from '@testing-library/react-native';
import EditReviewers from '~app/screens/MergeRequests/EditReviewers';
import mockIssue from '~spec/__mocks__/MergeRequestIssue';
import mockIssueDetails from '~spec/__mocks__/MergeRequestIssueDetails';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';
import mockMembers from '~spec/__mocks__/ProjectMembers';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

describe('AcceptMergeRequest', () => {
  const setShowTextInput = jest.fn();
  const getMergeRequestsDetails = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <EditReviewers
            iid={mockIssue.iid}
            projectId={mockIssueDetails.projectId}
            setShowTextInput={setShowTextInput}
            getMergeRequestsDetails={getMergeRequestsDetails}
            members={mockMembers}
            currentReviewers={mockMembers}
          />
        </ThemeProvider>,
      );
    });

    it('renders error alert component', () => {
      expect(screen.getByTestId('errorAlert')).toBeDefined();
    });

    it('renders warning alert component', () => {
      expect(screen.getByTestId('warningAlert')).toBeDefined();
    });

    it('renders input element', () => {
      expect(screen.getByPlaceholderText('Request review from ...')).toBeDefined();
    });

    it('renders add as reviewer button', () => {
      expect(screen.getByTestId('addButton')).toBeDefined();
    });

    it('renders cancel button', () => {
      expect(screen.getByTestId('cancelButton')).toBeDefined();
    });
  });
});
