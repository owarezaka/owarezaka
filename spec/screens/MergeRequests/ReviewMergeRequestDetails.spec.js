import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import ReviewMRDetails from '~app/screens/MergeRequests/ReviewMergeRequestDetails';
import GitlabApi from '~app/lib/auth/GitlabApi';
import '~config/i18n.config';
import mockNote from '~spec/__mocks__/Note';
import mockIssue from '~spec/__mocks__/MergeRequestIssue';
import mockIssueDetails from '~spec/__mocks__/MergeRequestIssueDetails';
import mockDiffs from '~spec/__mocks__/MergeRequestChanges';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('~app/lib/auth/GitlabApi');

describe('Review Merge Request Details Screen', () => {
  const handlePress = jest.fn();
  const refreshIssues = jest.fn();

  describe('render overview elements correctly', () => {
    let tree;

    beforeEach(() => {
      GitlabApi.getMergeRequestsDetails.mockResolvedValue(mockIssueDetails);
      GitlabApi.getAllMergeRequestNotes.mockResolvedValue(mockNote);

      tree = render(
        <ThemeProvider>
          <ReviewMRDetails issue={mockIssue} onPress={handlePress} refreshIssues={refreshIssues} />
        </ThemeProvider>,
      );
    });

    afterEach(() => {
      GitlabApi.getMergeRequestsDetails.mockRestore();
      GitlabApi.getAllMergeRequestNotes.mockRestore();
    });

    it('renders navigation button', () => {
      expect(tree.getByText('Merge Requests')).toBeDefined();
      expect(tree.getByText(`${mockIssue.reference}`)).toBeDefined();
    });

    it('renders overview header component', () => {
      expect(tree.getByTestId('MergeRequestsHeader')).toBeDefined();
    });

    it('renders overview body component', () => {
      expect(tree.getByTestId('MRDetailsBody')).toBeDefined();
    });

    it('renders approve button component', () => {
      expect(tree.getByTestId('approveMergeRequest')).toBeDefined();
    });

    it('renders sendComment component', () => {
      expect(tree.getByTestId('sendComment')).toBeDefined();
    });

    it('renders close merge request button', () => {
      expect(tree.getByText('Close Merge Request')).toBeDefined();
    });
  });

  describe('render Changes screen elements correctly', () => {
    beforeEach(() => {
      GitlabApi.getMergeRequestsDetails.mockResolvedValue(mockIssueDetails);
      GitlabApi.getAllMergeRequestNotes.mockResolvedValue(mockNote);
    });

    afterEach(() => {
      GitlabApi.getMergeRequestsDetails.mockRestore();
      GitlabApi.getAllMergeRequestNotes.mockRestore();
    });

    it('renders spinner as initial', () => {
      GitlabApi.getMergeRequestChanges.mockResolvedValue(null);

      const tree = render(
        <ThemeProvider>
          <ReviewMRDetails issue={mockIssue} onPress={handlePress} refreshIssues={refreshIssues} />
        </ThemeProvider>,
      );

      fireEvent.press(tree.getByText('Changes'));

      expect(tree.getByTestId('spinner').props.animating).toBe(true);

      GitlabApi.getMergeRequestChanges.mockRestore();
    });

    it('renders accordion element when loads diffs', async () => {
      GitlabApi.getMergeRequestChanges.mockResolvedValue(mockDiffs);

      const tree = render(
        <ThemeProvider>
          <ReviewMRDetails issue={mockIssue} onPress={handlePress} refreshIssues={refreshIssues} />
        </ThemeProvider>,
      );

      fireEvent.press(tree.getByText('Changes'));

      await waitFor(() => expect(tree.queryByTestId('diffs')).not.toBeNull());

      GitlabApi.getMergeRequestChanges.mockRestore();
    });

    it('renders acceptMergeRequest component when loads diffs', async () => {
      GitlabApi.getMergeRequestChanges.mockResolvedValue(mockDiffs);

      const tree = render(
        <ThemeProvider>
          <ReviewMRDetails issue={mockIssue} onPress={handlePress} refreshIssues={refreshIssues} />
        </ThemeProvider>,
      );

      fireEvent.press(tree.getByText('Changes'));
      fireEvent.press(tree.getByText('Overview'));

      await waitFor(() => expect(tree.queryByTestId('acceptMergeRequest')).not.toBeNull());

      GitlabApi.getMergeRequestChanges.mockRestore();
    });

    it('renders text when no diff files', async () => {
      GitlabApi.getMergeRequestChanges.mockResolvedValue([]);

      const tree = render(
        <ThemeProvider>
          <ReviewMRDetails issue={mockIssue} onPress={handlePress} refreshIssues={refreshIssues} />
        </ThemeProvider>,
      );

      fireEvent.press(tree.getByText('Changes'));

      expect(await tree.findByText(/No changes between/)).toBeDefined();

      GitlabApi.getMergeRequestChanges.mockRestore();
    });
  });
});
