import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, fireEvent } from '@testing-library/react-native';
import ReviewMergeRequestsScreen from '~app/screens/MergeRequests/ReviewMergeRequestsScreen';
import '~config/i18n.config';
import data from '~spec/__mocks__/ReviewMergeRequests';
import joe from '~spec/__mocks__/User.js';
import { Colors } from '~app/constants/GlobalStyles';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Review Merge Requests screen', () => {
  const mockDataMRReview = [data];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.reviewRequestedMergeRequests.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <ReviewMergeRequestsScreen />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.reviewRequestedMergeRequests.mockRestore();
    });
  });

  describe('when MR Review list is loaded', () => {
    describe('when MR Review list has items', () => {
      beforeEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockResolvedValue(mockDataMRReview);
      });

      it('displays fetched MR Reviews', async () => {
        const screen = render(
          <ThemeProvider>
            <ReviewMergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Review Merge Requests')).toBeDefined();
        expect(screen.queryByTestId('ReviewMRs-List')).not.toBeNull();
      });

      it('displays ReviewMRDetails Screen when pressed item', async () => {
        const screen = render(
          <ThemeProvider>
            <ReviewMergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Review Merge Requests')).toBeDefined();
        expect(screen.queryByTestId('ReviewMRs-List')).not.toBeNull();

        fireEvent.press(screen.getByTestId(`item${mockDataMRReview[0].id}`));

        expect(screen.getByText('Close Merge Request')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockRestore();
      });
    });

    describe('when list is empty', () => {
      beforeEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <ReviewMergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(
          await screen.findByText('There are no merge request reviews currently assigned to you.'),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockResolvedValue({
          success: false,
        });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <ReviewMergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText(/Connection failed./)).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.reviewRequestedMergeRequests.mockRestore();
      });
    });
  });
});
