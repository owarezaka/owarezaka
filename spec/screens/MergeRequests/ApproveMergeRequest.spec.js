import React from 'react';
import { render } from '@testing-library/react-native';
import ApproveMergeRequest from '~app/screens/MergeRequests/ApproveMergeRequest';
import mockIssue from '~spec/__mocks__/MergeRequestIssue';
import mockIssueDetails from '~spec/__mocks__/MergeRequestIssueDetails';
import user from '~spec/__mocks__/User.js';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('ApproveMergeRequest', () => {
  const setIsApproved = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <ApproveMergeRequest
            testID="approveMergeRequest"
            isApproved={{
              status: true,
              users: [mockIssueDetails.approvedBy.nodes],
            }}
            setIsApproved={setIsApproved}
            instanceUrl={user.instanceUrl}
            userToken={user.userToken}
            projectId={mockIssueDetails.projectId}
            iid={mockIssue.iid}
            approvalsRequired={mockIssueDetails.approvalsRequired}
          />
        </ThemeProvider>,
      );
    });

    it('renders alert component', () => {
      expect(screen.getByTestId('alert')).toBeDefined();
    });

    it('renders approval icon', () => {
      expect(screen.getByTestId('approval')).toBeDefined();
    });

    it('renders approve button', () => {
      expect(screen.getByTestId('approveBtn')).toBeDefined();
    });
    it('renders approve statement', () => {
      expect(screen.getByText('Approval is optional')).toBeDefined();
    });
  });

  describe('Approve Statements', () => {
    it('displays related statement and image if it is approved by user', () => {
      const screen = render(
        <ThemeProvider>
          <ApproveMergeRequest
            isApproved={{
              status: true,
              users: [mockIssueDetails.approvedBy.nodes],
            }}
          />
        </ThemeProvider>,
      );

      expect(screen.getByText('Approved by you')).toBeDefined();
      expect(screen.getByTestId(`img${mockIssueDetails.approvedBy.nodes.id}`)).toBeDefined();
    });

    it('displays related statement and image if it is approved by user and others', () => {
      const users = [
        {
          id: 'gid://gitlab/User/12340',
          username: 'susan_doe',
          avatarUrl: '#',
        },
        {
          id: 'gid://gitlab/User/56789',
          username: 'john_doe',
          avatarUrl: '#',
        },
      ];

      const screen = render(
        <ThemeProvider>
          <ApproveMergeRequest isApproved={{ status: true, users }} />
        </ThemeProvider>,
      );

      expect(screen.getByText('Approved by you and others')).toBeDefined();
      expect(screen.getByTestId(`img${mockIssueDetails.approvedBy.nodes.id}`)).toBeDefined();
    });

    it('displays related statement and image if it is approved by others', () => {
      const screen = render(
        <ThemeProvider>
          <ApproveMergeRequest
            isApproved={{
              status: false,
              users: [mockIssueDetails.approvedBy.nodes],
            }}
          />
        </ThemeProvider>,
      );

      expect(screen.getByText('Approved by')).toBeDefined();
      expect(screen.getByTestId(`img${mockIssueDetails.approvedBy.nodes.id}`)).toBeDefined();
    });

    it('displays how many users are needed for approval if approvalsRequired returns more than 0', () => {
      const screen = render(
        <ThemeProvider>
          <ApproveMergeRequest
            isApproved={{
              status: false,
              users: [mockIssueDetails.approvedBy.nodes],
            }}
            approvalsRequired={3}
          />
        </ThemeProvider>,
      );

      expect(screen.getByText('Requires 3 approval from eligible users.')).toBeDefined();
    });
  });
});
