import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { fireEvent, render, waitFor } from '@testing-library/react-native';
import MergeRequestsScreen from '~app/screens/MergeRequests/MergeRequestsScreen';
import { Colors } from '~app/constants/GlobalStyles';
import '~config/i18n.config';
import data from '~spec/__mocks__/MergeRequestIssue';
import joe from '~spec/__mocks__/User.js';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

describe('Merge Requests screen', () => {
  const mockDataMRList = [data];

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.assignedMergeRequests.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <MergeRequestsScreen />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.assignedMergeRequests.mockRestore();
    });
  });

  describe('when MR list is loaded', () => {
    describe('when MR list has items', () => {
      beforeEach(() => {
        GitlabApi.assignedMergeRequests.mockResolvedValue(mockDataMRList);
      });

      it('displays fetched MRs', async () => {
        const screen = render(
          <ThemeProvider>
            <MergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Merge Requests')).toBeDefined();
        expect(screen.queryByTestId('MR-List')).not.toBeNull();
      });

      it('displays MRDetails Screen when pressed item', async () => {
        const screen = render(
          <ThemeProvider>
            <MergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText('Merge Requests')).toBeDefined();
        expect(screen.queryByTestId('MR-List')).not.toBeNull();

        fireEvent.press(screen.getByTestId(`item${mockDataMRList[0].id}`));

        expect(screen.getByText('Close Merge Request')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.assignedMergeRequests.mockRestore();
      });
    });

    describe('when MR list is empty', () => {
      beforeEach(() => {
        GitlabApi.assignedMergeRequests.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <MergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(
          await screen.findByText('There are no merge requests currently assigned to you.'),
        ).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.assignedMergeRequests.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.assignedMergeRequests.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <MergeRequestsScreen />
          </ThemeProvider>,
        );

        expect(await screen.findByText(/Connection failed./)).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.assignedMergeRequests.mockRestore();
      });
    });
  });
});
