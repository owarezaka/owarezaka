import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import SettingsScreen from '~app/screens/SettingsScreen';
import { Colors } from 'app/constants/GlobalStyles';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

describe('Settings screen', () => {
  const darkModeStyle = [{ color: Colors.secondary }, { fontSize: 16 }];

  describe('render elements correctly', () => {
    let testInstance;

    beforeEach(() => {
      const testRenderer = renderer.create(
        <ThemeProvider>
          <SettingsScreen />
        </ThemeProvider>,
      );
      testInstance = testRenderer.root;
    });

    it('renders dark mode setting', () => {
      expect(testInstance.findByProps({ testID: 'dark-mode' })).toBeDefined();
      expect(testInstance.findByProps({ testID: 'switch' })).toBeDefined();
    });
  });

  it('switches to dark mode by pressing button', () => {
    const { getByTestId } = render(
      <ThemeProvider>
        <SettingsScreen />
      </ThemeProvider>,
    );

    fireEvent(getByTestId('switch'), 'valueChange', true);
    expect(getByTestId('dark-mode').props.style).toStrictEqual(darkModeStyle);
  });
});
