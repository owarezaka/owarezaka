import React from 'react';
import { render } from '@testing-library/react-native';
import HelpScreen from '~app/screens/HelpScreen';
import '~config/i18n.config';
import { ThemeProvider } from '~app/store/theme-context';

describe('Help screen', () => {
  describe('renders elements correctly', () => {
    let tree;

    beforeEach(() => {
      tree = render(
        <ThemeProvider>
          <HelpScreen />
        </ThemeProvider>,
      );
    });

    it('renders title', () => {
      expect(tree.getByText(/Owarezaka GitLab Client/)).toBeDefined();
    });

    it('renders descriptions', () => {
      expect(
        tree.getByText(/Owarezaka is a mobile client for GitLab for IOS and Android mobile phones/),
      ).toBeDefined();
      expect(tree.getByText(/GitLab is an open source devops platform/)).toBeDefined();
    });

    it('renders read more button', () => {
      expect(tree.getByTestId('readMore')).toBeDefined();
    });

    it('renders GitLab documentation button', () => {
      expect(tree.getByTestId('doc')).toBeDefined();
    });
  });
});
