import React from 'react';
import { render } from '@testing-library/react-native';
import { ThemeProvider } from '~app/store/theme-context';
import OverviewHeader from '~app/screens/Groups/OverviewHeader';
import group from '~spec/__mocks__/Groupitem';
import '~config/i18n.config';

describe('OverviewHeader', () => {
  const source = require('~app/assets/groupAvatarDefault.png');

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <OverviewHeader
            fullName={group.group.name}
            visibility={group.group.visibility}
            source={source}
          />
        </ThemeProvider>,
      );
    });

    it('renders group avatar', () => {
      expect(screen.getByTestId('avatar')).toBeDefined();
    });

    it('renders group name', () => {
      expect(screen.getByText(group.group.name)).toBeDefined();
    });

    it('renders earth icon by visibility of group is public', () => {
      expect(screen.getByTestId('public-icon')).toBeDefined();
    });
  });
});
