import React from 'react';
import { waitFor, render } from '@testing-library/react-native';
import GitlabApi from '~app/lib/auth/GitlabApi';
import GroupDetails from '~app/screens/Groups/GroupDetails';
import group from '~spec/__mocks__/Groupitem';
import groupDetails from '~spec/__mocks__/GroupDetails';
import { ThemeProvider } from '~app/store/theme-context';
import '~config/i18n.config';

jest.mock('~app/lib/auth/GitlabApi');

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  const actualNav = jest.requireActual('@react-navigation/native');
  return {
    ...actualNav,
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('Group Details Screen', () => {
  const handlePress = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <GroupDetails group={group} onPress={handlePress} navigation={mockedNavigate} />
        </ThemeProvider>,
      );
    });

    it('renders navigation button', () => {
      expect(screen.getByText('Groups')).toBeDefined();
    });

    it('renders overview header component', () => {
      expect(screen.getByTestId('overviewHeader')).toBeDefined();
    });

    it('renders title', () => {
      expect(screen.getByText('Subgroups and projects')).toBeDefined();
    });
  });

  describe('renders subgroups and projects', () => {
    let screen;

    beforeEach(() => {
      GitlabApi.getSubgroupsAndProjectsDetailsofGroup.mockResolvedValue(groupDetails);

      screen = render(
        <ThemeProvider>
          <GroupDetails group={group} onPress={handlePress} navigation={mockedNavigate} />
        </ThemeProvider>,
      );
    });

    it('renders subgroup list', async () => {
      await waitFor(() => expect(screen.queryByTestId('subgroups')).toBeDefined());
    });

    it('renders project list', async () => {
      await waitFor(() => expect(screen.queryByTestId('projects')).toBeDefined());
    });
  });
});
