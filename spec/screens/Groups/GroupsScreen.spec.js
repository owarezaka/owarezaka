import React from 'react';
import UserStorage from '~app/lib/auth/UserStorage';
import GitlabApi from '~app/lib/auth/GitlabApi';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import GroupsScreen from '~app/screens/Groups/GroupsScreen';
import '~config/i18n.config';
import data from '~spec/__mocks__/Groupitem';
import joe from '~spec/__mocks__/User.js';
import { Colors } from '~app/constants/GlobalStyles';
import { ThemeProvider } from '~app/store/theme-context';

jest.mock('~app/lib/auth/GitlabApi');
jest.mock('~app/lib/auth/UserStorage');

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  const actualNav = jest.requireActual('@react-navigation/native');
  return {
    ...actualNav,
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('Groups screen', () => {
  const mockDataGroups = [data];
  const navigation = { goBack: jest.fn() };

  beforeEach(() => {
    UserStorage.getUserInfo.mockResolvedValue(joe);
  });

  describe('loading state', () => {
    beforeEach(() => {
      GitlabApi.getAllGroups.mockResolvedValue(null);
    });

    it('renders a spinner', () => {
      const screen = render(
        <ThemeProvider>
          <GroupsScreen navigation={navigation} />
        </ThemeProvider>,
      );

      const spinner = screen.getByTestId('spinner');
      expect(spinner.props).toStrictEqual({
        animating: true,
        children: undefined,
        color: Colors.darkGray2,
        style: {
          padding: 20,
        },
        size: 'large',
        testID: 'spinner',
      });
    });

    afterEach(() => {
      GitlabApi.getAllGroups.mockRestore();
    });
  });

  describe('when Groups list is loaded', () => {
    describe('when a group list is available', () => {
      beforeEach(() => {
        GitlabApi.getAllGroups.mockResolvedValue(mockDataGroups);
      });

      it('displays the fetched groups', async () => {
        const screen = render(
          <ThemeProvider>
            <GroupsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.queryByTestId('Groups-List')).not.toBeNull());
      });

      it('displays Group Details screen when pressed item', async () => {
        const screen = render(
          <ThemeProvider>
            <GroupsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        await waitFor(() => expect(screen.queryByTestId('Groups-List')).not.toBeNull());

        fireEvent.press(screen.getByTestId(`item${mockDataGroups[0].id}`));

        expect(screen.getByTestId('overviewHeader')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getTodos.mockRestore();
      });
    });

    describe('when groups list is empty', () => {
      beforeEach(() => {
        GitlabApi.getAllGroups.mockResolvedValue([]);
      });

      it('displays an empty list', async () => {
        const screen = render(
          <ThemeProvider>
            <GroupsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(await screen.findByText('You are not currently part of any groups.')).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllGroups.mockRestore();
      });
    });

    describe('when an error occurs', () => {
      beforeEach(() => {
        GitlabApi.getAllGroups.mockResolvedValue({ success: false });
      });

      it('displays an alert', async () => {
        const screen = render(
          <ThemeProvider>
            <GroupsScreen navigation={navigation} />
          </ThemeProvider>,
        );

        expect(await screen.findByText(/Connection failed./)).toBeDefined();
      });

      afterEach(() => {
        GitlabApi.getAllGroups.mockRestore();
      });
    });
  });
});
