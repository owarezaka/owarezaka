import React from 'react';
import { render } from '@testing-library/react-native';
import GroupItem from '~app/screens/Groups/GroupItem';
import * as Utils from '~app/utils/Utils';
import { ThemeProvider } from '~app/store/theme-context';
import group from '~spec/__mocks__/Groupitem';

describe('GroupItem', () => {
  const onPress = jest.fn();

  describe('render elements correctly', () => {
    let screen;

    beforeEach(() => {
      screen = render(
        <ThemeProvider>
          <GroupItem
            accessLevel={group.accessLevel.integerValue}
            avatarUrl={group.group.avatarUrl}
            description={group.group.description}
            id={group.id}
            name={group.group.name}
            source={group.group.avatarUrl}
            visibility={group.group.visibility}
            group={group}
            onPress={onPress}
          />
        </ThemeProvider>,
      );
    });

    it('renders Image element', () => {
      expect(screen.getByTestId('avatar')).toBeDefined();
    });

    it('renders group name', () => {
      expect(screen.getByText(group.group.name)).toBeDefined();
    });

    it('renders visibility Icon element if project is public', () => {
      expect(screen.getByTestId('earth-icon')).toBeDefined();
    });

    it('renders role element correctly', () => {
      expect(
        screen.getByText(Utils.findRoleByAccessLevel(group.accessLevel.integerValue)),
      ).toBeDefined();
    });

    it('renders description if provided', () => {
      expect(screen.getByTestId('description')).toBeDefined();
    });
  });

  describe('renders correct icon', () => {
    it('renders subgroup icon if receives subgroup true', () => {
      const screen = render(
        <ThemeProvider>
          <GroupItem
            accessLevel={group.accessLevel.integerValue}
            avatarUrl={group.group.avatarUrl}
            description={group.group.description}
            id={group.id}
            name={group.group.name}
            source={group.group.avatarUrl}
            visibility={group.group.visibility}
            group={group}
            onPress={onPress}
            subgroup={true}
          />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('subgroup-icon')).toBeDefined();
    });

    it('renders project icon if receives project true', () => {
      const screen = render(
        <ThemeProvider>
          <GroupItem
            accessLevel={group.accessLevel.integerValue}
            avatarUrl={group.group.avatarUrl}
            description={group.group.description}
            id={group.id}
            name={group.group.name}
            source={group.group.avatarUrl}
            visibility={group.group.visibility}
            group={group}
            onPress={onPress}
            project={true}
          />
        </ThemeProvider>,
      );

      expect(screen.getByTestId('project-icon')).toBeDefined();
    });
  });
});
