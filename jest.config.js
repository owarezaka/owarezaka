const { defaults } = require('jest-config');

module.exports = {
  verbose: true,
  testTimeout: 20000,
  transformIgnorePatterns: [
    'node_modules/(?!(react-native|@react-native|react-native-vector-icons|@react-navigation|react-native-reanimated|@react-native-community|react-native-markdown-package|react-native-lightbox|react-syntax-highlighter|react-native-ui-lib)/)',
  ],
  moduleFileExtensions: [...defaults.moduleFileExtensions],
  globals: {
    'babel-jest': {
      babelConfig: true,
    },
  },
  preset: 'react-native',
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
  setupFilesAfterEnv: ['@testing-library/jest-native/extend-expect'],
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{js,jsx}', '!**/node_modules/**', '!**/vendor/**'],
  coverageDirectory: './spec/coverage-report/',
  coverageReporters: ['clover', 'json', 'lcov', ['text', { skipFull: true }]],
  coveragePathIgnorePatterns: [
    './android',
    './ios',
    './config',
    './locale',
    './node_modules/',
    './app/assets',
    './app/constants',
    './.eslintrc.js',
    './babel.config.js',
    './index.js',
    './jest.config.js',
    './metro.config.js',
    './spec/coverage-report',
    './app/navigation/',
    './app/lib/*',
  ],
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 50,
    },
  },
};
