import { AppRegistry, LogBox } from 'react-native';
import App from '~app/App';
import { name as appName } from './app.json';
import '~config/i18n.config';

LogBox.ignoreLogs(['Non-serializable values were found in the navigation state']);

AppRegistry.registerComponent(appName, () => App);
