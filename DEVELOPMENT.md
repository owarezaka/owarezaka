## Development 
---
Developer documentation for Owarezaka, an unofficial mobile application for GitLab on IOS and Android. 

### Readme
---

See [README.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/README.md) for general information about the project.

### How to Install
---
See [USAGE.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/USAGE.md) for instructions and details for installation.

### Testing
---
Refer to [TESTING.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/TESTING.md) for instructions and details on testing.

### Linting
---
Refer to [LINTING.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/linting.md) in order to learn about how we handle linting.

### Advanced Usage
---
Refer to [ADVANCED_USAGE.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/ADVANCED_USAGE.md) in order to discover what you can do in the app and to view the application screens in depth.

### Contribute
---
Refer to [CONTRIBUTION.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/CONTRIBUTION.md) for instructions and details on how to contribute.