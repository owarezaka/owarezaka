module.exports = {
  env: {
    browser: false,
    es2021: true,
    'react-native/react-native': true,
  },
  extends: [
    'eslint:recommended',
    '@react-native-community',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:react-native/all',
    'plugin:import/recommended',
    'plugin:jest/recommended',
    'prettier',
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    babelOptions: {
      presets: ['@babel/preset-react'],
    },
  },
  plugins: ['prettier', 'react', 'react-native', 'react-hooks', 'import', 'jest'],
  rules: {
    'prettier/prettier': 'error',
    'react-native/no-unused-styles': 2,
    'react-native/split-platform-components': 2,
    'react-native/no-inline-styles': 2,
    'react-native/no-color-literals': 2,
    'react-native/no-raw-text': 'off',
    'react/prop-types': 'warn',
    'react/display-name': 'off',
    'react-native/sort-styles': [
      'error',
      'asc',
      {
        ignoreClassNames: false,
        ignoreStyleProperties: false,
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'import/default': 'off',
    'import/namespace': 'off',
  },
  overrides: [
    {
      files: ['**/*.spec.js', 'jest.config.js'],
      env: {
        jest: true,
      },
      rules: {
        'no-global-assign': 'off',
        'jest/no-mocks-import': 'off',
      },
    },
  ],
  settings: {
    'import/ignore': ['react-native'],
    'import/resolver': {
      'babel-module': {},
    },
  },
};
