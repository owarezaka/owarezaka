# Owarezaka

An unofficial mobile GitLab Mobile for IOS and Android. Developed using React Native.

Open for community contributions.

The first iteration (14.10) To be released in April.

 <br/>

## How to Install

See [USAGE.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/USAGE.md) for instructions and details for installation.

### Testing

```sh
npm run test
```

### Linting

```sh
npm run lint
```

<br/> 
<hr>

## Contribute

Refer to [CONTRIBUTION.md](https://gitlab.com/owarezaka/owarezaka/-/blob/main/CONTRIBUTION.md) for instructions and details on how to contribute.


## Usage Video

Watch it on YouTube: [Owarezaka - Mobile GitLab client](https://www.youtube.com/watch?v=s-_4Yv8SF2A)
