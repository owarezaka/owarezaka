## Contribute

Any contributions you make are warmly welcome. 

If you have a suggestion that would make this project better, please fork the repo and create a merge request. 

Don't forget to give the project a star! Thanks again!

### Guides
- [Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)
- Whenever in doubt, respect the behavioral contribution guides on [Gitlab CE/EE contribution guide](https://docs.gitlab.com/ee/development/contributing/)
- [React Native](https://reactnative.dev/)

### Opening Issues

Open an issue with the relevant type label such as bug, feature.

### Contributing Merge requests

```sh
# Fork the project and create your feature branch
git checkout -b feature/newFeature

# Commit your changes
git commit -m 'Add some newFeature'

# Push to the branch
git push origin feature/newFeature

4. Open a merge request
```


## Setting Up the Development Environment

You will need Node, the React Native command line interface, a JDK, and Android Studio.

### Chocolatey

Firstly you need to install Chocolatey, a package manager for Windows.

You can run the following command in cmd.exe (Command Prompt) in order to install Chocolatey.

```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin" 
```

### Node and JDK

Secondly, you need to install Node and JDK.
It is recommended to use an LTS version of Node. 
You can either:
-  Open an Administrator Command Prompt (right click Command Prompt and select "Run as Administrator"), then run the following command:

```
choco install -y nodejs-lts openjdk11
```
OR

- Download the pre-built installer by following this link and choosing the desired platform: https://nodejs.org/en/download/


### Android Studio

After this, you need to download and install Android Studio, which you can do so by following the link and downloading the executable: https://developer.android.com/studio

While on Android Studio installation wizard, make sure the boxes next to all of the following items are checked:

- Android SDK
- Android SDK Platform
- Android Virtual Device
- If you are not already using Hyper-V: Performance (Intel ® HAXM) (See here for AMD or Hyper-V)
 
Building a React Native app with native code, requires the Android 11 (R) SDK in particular. To do that, open Android Studio, click on "Configure" button and select "SDK Manager".

Select the "SDK Platforms" tab from within the SDK Manager, then check the box next to "Show Package Details" in the bottom right corner. Look for and expand the Android 11 (R) entry, then make sure the following items are checked, then apply:

- Android SDK Platform 30
- Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System Image


### Configuring Environment Variables

Then you need to configure the ANDROID_HOME environment variable
The React Native tools require some environment variables to be set up in order to build apps with native code.

1. Open the Windows Control Panel.
2. Click on User Accounts, then click User Accounts again
3. Click on Change my environment variables
4. Click on New... to create a new ANDROID_HOME user variable that points to the path to your Android SDK:

The SDK is installed, by default, at the following location:

```%LOCALAPPDATA%\Android\Sdk```

You can find the actual location of the SDK in the Android Studio "Settings" dialog, under Appearance & Behavior → System Settings → Android SDK.

Open a new Command Prompt window to ensure the new environment variable is loaded before proceeding to the next step.

1. Open powershell
2. Copy and paste ```Get-ChildItem -Path Env:\ into powershell```
3. Verify ANDROID_HOME has been added

Then you need to add platform-tools to Path:

1. Open the Windows Control Panel.
2. Click on User Accounts, then click User Accounts again
3. Click on Change my environment variables
4. Select the Path variable. 
5. Click Edit.
6. Click New and add the path to platform-tools to the list.
The default location for this folder is:

```%LOCALAPPDATA%\Android\Sdk\platform-tools```

This portion of the guide may also be found in the following website: https://reactnative.dev/docs/environment-setup


### Git

You will also need to install Git (A free and open source distributed version control system) in order to clone the project. You can acquire Git by clicking on the following link and downloading the executable: https://gitforwindows.org/


After this you need to clone the repo by opening up a terminal and entering the following:


```
git clone  https://gitlab.com/owarezaka/owarezaka.git
```



Next step is to install all the dependencies. Firstly navigate inside the project folder where it is located in your machine then run the following: 

```
npm install
```


Then finally to start the project on Android you can enter:

```
npm run android
```

## Troubleshooting

If you have already installed Node on your system, make sure it is Node 14 or newer.

Q: I face an error as following:

```
error: TypeError: Cannot read properties of undefined (reading 'transformFile')
```
A: Your Node.js version may not be compatible with that of the project. You need to install nvm (Node Version Manager) in order to downgrade your Node.js version. You can do so by downloading the nvm-setup.exe file under the Assets section by clicking on the following link: 
https://github.com/coreybutler/nvm-windows/releases

After the installation is complete, go to Powershell and run:

```
> nvm list // to see all available node versions

> nvm install [The version you wish to downgrade to] // to install the version you want

> nvm use [Desired version]  // use the installed version 

OR

> nvm alias default [Desired version] // use the installed version as DEFAULT 
```
